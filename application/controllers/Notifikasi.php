<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifikasi extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        $this->load->model('m_notifikasi');
		check_session();
    }
	
	public function klik_notif() {
		$user = $_POST["user"];
		$user_grup = $_POST["user_grup"];
		$record = $this->m_notifikasi->update_notifikasi($user,$user_grup);
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
}
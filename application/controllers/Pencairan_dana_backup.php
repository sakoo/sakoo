<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pencairan_dana extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model('m_pencairan_dana');
		check_session();
    }
	
	public function index() {
        $data['record'] = $this->m_pencairan_dana->show_status_transaksi();
        $this->template->load('template','pencairan_dana/pencairan_dana',$data);
    }
	
	//proses konfirmasi transfer
	public function proses_transfer($id_transaksi_seller) {
		$data['record'] = $this->m_pencairan_dana->show_satu_status_transaksi_seller($id_transaksi_seller);
		$this->template->load('template','pencairan_dana/form_proses',$data);
	}
	
	function bank_check($str) {
		if ($str == '--Pilih Bank--') {
			$this->form_validation->set_message('bank_check', 'Pilihan Bank harap diisi');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	//proses konfirmasi transfer
	public function proses_completed() {
		$id_transaksi_seller = $_POST["idTransaksiSeller"];
		$id_transaksi = $_POST["idTransaksi"];
		$id_toko = $_POST["idToko"];
		$no_trx = $_POST["noTrx"];
		$bank = $_POST["bank"];
		$this->form_validation->set_rules('bank', 'bank', 'required|trim|callback_bank_check');

        if ($this->form_validation->run() == FALSE) {
			$data['record'] = $this->m_pencairan_dana->show_satu_status_transaksi_seller($id_transaksi_seller);
            $this->template->load('template','pencairan_dana/form_proses',$data);
        } 
		else {
			//handle update data
			//image dulu
			if ( isset($_FILES["file"]["type"]) ) {
				$max_size = 500 * 1024; // 500 KB
				$destination_directory = "/var/www/html/admin/assets/img/bukti_transfer/";
				$validextensions = array("jpeg", "jpg", "png");
				$temporary = explode(".", $_FILES["file"]["name"]);
				$file_extension = end($temporary);
				$file_name = $_FILES["file"]["name"];
				$actual_name = pathinfo($file_name, PATHINFO_FILENAME);
				// We need to check for image format and size again, because client-side code can be altered
				if ( (($_FILES["file"]["type"] == "image/png") ||
					($_FILES["file"]["type"] == "image/jpg") ||
					($_FILES["file"]["type"] == "image/jpeg")
				   ) && in_array($file_extension, $validextensions)) {
					if ( $_FILES["file"]["size"] < ($max_size) ) {
						if ( $_FILES["file"]["error"] > 0 ) {
							$this->session->set_flashdata('result_login', '<br>Error: <strong>'.$_FILES["file"]["error"].'</strong>');
							//change to next name
						}
						else {
							if ( file_exists($destination_directory . $_FILES["file"]["name"]) ) {
								$this->session->set_flashdata('result_login', '<br>Error: File <strong>'.$_FILES["file"]["name"].'</strong> already exists');
								$i=1;
								while(file_exists($destination_directory . $file_name)) {
									$actual_name = (string)$actual_name.$i;
									$file_name = $actual_name.".".$file_extension;
									$i++;
								}
							}
							//else {
								$sourcePath = $_FILES["file"]["tmp_name"];
								$targetPath = $destination_directory . $file_name;
								move_uploaded_file($sourcePath, $targetPath);
								//update data
								$this->m_pencairan_dana->edit_data_transfer($id_transaksi_seller,$file_name,$bank);
								//send notification
								$deskripsi = "Pencairan Dana Transaksi ".$no_trx." berhasil";
								$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
								$this->send_to_app($id_toko,"Pencairan Dana berhasil",$deskripsi,$id_transaksi_seller,"saldo","kirim");
								redirect('pencairan_dana');
							//}
						}
					}
					else
					{
						$this->session->set_flashdata('result_login', "The size of image you are attempting to upload is ".round($_FILES["file"]["size"]/1024, 2)." KB, maximum size allowed is " .round($max_size/1024, 2).".");
					}
				}
				else
				{
					$this->session->set_flashdata('result_login', '<br>Unvalid image format. Allowed formats: JPG, JPEG, PNG.');
				}
			}
            $data['record'] = $this->m_pencairan_dana->show_satu_status_transaksi_seller($id_transaksi_seller);
            $this->template->load('template','pencairan_dana/form_proses',$data);
		}
		/*
		print_r($_FILES["file"]);
		if ( isset($_FILES["file"]["type"]) ) {
			echo "berhasil upload";
		  $max_size = 500 * 1024; // 500 KB
		  $destination_directory = "C:\\xampp\\htdocs\\sate\\assets\\img\\bukti_transfer\\";
		  $validextensions = array("jpeg", "jpg", "png");
		  $temporary = explode(".", $_FILES["file"]["name"]);
		  $file_extension = end($temporary);
		  // We need to check for image format and size again, because client-side code can be altered
		  if ( (($_FILES["file"]["type"] == "image/png") ||
				($_FILES["file"]["type"] == "image/jpg") ||
				($_FILES["file"]["type"] == "image/jpeg")
			   ) && in_array($file_extension, $validextensions))
		  {
			if ( $_FILES["file"]["size"] < ($max_size) )
			{
			  if ( $_FILES["file"]["error"] > 0 )
			  {
				echo "<div class=\"alert alert-danger\" role=\"alert\">Error: <strong>" . $_FILES["file"]["error"] . "</strong></div>";
			  }
			  else
			  {
				if ( file_exists($destination_directory . $_FILES["file"]["name"]) )
				{
				  echo $destination_directory;
				  echo "<div class=\"alert alert-danger\" role=\"alert\">Error: File <strong>" . $_FILES["file"]["name"] . "</strong> already exists.</div>";
				}
				else
				{
				  $sourcePath = $_FILES["file"]["tmp_name"];
				  $targetPath = $destination_directory . $_FILES["file"]["name"];
				  move_uploaded_file($sourcePath, $targetPath);
				  echo "<div class=\"alert alert-success\" role=\"alert\">";
				  echo "<p>Image uploaded successful</p>";
				  echo "<p>File Name: <a href=\"". $targetPath . "\"><strong>" . $targetPath . "</strong></a></p>";
				  echo "<p>Type: <strong>" . $_FILES["file"]["type"] . "</strong></p>";
				  echo "<p>Size: <strong>" . round($_FILES["file"]["size"]/1024, 2) . " kB</strong></p>";
				  echo "<p>Temp file: <strong>" . $_FILES["file"]["tmp_name"] . "</strong></p>";
				  echo "</div>";
				}
			  }
			}
			else
			{
			  echo "<div class=\"alert alert-danger\" role=\"alert\">The size of image you are attempting to upload is " . round($_FILES["file"]["size"]/1024, 2) . " KB, maximum size allowed is " . round($max_size/1024, 2) . " KB</div>";
			}
		  }
		  else
		  {
			echo "<div class=\"alert alert-danger\" role=\"alert\">Unvalid image format. Allowed formats: JPG, JPEG, PNG.</div>";
		  }
		}
		else {
			echo "gak ketemu filenya";
		}
		
		//$data['record'] = $this->m_pencairan_dana->show_satu_status_transaksi($id_transaksi);
		//$this->template->load('template','pencairan_dana/form_proses',$data);
		*/
	}
	
	//proses untuk input resi modal
	public function modal_input() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["id_transaksi"] = $r->id_transaksi;
			$return["buyer"] = $r->buyer;
			$return["nama_ecommerce"] = $r->nama_ecommerce;
			$return["alamat_pengiriman"] = $r->alamat_pengiriman;
		}
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	
	public function modal_input_ok() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$no_resi = $_POST["no_resi"];
        if ($no_resi == "") {
			header('HTTP/1.1 500 Internal Server Booboo');
			header('Content-Type: application/json; charset=UTF-8');
        } 
		else {
			$record = $this->m_transaksi->edit_no_resi($id_transaksi,$no_resi);
		}
	}
	
	//proses untuk input resi modal
	public function modal() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["id_transaksi"] = $r->id_transaksi;
			$return["buyer"] = $r->buyer;
			$return["nama_ecommerce"] = $r->nama_ecommerce;
			$return["alamat_pengiriman"] = $r->alamat_pengiriman;
			$return["no_resi"] = $r->no_resi;
		}
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//handler di modal
	public function modal_checklist() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->checked($id_transaksi);
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function send_to_app($id_toko,$title,$deskripsi,$id,$id_tipe_alert,$param_notif) {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$token = "\"userId\":\"".$id_toko."\",\"title\":\"".$title."\",\"body\":\"".trim($deskripsi)."\",\"id\":\"".$id."\",\"type\":\"".$id_tipe_alert."\",\"paramNotif\":\"".$param_notif."\"";
		//echo "Token : ".$token."<br>";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://103.195.31.220:5000/v1/fcm/sendOneNotification");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{".$token."}");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		//print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
	}
	
}
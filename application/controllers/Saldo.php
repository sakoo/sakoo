<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saldo extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model('m_transaksi');
		$this->load->model('m_barang');
		$this->load->model('m_pencairan_dana');
		$this->load->model('m_saldo');
		$this->load->model('m_email');
		check_session();
    }
	
	public function index() {
        $data['bri'] = $this->m_pencairan_dana->show_piutang(1);
		$data['mandiri'] = $this->m_pencairan_dana->show_piutang(2);
		$data['bni'] = $this->m_pencairan_dana->show_piutang(3);
		$data['bca'] = $this->m_pencairan_dana->show_piutang(4);
        $this->template->load('template','saldo/saldo',$data);
    }
	
	public function transfer() {
		if(isset($_POST['transfer'])){
            // proses transaksi
            $id_bank = $this->input->post('bank');
            $data['record'] = $this->m_saldo->show_bank($id_bank);
			$data['piutang'] = $this->m_pencairan_dana->show_piutang($id_bank);
            $this->template->load('template','saldo/form_proses',$data);
        }
	}
	
	function ecommerce_check($str) {
		if ($str == '--Pilih Ecommerce--') {
			$this->form_validation->set_message('ecommerce_check', 'The Ecommerce field is required.');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	public function proses_completed() {
		$id_bank = $_POST["idBank"];
		$id_ecommerce = $_POST["ecommerce"];
		$jumlah = $_POST["jumlah"];
		$id_transfer = $_POST["idTransfer"];
		$this->form_validation->set_rules('ecommerce', 'Ecommerce', 'required|trim|callback_ecommerce_check');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|trim');
		$this->form_validation->set_rules('idTransfer', 'ID Transfer', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
			$data['record'] = $this->m_saldo->show_bank($id_bank);
			$data['piutang'] = $this->m_pencairan_dana->show_piutang($id_bank);
            $this->template->load('template','saldo/form_proses',$data);
        } 
		else {
			$this->m_saldo->insert_saldo($id_bank,$jumlah,2,$id_ecommerce,$id_transfer);
			$data['bri'] = $this->m_pencairan_dana->show_piutang(1);
			$data['mandiri'] = $this->m_pencairan_dana->show_piutang(2);
			$data['bni'] = $this->m_pencairan_dana->show_piutang(3);
			$data['bca'] = $this->m_pencairan_dana->show_piutang(4);
			$this->template->load('template','saldo/saldo',$data);
		}
	}
	
	public function mutasi() {
		if(isset($_POST['mutasi'])){
            // proses transaksi
            $id_bank = $this->input->post('bank');
            $data['record'] = $this->m_saldo->show_bank($id_bank);
			$data['piutang'] = $this->m_saldo->show_mutasi($id_bank);
            $this->template->load('template','saldo/mutasi',$data);
        }
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seller extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model('m_toko');
		$this->load->model('m_barang');
		check_session();
    }
	
	public function index() {
        $data['record'] = $this->m_toko->show_toko();
		$data['jumlah_toko'] = $this->m_toko->jumlah_toko();
		$data['total_barang'] = $this->m_barang->total_barang();
        $this->template->load('template','seller/seller',$data);
    }
	
	
	
}
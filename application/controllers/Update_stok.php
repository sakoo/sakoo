<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Update_stok extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        $this->load->model('m_update_stok');
		$this->load->model('m_barang');
		check_session();
    }
	
	public function index() {
        $data['record'] = $this->m_update_stok->show_all_update();
		$data['status_dash'] = "update";
		$data['total_update'] = $this->m_update_stok->total_update();
		$data['total_checklist'] = $this->m_update_stok->total_checklist();
		$data['total_updated'] = $this->m_update_stok->total_updated();
        $this->template->load('template','update_stok/update_stok',$data);
    }
	
	public function show_update() {
        $data['record'] = $this->m_update_stok->show_all_update();
		$data['status_dash'] = "update";
		$data['total_update'] = $this->m_update_stok->total_update();
		$data['total_checklist'] = $this->m_update_stok->total_checklist();
		$data['total_updated'] = $this->m_update_stok->total_updated();
        $this->template->load('template','update_stok/update_stok',$data);
    }
	
	public function show_checklist() {
        $data['record'] = $this->m_update_stok->show_all_checklist();
		$data['status_dash'] = "checklist";
		$data['total_update'] = $this->m_update_stok->total_update();
		$data['total_checklist'] = $this->m_update_stok->total_checklist();
		$data['total_updated'] = $this->m_update_stok->total_updated();
        $this->template->load('template','update_stok/update_stok',$data);
    }
	
	public function show_updated() {
        $data['record'] = $this->m_update_stok->show_all_updated();
		$data['status_dash'] = "updated";
		$data['total_update'] = $this->m_update_stok->total_update();
		$data['total_checklist'] = $this->m_update_stok->total_checklist();
		$data['total_updated'] = $this->m_update_stok->total_updated();
        $this->template->load('template','update_stok/update_stok',$data);
    }
	
	//proses data dari page upload barang
	public function proses_updating($id_barang = '',$id_ecommerce = '',$id_status_penyebab = '') {
		if(isset($_POST['submit'])){
            // proses barang
            $id_barang = $this->input->post('kodeBarang');
            $id_ecommerce = $this->input->post('idEcommerce');
			$id_status_penyebab = $this->input->post('idStatusPenyebab');
            $this->m_update_stok->edit($id_barang,$id_ecommerce,2,$id_status_penyebab);
            redirect('update_stok');
        }
		else{
			if(isset($_POST['update_status_1']) || isset($_POST['update_status_2']) || isset($_POST['update_status_3']) || isset($_POST['update_status_4'])) {
				if(isset($_POST['update_status_1'])) $id_ecommerce = 1;
				if(isset($_POST['update_status_2'])) $id_ecommerce = 2;
				if(isset($_POST['update_status_3'])) $id_ecommerce = 3;
				if(isset($_POST['update_status_4'])) $id_ecommerce = 4;
				$id_barang = $this->input->post('kodeBarang');
				$id_status_penyebab = $this->input->post('idStatusPenyebab');
				$this->m_update_stok->edit($id_barang,$id_ecommerce,2,$id_status_penyebab);
				
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_update_stok->show_status_update_stok_all($id_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				$this->template->load('template','update_stok/form_proses',$data);
			}
			elseif(isset($_POST['checklist_status_1']) || isset($_POST['checklist_status_2']) || isset($_POST['checklist_status_3']) || isset($_POST['checklist_status_4'])) {
				if(isset($_POST['checklist_status_1'])) $id_ecommerce = 1;
				if(isset($_POST['checklist_status_2'])) $id_ecommerce = 2;
				if(isset($_POST['checklist_status_3'])) $id_ecommerce = 3;
				if(isset($_POST['checklist_status_4'])) $id_ecommerce = 4;
				$id_barang = $this->input->post('kodeBarang');
				$id_status_penyebab = $this->input->post('idStatusPenyebab');
				$this->m_update_stok->edit($id_barang,$id_ecommerce,3,$id_status_penyebab);
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_update_stok->show_status_update_stok_all($id_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				$this->template->load('template','update_stok/form_proses',$data);
			}
			else{
				//$id=  $this->uri->segment(3);
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_update_stok->show_status_update_stok_all($id_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				$this->template->load('template','update_stok/form_proses',$data);
			}
		}
	}
	//proses untuk cheklist modal
	public function modal() {
		$id_barang = $_POST["id_barang"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$record = $this->m_barang->show_one($id_barang);
        $status = $this->m_update_stok->show_status_update_stok($id_barang,$id_ecommerce);
		$foto = $this->m_barang->show_foto_tambahan($id_barang);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama_barang"] = $r->nama_barang;
			$return["nama_toko"] = $r->nama_toko;
			$return["id_barang"] = $r->id_barang;
			$return["stok"] = $r->stok;
			$return["stok_ecommerce"] = $r->stok_ecommerce;
			$deskripsi = "Penjual : ".$r->nama_toko." ";
			$deskripsi .= "Dikirim dari : ".$r->lokasi." ";
			if($r->merk != "")
				$deskripsi .= "Merk : ".$r->merk." ";
			if($r->bahan != "")
				$deskripsi .= "Bahan : ".$r->bahan." ";
			if($r->volume != "0x0x0")
				$deskripsi .= "Volume : ".$r->volume." ";
			$deskripsi .= $r->deskripsi;
			$return["deskripsi"] = $deskripsi;
			//foto
			$link_foto = "<a href=\"".$r->foto."\" download=\"".$r->foto."\">
							<img src=\"".$r->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$link_foto = preg_replace('/"([^"]+)"\s*:\s*/', '$1:',$link_foto);
			$return["foto"] = $link_foto;
			$return["waktu_upload"] = $r->tanggal_upload;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["harga_markup"] = $r->harga_markup;
			$return["lokasi"] = $r->lokasi;
		}
		foreach ($status->result() as $s) { 
			$return["id_status"] = $s->id_status;
			$return["status_barang"] = $s->status_update_stok;
			$return["id_ecommerce"] = $s->id_ecommerce;
			$return["nama_ecommerce"] = $s->nama;
			$return["penyebab"] = $s->status_penyebab_update_stok;
			$return["url"] = $s->url;
			$return["id_status_penyebab"] = $s->id_status_penyebab;
		}
		$list = array();
		foreach ($foto->result() as $f) {  
			$link_foto_tambahan = "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
							<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$list[] = $link_foto_tambahan;
		}
		$return["foto_tambahan"] = $list;
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//proses untuk preview
	public function preview() {
		$id_barang = $_POST["id_barang"];
		$record = $this->m_barang->show_one($id_barang);
        $status = $this->m_barang->show_status_upload_all($id_barang);
		$foto = $this->m_barang->show_foto_tambahan($id_barang);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama_barang"] = $r->nama_barang;
			$return["nama_toko"] = $r->nama_toko;
			$return["id_barang"] = $r->id_barang;
			$return["stok"] = $r->stok;
			$deskripsi = "Penjual : ".$r->nama_toko." ";
			$deskripsi .= "Dikirim dari : ".$r->lokasi." ";
			if($r->merk != "")
				$deskripsi .= "Merk : ".$r->merk." ";
			if($r->bahan != "")
				$deskripsi .= "Bahan : ".$r->bahan." ";
			if($r->volume != "0x0x0")
				$deskripsi .= "Volume : ".$r->volume." ";
			$deskripsi .= $r->deskripsi;
			$return["deskripsi"] = $deskripsi;
			//foto
			$link_foto = "<a href=\"".$r->foto."\" download=\"".$r->foto."\">
							<img src=\"".$r->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$link_foto = preg_replace('/"([^"]+)"\s*:\s*/', '$1:',$link_foto);
			$return["foto"] = $link_foto;
			$return["waktu_upload"] = $r->tanggal_upload;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["harga_markup"] = $r->harga_markup;
			$return["lokasi"] = $r->lokasi;
		}
		$url_list = array();
		foreach ($status->result() as $s) { 
			$return["id_status"] = $s->id_status;
			$return["status_barang"] = $s->status;
			$return["id_ecommerce"] = $s->id_ecommerce;
			$return["nama_ecommerce"] = $s->nama;
			$return["url"] = $s->url;
			$url_list[] = $s->url;
		}
		$list = array();
		foreach ($foto->result() as $f) {  
			$link_foto_tambahan = "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
							<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$list[] = $link_foto_tambahan;
		}
		$return["url_list"] = $url_list;
		$return["foto_tambahan"] = $list;
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//handler di modal
	public function modal_checklist() {
		$id_barang = $_POST["id_barang"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$id_status_penyebab = $_POST["id_status_penyebab"];
		$record = $this->m_update_stok->edit($id_barang,$id_ecommerce,3,$id_status_penyebab);
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//sinkronisasi stok
	public function proses_sinkronisasi($id_barang,$id_penyebab) {
		$record = $this->m_update_stok->sinkronisasi($id_barang,$id_penyebab);
		redirect('update_stok');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
    public function index()
	{
		check_session();
		$this->load->model('m_transaksi');
		$this->load->model('m_barang');
		$data['total_revenue'] = $this->m_transaksi->total_revenue();
		$data['target_revenue'] = $this->m_transaksi->target_revenue();
		$data['total_penjualan'] = $this->m_transaksi->total_sales();
		$data['target_penjualan'] = $this->m_transaksi->target_sales();
		$data['total_barang'] = $this->m_barang->total_barang();
		$data['target_barang'] = $this->m_barang->target_barang();
		$data['total_seller'] = $this->m_barang->total_seller();
		$data['target_seller'] = $this->m_barang->target_seller();
		$data['latest_seller'] = $this->m_barang->latest_seller();
		$data['recently_barang'] = $this->m_barang->recently_barang();
		$data['latest_transaksi'] = $this->m_transaksi->latest_transaksi();
		$data['rekap_penjualan'] = $this->m_transaksi->rekap_penjualan();
		$data['rekap_sales'] = $this->m_transaksi->rekap_sales();
		$this->template->load('template','dashboard',$data);
	}
	function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
	
}


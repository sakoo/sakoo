<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_barang extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        $this->load->model('m_barang');
		check_session();
    }
	
	public function index() {
        $data['record'] = $this->m_barang->show_all_listing();
		$data['status_dash'] = "listing";
		//$data['record'] = $this->m_barang->show_all();
		//$data['status_dash'] = "all";
		$data['total_listing'] = $this->m_barang->total_listing();
		$data['total_checklist'] = $this->m_barang->total_checklist();
		$data['total_listed'] = $this->m_barang->total_listed();
        $this->template->load('template','barang/upload_barang',$data);
    }
	
	public function show_listing() {
        $data['record'] = $this->m_barang->show_all_listing();
		$data['status_dash'] = "listing";
		$data['total_listing'] = $this->m_barang->total_listing();
		$data['total_checklist'] = $this->m_barang->total_checklist();
		$data['total_listed'] = $this->m_barang->total_listed();
        $this->template->load('template','barang/upload_barang',$data);
    }
	
	public function show_checklist() {
        $data['record'] = $this->m_barang->show_all_checklist();
		$data['status_dash'] = "checklist";
		$data['total_listing'] = $this->m_barang->total_listing();
		$data['total_checklist'] = $this->m_barang->total_checklist();
		$data['total_listed'] = $this->m_barang->total_listed();
        $this->template->load('template','barang/upload_barang',$data);
    }
	
	public function show_listed() {
        $data['record'] = $this->m_barang->show_all_listed();
		$data['status_dash'] = "listed";
		$data['total_listing'] = $this->m_barang->total_listing();
		$data['total_checklist'] = $this->m_barang->total_checklist();
		$data['total_listed'] = $this->m_barang->total_listed();
        $this->template->load('template','barang/upload_barang',$data);
    }
	
	function penyebab_check($str) {
		if ($str == '0') {
			$this->form_validation->set_message('penyebab_check', 'Pilihan Penyebab Unlisting harap diisi');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	//proses data dari page upload barang
	public function proses_listing($id_barang = '',$id_ecommerce = '') {
		//echo "test";
		if(isset($_POST['submit'])){
			// proses barang
			$id_barang = $this->input->post('kodeBarang');
			$id_ecommerce = $this->input->post('idEcommerce');
			$url = $this->input->post('url');
			$penyebab = $this->input->post('penyebab');
			
			$this->form_validation->set_rules('url', 'url', 'required|trim');
			$this->form_validation->set_rules('penyebab', 'penyebab', 'required|trim|callback_penyebab_check');

			if ($this->form_validation->run() == FALSE) {
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_upload($id_barang,$id_ecommerce);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				//print_r($data);
				$this->template->load('template','barang/form_proses',$data);
			} 
			else {
				$this->m_barang->edit_url($id_barang,$id_ecommerce,2,$url);
				redirect('upload_barang');
			}
		}
        else{
			if(isset($_POST['proses_url_1']) || isset($_POST['proses_url_2']) || isset($_POST['proses_url_3']) || isset($_POST['proses_url_4'])){
				// proses barang
				if(isset($_POST['proses_url_1'])) $id_ecommerce = 1;
				if(isset($_POST['proses_url_2'])) $id_ecommerce = 2;
				if(isset($_POST['proses_url_3'])) $id_ecommerce = 3;
				if(isset($_POST['proses_url_4'])) $id_ecommerce = 4;
				$id_barang = $this->input->post('kodeBarang');
				$url_id = 'url_'.$id_ecommerce;
				$url = $this->input->post($url_id);
				
				$this->form_validation->set_rules($url_id, $url_id, 'required|trim');

				if ($this->form_validation->run() == FALSE) {
					$data['record'] = $this->m_barang->show_one($id_barang);
					$data['status'] = $this->m_barang->show_status_upload_all($id_barang);
					$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
					//print_r($data);
					$this->template->load('template','barang/form_proses',$data);
				} 
				else {
					$this->m_barang->edit_url($id_barang,$id_ecommerce,2,$url);
					$data['record'] = $this->m_barang->show_one($id_barang);
					$data['status'] = $this->m_barang->show_status_upload_all($id_barang);
					$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
					$this->template->load('template','barang/form_proses',$data);
					/*
					//send notifikasi
					$min_status = $this->m_barang->search_status($id_barang,3);
					foreach ($min_status->result() as $m) { 
						if($m->jumlah > 1) {
							//barang udah listing
						}
						else {
							$nama_barang = "";
							$id_toko = "";
							$nama_id_toko =  $this->m_barang->show_nama($id_barang);
							foreach ($nama_id_toko->result() as $nid) {
								$nama_barang = $nid->nama_barang;
								$id_toko = $nid->id_toko;
							}
							$deskripsi = trim($nama_barang)." telah listing.";
							$this->send_to_app($id_toko,'Barang listing',$deskripsi,$id_barang,"barang","kirim");
						}
					}
					*/
				}
			}
			elseif(isset($_POST['checklist_url_1']) || isset($_POST['checklist_url_2']) || isset($_POST['checklist_url_3']) || isset($_POST['checklist_url_4'])) {
				if(isset($_POST['checklist_url_1'])) $id_ecommerce = 1;
				if(isset($_POST['checklist_url_2'])) $id_ecommerce = 2;
				if(isset($_POST['checklist_url_3'])) $id_ecommerce = 3;
				if(isset($_POST['checklist_url_4'])) $id_ecommerce = 4;
				$id_barang = $this->input->post('kodeBarang');
				$record = $this->m_barang->edit($id_barang,$id_ecommerce,3);
				//check listing masing2 ecommerce
				$min_status = $this->m_barang->search_status($id_barang,3);
				//listing dari masing2 ecommerce, manually 3
				//listing dari masing2 ecommerce, manually 2
				//listing dari masing2 ecommerce, manually 1
				foreach ($min_status->result() as $m) { 
					if($m->jumlah >= 1) {
						//echo "update tanggal listing";
						$this->m_barang->edit_listing($id_barang);
					}
					else {
						//echo "tidak update tanggal listing";
					}
				}
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_upload_all($id_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				$this->template->load('template','barang/form_proses',$data);
				//send notifikasi
				$min_status = $this->m_barang->search_status($id_barang,3);
				foreach ($min_status->result() as $m) { 
					if($m->jumlah > 1) {
						//barang udah listing
					}
					else {
						$nama_barang = "";
						$id_toko = "";
						$nama_id_toko =  $this->m_barang->show_nama($id_barang);
						foreach ($nama_id_toko->result() as $nid) {
							$nama_barang = $nid->nama_barang;
							$id_toko = $nid->id_toko;
						}
						$deskripsi = trim($nama_barang)." telah listing.";
						$this->send_to_app($id_toko,'Barang listing',$deskripsi,$id_barang,"barang","kirim");
					}
				}
			}
			elseif(isset($_POST['penyebab'])) {
				$id_barang = $this->input->post('kodeBarang');
				$id_ecommerce = $this->input->post('idEcommerce');
				$penyebab = $this->input->post('penyebab');
				$this->form_validation->set_rules('penyebab', 'penyebab', 'required|trim|callback_penyebab_check');

				if ($this->form_validation->run() == FALSE) {
					$data['record'] = $this->m_barang->show_one($id_barang);
					$data['status'] = $this->m_barang->show_status_upload_all($id_barang,$id_ecommerce);
					$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
					//print_r($data);
					$this->template->load('template','barang/form_proses',$data);
				} 
				else {
					$this->m_barang->edit_status_penyebab($id_barang,$penyebab);
					$data['record'] = $this->m_barang->show_one($id_barang);
					$data['status'] = $this->m_barang->show_status_upload_all($id_barang,$id_ecommerce);
					$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
					//print_r($data);
					//send notifikasi ke user
					//alert
					if($penyebab != 1) {
						$min_status = $this->m_barang->search_status($id_barang,3);
						foreach ($min_status->result() as $m) { 
							if($m->jumlah >= 1) {
								//barang udah listing
							}
							else {
								//belum listing	
								$nama_barang = "";
								$status_penyebab = "";
								$id_toko = "";
							    $nama_id_toko =  $this->m_barang->show_nama($id_barang);
								foreach ($nama_id_toko->result() as $nid) {
									$nama_barang = $nid->nama_barang;
									$id_toko = $nid->id_toko;
								}
								$nama_penyebab =  $this->m_barang->show_nama_penyebab($penyebab);
								foreach ($nama_penyebab->result() as $np) {
									$status_penyebab = $np->status;
								}
								$deskripsi = "Upload ".trim($nama_barang)." ditolak karena ".$status_penyebab.".";
								$this->send_to_app($id_toko,'Upload barang ditolak',$deskripsi,$id_barang,"barang","kirim");
							}
						}
					}
					$this->template->load('template','barang/form_proses',$data);
				}
			}
			else {
				//$id=  $this->uri->segment(3);
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_upload_all($id_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				//print_r($data);
				$this->template->load('template','barang/form_proses',$data);
			}
        }
	}
	
	public function send_to_app($id_toko,$title,$deskripsi,$id,$id_tipe_alert,$param_notif) {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$token = "\"userId\":\"".$id_toko."\",\"title\":\"".$title."\",\"body\":\"".trim($deskripsi)."\",\"id\":\"".$id."\",\"type\":\"".$id_tipe_alert."\",\"paramNotif\":\"".$param_notif."\"";
		//echo "Token : ".$token."<br>";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://103.195.31.220:5000/v1/fcm/sendOneNotification");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{".$token."}");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		//print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
	}
	
	//proses data dari page upload barang
	public function proses_listing_all() {
		//echo "test";
	}
	
	//proses untuk cheklist modal
	public function modal() {
		$id_barang = $_POST["id_barang"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$record = $this->m_barang->show_one($id_barang);
        $status = $this->m_barang->show_status_upload($id_barang,$id_ecommerce);
		$foto = $this->m_barang->show_foto_tambahan($id_barang);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama_barang"] = $r->nama_barang;
			$return["nama_toko"] = $r->nama_toko;
			$return["id_barang"] = $r->id_barang;
			$return["stok"] = $r->stok;
			//Penjual :
			//Dikirim dari : 
			$deskripsi = "Penjual : ".$r->nama_toko." ";
			$deskripsi .= "Dikirim dari : ".$r->lokasi." ";
			if($r->merk != "")
				$deskripsi .= "Merk : ".$r->merk." ";
			if($r->bahan != "")
				$deskripsi .= "Bahan : ".$r->bahan." ";
			if($r->volume != "0x0x0")
				$deskripsi .= "Volume : ".$r->volume." ";
			$deskripsi .= $r->deskripsi;
			$return["deskripsi"] = $deskripsi;
			//foto
			$link_foto = "<a href=\"".$r->foto."\" download=\"".$r->foto."\">
							<img src=\"".$r->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$link_foto = preg_replace('/"([^"]+)"\s*:\s*/', '$1:',$link_foto);
			$return["foto"] = $link_foto;
			$return["waktu_upload"] = $r->tanggal_upload;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["harga_markup"] = $r->harga_markup;
			$return["lokasi"] = $r->lokasi;
		}
		foreach ($status->result() as $s) { 
			$return["id_status"] = $s->id_status;
			$return["status_barang"] = $s->status;
			$return["id_ecommerce"] = $s->id_ecommerce;
			$return["nama_ecommerce"] = $s->nama;
			$return["url"] = $s->url;
		}
		$list = array();
		foreach ($foto->result() as $f) {  
			$link_foto_tambahan = "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
							<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$list[] = $link_foto_tambahan;
		}
		$return["foto_tambahan"] = $list;
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//proses untuk preview
	public function preview() {
		$id_barang = $_POST["id_barang"];
		$record = $this->m_barang->show_one($id_barang);
        $status = $this->m_barang->show_status_upload_all($id_barang);
		$foto = $this->m_barang->show_foto_tambahan($id_barang);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama_barang"] = $r->nama_barang;
			$return["nama_toko"] = $r->nama_toko;
			$return["id_barang"] = $r->id_barang;
			$return["stok"] = $r->stok;
			$deskripsi = "Penjual : ".$r->nama_toko." ";
			$deskripsi .= "Dikirim dari : ".$r->lokasi." ";
			if($r->merk != "")
				$deskripsi .= "Merk : ".$r->merk." ";
			if($r->bahan != "")
				$deskripsi .= "Bahan : ".$r->bahan." ";
			if($r->volume != "0x0x0")
				$deskripsi .= "Volume : ".$r->volume." ";
			$deskripsi .= $r->deskripsi;
			$return["deskripsi"] = $deskripsi;
			//foto
			$link_foto = "<a href=\"".$r->foto."\" download=\"".$r->foto."\">
							<img src=\"".$r->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$link_foto = preg_replace('/"([^"]+)"\s*:\s*/', '$1:',$link_foto);
			$return["foto"] = $link_foto;
			$return["waktu_upload"] = $r->tanggal_upload;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["harga_markup"] = $r->harga_markup;
			$return["lokasi"] = $r->lokasi;
		}
		$url_list = array();
		foreach ($status->result() as $s) { 
			$return["id_status"] = $s->id_status;
			$return["status_barang"] = $s->status;
			$return["id_ecommerce"] = $s->id_ecommerce;
			$return["nama_ecommerce"] = $s->nama;
			$return["url"] = $s->url;
			$url_list[] = $s->url;
		}
		$list = array();
		foreach ($foto->result() as $f) {  
			$link_foto_tambahan = "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
							<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$list[] = $link_foto_tambahan;
		}
		$return["url_list"] = $url_list;
		$return["foto_tambahan"] = $list;
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function modal_checklist() {
		$id_barang = $_POST["id_barang"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$record = $this->m_barang->edit($id_barang,$id_ecommerce,3);
		//check listing masing2 ecommerce
		$min_status = $this->m_barang->search_status($id_barang,3);
		//listing dari masing2 ecommerce, manually 3
		foreach ($min_status->result() as $m) { 
			if($m->jumlah == 3) {
				//echo "update tanggal listing";
				$this->m_barang->edit_listing($id_barang);
			}
			else {
				//echo "tidak update tanggal listing";
			}
		}
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
}
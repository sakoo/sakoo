<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crontab_email extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        $this->load->model('m_email');
		$this->load->model('m_crontab');
		$this->load->model('m_barang');
		$this->load->model('m_transaksi');
		date_default_timezone_set("Asia/Bangkok");
    }
	
	//Tokopedia,Bukalapak
	public function get_new_unread_email() {
		set_time_limit(3000); 
		$begin_time = microtime(true);
		//connect to gmail with your credentials
		$hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
		$username = 'sakoofighting2@gmail.com';
		$password = '1tokoonline';
		//try to connect
		$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

		//grab emails
		$emails = imap_search($inbox,'UNSEEN');
		
		//variabel yang diambil
		$buyer = "";
		$alamat_pengiriman = "";
		$no_hp = "";
		$no_transaksi = "";
		$barang = "";
		$id_barang = "";
		$jumlah = "";
		$harga = "";
		$biaya_kirim = "";
	    $waktu_pemesanan = "";
		$waktu_email = "";
		$ecommerce = "";
		$jasa_pengiriman = "";
		$total_harga = "";
		$total_pembayaran = "";
		$asuransi = "";
		$id_transaksi = "";
		$keterangan = "";
		$keterangan_barang = "";

		//if emails are returned, cycle through each
		if($emails) {
			
			//begin output var
			$output = '';
			
			//put the newest emails on top
			rsort($emails);
			
			//for every email
			foreach($emails as $email_number) {
				//untuk cek multiseller
				$multi_seller = 0;
				$id_transaksi_seller = "";
				
				$overview = imap_fetch_overview($inbox,$email_number,0);
				$structure = imap_fetchstructure($inbox, $email_number);
				//print_r($structure);

				if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
					$part = $structure->parts[1];
					$message = imap_fetchbody($inbox,$email_number,2);

					if($part->encoding == 3) {
						$message = imap_base64($message);
					} else if($part->encoding == 1) {
						$message = imap_8bit($message);
					} else {
						$message = imap_qprint($message);
					}
				}
				elseif(!isset($structure->parts)) {
					$message = imap_fetchbody($inbox,$email_number,1);

					if($structure->encoding == 3) {
						$message = imap_base64($message);
					} else if($structure->encoding == 1) {
						$message = imap_8bit($message);
					} else {
						$message = imap_qprint($message);
					}
				}
				
				$output.= '<div class="toggle'.($overview[0]->seen ? 'read' : 'unread').'">';
				$output.= '<span class="from">From: '.utf8_decode(imap_utf8($overview[0]->from)).'</span>';
				$output.= '<span class="date">on '.utf8_decode(imap_utf8($overview[0]->date)).'</span>';
				if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
					$output.= '<br /><span class="subject">Subject('.$part->encoding.'): '.utf8_decode(imap_utf8($overview[0]->subject)).'</span> ';
				}
				elseif(!isset($structure->parts)) {
					$output.= '<br /><span class="subject">Subject('.$structure->encoding.'): '.utf8_decode(imap_utf8($overview[0]->subject)).'</span> ';
				}
				
				$output.= '</div>';

				$output.= '<div class="body">'.$message.'</div><hr />';
				
				//echo "===END OF EMAIL===";
				$from = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->from))));
				$date = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->date))));
				if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
					$subject = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->subject))));
				}
				elseif(!isset($structure->parts)) {
					$subject = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->subject))));
				}
				echo $from."---".$date."---".$subject."<br>";
				//cek ecommerce
				$record = $this->m_email->show_all_ecommerce();
				foreach ($record->result() as $r) { 
					$id_ecommerce = $r->id;
					$nama = $r->nama;
					echo "Nama Ecommerce : ".$nama."<br>";
					if($from === $nama) {
						$ecommerce = $id_ecommerce;
						$waktu_email = date('Y-m-d H:i:s', strtotime($date));
						//blanja
						if($ecommerce === "1") {
							echo "wes masukk <br>";
							$get_pesanan_baru = "Pemberitahuan : Pesanan telah Dibayar";
							//cek email pesanan baru
							if(strpos($subject, $get_pesanan_baru) !== false) {
								//get DOM from URL or file
								$html = str_get_html($message);
								//Find all links 
								$string = $html->plaintext;
								echo $string;
								//rock the star
								if(0)
								{
								//buyer
								$buyer = preg_match("/Email(?P<buyer>.*)Telepon/", $string, $matches);
								$buyer = trim($matches['buyer']);
								//no transaksi
								$no_transaksi = preg_match("/No order :(?P<no_transaksi>.*)(per tanggal/", $string, $matches);
								$no_transaksi = trim($matches['no_transaksi']);
								//tanggal pemesanan
								$waktu_pemesanan = preg_match("/(per tanggal(?P<waktu_pemesanan>.*)WIB/", $string, $matches);
								$raw_waktu_pemesanan = trim($matches['waktu_pemesanan']);
								//setting waktu
								$new_date = str_replace('/', '-', $raw_waktu_pemesanan);
								$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($new_date));
								//echo "Waktu pemesanan test : ".$matches['waktu_pemesanan']."<br>";
								//pengiriman
								$jasa_pengiriman = "-";
								//asuransi
								$asuransi = "-";
								//alamat
								$alamat_pengiriman = preg_match("/Informasi pengiriman(?P<alamat_pengiriman>[\s\S]*)Buyer message/", $string, $matches);
								$alamat_pengiriman = trim($matches['alamat_pengiriman']);
								//telp
								$no_hp = preg_match("/Telepon(?P<no_hp>.*)Informasi pengiriman/", $string, $matches);
								$no_hp = trim($matches['no_hp']);
								//total harga produk
								$total_harga = preg_match("/Total Harga Produk:(?P<total_harga>.*)Ongkos kirim:/", $string, $matches);
								$total_harga = clean_currency(trim($matches['total_harga']));
								//ongkos kirim
								$biaya_kirim = preg_match("/Ongkos kirim:(?P<biaya_kirim>.*)Total Pembayaran:/", $string, $matches);
								$biaya_kirim = clean_currency(trim($matches['biaya_kirim']));
								//total pembayaran
								$total_pembayaran = preg_match("/Total Pembayaran:(?P<total_pembayaran>.*)Mohon konfirmasi/", $string, $matches);
								$total_pembayaran = clean_currency(trim($matches['total_pembayaran']));
								
								//`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,`keterangan`,`status_transaksi`,`checked`,`multi_seller`
								$id_transaksis = $this->m_crontab->new_transaksi($no_transaksi,$no_transaksi,'paid',$waktu_email,$waktu_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','1','0');
								//masih belum fix
								
									if(!$id_transaksis) {
										echo "Transaksi sudah ada<br>";
									}
									else{
										//notifikasi
										echo "Pesananan baru <br>";
										foreach ($id_transaksis->result() as $id) { 
											$id_transaksi = $id->id_transaksi;
										}
										
										//call api transaksi
										//if($id_transaksis) {
										//	
										//}
										
										$jumlah_barang = 0;
										$prev_toko = NULL;
										foreach($html->find('ol li') as $element) {
											//cek multiseller juga
											$jumlah_barang++;
											$string_barang = $element->plaintext;
											echo "String barang : ".$string_barang."<br>";
											//barang
											$barang = preg_match("/[\s\S]+?(?=Jumlah:)/", $string_barang, $matches);
											$barang = trim($matches[0]);
											$id_barang = "";
											//jumlah
											$jumlah = preg_match("/Jumlah:(?P<jumlah>[\s\S]*)Buah/", $string_barang, $matches);
											$jumlah = trim($matches['jumlah']);
											//harga
											$harga = preg_match("/@(?P<harga>.*)\)/", $string_barang, $matches);
											$harga = clean_currency(trim($matches['harga']));
											//keterangan
											$keterangan_barang = preg_match("/Keterangan:(?P<keterangan>.*)/", $string_barang, $matches);
											if($matches['keterangan']) {
												$keterangan_barang = trim($matches['keterangan']);
											}
											else {
												$keterangan_barang = "";
											}
											
											
											//cek barang dan seller
											//Hotwheels 1 sk_1
											if (($pos = strpos(strtoupper($barang), "SK-")) !== FALSE) { 
												$id_barang = substr($barang, $pos+3); 
												$record = $this->m_barang->show_one($id_barang);
												foreach ($record->result() as $r) { 
													$id_toko = $r->id_toko;
													$nama_toko = $r->nama_toko;
													$nama_barang = $r->nama_barang;
												}
												//set prev toko untuk iterasi 1
												if($jumlah_barang == 1) {
													$prev_toko = $id_toko;
												}
												//insert data transaksi seller
												//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
												echo "id transaksi : ".$id_transaksi."<br>";
												echo "id toko : ".$id_toko."<br>";
												$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
												foreach ($id_transaksi_sellers->result() as $id) { 
													$id_transaksi_seller = $id->id;
												}
												//insert data detail transaksi
												$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
												//update jika multiseller
												if($prev_toko != $id_toko) {
													$this->m_transaksi->edit($id_transaksi,'6');
													$multi_seller = 1;
												}
												$prev_toko = $id_toko;
												
												//update stok 
												//update tanggal otomatis dari trigger
												$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
												$record2 = $this->m_transaksi->show_ecommerce();
												foreach ($record2->result() as $z) {
													if($z->id == $id_ecommerce) {
														$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
													}
													else {
														$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
													}
												}
												//alert
												$deskripsi = "Ada Pembelian ".$nama_barang;
												$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
												$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi");
												//telegram
												$this->send_notifikasi_telegram($deskripsi);
											}
										}
										//alert
										$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
										$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_email,$deskripsi_notifikasi);
										if(!$cek_input_alert) {
											echo "Alert gagal diinput<br>";
										}
										else {
											foreach ($cek_input_alert->result() as $c) { 
												$id_alert = $c->id_alert;
											}
											//input notifikasi
											//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
											$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
										}
									}
								}
							}
						}
						//tokopedia
						if($ecommerce === "2") {
							echo "wes masukk <br>";
							$get_pesanan_baru = "Pesanan baru dari";
							//cek email pesanan baru
							if(strpos($subject, $get_pesanan_baru) !== false) {
								//buyer
								$get_buyer = "Pesanan baru dari";
								$buyer_leng = strlen($get_buyer);
								$buyer_pos = strpos($subject, $get_buyer)+$buyer_leng;
								$get_field = "tanggal";
								$field_leng = strlen($get_field);
								$field_pos = strpos($subject,$get_field);
								$buyer = html_entity_decode(substr($subject, $buyer_pos, $field_pos-$buyer_pos), ENT_QUOTES);
								
								//get DOM from URL or file
								$html = str_get_html($message);
								//Find all links 
								$string = $html->plaintext;
								//echo $string;
								//rock the star
								//no transaksi
								$no_transaksi = preg_match("/Nomor Pemesanan:(?P<no_transaksi>.*)Tanggal Pemesanan:/", $string, $matches);
								$no_transaksi = trim($matches['no_transaksi']);
								//tanggal pemesanan
								$waktu_pemesanan = preg_match("/Tanggal Pemesanan:(?P<waktu_pemesanan>.*)Total Pesanan:/", $string, $matches);
								$raw_waktu_pemesanan = trim($matches['waktu_pemesanan']);
								//setting bulan
								$mc = array("januari"=>"January","februari"=>"February","februari"=>"February","maret"=>"March","april"=>"April","mei"=>"May","juni"=>"June","juli"=>"July","agustus"=>"August","september"=>"September","oktober"=>"October","november"=>"November","desember"=>"December");
								$new_date = explode(' ',$raw_waktu_pemesanan);
								$new_date[1] = $mc[strtolower($new_date[1])];
								$new_date = implode('-',$new_date);
								$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($new_date));
								//echo "Waktu pemesanan test : ".$matches['waktu_pemesanan']."<br>";
								//pengiriman
								$jasa_pengiriman = preg_match("/Pengiriman via:(?P<jasa_pengiriman>.*)Asuransi:/", $string, $matches);
								$jasa_pengiriman = trim($matches['jasa_pengiriman']);
								//asuransi
								$asuransi = preg_match("/Asuransi:(?P<asuransi>.*)Rincian Pesanan:/", $string, $matches);
								$asuransi = trim($matches['asuransi']);
								//alamat
								$alamat_pengiriman = preg_match("/Tujuan Pengiriman:(?P<alamat_pengiriman>[\s\S]*)Total Harga Produk:/", $string, $matches);
								$alamat_pengiriman = trim($matches['alamat_pengiriman']);
								//telp
								$no_hp = preg_match("/Telp:(?P<no_hp>.*)Total Harga Produk:/", $string, $matches);
								$no_hp = trim($matches['no_hp']);
								//total harga produk
								$total_harga = preg_match("/Total Harga Produk:(?P<total_harga>.*)Ongkos kirim:/", $string, $matches);
								$total_harga = clean_currency(trim($matches['total_harga']));
								//ongkos kirim
								$biaya_kirim = preg_match("/Ongkos kirim:(?P<biaya_kirim>.*)Total Pembayaran:/", $string, $matches);
								$biaya_kirim = clean_currency(trim($matches['biaya_kirim']));
								//total pembayaran
								$total_pembayaran = preg_match("/Total Pembayaran:(?P<total_pembayaran>.*)Mohon konfirmasi/", $string, $matches);
								$total_pembayaran = clean_currency(trim($matches['total_pembayaran']));
								
								//`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,`keterangan`,`status_transaksi`,`checked`,`multi_seller`
								$id_transaksis = $this->m_crontab->new_transaksi($no_transaksi,$no_transaksi,'paid',$waktu_email,$waktu_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','1','0');
								//masih belum fix
								
								if(!$id_transaksis) {
									echo "Transaksi sudah ada<br>";
								}
								else{
									//notifikasi
									echo "Pesananan baru <br>";
									foreach ($id_transaksis->result() as $id) { 
										$id_transaksi = $id->id_transaksi;
									}
									
									//call api transaksi
									//if($id_transaksis) {
									//	
									//}
									
									$jumlah_barang = 0;
									$prev_toko = NULL;
									foreach($html->find('ol li') as $element) {
										//cek multiseller juga
										$jumlah_barang++;
										$string_barang = $element->plaintext;
										echo "String barang : ".$string_barang."<br>";
										//barang
										$barang = preg_match("/[\s\S]+?(?=Jumlah:)/", $string_barang, $matches);
										$barang = trim($matches[0]);
										$id_barang = "";
										//jumlah
										$jumlah = preg_match("/Jumlah:(?P<jumlah>[\s\S]*)Buah/", $string_barang, $matches);
										$jumlah = trim($matches['jumlah']);
										//harga
										$harga = preg_match("/@(?P<harga>.*)\)/", $string_barang, $matches);
										$harga = clean_currency(trim($matches['harga']));
										//keterangan
										$keterangan_barang = preg_match("/Keterangan:(?P<keterangan>.*)/", $string_barang, $matches);
										if($matches['keterangan']) {
											$keterangan_barang = trim($matches['keterangan']);
										}
										else {
											$keterangan_barang = "";
										}
										
										
										//cek barang dan seller
										//Hotwheels 1 sk_1
										if (($pos = strpos(strtoupper($barang), "SK-")) !== FALSE) { 
											$id_barang = substr($barang, $pos+3); 
											$record = $this->m_barang->show_one($id_barang);
											foreach ($record->result() as $r) { 
												$id_toko = $r->id_toko;
												$nama_toko = $r->nama_toko;
												$nama_barang = $r->nama_barang;
											}
											//set prev toko untuk iterasi 1
											if($jumlah_barang == 1) {
												$prev_toko = $id_toko;
											}
											//insert data transaksi seller
											//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
											echo "id transaksi : ".$id_transaksi."<br>";
											echo "id toko : ".$id_toko."<br>";
											$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
											foreach ($id_transaksi_sellers->result() as $id) { 
												$id_transaksi_seller = $id->id;
											}
											//insert data detail transaksi
											$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
											//update jika multiseller
											if($prev_toko != $id_toko) {
												$this->m_transaksi->edit($id_transaksi,'6');
												$multi_seller = 1;
											}
											$prev_toko = $id_toko;
											
											//update stok 
											//update tanggal otomatis dari trigger
											$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
											$record2 = $this->m_transaksi->show_ecommerce();
											foreach ($record2->result() as $z) {
												if($z->id == $id_ecommerce) {
													$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
												}
												else {
													$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
												}
											}
											//alert
											$deskripsi = "Ada Pembelian ".$nama_barang;
											$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
											$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi");
											//telegram
											$this->send_notifikasi_telegram($deskripsi);
										}
									}
									//alert
									$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
									$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_email,$deskripsi_notifikasi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										foreach ($cek_input_alert->result() as $c) { 
											$id_alert = $c->id_alert;
										}
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
									}
									//}
									//else {
									//	echo "id sudah ada";
									//}
									/*
									//get html data
									$dom = new domDocument;
									$internalErrors = libxml_use_internal_errors(true);
									$dom->loadHTML($message); 
									$dom->preserveWhiteSpace = false; 
									$xpath = new DOMXPath($dom);
									
									// A name attribute on a <div>???
									$node = $xpath->query( '//table/tr')->item(1);
									var_dump($node);
									
									//mendapatkan tabel dari id
									$table = $dom->getElementByTagName('table')->item(1);
									//$table = $tables->item(1);
									//mendapatkan id transaksi
									$header = $dom->getElementsByTagName('th');
									$no_transaksi = $header->item(0)->nodeValue;
									//mendapatkan isi tabel
									$rows = $table->getElementsByTagName('tr');
									// loop over the table rows
									foreach ($rows as $row) 
									{ 
										// get each column by tag name
										$cols = $row->getElementsByTagName('td'); 
										// echo the values  
										echo $cols->item(0)->nodeValue.'<br />'; 
										echo $cols->item(1)->nodeValue.'<br />'; 
										echo $cols->item(2)->nodeValue;
									} 
									*/
									/*
									$alamat_pengiriman = "";
									$no_hp = "";
									$no_transaksi = "";
									$barang = "";
									$id_barang = "";
									$jumlah = "";
									$harga = "";
									$biaya_kirim = "";
									$waktu_pemesanan = "";
									*/
								}
							}
							
							//untuk uang masuk
							//notifikasi
							$uang_masuk = "Transaksi Penjualan dengan";
							//cek email pesanan baru
							if(strpos($subject, $uang_masuk) !== false) {
								//get DOM from URL or file
								$html = str_get_html($message);
								//Find all links 
								$string = $html->plaintext;
								$no_transaksi = preg_match("/Nomor Faktur:(?P<no_transaksi>.*)Jasa Pengiriman:/", $string, $matches);
								$no_trx = trim($matches['no_transaksi']);
								echo "ini yang sudah dibayar";
								echo $no_trx."<br>";
								$record = $this->m_crontab->update_uang_masuk($no_trx,$ecommerce);
								if(!$record) {
									echo "No trx tidak ada di db <br>";
								}
								else {
									echo "No trx ada di db <br>";
									foreach ($record->result() as $r) { 
										$no_trx_updated = trim($r->no_trx);
										$id_transaksi = $r->id_transaksi;
										$tanggal_pemesanan = $r->tanggal_pemesanan;
										if($r->status_transaksi == 4) {
											echo "Transaksi sudah update di db dan alert";
										}
										else {
											$this->m_crontab->update_status_transaksi($no_trx_updated,4);
											//alert
											//cek per transaksi seller barang
											$record_transaksi_seller = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
											foreach ($record_transaksi_seller->result() as $rts) { 
												$id_transaksi_seller = $rts->id;
												$id_toko = $rts->id_toko;
												//cek per toko
												$record_proses = $this->m_transaksi->cari_detail_transaksi_proses($id_transaksi_seller);
												$nama_barang = "";
												if($record_proses) {
													foreach ($record_proses->result() as $rp) { 
														$nama_barang .= $rp->nama_barang." dan ";
													}
													//alert
													$deskripsi = "Transaksi Penjualan produk ".substr(trim($nama_barang), 0, -5)." telah selesai.";
													$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
													$this->send_to_app($id_toko,'Transaksi Penjualan Telah Selesai',$deskripsi,$id_transaksi_seller,"saldo");
												}
												
											}
											
											$cek_alert = $this->m_crontab->cek_alert(5,$id_transaksi,$tanggal_pemesanan);
											if(!$cek_alert) {
												echo "Alert tidak ada di db <br>";
												$deskripsi = "Transaksi Penjualan ".$no_trx_updated." telah selesai";
												$cek_input_alert = $this->m_crontab->input_alert(5,$id_transaksi,$tanggal_pemesanan,$deskripsi);
												if(!$cek_input_alert) {
													echo "Alert gagal diinput<br>";
												}
												else {
													foreach ($cek_input_alert->result() as $c) { 
														$id_alert = $c->id_alert;
													}
													//input notifikasi
													//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
													$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
												}
											}
										}
									}
								}
							}
							
						}
						//bukalapak
						if(0) {
							//elseif($ecommerce === "4") 
							if ($ecommerce === "4") 
							{
								echo "wes masukk <br>";
								//cek email pesanan baru
								$get_pesanan_baru = "Pesanan Baru";
								//cek email pesanan baru
								if(strpos($subject, $get_pesanan_baru) !== false) {
									//buyer
									$dom = new domDocument;
									$dom->loadHTML($message); 
									$dom->preserveWhiteSpace = false; 
									$xpath = new DOMXPath($dom);
									//mendapatkan tabel dari id
									$tables = $dom->getElementById('templateList');
									//mendapatkan id transaksi
									$header = $dom->getElementsByTagName('th');
									$no_transaksi = $header->item(0)->nodeValue;
									//mendapatkan isi tabel
									$rows = $tables->getElementsByTagName('tr');
									foreach ($rows as $row) {
										$cols = $row->getElementsByTagName('td');
										print_r($cols);
										if(trim($cols->item(0)->nodeValue) === "Pembeli") {
											//buyer
											$buyer = trim($cols->item(1)->nodeValue);
										}
										elseif(trim($cols->item(0)->nodeValue) === "Jasa Pengiriman") {
											//jasa pengiriman
											$jasa_pengiriman = trim($cols->item(1)->nodeValue);
										}
										elseif ($xpath->evaluate('count(./a)', $cols->item(0)) > 0) {
											//if($cols->getElementsByTagName('a')->item(0)->getAttribute('class') === "product-name-link") {
												//barang bisa banyak
												echo "horeee nemu anchor";
												$barang = trim($cols->item(0)->nodeValue);
												$jumlah = trim($cols->item(1)->nodeValue);
												$harga = trim($cols->item(3)->nodeValue);
											//}
										}
										elseif(trim($cols->item(0)->nodeValue) === "Total Harga Barang") {
											//total harga
											$total_harga = trim($cols->item(1)->nodeValue);
										}
										elseif(trim($cols->item(0)->nodeValue) === "Biaya Kirim") {
											//biaya kirim
											$biaya_kirim = trim($cols->item(1)->nodeValue);
										}
										elseif(trim($cols->item(0)->nodeValue) === "TOTAL PEMBAYARAN") {
											//total pembayaran
											$total_pembayaran = trim($cols->item(1)->nodeValue);
										}
										//alamat_pengiriman
										
									}
									/*
									$alamat_pengiriman = "";
									$no_hp = "";
									$no_transaksi = "";
									$barang = "";
									$id_barang = "";
									$jumlah = "";
									$harga = "";
									$biaya_kirim = "";
									$waktu_pemesanan = "";
									*/
								}
							}
						}
						else {
							echo "loh gak nemu id ecommerce nya <br>";
						}
					}
					else {
						echo "badalah g ada nama ecommerce yang cocok <br>";
					}
				}
				echo "Ecommerce : ".$ecommerce."<br>";
				echo "Waktu Email : ".$waktu_email."<br>";
				echo "Buyer : ".$buyer."<br>";
				echo "Alamat Pengiriman : ".$alamat_pengiriman."<br>";
				echo "Jasa Pengiriman : ".$jasa_pengiriman."<br>";
				echo "No HP : ".$no_hp."<br>";
				echo "No Transaksi : ".$no_transaksi."<br>";
				echo "Barang : ".$barang."<br>";
				echo "Kode Barang : ".$id_barang."<br>";
				echo "Jumlah : ".$jumlah."<br>";
				echo "Harga Barang : ".$harga."<br>";
				echo "Keterangan Barang : ".$keterangan_barang."<br>";
				echo "Total Harga Barang: ".$total_harga."<br>";
				echo "Biaya Kirim : ".$biaya_kirim."<br>";
				echo "Total Pembayaran: ".$total_pembayaran."<br>";
				echo "Waktu Pemesanan : ".$waktu_pemesanan."<br>";
				echo "Asuransi : ".$asuransi."<br>";
				echo "ID Transaksi : ".$id_transaksi."<br>";
				echo "Keterangan : ".$keterangan."<br>";
				
				echo "Message nya adalah : ".$message."<br>";
				
				$output = '';
				$message = '';
				$subject = '';
				if(!$multi_seller) {
					//update biaya kirim
					$this->m_transaksi->update_biaya_kirim_per_seller($id_transaksi_seller,$biaya_kirim);
				}
			}
			//echo $output;
		} 

		//close the connection
		imap_close($inbox);
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'unread email');
		echo "Total waktu script: ".$total_time."<br>";
    }
	
	//new transaksi bukalapak
	public function get_new_transaksi_bukalapak() {
		//token fpz6OcWDGmI8K5sShkI
		//user id 35758617
		//ambil data transaksi dengan id 2
		//cek status yang paid
		//ambil datanya
		$begin_time = microtime(true);
		$data = $this->get_transaksi_status(2);
		print_r($data);
		if($data['status'] == 'OK') {
			//transaction
			foreach ($data['transactions'] as $value) { 
				//variabel yang diambil
				$buyer = "";
				$alamat_pengiriman = "";
				$no_hp = "";
				$no_transaksi = "";
				$barang = "";
				$id_barang = "";
				$jumlah = "";
				$harga = "";
				$biaya_kirim = "";
				$waktu_pemesanan = "";
				$waktu_email = "";
				$ecommerce = "3";
				$jasa_pengiriman = "";
				$total_harga = "";
				$total_pembayaran = "";
				$asuransi = "";
				$id_transaksi = "";
				$keterangan = "";
				$keterangan_barang = "";
				//untuk cek multiseller
				$multi_seller = 0;
				$id_transaksi_seller = "";
				
				//$id_transaksi = trim($value['id']);
				//if($value['state'] == "received") 
				if($value['state'] == "paid") {
					// [id] => 498541682
					$id_trx = trim($value['id']);
					//[transaction_id] => 170495737447 
					$no_transaksi = trim($value['transaction_id']);
					//[amount] => 1000 
					$total_harga = trim($value['amount']);
					//[courier] => JNE REG 
					$jasa_pengiriman = trim($value['courier']);
					//[buyer_notes] => .
					$keterangan = trim($value['buyer_notes']);
					//[shipping_fee] => 9000
					$biaya_kirim = trim($value['shipping_fee']);
					//[insurance_cost] => 0
					$Asuransi = trim($value['insurance_cost']);
					//[total_amount] => 10000
					$total_pembayaran = trim($value['total_amount']);
					//alamat dan no hp
					/*
					[name] => Reti 
					[phone] => 082216920115 
					[address] => Jl Jendral Gatot Subroto Kav.52 
					[area] => Mampang Prapatan 
					[city] => Jakarta Selatan 
					[province] => DKI Jakarta 
					[post_code] => 12710
					*/
					$no_hp = trim($value['consignee']['phone']);
					$alamat_pengiriman = trim($value['consignee']['name'])." ".trim($value['consignee']['phone'])." ".trim($value['consignee']['address']).", ".trim($value['consignee']['area']).", ".trim($value['consignee']['city']).", ".trim($value['consignee']['province']).", ".trim($value['consignee']['post_code']);
					//[buyer] => Array ( [id] => 14135146 [name]
					$buyer = trim($value['buyer']['name']);
					//[state_changes] => Array (
					$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($value['state_changes']['paid_at']));
					$waktu_email = $waktu_pemesanan;
					
					$id_transaksis = $this->m_crontab->new_transaksi($id_trx,$no_transaksi,'paid',$waktu_pemesanan,$waktu_email,$ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','1','0');
					if(!$id_transaksis) {
						echo "Transaksi sudah ada<br>";
					}
					else{
						echo "Pesananan baru <br>";
						foreach ($id_transaksis->result() as $id) { 
							$id_transaksi = $id->id_transaksi;
						}
									
						$jumlah_barang = 0;
						$prev_toko = NULL;
						//[products] => Array 
						foreach($value['products'] as $value3) {
							$jumlah_barang++;
							//[name] => hot wheel
							$barang = trim($value3['name']);
							//[order_quantity] => 1
							$jumlah = trim($value3['order_quantity']);
							//[price] => 500
							$harga = trim($value3['price']);
							$keterangan_barang = "";
							
							//cek barang dan seller
							//Hotwheels 1 sk_1
							if (($pos = strpos(strtoupper($barang), "SK-")) !== FALSE) { 
								$id_barang = substr($barang, $pos+3); 
								$record = $this->m_barang->show_one($id_barang);
								foreach ($record->result() as $r) { 
									$id_toko = $r->id_toko;
									$nama_toko = $r->nama_toko;
									$nama_barang = $r->nama_barang;
								}
								//set prev toko untuk iterasi 1
								if($jumlah_barang == 1) {
									$prev_toko = $id_toko;
								}
								//insert data transaksi seller
								//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
								$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
								foreach ($id_transaksi_sellers->result() as $id) { 
									$id_transaksi_seller = $id->id;
								}
								//insert data detail transaksi
								$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
								//update jika multiseller
								if($prev_toko != $id_toko) {
									$this->m_transaksi->edit($id_transaksi,'6');
									$multi_seller = 1;
								}
								$prev_toko = $id_toko;
								
								//update stok 
								//update tanggal otomatis dari trigger
								$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
								$record2 = $this->m_transaksi->show_ecommerce();
								foreach ($record2->result() as $z) {
									if($z->id == 3) {
										$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
									}
									else {
										$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
									}
								}
								//alert
								//alert
								$deskripsi = "Ada Pembelian ".$nama_barang;
								$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
								$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi");
								//telegram
								$this->send_notifikasi_telegram($deskripsi);
							}
						}
						//alert
						$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
						$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_pemesanan,$deskripsi_notifikasi);
						if(!$cek_input_alert) {
							echo "Alert gagal diinput<br>";
						}
						else {
							foreach ($cek_input_alert->result() as $c) { 
								$id_alert = $c->id_alert;
							}
							//input notifikasi
							//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
							$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
						}
					}
				}
				echo "Ecommerce : ".$ecommerce."<br>";
				echo "Waktu Email : ".$waktu_email."<br>";
				echo "Buyer : ".$buyer."<br>";
				echo "Alamat Pengiriman : ".$alamat_pengiriman."<br>";
				echo "Jasa Pengiriman : ".$jasa_pengiriman."<br>";
				echo "No HP : ".$no_hp."<br>";
				echo "No Transaksi : ".$no_transaksi."<br>";
				echo "Barang : ".$barang."<br>";
				echo "Kode Barang : ".$id_barang."<br>";
				echo "Jumlah : ".$jumlah."<br>";
				echo "Harga Barang : ".$harga."<br>";
				echo "Keterangan Barang : ".$keterangan_barang."<br>";
				echo "Total Harga Barang : ".$total_harga."<br>";
				echo "Biaya Kirim : ".$biaya_kirim."<br>";
				echo "Total Pembayaran: ".$total_pembayaran."<br>";
				echo "Waktu Pemesanan : ".$waktu_pemesanan."<br>";
				echo "Asuransi : ".$asuransi."<br>";
				echo "ID Transaksi : ".$id_transaksi."<br>";
				echo "Keterangan : ".$keterangan."<br>";
				
				if(!$multi_seller) {
					//update biaya kirim
					$this->m_transaksi->update_biaya_kirim_per_seller($id_transaksi_seller,$biaya_kirim);
				}
			}
		}
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'new transaksi bukalapak');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	public function get_uang_masuk() {
		//token fpz6OcWDGmI8K5sShkI
		//user id 35758617
		//ambil data transaksi dengan id 2
		//cek status yang paid
		//ambil datanya
		$begin_time = microtime(true);
		$data = $this->get_transaksi_status(5);
		print_r($data);
		if($data['status'] == 'OK') {
			//transaction
			foreach ($data['transactions'] as $value) { 
				
				$id_transaksi = trim($value['id']);
				//if($value['state'] == "received") 
				if($value['state'] == "remitted") {
					echo "ini yang sudah dibayar";
					$no_trx = $value['transaction_id'];
					echo $no_trx."<br>";
					$record = $this->m_crontab->update_uang_masuk($no_trx,3);
					if(!$record) {
						echo "No trx tidak ada di db <br>";
					}
					else {
						echo "No trx ada di db <br>";
						foreach ($record->result() as $r) { 
							$no_trx_updated = trim($r->no_trx);
							$id_transaksi = $r->id_transaksi;
							$tanggal_pemesanan = $r->tanggal_pemesanan;
							if($r->status_transaksi == 4) {
								echo "Transaksi sudah update di db dan alert";
							}
							else {
								$this->m_crontab->update_status_transaksi($no_trx_updated,4);
								//alert
								//cek per transaksi seller barang
								$record_transaksi_seller = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
								foreach ($record_transaksi_seller->result() as $rts) { 
									$id_transaksi_seller = $rts->id;
									$id_toko = $rts->id_toko;
									//cek per toko
									$record_proses = $this->m_transaksi->cari_detail_transaksi_proses($id_transaksi_seller);
									$nama_barang = "";
									if($record_proses) {
										foreach ($record_proses->result() as $rp) { 
											$nama_barang .= $rp->nama_barang." dan ";
										}
										//alert
										$deskripsi = "Transaksi Penjualan produk ".substr(trim($nama_barang), 0, -5)." telah selesai.";
										$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
										$this->send_to_app($id_toko,'Transaksi Penjualan Telah Selesai',$deskripsi,$id_transaksi_seller,"saldo");
									}
									
								}
								
								$cek_alert = $this->m_crontab->cek_alert(5,$id_transaksi,$tanggal_pemesanan);
								if(!$cek_alert) {
									echo "Alert tidak ada di db <br>";
									$deskripsi = "Transaksi Penjualan ".$no_trx_updated." telah selesai";
									$cek_input_alert = $this->m_crontab->input_alert(5,$id_transaksi,$tanggal_pemesanan,$deskripsi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										foreach ($cek_input_alert->result() as $c) { 
											$id_alert = $c->id_alert;
										}
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
									}
								}
							}
						}
					}
				}
			}
		}
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'uang masuk bukalapak');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	public function cek_transaksi_bukapalak($id_sistem) {
		//cek status transaksi dengan id
		//cek status yang remitted
		//update proses uang masuk
	}
	
	public function token_api_bukalapak() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/authenticate.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "sakoofighting2@gmail.com" . ":" . "1tokoonline");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		var_dump(json_decode($server_output, true));
	}
	
	public function get_transaksi_all() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	//mendapatkan data transaksi dengan id
	public function get_transaksi_status($status) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions.json?status=".$status."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		print_r($server_output);
		$return_value = json_decode($server_output, true);
		//print_r(json_decode($server_output, true));
		return $return_value;
	}
	
	public function get_transaksi() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions/524447227.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	public function get_transaksi_id($id) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions/".$id.".json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	public function get_notifikasi() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/notifications/list.json?[type]=reminder");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	public function parser() {
		// new dom object
		$dom = new DOMDocument();

		//load the html
		$html = $dom->loadHTML("contoh/pesan_baru_tokped.html");
		//$html = utf8_encode($html);
		
		print_r($html);
		
		//discard white space 
		$dom->preserveWhiteSpace = false; 

		//the table by its tag name
		$tables = $dom->getElementsByTagName('table'); 

		//get all rows from the table
		$rows = $tables->item(0)->getElementsByTagName('tr'); 

		// loop over the table rows
		foreach ($rows as $row) 
		{ 
		// get each column by tag name
		  $cols = $row->getElementsByTagName('td'); 
		// echo the values  
		  echo $cols->item(0)->nodeValue.'<br />'; 
		  echo $cols->item(1)->nodeValue.'<br />'; 
		  echo $cols->item(2)->nodeValue;
		} 
	}
	
	public function test_api() {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://103.195.31.220:5000/v1/fcm/sendOneNotification");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"userId\":\"1\",\"title\":\"tes\",\"body\":\"Halooo marcho\",\"id\":\"3\",\"type\":\"transaksi\"}");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
	}
	
	public function send_to_app($id_toko,$title,$deskripsi,$id,$id_tipe_alert) {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$token = "\"userId\":\"".$id_toko."\",\"title\":\"".$title."\",\"body\":\"".trim($deskripsi)."\",\"id\":\"".$id."\",\"type\":\"".$id_tipe_alert."\"";
		echo "Token : ".$token."<br>";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://103.195.31.220:5000/v1/fcm/sendOneNotification");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{".$token."}");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
	}
	
	//crontab update status dan resi
	public function update_status_transaksi_dan_resi() {
		$begin_time = microtime(true);
		//cari semua transaksi yang masih open
		//cari anakan dan cek status
		//jika statusnya oke langsung ubah status indukan
		//untuk status transaksi
		$proses_not_all = 0;
		$record = $this->m_transaksi->cari_transaksi_status(1);
		//parameter 
		foreach ($record->result() as $r) { 
			$id_transaksi = $r->id_transaksi;
			$tanggal_pemesanan = $r->tanggal_pemesanan;
			$selisih = $r->selisih;
			$record2 = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
			foreach ($record2->result() as $r2) {
				$id_transaksi_seller = $r2->id;
				$id_toko = $r2->id_toko;
				$record3 = $this->m_transaksi->cari_detail_transaksi($id_transaksi_seller);
				foreach ($record3->result() as $r3) {
					$id_status = $r3->id_status;
					$id_detail_transaksi = $r3->id_detail_transaksi;
					$nama_barang = $r3->nama_barang;
					if($id_status != 1) {
						//1x24 jam ke seller
						if($id_status == 0) {
							if($selisih >= 24) {
								$cek_alert = $this->m_crontab->cek_alert(2,$id_detail_transaksi,$tanggal_pemesanan);
								if(!$cek_alert) {
									echo "Alert tidak ada di db <br>";
									$deskripsi = "Mohon proses atau tolak pemesanan ".$nama_barang." karena lebih dari 24 jam";
									$cek_input_alert = $this->m_crontab->input_alert(2,$id_detail_transaksi,$tanggal_pemesanan,$deskripsi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										foreach ($cek_input_alert->result() as $c) { 
											$id_alert = $c->id_alert;
										}
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
										//alert
										$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
										$this->send_to_app($id_toko,"Mohon proses/tolak transaksi",$deskripsi,$id_detail_transaksi,"detail_transaksi");
									}
								}
							}
						}
						$proses_not_all = 1;
					}
				}
			}
			if(!$proses_not_all) {
				$this->m_transaksi->edit($id_transaksi,'2');
			}
			$proses_not_all = 0;
		}
		
		//untuk input resi
		$resi_not_all = 0;
		$record = $this->m_transaksi->cari_transaksi_status(2);
		//parameter 
		foreach ($record->result() as $r) { 
			$id_transaksi = $r->id_transaksi;
			$tanggal_pemesanan = $r->tanggal_pemesanan;
			$selisih = $r->selisih;
			$record2 = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
			foreach ($record2->result() as $r2) {
				$id_transaksi_seller = $r2->id;
				$id_toko = $r2->id_toko;
				$record3 = $this->m_transaksi->cari_detail_transaksi($id_transaksi_seller);
				foreach ($record3->result() as $r3) {
					$nomor_resi = $r3->nomor_resi;
					$id_status = $r3->id_status;
					$id_detail_transaksi = $r3->id_detail_transaksi;
					$nama_barang = $r3->nama_barang;
					$tanggal_action = $r3->tanggal_action;
					$selisih_input = $r3->selisih_input;
					if($id_status == 1) {
						if($nomor_resi == "") {
							//1x24 jam ke seller
							if($selisih_input >= 24) {
								$cek_alert = $this->m_crontab->cek_alert(3,$id_detail_transaksi,$tanggal_action);
								if(!$cek_alert) {
									echo "Alert tidak ada di db <br>";
									$deskripsi = "Mohon input resi pemesanan ".$nama_barang." karena lebih dari 24 jam";
									$cek_input_alert = $this->m_crontab->input_alert(3,$id_detail_transaksi,$tanggal_action,$deskripsi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										//foreach ($cek_input_alert->result() as $c) { 
										//	$id_alert = $c->id_alert;
										//}
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										//alert
										$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
										$this->send_to_app($id_toko,"Mohon Input Resi",$deskripsi,$id_detail_transaksi,"detail_transaksi");
									}
								}
							}
							//2x24 jam ke admin
							if($selisih_input >= 48) {
								$cek_alert = $this->m_crontab->cek_alert(6,$id_detail_transaksi,$tanggal_action);
								if(!$cek_alert) {
									echo "Alert tidak ada di db <br>";
									$deskripsi = "Mohon input resi pemesanan ".$nama_barang." karena lebih dari 48 jam";
									$cek_input_alert = $this->m_crontab->input_alert(6,$id_detail_transaksi,$tanggal_action,$deskripsi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										foreach ($cek_input_alert->result() as $c) { 
											$id_alert = $c->id_alert;
										}
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
										//alert
										//$this->send_to_app($id_toko,'Mohon Input Resi',$deskripsi,$id_detail_transaksi,6);
									}
								}
							}
							$resi_not_all = 1;
						}
					}
				}
			}
			if(!$resi_not_all) {
				$this->m_transaksi->edit($id_transaksi,'3');
			}
			$resi_not_all = 0;
		}
		
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'update status dan resi');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	//alert untuk barang listing > 5 hari dan transaksi > 5 hari
	public function update_stok() {
		$begin_time = microtime(true);
		//alert
		//cari barang yang sudah listing > 5 hari no need
		//input di db
		//cari barang yang sudah transaksi > 5 hari
		$record = $this->m_barang->search_barang_tanggal_transaksi(5);
		foreach ($record->result() as $r) {
			$id_barang = $r->id_barang;
			$tanggal = $r->tanggal;
			$nama_barang = $r->nama_barang;
			$id_toko = $r->id_toko;
			$record2 = $this->m_crontab->cek_alert(4,$id_barang,$tanggal);
			if(!$record2) {
				echo "Alert tidak ada di db <br>";
				$deskripsi = $nama_barang." tidak ada transaksi lebih dari 5 hari mohon update stok";
				$cek_input_alert = $this->m_crontab->input_alert(4,$id_barang,$tanggal,$deskripsi);
				if(!$cek_input_alert) {
					echo "Alert gagal diinput<br>";
				}
				else {
					foreach ($cek_input_alert->result() as $c) { 
						$id_alert = $c->id_alert;
					}
					//input notifikasi
					//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
					//$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
					//input update stok
					//$record3 = $this->m_transaksi->show_ecommerce();
					//foreach ($record3->result() as $z) {
					//	$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','1');
					//}		
					//alert
					$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
					$this->send_to_app($id_toko,'Tidak ada transaksi > 5 Hari',$deskripsi,$id_barang,"barang");
				}
			}
			else {
				echo "Alert ada di db 5 hari<br>";
				//cek 6 hari
				$record_6_hari = $this->m_barang->search_barang_tanggal_transaksi_2($id_barang,6);
				if($record_6_hari) {
					echo "Sudah 6 hari <br>";
					$record_6_hari2 = $this->m_crontab->cek_alert(7,$id_barang,$tanggal);
					if(!$record_6_hari2) {
						echo "Alert tidak ada di db 6 hari<br>";
						$deskripsi = $nama_barang." tidak ada transaksi lebih dari 6 hari mohon update stok";
						$cek_input_alert = $this->m_crontab->input_alert(7,$id_barang,$tanggal,$deskripsi);
						if(!$cek_input_alert) {
							echo "Alert gagal diinput<br>";
						}
						else {
							foreach ($cek_input_alert->result() as $c) { 
								$id_alert = $c->id_alert;
							}
							//input notifikasi
							//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
							//$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
							//input update stok
							//$record3 = $this->m_transaksi->show_ecommerce();
							//foreach ($record3->result() as $z) {
							//	$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','1');
							//}		
							//alert
							$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
							$this->send_to_app($id_toko,'Tidak ada transaksi > 6 Hari',$deskripsi,$id_barang,"barang");
						}
					}
					else {
						//cek 7 hari
						echo "Alert ada di db 6 hari<br>";
						$record_7_hari = $this->m_barang->search_barang_tanggal_transaksi_2($id_barang,7);
						if($record_7_hari) {
							$record_7_hari2 = $this->m_crontab->cek_alert(8,$id_barang,$tanggal);
							if(!$record_7_hari2) {
								echo "Alert tidak ada di db 7 hari<br>";
								$deskripsi = $nama_barang." tidak ada transaksi lebih dari 7 hari mohon update stok";
								$cek_input_alert = $this->m_crontab->input_alert(8,$id_barang,$tanggal,$deskripsi);
								if(!$cek_input_alert) {
									echo "Alert gagal diinput<br>";
								}
								else {
									foreach ($cek_input_alert->result() as $c) { 
										$id_alert = $c->id_alert;
									}
									//input notifikasi
									//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
									$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
									//input update stok
									$this->m_transaksi->update_stok_as_is(0,$id_barang);
									$record3 = $this->m_transaksi->show_ecommerce();
									foreach ($record3->result() as $z) {
										$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','1');
									}		
									//alert
									//$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
									//$this->send_to_app($id_toko,'Tidak ada transaksi > 6 Hari',$deskripsi,$id_barang,"barang");
								}
							}
						}
					}
				}
				else{
					echo "bla bla<br>";
				}
			}
		}
		//input di db
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'update stok');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	public function test_notifikasi() {
		$id_alert = 1;
		$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
	}
	
	//penentuan update detail toko
	public function update_detail_toko() {
		//update last update barang
		$this->m_crontab->update_last_update_barang();
		//update jumlah transaksi
		$this->m_crontab->update_jumlah_transaksi();
		//update jumlah listing
		$this->m_crontab->update_jumlah_listing();
	}
	
	//penentuan rank seller
	public function rank_seller() {
		//update rank
		$this->m_crontab->update_rank();
	}
	
	//send notifikasi telegram
	public function send_notifikasi_telegram($message) {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot479784616:AAFWWgfOwY7Lksc_T2WqVAjcNnSSv-UiUu0/sendMessage");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "chat_id=-264510550&text=".$message."");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
	}
}
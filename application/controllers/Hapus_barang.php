<?php
class Hapus_barang extends CI_Controller {

    function __construct() {
        parent::__construct();
        //session_start();
		$this->load->model('m_barang');   
		check_session();
    }
    function index() {
        $data['record'] = $this->m_barang->show_hapus_barang_delete();
		$data['status_dash'] = "delete";
		$data['total_delete'] = $this->m_barang->total_delete();
		$data['total_checklist'] = $this->m_barang->total_delete_checklist();
		$data['total_deleted'] = $this->m_barang->total_deleted();
        $this->template->load('template','barang/hapus_barang',$data);
    }
	
	function show_delete() {
        $data['record'] = $this->m_barang->show_hapus_barang_delete();
		$data['status_dash'] = "delete";
		$data['total_delete'] = $this->m_barang->total_delete();
		$data['total_checklist'] = $this->m_barang->total_delete_checklist();
		$data['total_deleted'] = $this->m_barang->total_deleted();
        $this->template->load('template','barang/hapus_barang',$data);
    }
	
	function show_checklist() {
        $data['record'] = $this->m_barang->show_hapus_barang_checklist();
		$data['status_dash'] = "checklist";
		$data['total_delete'] = $this->m_barang->total_delete();
		$data['total_checklist'] = $this->m_barang->total_delete_checklist();
		$data['total_deleted'] = $this->m_barang->total_deleted();
        $this->template->load('template','barang/hapus_barang',$data);
    }
	
	function show_deleted() {
        $data['record'] = $this->m_barang->show_hapus_barang_deleted();
		$data['status_dash'] = "deleted";
		$data['total_delete'] = $this->m_barang->total_delete();
		$data['total_checklist'] = $this->m_barang->total_delete_checklist();
		$data['total_deleted'] = $this->m_barang->total_deleted();
        $this->template->load('template','barang/hapus_barang',$data);
    }
	
	//function
	//proses data dari page hapus barang
	public function proses_hapus($id_barang = '',$id_ecommerce = '',$id_log_hapus_barang = '') {
		//echo "test";
		if(isset($_POST['submit'])){
			// proses barang
			$id_barang = $this->input->post('kodeBarang');
			$id_ecommerce = $this->input->post('idEcommerce');
			$url = $this->input->post('url');
			$penyebab = $this->input->post('penyebab');
			
			$this->form_validation->set_rules('url', 'url', 'required|trim');
			$this->form_validation->set_rules('penyebab', 'penyebab', 'required|trim|callback_penyebab_check');

			if ($this->form_validation->run() == FALSE) {
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_upload($id_barang,$id_ecommerce);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				//print_r($data);
				$this->template->load('template','barang/form_proses',$data);
			} 
			else {
				$this->m_barang->edit_url($id_barang,$id_ecommerce,2,$url);
				redirect('upload_barang');
			}
		}
        else{
			if(isset($_POST['proses_url_1']) || isset($_POST['proses_url_2']) || isset($_POST['proses_url_3']) || isset($_POST['proses_url_4'])){
				// proses barang
				if(isset($_POST['proses_url_1'])) $id_ecommerce = 1;
				if(isset($_POST['proses_url_2'])) $id_ecommerce = 2;
				if(isset($_POST['proses_url_3'])) $id_ecommerce = 3;
				if(isset($_POST['proses_url_4'])) $id_ecommerce = 4;
				
				$id_barang = $this->input->post('kodeBarang');
				$id_log_hapus_barang = $this->input->post('idLogHapusBarang');
			
				$this->m_barang->proses_hapus_barang($id_log_hapus_barang,$id_ecommerce,2);
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_hapus_all($id_log_hapus_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				$this->template->load('template','barang/form_proses_hapus_barang',$data);
			}
			elseif(isset($_POST['checklist_url_1']) || isset($_POST['checklist_url_2']) || isset($_POST['checklist_url_3']) || isset($_POST['checklist_url_4'])) {
				if(isset($_POST['checklist_url_1'])) $id_ecommerce = 1;
				if(isset($_POST['checklist_url_2'])) $id_ecommerce = 2;
				if(isset($_POST['checklist_url_3'])) $id_ecommerce = 3;
				if(isset($_POST['checklist_url_4'])) $id_ecommerce = 4;
				
				$id_barang = $this->input->post('kodeBarang');
				$id_log_hapus_barang = $this->input->post('idLogHapusBarang');
				
				$this->m_barang->proses_hapus_barang($id_log_hapus_barang,$id_ecommerce,3);
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_hapus_all($id_log_hapus_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				$this->template->load('template','barang/form_proses_hapus_barang',$data);
			}
			else {
				//$id=  $this->uri->segment(3);
				$data['record'] = $this->m_barang->show_one($id_barang);
				$data['status'] = $this->m_barang->show_status_hapus_all($id_log_hapus_barang);
				$data['foto_tambahan'] =  $this->m_barang->show_foto_tambahan($id_barang);
				//print_r($data);
				$this->template->load('template','barang/form_proses_hapus_barang',$data);
			}
        }
	}
	
	//proses untuk cheklist modal
	public function modal() {
		$id_barang = $_POST["id_barang"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$id_log_hapus_barang = $_POST["id_log_hapus_barang"];
		$record = $this->m_barang->show_one($id_barang);
        $status = $this->m_barang->show_status_hapus($id_log_hapus_barang,$id_ecommerce);
		$foto = $this->m_barang->show_foto_tambahan($id_barang);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama_barang"] = $r->nama_barang;
			$return["nama_toko"] = $r->nama_toko;
			$return["id_barang"] = $r->id_barang;
			$return["stok"] = $r->stok;
			$return["deskripsi"] = "Penjual : ".$r->nama_toko." ";
			$return["deskripsi"] .= "Dikirim dari : ".$r->lokasi." ";
			if($r->merk != "")
				$return["deskripsi"] .= "Merk : ".$r->merk." ";
			if($r->bahan != "")
				$return["deskripsi"] .= "Bahan : ".$r->bahan." ";
			if($r->volume != "0x0x0")
				$return["deskripsi"] .= "Volume : ".$r->volume." ";
			$return["deskripsi"] .= $r->deskripsi;
			//foto
			$link_foto = "<a href=\"".$r->foto."\" download=\"".$r->foto."\">
							<img src=\"".$r->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$link_foto = preg_replace('/"([^"]+)"\s*:\s*/', '$1:',$link_foto);
			$return["foto"] = $link_foto;
			$return["waktu_upload"] = $r->tanggal_upload;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["harga_markup"] = $r->harga_markup;
			$return["lokasi"] = $r->lokasi;
		}
		foreach ($status->result() as $s) { 
			$return["id_status"] = $s->id_status;
			$return["status_barang"] = $s->status_hapus_barang;
			$return["id_ecommerce"] = $s->id_ecommerce;
			$return["nama_ecommerce"] = $s->nama;
			$return["url"] = $s->url;
		}
		$list = array();
		foreach ($foto->result() as $f) {  
			$link_foto_tambahan = "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
							<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$list[] = $link_foto_tambahan;
		}
		$return["foto_tambahan"] = $list;
		$return["id_log_hapus_barang"] = $id_log_hapus_barang;
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function modal_checklist() {
		$id_barang = $_POST["id_barang"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$id_log_hapus_barang = $_POST["id_log_hapus_barang"];
		$record = $this->m_barang->proses_hapus_barang($id_log_hapus_barang,$id_ecommerce,3);
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//proses untuk preview
	public function preview() {
		$id_barang = $_POST["id_barang"];
		$record = $this->m_barang->show_one($id_barang);
        $status = $this->m_barang->show_status_upload_all($id_barang);
		$foto = $this->m_barang->show_foto_tambahan($id_barang);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama_barang"] = $r->nama_barang;
			$return["nama_toko"] = $r->nama_toko;
			$return["id_barang"] = $r->id_barang;
			$return["stok"] = $r->stok;
			$return["deskripsi"] = "Penjual : ".$r->nama_toko." ";
			$return["deskripsi"] .= "Dikirim dari : ".$r->lokasi." ";
			if($r->merk != "")
				$return["deskripsi"] .= "Merk : ".$r->merk." ";
			if($r->bahan != "")
				$return["deskripsi"] .= "Bahan : ".$r->bahan." ";
			if($r->volume != "0x0x0")
				$return["deskripsi"] .= "Volume : ".$r->volume." ";
			$return["deskripsi"] .= $r->deskripsi;
			//foto
			$link_foto = "<a href=\"".$r->foto."\" download=\"".$r->foto."\">
							<img src=\"".$r->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$link_foto = preg_replace('/"([^"]+)"\s*:\s*/', '$1:',$link_foto);
			$return["foto"] = $link_foto;
			$return["waktu_upload"] = $r->tanggal_upload;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["harga_markup"] = $r->harga_markup;
			$return["lokasi"] = $r->lokasi;
		}
		$url_list = array();
		foreach ($status->result() as $s) { 
			$return["id_status"] = $s->id_status;
			$return["status_barang"] = $s->status;
			$return["id_ecommerce"] = $s->id_ecommerce;
			$return["nama_ecommerce"] = $s->nama;
			$return["url"] = $s->url;
			$url_list[] = $s->url;
		}
		$list = array();
		foreach ($foto->result() as $f) {  
			$link_foto_tambahan = "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
							<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
						  </a>";
			$list[] = $link_foto_tambahan;
		}
		$return["url_list"] = $url_list;
		$return["foto_tambahan"] = $list;
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
}	
?>
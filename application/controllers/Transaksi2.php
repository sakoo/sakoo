<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		$this->load->model('m_transaksi');
		$this->load->model('m_barang');
		$this->load->model('m_crontab');
		check_session();
    }
	
	public function index() {
        $data['record'] = $this->m_transaksi->show_status_transaksi();
        $this->template->load('template','transaksi/transaksi',$data);
    }
	
	public function tambah($id_transaksi = '') {
		$id_user = $this->session->userdata('u_id');
		if($id_transaksi != ''){
			$data['record'] = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);
			$data['seller'] = $this->m_transaksi->show_seller($id_transaksi);
			$this->template->load('template','transaksi/form_proses_tambah',$data);
		}
		else {
			//hapus data user yang lama set status complete 2, cari transaksi dengan complete 0 dan user id yang sesuai
			$data['record'] = "";
			$data['seller'] = $this->m_transaksi->show_seller($id_transaksi);
			$this->m_transaksi->delete_transaksi($id_user);
			$this->template->load('template','transaksi/form_proses_tambah',$data);
		}
        
    }
	
	//proses konfirmasi transaksi
	public function proses_konfirmasi($id_transaksi) {
		if(isset($_POST['proses'])){
            // proses transaksi
            $id_transaksi = $this->input->post('idTransaksi');
            $this->m_transaksi->edit($id_transaksi,2);
            redirect('transaksi');
        }
		elseif(isset($_POST['lanjut'])){
            // proses transaksi
            $id_transaksi = $this->input->post('idTransaksi');
            $this->m_transaksi->edit($id_transaksi,1);
            redirect('transaksi');
        }
		elseif(isset($_POST['tolak'])) {
			$id_transaksi = $this->input->post('idTransaksi');
            $this->m_transaksi->edit($id_transaksi,5);

			//update stok
			$record = $this->m_transaksi->show_detail_transaksi($id_transaksi);
			foreach ($record->result() as $r) { 
				//cek juga status konfirmasi karena bisa juga ada status konfirmasi yang proses
				$update_stok = $r->jumlah_pembelian;
				$id_barang = $r->id_barang;
				$status_konfirmasi = $r->status_konfirmasi;
				if($status_konfirmasi != 2) {
					$this->m_transaksi->update_stok($update_stok,$id_barang);
					//update stok
					$record2 = $this->m_transaksi->show_ecommerce();
					foreach ($record2->result() as $z) {
						$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','5');
					}
				}
			}
            redirect('transaksi');
		}
        else{
            $data['record'] = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);
            $data['seller'] = $this->m_transaksi->show_seller($id_transaksi);
            $this->template->load('template','transaksi/form_proses',$data);
        }
	}
	
	//proses konfirmasi transaksi
	public function proses_input_resi() {
		if(isset($_POST['proses'])){
            // proses transaksi
            $id_transaksi = $this->input->post('idTransaksi');
            $this->m_transaksi->edit($id_transaksi,3);
            redirect('transaksi');
        }
	}
	
	//proses untuk input resi modal
	public function modal_input() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["id_transaksi"] = $r->id_transaksi;
			$return["buyer"] = $r->buyer;
			$return["nama_ecommerce"] = $r->nama_ecommerce;
			$return["alamat_pengiriman"] = $r->alamat_pengiriman;
		}
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//proses untuk input resi modal
	public function input_resi($id_transaksi,$id_ecommerce) {
		$data['record'] = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);
		$data['resi'] = $this->m_transaksi->show_resi_detail_transaksi($id_transaksi);
		$this->template->load('template','transaksi/input_resi',$data);
	}
	
	public function modal_input_ok() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$no_resi = $_POST["no_resi"];
        if ($no_resi == "") {
			header('HTTP/1.1 500 Internal Server Booboo');
			header('Content-Type: application/json; charset=UTF-8');
        } 
		else {
			$record = $this->m_transaksi->edit_no_resi($id_transaksi,$no_resi);
		}
	}
	
	//proses input resi ok
	public function modal_input_resi_ok() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$id_detail_transaksi = json_decode($_POST["id_detail_transaksi"]);
		$nomor_resi = $_POST["nomor_resi"];
		//$status_transaksi = $_POST["status_transaksi"];
        if ($nomor_resi == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			foreach($id_detail_transaksi as $d) {
				$record = $this->m_transaksi->edit_no_resi2($id_transaksi,$d,$nomor_resi);
			}
		}
		/*
		else {
			$record = $this->m_transaksi->edit_no_resi2($id_transaksi,$id_detail_transaksi,$nomor_resi,$status_transaksi);
			//update stok
			$record2 = $this->m_transaksi->show_ecommerce();
			foreach ($record2->result() as $z) {
				$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
			}
		}
		*/
	}
	
	//proses input resi ok
	public function modal_edit_resi_ok() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$id_detail_transaksi = json_decode($_POST["id_detail_transaksi"]);
		$nomor_resi = $_POST["nomor_resi"];
		//$status_transaksi = $_POST["status_transaksi"];
		$data = $this->m_transaksi->empty_resi($nomor_resi);
        if ($nomor_resi == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			foreach($id_detail_transaksi as $d) {
				$record = $this->m_transaksi->edit_no_resi2($id_transaksi,$d,$nomor_resi);
			}
		}
		/*
		else {
			$record = $this->m_transaksi->edit_no_resi2($id_transaksi,$id_detail_transaksi,$nomor_resi,$status_transaksi);
			//update stok
			$record2 = $this->m_transaksi->show_ecommerce();
			foreach ($record2->result() as $z) {
				$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
			}
		}
		*/
	}
	
	//proses untuk input resi modal
	public function modal() {
		$id_transaksi = $_POST["id_transaksi"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$record = $this->m_transaksi->show_edit_barang2($id_transaksi,$id_ecommerce);
		$return = array();
		
		//parameter 
		foreach ($record->result() as $r) { 
			$temp_return = array();
			$temp_return['nama'] = $r->nama;
			$temp_return['id_barang'] = $r->id;
			$temp_return['id_detail_transaksi'] = $r->id_detail_transaksi;
			$temp_return['nama_toko'] = $r->nama_toko;
			$temp_return['harga_satuan'] = $r->harga_satuan;
			$temp_return['stok_ecommerce'] = $r->stok_ecommerce;
			$temp_return['jumlah_pembelian'] = $r->jumlah_pembelian;
			$temp_return['harga_pembelian'] = $r->harga_pembelian;
			$temp_return['url'] = $r->url;
			$temp_return['keterangan'] = $r->keterangan;
			$temp_return['nomor_resi'] = $r->nomor_resi;
			$temp_return['harga_satuan_ecommerce'] = $r->harga_satuan_ecommerce;
			$temp_return['harga_pembelian_ecommerce'] = $r->harga_pembelian_ecommerce;
			$temp_return['harga_total'] = $r->harga_total;
			$temp_return['harga_total_ecommerce'] = $r->harga_total_ecommerce;
			$temp_return['biaya_kirim_seller'] = $r->biaya_kirim_seller;
			$return[] = $temp_return;
		}
		echo json_encode($return);
	}
	
	//proses untuk input resi modal
	public function modal2() {
		$return = array();
		$temp_return = array();
		$biaya_kirim_total_barang = "";
		$url = "";  
		$id_transaksi = $_POST["id_transaksi"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$record = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);
		foreach ($record->result() as $r) {
			$temp_return['biaya_kirim'] = $r->biaya_kirim;
			$temp_return['harga_total'] = $r->harga_total;
			$temp_return['harga_total_barang'] = $r->harga_total_barang;
			$temp_return['harga_pembayaran'] = $r->harga_pembayaran;
			$temp_return['harga_pembayaran_barang'] = $r->harga_pembayaran_barang;
		}
		//seller
        $seller = $this->m_transaksi->show_seller($id_transaksi);
		//array per seller
		$per_seller = array();
		foreach ($seller->result() as $s) { 
			//$per_seller[] = array('nama_toko' => $s->nama,'biaya_kirim_seller' => $s->biaya_kirim_seller); 
			//$temp_return['per_seller'][]['nama_toko'] = $s->nama;   
			//$temp_return['per_seller'][]['biaya_kirim_seller'] = $s->biaya_kirim_seller;
			$id_toko = $s->id_toko;
			//$nama_toko = $s->nama;
			$id_transaksi_seller = $s->id;
			$biaya_kirim_seller = $s->biaya_kirim_seller;
			$biaya_kirim_total_barang += $biaya_kirim_seller;
			
			$eksekusi = $this->m_transaksi->show_detail_transaksi_seler($id_transaksi,$id_toko);
			$detail_array = array();
			foreach ($eksekusi->result_array() as $data) {
				$id_barang = $data['id_barang'];
				$id_detail_transaksi = $data['id_detail_transaksi'];
				//$temp_return['per_seller']['detail'][]['nomor_resi'] = $data['nomor_resi'];
				$status_konfirmasi = $data['status_konfirmasi'];
				$status_konfirmasi_txt = "";
				if ($status_konfirmasi == 0) {
					$status_konfirmasi_txt = "Belum Konfirmasi";
					$belum_konfirmasi = 1;
				}
				elseif($status_konfirmasi == 1) {
					$status_konfirmasi_txt = "Proses";
				}
				elseif($status_konfirmasi == 2) {
					$status_konfirmasi_txt = "Tolak";
				}
				//$temp_return['per_seller']['detail'][]['status_konfirmasi'] = $status_konfirmasi_txt;
				$eksekusi = $this->m_transaksi->show_url($id_barang,$id_ecommerce);
				foreach ($eksekusi->result_array() as $data2) {
					$url = $data2['url'];
					$nama = $data2['nama'];
				}
				/*
				$temp_return['per_seller']['detail'][]['url'] = $url;
				$temp_return['per_seller']['detail'][]['nama_barang'] = $data['nama'];
				$temp_return['per_seller']['detail'][]['harga_satuan'] = $data['harga_satuan'];
				$temp_return['per_seller']['detail'][]['harga_satuan_ecommerce'] = $data['harga_satuan_ecommerce'];
				$temp_return['per_seller']['detail'][]['jumlah_pembelian'] = $data['jumlah_pembelian'];
				$temp_return['per_seller']['detail'][]['harga_pembelian'] = $data['harga_satuan']*$data['jumlah_pembelian'];
				$temp_return['per_seller']['detail'][]['harga_pembelian_ecommerce'] = $data['harga_satuan_ecommerce']*$data['jumlah_pembelian'];
				*/
				$detail_array[] = array('nomor_resi' => $data['nomor_resi'],'status_konfirmasi' => $status_konfirmasi_txt,'harga_satuan_ecommerce' => $data['harga_satuan_ecommerce'],
					'url' => $url,'nama_barang' => $data['nama'],'harga_satuan' => $data['harga_satuan'],'jumlah_pembelian' => $data['jumlah_pembelian'],
					'harga_pembelian' => $data['harga_satuan']*$data['jumlah_pembelian'],'harga_pembelian_ecommerce' => $data['harga_satuan_ecommerce']*$data['jumlah_pembelian']);
			}
			$per_seller[] = array('nama_toko' => $s->nama,'biaya_kirim_seller' => $s->biaya_kirim_seller,'detail' => $detail_array);
			$temp_return['per_seller'] = $per_seller;
		}
		$temp_return['biaya_kirim_barang'] = $biaya_kirim_total_barang;
		$return = $temp_return;
		echo json_encode($return);
	}
	
	//handler di modal
	public function modal_checklist() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->checked($id_transaksi);
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function modal_completed() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->edit($id_transaksi,4);
		$return = $this->input->post();
		//parameter 
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	//proses untuk add data barang
	public function modal_add_barang() {
		$id_transaksi = $_POST["id_transaksi"];
		$record = $this->m_transaksi->show_satu_status_transaksi($id_transaksi);

		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["id_transaksi"] = $r->id_transaksi;
			$return["buyer"] = $r->buyer;
			$return["nama_ecommerce"] = $r->nama_ecommerce;
			$return["alamat_pengiriman"] = $r->alamat_pengiriman;
			$return["no_resi"] = $r->no_resi;
		}
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function lookup(){
		// process posted form data (the requested items like province)
		$keyword = $this->input->post('name_startsWith');
		$id_ecommerce = $this->input->post('id_ecommerce');
		$data['response'] = 'false'; //Set default response
		$query = $this->m_transaksi->show_barang($keyword,$id_ecommerce); //Search DB
		if( ! empty($query) ) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach( $query->result()  as $row )
			{
				$data['message'][] = array( 
                                        'id'=>$row->id,
                                        'nama' => $row->nama,
										'harga_satuan' => $row->harga_satuan,
										'nama_toko' => $row->nama_toko,
										'url' => $row->url,
										'stok' => $row->stok,
										'id_toko' => $row->id_toko
                                     );
			}
		}
		$coba1[0] = "Hotwel"; 
		$coba1[1] = "Hotwellss"; 
		if('IS_AJAX') {
			//print_r($data);
			echo json_encode($data); //echo json string if ajax request
		}
	}
	
	public function modal_input_add() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$id_barang = $_POST["id_barang"];
		$jumlah = $_POST["jumlah"];
		$harga_satuan = $_POST["satuan"];
		$harga_satuan_ecommerce = $_POST["satuan_ecommerce"];
		$keterangan = $_POST["ket"];
		$id_toko = $_POST["id_toko"];
		$stok = $_POST["stok"];
        if ($id_barang == "" || $jumlah == "" || $harga_satuan_ecommerce == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        } 
		elseif($stok > $jumlah) {
			header('HTTP/1.1 400 understood the request, but request content was invalid.');
			header('Content-Type: application/json; charset=UTF-8');
		}
		else {
			$record = $this->m_transaksi->add_detail_transaksi($id_transaksi,$id_barang,$harga_satuan,$harga_satuan_ecommerce,$jumlah,$keterangan,$id_toko);
			//update stok
			$record2 = $this->m_transaksi->show_ecommerce();
			foreach ($record2->result() as $z) {
				$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
			}
		}
	}
	
	public function cek_ecommerce_before_add_barang() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_ecommerce = $_POST["id_ecommerce"];
		$id_transaksi = $_POST["id_transaksi"];
		
        if ($id_ecommerce == 0) {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
			echo "error";
        }
		else {
			if($id_transaksi != " ") {
				$id_user = $this->session->userdata('u_id');
				$no_transaksi = time();
				$waktu_email = date("Y-m-d H:i:s",$no_transaksi);
				$jasa_pengiriman = "";
				//$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp
				$biaya_kirim = 0;
				$total_harga = 0;
				$total_pembayaran = 0;
				$asuransi = 0;
				$buyer = "";
				$alamat_pengiriman = "";
				$no_hp = "";
				$id_transaksis = $this->m_crontab->new_transaksi($no_transaksi,$no_transaksi,'paid',$waktu_email,$waktu_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','1','0','0',$id_user);
				//return $id_transaksis;
				foreach ($id_transaksis->result() as $id) { 
					$id_transaksi = $id->id_transaksi;
				}
			}
			$return["id_transaksis"] = $id_transaksi;
			echo json_encode($return);
		}
	}
	
	public function modal_input_biaya_kirim_seller() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi_seller = $_POST["id_transaksi_seller"];
		$biaya_kirim = $_POST["jumlah"];
		//biaya_kirim_awal="+biaya_kirim_awal+"&id_transaksi="+id_transaksi
		$biaya_kirim_awal = $_POST["biaya_kirim_awal"];
		$id_transaksi = $_POST["id_transaksi"];
		$id_ecommerce = $_POST["id_ecommerce"];
        if ($biaya_kirim == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			$record = $this->m_transaksi->update_biaya_kirim_per_seller_transaksi($id_transaksi_seller,$biaya_kirim,$id_transaksi,$biaya_kirim_awal,$id_ecommerce);
		}
	}
	
	public function modal_input_pembelian_ecommerce() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$harga_total_awal = $_POST["harga_total_awal"];
		$harga_total_baru = $_POST["harga_total_baru"];
        if ($harga_total_baru == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			$record = $this->m_transaksi->update_harga_total_pembelian_ecommerce($id_transaksi,$harga_total_awal,$harga_total_baru);
		}
	}
	
	public function modal_input_biaya_kirim_ecommerce() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$biaya_kirim_awal = $_POST["biaya_kirim_awal"];
		$biaya_kirim_baru = $_POST["biaya_kirim_baru"];
        if ($biaya_kirim_baru == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			$record = $this->m_transaksi->update_biaya_kirim_ecommerce($id_transaksi,$biaya_kirim_awal,$biaya_kirim_baru);
		}
	}
	
	public function modal_input_harga_pembayaran_ecommerce() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$harga_awal = $_POST["harga_awal"];
		$harga_baru = $_POST["harga_baru"];
        if ($harga_baru == "") {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			$record = $this->m_transaksi->update_harga_pembayaran_ecommerce($id_transaksi,$harga_awal,$harga_baru);
		}
	}
	
	function jasa_pengiriman_check($str) {
		if ($str == '0') {
			$this->form_validation->set_message('jasa_pengiriman_check', 'Pilihan Jasa Pengiriman harap diisi');
			return FALSE;
		}
		else {
			return TRUE;
		}
	}
	
	public function edit_jasa_pengiriman() {
		//$this->form_validation->set_rules('jasaPengiriman', 'jasaPengiriman', 'required|trim|callback_jasa_pengiriman_check');
		$id_transaksi = $_POST["id_transaksi"];
		$jasa_pengiriman = $_POST["jasa_pengiriman"];
        if ($jasa_pengiriman == '0') {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
        }
		else {
			$record = $this->m_transaksi->update_jasa_pengiriman($id_transaksi,$jasa_pengiriman);
		}
	}
	
	public function modal_edit_barang() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_detail_transaksi = $_POST["id_detail_transaksi"];
		$id_ecommerce = $_POST["id_ecommerce"];
        $record = $this->m_transaksi->show_edit_barang($id_detail_transaksi,$id_ecommerce);
		$return = $this->input->post();
		//parameter 
		foreach ($record->result() as $r) { 
			$return["nama"] = $r->nama;
			$return["id_barang"] = $r->id;
			$return["id_detail_transaksi"] = $r->id_detail_transaksi;
			$return["nama_toko"] = $r->nama_toko;
			$return["harga_satuan"] = $r->harga_satuan;
			$return["stok_ecommerce"] = $r->stok_ecommerce;
			$return["jumlah_pembelian"] = $r->jumlah_pembelian;
			$return["harga_pembelian"] = $r->harga_pembelian;
			$return["url"] = $r->url;
			$return["keterangan"] = $r->keterangan;
			$return["id_transaksi_seller"] = $r->id_transaksi_seller; 
			$return["stat_konfirm"] = $r->stat_konfirm;
			$return["harga_satuan_ecommerce"] = $r->harga_satuan_ecommerce;
			$return["harga_pembelian_ecommerce"] = $r->harga_pembelian_ecommerce;
		}
		$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function modal_edit_resi() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$nomor_resi = $_POST["nomor_resi"];
		$id_ecommerce = $_POST["id_ecommerce"];
		$id_transaksi = $_POST["id_transaksi"];
        $record = $this->m_transaksi->show_edit_barang2($id_transaksi,$id_ecommerce);
		$return = array();
		//parameter 
		foreach ($record->result() as $r) { 
			$temp_return = array();
			$temp_return['nama'] = $r->nama;
			$temp_return['id_barang'] = $r->id;
			$temp_return['id_detail_transaksi'] = $r->id_detail_transaksi;
			$temp_return['nama_toko'] = $r->nama_toko;
			$temp_return['harga_satuan'] = $r->harga_satuan;
			$temp_return['stok_ecommerce'] = $r->stok_ecommerce;
			$temp_return['jumlah_pembelian'] = $r->jumlah_pembelian;
			$temp_return['harga_pembelian'] = $r->harga_pembelian;
			$temp_return['url'] = $r->url;
			$temp_return['keterangan'] = $r->keterangan;
			$temp_return['nomor_resi'] = $r->nomor_resi;
			if($nomor_resi == $r->nomor_resi) {
				$temp_return['nomor_resi'] = 1;
			}
			elseif($r->nomor_resi != "") {
				$temp_return['nomor_resi'] = 2;
			}
			else {
				$temp_return['nomor_resi'] = 0;
			}
			$return[] = $temp_return;
		}
		echo json_encode($return);
	}
	
	public function modal_add_resi() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_transaksi = $_POST["id_transaksi"];
		$id_ecommerce = $_POST["id_ecommerce"];
        $record = $this->m_transaksi->show_edit_barang2($id_transaksi,$id_ecommerce);
		$return = array();
		//$temp_return = array();
		//parameter 
		foreach ($record->result() as $r) { 
			if($r->nomor_resi == "")
			{
				$temp_return = array();
				$temp_return['nama'] = $r->nama;
				$temp_return['id_barang'] = $r->id;
				$temp_return['id_detail_transaksi'] = $r->id_detail_transaksi;
				$temp_return['nama_toko'] = $r->nama_toko;
				$temp_return['harga_satuan'] = $r->harga_satuan;
				$temp_return['stok_ecommerce'] = $r->stok_ecommerce;
				$temp_return['jumlah_pembelian'] = $r->jumlah_pembelian;
				$temp_return['harga_pembelian'] = $r->harga_pembelian;
				$temp_return['url'] = $r->url;
				$temp_return['keterangan'] = $r->keterangan;
				$return[] = $temp_return;
			}
		}
		//echo "sate";
		//print_r($return);
		//echo "sate";
		//$return["json"] = json_encode($return);
		echo json_encode($return);
	}
	
	public function modal_input_edit() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_detail_transaksi = $_POST["id_detail_transaksi"];
		$id_transaksi = $_POST["id_transaksi"];
		$id_barang = $_POST["id_barang"];
		$harga_satuan = $_POST["harga_satuan"];
		$harga_satuan_ecommerce = $_POST["harga_satuan_ecommerce"];
		$jumlah = $_POST["jumlah"];
		$jumlah_awal = $_POST["jumlah_awal"];
		$keterangan = $_POST["ket"];
		$stok = $_POST["stok"];
		$updated_stok = $stok;
		$stat_konfirm = $_POST["stat_konfirm"];
		$stat_konfirm_awal = $_POST["stat_konfirm_awal"];
		$var_harga_total = $jumlah * $harga_satuan;
		$var_harga_total_ecommerce = $jumlah * $harga_satuan_ecommerce;
		//cek jumlah
		if ($jumlah == "" || $jumlah < 0) {
			header('HTTP/1.1 500 Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
		} 
		else {
			//echo "Stat konfirm : ".$stat_konfirm."<br>";
			if($stat_konfirm != $stat_konfirm_awal) {
				if(($stat_konfirm_awal == 0 || $stat_konfirm_awal == 1) && $stat_konfirm == 2) {
					//awal proses/belum konfirm akhir tolak
					$updated_stok = $stok + $jumlah_awal;
					$var_harga_total = $jumlah * $harga_satuan;
					$status = "mengurangi";
					$record = $this->m_transaksi->update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
					//update stok
					$record2 = $this->m_transaksi->show_ecommerce();
					foreach ($record2->result() as $z) {
						$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
					}
				}
				elseif($stat_konfirm_awal == 2 && ($stat_konfirm == 0 || $stat_konfirm == 1)) {
					//awal tolak akhir proses/belum konfirm
					$updated_stok = $stok - $jumlah_awal;
					$var_harga_total = $jumlah * $harga_satuan;
					$status = "menambah";
					$record = $this->m_transaksi->update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
					//update stok
					$record2 = $this->m_transaksi->show_ecommerce();
					foreach ($record2->result() as $z) {
						$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
					}
					if($jumlah != $jumlah_awal) {
						if($jumlah_awal > $jumlah) {
							$selisih = $jumlah_awal - $jumlah;
							//stok di barang
							$updated_stok = $updated_stok + $selisih;
							//harga total di transaksi
							$var_harga_total = $selisih * $harga_satuan;
							$status = "mengurangi";
							//selisih yang dikirim sebagai parameter pengurang
							//$query = "UPDATE detail_transaksi SET jumlah_pembelian = '".$jumlah."', keterangan = '".$keterangan."', id_status = ".$stat_konfirm." WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
							//echo $query;
							$record = $this->m_transaksi->update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
							//update stok
							$record2 = $this->m_transaksi->show_ecommerce();
							foreach ($record2->result() as $z) {
								$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
							}
						}
						elseif($jumlah_awal < $jumlah) {
							$selisih = $jumlah - $jumlah_awal;
							if($stok >= $selisih) {
								//stok di barang
								$updated_stok = $updated_stok - $selisih;
								//harga total di transaksi
								$var_harga_total = $selisih * $harga_satuan;
								$status = "menambah";
								//selisih yang dikirim sebagai parameter penambah
								//$query = "UPDATE detail_transaksi SET jumlah_pembelian = '".$jumlah."', keterangan = '".$keterangan."', id_status = ".$stat_konfirm." WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
								//echo $query;
								$record = $this->m_transaksi->update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
								//update stok
								$record2 = $this->m_transaksi->show_ecommerce();
								foreach ($record2->result() as $z) {
									$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
								}
							}
							else {
								header('HTTP/1.1 400 understood the request, but request content was invalid.');
								header('Content-Type: application/json; charset=UTF-8');
							}
						}
					}
				}
				else {
					$record = $this->m_transaksi->add_detail_transaksi($id_transaksi,$id_barang,$harga_satuan,$harga_satuan_ecommerce,$jumlah,$keterangan,$id_toko);
					$record = $this->m_transaksi->simple_update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
				}
			}
			elseif(($stat_konfirm == $stat_konfirm_awal))
			{
				//status sama
				if($stat_konfirm != 2) {
					//status tolak
					//cek jumlah dan stok
					if($jumlah != $jumlah_awal) {
						//jumlah awal 5 jumlah 4
						if($jumlah_awal > $jumlah) {
							$selisih = $jumlah_awal - $jumlah;
							//stok di barang
							$updated_stok = $stok + $selisih;
							//harga total di transaksi
							$var_harga_total = $selisih * $harga_satuan;
							$status = "mengurangi";
							//selisih yang dikirim sebagai parameter pengurang
							//$query = "UPDATE detail_transaksi SET jumlah_pembelian = '".$jumlah."', keterangan = '".$keterangan."', id_status = ".$stat_konfirm." WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
							//echo $query;
							$record = $this->m_transaksi->update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
							//update stok
							$record2 = $this->m_transaksi->show_ecommerce();
							foreach ($record2->result() as $z) {
								$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
							}
						}
						elseif($jumlah_awal < $jumlah) {
							$selisih = $jumlah - $jumlah_awal;
							if($stok >= $selisih) {
								//stok di barang
								$updated_stok = $stok - $selisih;
								//harga total di transaksi
								$var_harga_total = $selisih * $harga_satuan;
								$status = "menambah";
								//selisih yang dikirim sebagai parameter penambah
								//$query = "UPDATE detail_transaksi SET jumlah_pembelian = '".$jumlah."', keterangan = '".$keterangan."', id_status = ".$stat_konfirm." WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
								//echo $query;
								$record = $this->m_transaksi->update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
								//update stok
								$record2 = $this->m_transaksi->show_ecommerce();
								foreach ($record2->result() as $z) {
									$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
								}
							}
							else {
								header('HTTP/1.1 400 understood the request, but request content was invalid.');
								header('Content-Type: application/json; charset=UTF-8');
							}
						}
					}	
					else {
						$status = "";
						$record = $this->m_transaksi->simple_update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm);
					}
				}
			}
		}
	}
	
	public function modal_input_delete() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$id_detail_transaksi = $_POST["id_detail_transaksi"];
		$id_transaksi = $_POST["id_transaksi"];
		$id_barang = $_POST["id_barang"];
		$harga_satuan = $_POST["harga_satuan"];
		$jumlah = $_POST["jumlah"];
		$var_harga_total = $harga_satuan * $jumlah;
		$stok = $_POST["stok"];
		$id_transaksi_seller = $_POST["id_transaksi_seller"];
		$updated_stok = $stok + $jumlah;
		$jumlah_transaksi_seller = 0;
		//update stok dan pembayaran		
		$record = $this->m_transaksi->delete_detail_transaksi($id_detail_transaksi,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$$id_transaksi_seller);
		//cek transaksi seller
		$cek_jumlah = $this->m_transaksi->cek_transaksi_seller($id_transaksi_seller);
		foreach ($cek_jumlah->result() as $r) { 
			$jumlah_transaksi_seller = $r->jumlah;
		}
		if($jumlah_transaksi_seller == 0) {
			$delete_record = $this->m_transaksi->delete_transaksi_seller($id_transaksi_seller);
		}
		//update stok
		$record2 = $this->m_transaksi->show_ecommerce();
		foreach ($record2->result() as $z) {
			$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','4');
		}
	}
	
	public function modal_delete_resi() {
		//$this->form_validation->set_rules('no_resi_i', 'no_resi_i', 'required|trim');
		$nomor_resi = $_POST["nomor_resi"];
		//update resi jadi kosong		
		$record = $this->m_transaksi->empty_resi($nomor_resi);
	}
}
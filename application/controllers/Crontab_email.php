<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Crontab_email extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        $this->load->model('m_email');
		$this->load->model('m_crontab');
		$this->load->model('m_barang');
		$this->load->model('m_transaksi');
		date_default_timezone_set("Asia/Bangkok");  
    }
	
	//Tokopedia,Bukalapak

	public function testingmail(){
		$stringHtml = '<div style="background-color:#f7f7f7;width:100%;max-width:600px;margin: 0 auto" align="center"> <div class="brd-box" style="line-height: 20px; font-size: 14px;padding: 20px;margin: 0 10px;box-sizing: border-box;background-color: #ffffff;color: #666666;border: 1px solid #e7e7e7;border-radius: 4px"> <div style="width: 100%;text-align: left; margin-bottom: 20px;padding-bottom: 20px;border-bottom: 1px solid #e7e7e7"> <a href="https://mandrillapp.com/track/click/30259155/www.blanja.com?p=eyJzIjoiMmVNXzR3ODBmcmJmeEstaVNaVmlEZzc4OUljIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3LmJsYW5qYS5jb21cIixcImlkXCI6XCIxOTI1MDI1ZDJhMmE0YTdhODY0YzgzMDc4NmMxM2EzZVwiLFwidXJsX2lkc1wiOltcIjc1OWFmZWU3NjQwM2YzMTdiNmY3OWFhMGUzMTUyNzczNmE3MjE1NDBcIl19In0" target="_blank"><img width="120px" src="http://s2.blanja.com/static/images/public/blanja-logo.png" style="width: 120px" border="0"></a> </div>
		<div style="text-align: left"> <h1 style="font-size: 18px; color: #333333">Hai Toko SaKoO (Satu Toko Online),</h1> <p style="margin: 0">Status pesanan dengan nomor 418355257224858325 <b>telah dibayar</b> oleh pembeli. Mohon segera melakukan pengiriman sebelum tanggal <b>05 Jun 2018, 01:00:24 WIB</b></p>
		</div> <div style="text-align: left; margin: 20px 0 30px 0; border-top: 1px solid #e7e7e7"> <p style="margin: 20px 0; color: #333333;"> <span style="background-color: #E3211D; margin-right: 5px">&nbsp;</span><b>Detil Blanjaan - 29 Mei 2018, 00:57:23 WIB</b> </p> <div style="margin-bottom: 10px;"> <table width="100%"> <tbody><tr style="vertical-align: top; font-weight: bold;"> <td width="50%">Nama Barang</td> <td width="20%" align="right">Jumlah</td> <td width="30%" align="right">Harga</td> </tr> </tbody></table> <div style="margin: 10px 0; padding: 20px 0 0 0; border-top: 1px solid #e7e7e7; border-bottom: 1px solid #e7e7e7;"> <table width="100%"> <tbody><tr style="vertical-align: top;"> <td width="50%"><p style="margin: 0">Baju pria shanghai polos (bhs) sk-1013</p><p style="margin: 10px 0 20px 0">@Rp 110.000</p></td> <td width="20%" align="right">1</td> <td width="30%" align="right">Rp 110.000</td> </tr> </tbody></table> </div> <div style="margin-bottom: 10px; padding-bottom: 10px; border-bottom: 1px solid #e7e7e7;"> <table width="100%"> <tbody><tr style="vertical-align: top;"> <td width="70%" align="right">Blanjaan:</td> <td width="30%" align="right">Rp 110.000</td> </tr> <tr style="vertical-align: top;"> <td width="70%" align="right">Biaya kirim:</td> <td width="30%" align="right">Rp 45.000</td> </tr> <tr style="vertical-align: top;"> <td width="70%" align="right"><b>Total Pembayaran:</b></td> <td width="30%" align="right"><b>Rp 55.000</b></td> </tr> </tbody></table> </div> </div>
		</div> <div style="text-align: left; margin: 20px 0 30px 0;"> <p style="margin: 20px 0; color: #333333;"> <span style="background-color: #E3211D; margin-right: 5px">&nbsp;</span><b>Informasi Pengiriman</b> </p> <div style="padding: 10px; border: 1px solid #e7e7e7; background-color: #f0f0f0;"> <div style="margin: 0"> <b>Jasa Pengiriman</b> <p style="margin: 0">Kurir Standar</p> </div> <div style="margin: 30px 0"> <b>Alamat</b> <p style="margin: 0"> Pani Septian </p> <p style="margin: 0"> Jl. Daan Mogot KM11 Komplek Casa Jardin Blok F2/12 </p> <p style="margin: 0"> Cengkareng, Kota Jakarta Barat, 11710</p> <p style="margin: 0"> DKI Jakarta </p> <p style="margin: 0">Telp: 08998099996</p> </div> <div style="margin: 0px"> <b>Pesan Pembeli</b> <p style="margin: 0">L kalo gk ada M</p> </div> </div>
		</div> <div style="text-align: left; margin-bottom: 20px 0;"> <p style="margin: 0">Silakan cek pesanan dengan <a href="https://mandrillapp.com/track/click/30259155/seller.blanja.com?p=eyJzIjoiSS1KcGlyZ3N2VUhvVHhFa0tKLTBwR3ZQMXBnIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3NlbGxlci5ibGFuamEuY29tXCIsXCJpZFwiOlwiMTkyNTAyNWQyYTJhNGE3YTg2NGM4MzA3ODZjMTNhM2VcIixcInVybF9pZHNcIjpbXCIwNzRmNTRjYzBkNjkyOTFiYThjNDQzOTFjY2FkZjgxYzM3OTIwODIwXCJdfSJ9" target="_blank" style="color: #e3211d; text-decoration: none;">login</a> ke BLANJA.com, jangan lupa untuk memasukkan nomor resi pengiriman (AWB).</p> <p style="margin: 20px 0">Kecepatan pengiriman akan menentukan performa toko kamu.</p> <p style="margin: 0">BLANJA.com tidak bertanggung jawab atas :</p> <ol style="margin: 0; padding-left: 15px;"> <li>Pengiriman pesanan yang belum diverifikasi.</li> <li>Pesanan yang batal otomatis karena keterlambatan input resi.</li> </ol>
		</div> <div style="text-align: left;margin: 20px 0;padding: 20px 0;border-top: 1px solid #e7e7e7; border-bottom: 1px solid #e7e7e7;"> <div style="display: table; width: 100%"> <div style="display: table-cell; width: 50px;vertical-align: middle;text-align: center"><img src="http://s2.blanja.com/static/images/public/email/fill-52.png" alt="" width="30px" style="width: 30px"></div> <div style="display: table-cell;text-align: left;font-size: 12px"> <p style="margin: 0;margin-left: 10px">Harap tidak menginformasikan nomor kontak, e-mail, atau password Anda kepada siapapun dan TIDAK membuka link atau tautan apapun yang mengatasnamakan BLANJA.com karena kami tidak pernah memberikan promo/hadiah apapun tanpa informasi resmi.</p> </div> </div> </div> <div style="margin: 20px 0; border-bottom: 1px solid #e7e7e7"> <div style="width: 258px; margin-top: 0;margin-bottom: 20px;margin-left: auto;margin-right: auto;"> <div style="display: block;"> <p style="margin-bottom: 10px">Download Aplikasi BLANJA.com</p> <div><a href="https://mandrillapp.com/track/click/30259155/play.google.com?p=eyJzIjoiT3VFN2JWRGo3bm1SWHZnZFNwV0dzSzNUbUJRIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL3BsYXkuZ29vZ2xlLmNvbVxcXC9zdG9yZVxcXC9hcHBzXFxcL2RldGFpbHM_aWQ9Y29tLmJsYW5qYS5hcHBzLmFuZHJvaWRcIixcImlkXCI6XCIxOTI1MDI1ZDJhMmE0YTdhODY0YzgzMDc4NmMxM2EzZVwiLFwidXJsX2lkc1wiOltcImFkYTJjMWJmM2VlZjAwMjQ3MDNiMmI1N2YyY2YyNzdhNTYwMjJiNTVcIl19In0" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/google-play.png" alt="Google Play" width="120px" style="width: 120px;margin-right: 10px"></a><a href="https://mandrillapp.com/track/click/30259155/itunes.apple.com?p=eyJzIjoiUzlacEZfT1hJclZvQ0hHRnAwWGxJVk5FT0RBIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwczpcXFwvXFxcL2l0dW5lcy5hcHBsZS5jb21cXFwvaWRcXFwvYXBwXFxcL2JsYW5qYS1qdWFsLWJlbGktb25saW5lXFxcL2lkMTE2NzAyMjA2OD9sPWlkJm10PThcIixcImlkXCI6XCIxOTI1MDI1ZDJhMmE0YTdhODY0YzgzMDc4NmMxM2EzZVwiLFwidXJsX2lkc1wiOltcImRlOTIwMTFiZDk2OWI4NmVkMzRlYmI0MWNlYTk1ZTE1NGRmZGJhNjFcIl19In0" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/apple-store.png" alt="Apple Store" width="120px" style="width: 120px"></a></div> </div> <div style="display: block;"> <p style="margin-bottom: 10px">Ikuti Kami</p> <div><a href="https://mandrillapp.com/track/click/30259155/www.facebook.com?p=eyJzIjoiT3UxZEw2V0RqbWJuZDN5a29sWjl4enVPTWdzIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3LmZhY2Vib29rLmNvbVxcXC9ibGFuamFjb21cIixcImlkXCI6XCIxOTI1MDI1ZDJhMmE0YTdhODY0YzgzMDc4NmMxM2EzZVwiLFwidXJsX2lkc1wiOltcIjFiNDE5NDhjMzlkMjVlZTQ0YmQxOWU5MDA0YzNhYzQ2Nzk4ZDA1MWJcIl19In0" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/facebook.png" alt="Facebook" width="30px" style="margin-right: 10px;width: 30px"></a><a href="https://mandrillapp.com/track/click/30259155/twitter.com?p=eyJzIjoibHlrbENpanVyT2tSYUJ2ZndZcTFoOFVGTERVIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvdHdpdHRlci5jb21cXFwvYmxhbmphY29tXCIsXCJpZFwiOlwiMTkyNTAyNWQyYTJhNGE3YTg2NGM4MzA3ODZjMTNhM2VcIixcInVybF9pZHNcIjpbXCI2MjQyODllYWQ2OTFhYWY5OWVjZWE0MTIzMzc2ZTcyZTkxYzE4Mzg5XCJdfSJ9" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/twitter.png" alt="Twitter" width="30px" style="margin-right: 10px;width: 30px"></a><a href="https://mandrillapp.com/track/click/30259155/www.youtube.com?p=eyJzIjoiU2VaU3FIY2dLZTktOGU2MXF4RHhYLUFyRFJFIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3LnlvdXR1YmUuY29tXFxcL2NoYW5uZWxcXFwvVUNBbmtoY3lUeldpdWp1WU1YZXJydDBnXCIsXCJpZFwiOlwiMTkyNTAyNWQyYTJhNGE3YTg2NGM4MzA3ODZjMTNhM2VcIixcInVybF9pZHNcIjpbXCI4ZjFiMDExN2NhZmE5MWRjNTFkNmI2MjA3Nzc4YzcwYTZlNTU3MThjXCJdfSJ9" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/you-tube.png" alt="Youtube" width="30px" style="margin-right: 10px;width: 30px"></a><a href="https://mandrillapp.com/track/click/30259155/www.instagram.com?p=eyJzIjoiWlJxOTFSZGRSUUxrOVExbmhkQzVZbjQwdmJnIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3Lmluc3RhZ3JhbS5jb21cXFwvYmxhbmphY29tXCIsXCJpZFwiOlwiMTkyNTAyNWQyYTJhNGE3YTg2NGM4MzA3ODZjMTNhM2VcIixcInVybF9pZHNcIjpbXCI5ZjhmNWM1OTMyOGZhOWEyMzZhMWViZDkwMzU4ZTYxZDIxMjM5MmY0XCJdfSJ9" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/instagram.png" alt="Instagram" width="30px" style="margin-right: 10px;width: 30px"></a><a href="https://mandrillapp.com/track/click/30259155/www.linkedin.com?p=eyJzIjoiQjA4dGtvenQyMmdCbzdLNVduUm1OOUJKM1JnIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvd3d3LmxpbmtlZGluLmNvbVxcXC9jb21wYW55XFxcL2JsYW5qYS1jb21cIixcImlkXCI6XCIxOTI1MDI1ZDJhMmE0YTdhODY0YzgzMDc4NmMxM2EzZVwiLFwidXJsX2lkc1wiOltcIjcyMzVkYzdlM2RlZmVjMjMxYjBkYTIwZTQ2ZTdmYjliMWJhZWM1NTRcIl19In0" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/linkedin.png" alt="LinkedIn" width="30px" style="margin-right: 10px;width: 30px"></a><a href="https://mandrillapp.com/track/click/30259155/plus.google.com?p=eyJzIjoiWGJhQUdBLW9JVXNfQXVhU3F3MDNBTlV0ZWFjIiwidiI6MSwicCI6IntcInVcIjozMDI1OTE1NSxcInZcIjoxLFwidXJsXCI6XCJodHRwOlxcXC9cXFwvcGx1cy5nb29nbGUuY29tXFxcLytibGFuamFcIixcImlkXCI6XCIxOTI1MDI1ZDJhMmE0YTdhODY0YzgzMDc4NmMxM2EzZVwiLFwidXJsX2lkc1wiOltcImM2OWQyNTlkMGRkNzcyODQ5M2UxMTMzNDVlZmE0NzY2MGRjODQ4NmRcIl19In0" target="_blank" style="text-decoration: none"><img src="http://s2.blanja.com/static/images/public/email/google-plus.png" alt="Google+" width="30px" style="width: 30px"></a></div> </div> </div> </div> <div style="font-style: italic; font-size: 12px"> <p style="margin: 0">Punya pertanyaan? Jangan khawatir, hubungi kami <a href="mailto:support@blanja.com" style="color: #666666; text-decoration: underline;">support@blanja.com</a></p> <p style="margin: 0">Customer Service : 021 - 8066 7878 | Layanan Live Chat: Senin - Jumat, Pk 08.00 - 17.00 WIB</p> </div> </div> <div style="margin: 10px;color: #999999;font-size: 10px;padding-bottom: 15px"> <p style="margin: 0">Harap jangan membalas e-mail ini, karena e-mail ini dikirimkan secara otomatis oleh sistem.</p> <p style="margin: 15px 0 0 0;font-size: 12px;color: #666666">Copyright 2017 © Metraplasa. All Rights Reserved. | <a href="mailto:support@blanja.com" style="color: #ea5a25">support@blanja.com</a></p> </div> </div>';
		$html = str_get_html($stringHtml);
		$string = $html->plaintext;
		//var_dump($html->find('tr[style=vertical-align:top] td p'));
		$data = $html->find('tr[style=vertical-align: top;] td');
		$nama = $html->find('div[style="margin: 30px 0"] p');
		$buyer = $nama[0]->plaintext;
		echo $buyer;
		echo '</br>';
		$no_transaksi = preg_match("/Status pesanan dengan nomor (.*?) telah dibayar oleh pembeli/", $string, $matches);
		$no_transaksi = $matches[1];
		echo $no_transaksi;
		echo '</br>';
		$alamat_pengiriman_regex = preg_match("/Alamat (.*?) Telp:/", $string, $matches);
		$alamat_pengiriman = $matches[1];
		echo $alamat_pengiriman;
		echo '</br>';
		//telp
		$no_hp = preg_match("/Telp: (.*?) Pesan Pembeli/", $string, $matches);
		$no_hp = $matches[1];
		echo $no_hp;
		echo '</br>';
		//jasapengirim
		$jasa_pengiriman = $html->find('div[style="margin: 0"] p');
		$jasa_pengiriman = $jasa_pengiriman[0]->plaintext;
		echo $jasa_pengiriman;
		echo '</br>';
		//ongkos kirim
		$biaya_kirim = preg_match("/Biaya kirim: (.*?) Total Pembayaran:/", $string, $matches);
		$biaya_kirim = 	$matches[1];
		echo (int)str_replace(' ','',str_replace('Rp ','',str_replace('.','',$biaya_kirim)));
		echo '</br>';
		//keterangan
		$keterangan = preg_match("/Pesan Pembeli (.*?) Silakan cek pesanan dengan login/", $string, $matches);
		$keterangan = $matches[1];
		echo $keterangan;
		echo '</br>';

		//waktupemesanan
		$waktu_pemesanan = preg_match("/Detil Blanjaan - (.*?) WIB/", $string, $matches);
		$raw_waktu_pemesanan = $matches[1];
		$mc = array("Januari"=>"January","Februari"=>"February","Maret"=>"March","April"=>"April","Mei"=>"May","Juni"=>"June","Juli"=>"July","Agustus"=>"August","September"=>"September","Oktober"=>"October","November"=>"November","Desember"=>"December");
		$array_waktu_pemesanan = explode(', ',$raw_waktu_pemesanan);
		$array_tanggal = explode(' ',$array_waktu_pemesanan[0]);
		$array_tanggal[1] = strtolower($mc[$array_tanggal[1]]);
		$array_tanggal = implode('-',$array_tanggal);
		$array_waktu_pemesanan[0] = $array_tanggal;
		$array_waktu_pemesanan = implode(' ',$array_waktu_pemesanan);
		$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($array_waktu_pemesanan));
		echo $waktu_pemesanan;
		echo '</br>';
		$arrayBarang = null;
		$counterArrayBarang = 0;
		$counterMaxArrayBarang = 0;
		foreach($data as $element){
			echo $element->plaintext;
			echo "</br>";
			if($element->plaintext == 'Blanjaan:'){
				break;
			}else{
				if($counterArrayBarang == 0){
					$namaBarang = preg_match("/(.*?) sk-/", $element->plaintext, $matchesNamaBarang);
					$arrayBarang[$counterMaxArrayBarang]['namaBarang'] = $matchesNamaBarang[1];
					$idBarang = preg_match("/sk-(.*?)@Rp/", $element->plaintext, $matchesIdBarang);
					$arrayBarang[$counterMaxArrayBarang]['idBarang'] = $matchesIdBarang[1];
					$arrayBarang[$counterMaxArrayBarang]['hargaBarang'] = $hargaBarang = (int)str_replace('.','',substr($element->plaintext, strpos($element->plaintext, '@Rp ') + 4));
					$counterArrayBarang++;
				}else if($counterArrayBarang == 1){
					$arrayBarang[$counterMaxArrayBarang]['jumlahBarang'] = $element->plaintext;
					$counterArrayBarang++;
				}else if($counterArrayBarang == 2){
					$counterMaxArrayBarang++;
					$counterArrayBarang = 0;
				}	
			}
		}  
		var_dump($arrayBarang);
		echo "</br>"; 
		echo $string;
		 echo $stringHtml;
	}

	public function get_new_unread_email() {
		set_time_limit(3000000); 
		$begin_time = microtime(true);
		//connect to gmail with your credentials
		$hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
		$username = 'sakoofighting2@gmail.com';
		$password = '1tokoonlinebaru';
		//try to connect
		$inbox = imap_open($hostname,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

		//grab emails
		$emails = imap_search($inbox,'UNSEEN');
		
		//variabel yang diambil
		$buyer = "";
		$alamat_pengiriman = "";
		$no_hp = "";
		$no_transaksi = "";
		$barang = "";
		$id_barang = "";
		$jumlah = "";
		$harga = "";
		$biaya_kirim = "";
	    $waktu_pemesanan = "";
		$waktu_email = "";
		$ecommerce = "";
		$jasa_pengiriman = "";
		$total_harga = "";
		$total_harga_asli_barang = "";
		$total_pembayaran = "";
		$total_pembayaran_asli_barang = "";
		$asuransi = "";
		$id_transaksi = "";
		$keterangan = "";
		$keterangan_barang = "";

		//if emails are returned, cycle through each
		if($emails) {
			
			//begin output var
			$output = '';
			
			//put the newest emails on top
			rsort($emails);
			
			//for every email
			foreach($emails as $email_number) {
				//untuk cek multiseller
				$multi_seller = 0;
				$id_transaksi_seller = "";
				
				$overview = imap_fetch_overview($inbox,$email_number,0);
				$structure = imap_fetchstructure($inbox, $email_number);
				//print_r($structure);

				if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
					$part = $structure->parts[1];
					$message = imap_fetchbody($inbox,$email_number,2);

					if($part->encoding == 3) {
						$message = imap_base64($message);
					} else if($part->encoding == 1) {
						$message = imap_8bit($message);
					} else {
						$message = imap_qprint($message);
					}
				}
				elseif(!isset($structure->parts)) {
					$message = imap_fetchbody($inbox,$email_number,1);

					if($structure->encoding == 3) {
						$message = imap_base64($message);
					} else if($structure->encoding == 1) {
						$message = imap_8bit($message);
					} else {
						$message = imap_qprint($message);
					}
				}
				
				$output.= '<div class="toggle'.($overview[0]->seen ? 'read' : 'unread').'">';
				$output.= '<span class="from">From: '.utf8_decode(imap_utf8($overview[0]->from)).'</span>';
				$output.= '<span class="date">on '.utf8_decode(imap_utf8($overview[0]->date)).'</span>';
				if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
					$output.= '<br /><span class="subject">Subject('.$part->encoding.'): '.utf8_decode(imap_utf8($overview[0]->subject)).'</span> ';
				}
				elseif(!isset($structure->parts)) {
					$output.= '<br /><span class="subject">Subject('.$structure->encoding.'): '.utf8_decode(imap_utf8($overview[0]->subject)).'</span> ';
				}
				
				$output.= '</div>';

				if(isset($message)) {
					$output.= '<div class="body">'.$message.'</div><hr />';
					
					//echo "===END OF EMAIL===";
					$from = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->from))));
					$from = str_replace('"','',str_replace('.com','',$from));
					$from = ucfirst(strtolower($from));
					$date = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->date))));
					if(isset($structure->parts) && is_array($structure->parts) && isset($structure->parts[1])) {
						$subject = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->subject))));
					}
					elseif(!isset($structure->parts)) {
						$subject = trim(strip_tags(utf8_decode(imap_utf8($overview[0]->subject))));
					}
					echo $from."---".$date."---".$subject."<br>";
					//cek ecommerce
					$record = $this->m_email->show_all_ecommerce();
					foreach ($record->result() as $r) { 
						$id_ecommerce = $r->id;
						$nama = $r->nama;
						echo "Nama Ecommerce : ".$nama."<br>";
						if($from === $nama) {
							$ecommerce = $id_ecommerce;
							$waktu_email = date('Y-m-d H:i:s', strtotime($date));
							//blanja
							if($ecommerce === "1") {
								echo "wes masukk <br>";
								$get_pesanan_baru = "Pesanan Telah Dibayar";
								//cek email pesanan baru
								if(strpos($subject, $get_pesanan_baru) !== false) {
									//get DOM from URL or file
									$html = str_get_html($message);
									//Find all links
									$string = $html->plaintext;
									echo $string;
									//rock the star
									if(1)
									{
									//buyer
									/* $buyer = preg_match("/Email(?P<buyer>.*)Telepon/", $string, $matches);
									$buyer = strip_tags(trim($matches['buyer'])); */
									$buyer = $html->find('div[style="margin: 30px 0"] p');
									$buyer = $buyer[0]->plaintext;
									/* foreach($nama as $test){
										echo $test->plaintext;
										echo '</br>';
									} */
									//echo $buyer;
									/* $buyer = 'hahahaa'; */
									//no transaksi
									$no_transaksi = preg_match("/Status pesanan dengan nomor (.*?) telah dibayar oleh pembeli/", $string, $matches);
									$no_transaksi = $matches[1];
									//tanggal pemesanan
									//$waktu_pemesanan = preg_match("/Detil Blanjaan - (.*?) WIB Nama/", $string, $matches);
									$waktu_pemesanan = preg_match("/Detil Blanjaan - (.*?) WIB/", $string, $matches);
									$raw_waktu_pemesanan = $matches[1];
									$mc = array("Januari"=>"January","Februari"=>"February","Maret"=>"March","April"=>"April","Mei"=>"May","Juni"=>"June","Juli"=>"July","Agustus"=>"August","September"=>"September","Oktober"=>"October","November"=>"November","Desember"=>"December");
									$array_waktu_pemesanan = explode(', ',$raw_waktu_pemesanan);
									$array_tanggal = explode(' ',$array_waktu_pemesanan[0]);
									$array_tanggal[1] = strtolower($mc[$array_tanggal[1]]);
									$array_tanggal = implode('-',$array_tanggal);
									$array_waktu_pemesanan[0] = $array_tanggal;
									$array_waktu_pemesanan = implode(' ',$array_waktu_pemesanan);
									$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($array_waktu_pemesanan));
									//echo "Waktu pemesanan test : ".$matches['waktu_pemesanan']."<br>";
									//pengiriman
									$jasa_pengiriman = $html->find('div[style="margin: 0"] p');
									$jasa_pengiriman = $jasa_pengiriman[0]->plaintext;
									//asuransi
									$asuransi = "-";
									//alamat
									$alamat_pengiriman_regex = preg_match("/Alamat (.*?) Telp:/", $string, $matches);
									$alamat_pengiriman = $matches[1];
									//telp
									$no_hp = preg_match("/Telp: (.*?) Pesan Pembeli/", $string, $matches);
									$no_hp = $matches[1];
									//ongkos kirim
									$biaya_kirim = preg_match("/Biaya kirim: (.*?) Total Pembayaran:/", $string, $matches);
									$biaya_kirim = 	(int)str_replace(' ','',str_replace('Rp ','',str_replace('.','',$matches[1])));
									//keterangan
									$keterangan = preg_match("/Pesan Pembeli (.*?) Silakan cek pesanan dengan login/", $string, $matches);
									$keterangan = $matches[1];
									
									//temporary
									$total_harga = 0;
									$total_pembayaran = 0;
									$total_harga_asli_barang = 0;
									$total_pembayaran_asli_barang = 0;
									$jumlah_barang = 0;
									
									//`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,`keterangan`,`status_transaksi`,`checked`,`multi_seller`
									$id_transaksis = $this->m_crontab->new_transaksi($no_transaksi,$no_transaksi,'paid',$waktu_pemesanan,$waktu_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','1','0');
									//masih belum fix
										if(!$id_transaksis) {
											echo "Transaksi sudah ada<br>";
										}
										else{
											//notifikasi
											echo "Pesananan baru <br>";
											foreach ($id_transaksis->result() as $id) { 
												$id_transaksi = $id->id_transaksi;
											}
/* 											$id_transaksi = 9613912; */
											
											//call api transaksi
											//if($id_transaksis) {
											//	
											//}
											
											$jumlah_barang = 0;
											$prev_toko = NULL;
											
											//total pembayaran
											$data = $html->find('tr[style=vertical-align: top;] td');
											/* $string_barang_all = preg_match("/Barang yang dipesan(?P<string_barang_all>.*)Biaya Pengiriman/", $string, $matches);
											$string_barang_all = trim($matches['string_barang_all']);
											$string_barang_array = preg_match_all("/.+?(?=,00)/", $string_barang_all, $matches);   */
											/* echo "String barang All : ";
											print_r($string_barang_all); */
											// start from here detail barang
											// get array of detail barang
											$arrayBarang = null;
											$counterArrayBarang = 0;
											$counterMaxArrayBarang = 0;
											foreach($data as $element){
												echo $element->plaintext;
												echo "</br>";
												if($element->plaintext == 'Blanjaan:'){
													break;
												}else{
													if($counterArrayBarang == 0){
														$namaBarang = preg_match("/(.*?) sk-/", $element->plaintext, $matchesNamaBarang);
														$arrayBarang[$counterMaxArrayBarang]['namaBarang'] = $matchesNamaBarang[1];
														$idBarang = preg_match("/sk-(.*?)@Rp/", $element->plaintext, $matchesIdBarang);
														$arrayBarang[$counterMaxArrayBarang]['idBarang'] = $matchesIdBarang[1];
														$arrayBarang[$counterMaxArrayBarang]['hargaBarang'] = $hargaBarang = (int)str_replace('.','',substr($element->plaintext, strpos($element->plaintext, '@Rp ') + 4));
														$counterArrayBarang++;
													}else if($counterArrayBarang == 1){
														$arrayBarang[$counterMaxArrayBarang]['jumlahBarang'] = $element->plaintext;
														$counterArrayBarang++;
													}else if($counterArrayBarang == 2){
														$counterMaxArrayBarang++;
														$counterArrayBarang = 0;
													}	
												}
											}
											/* var_dump('iniiiii siii araaay BARAAANGGG',$arrayBarang);   */
											foreach ($arrayBarang as $string_barang_all) {
												$begin_time_blanja = microtime(true);
													//cek multiseller juga
													$jumlah_barang++;
													//barang
													//jumlah
													$jumlah = $string_barang_all['jumlahBarang'];
													//harga
													$harga = $string_barang_all['hargaBarang'];
													//total harga barang
													$total_harga_barang = $jumlah * $harga;
													$total_harga += $total_harga_barang;
													//keterangan
													$keterangan_barang = "-";
													
													//cek barang dan seller
													//Hotwheels 1 sk_1	 
														$id_barang = $string_barang_all['idBarang'];
														$record = $this->m_barang->show_one($id_barang);
														foreach ($record->result() as $r) { 
															$id_toko = $r->id_toko;
															$nama_toko = $r->nama_toko;
															$nama_barang = $r->nama_barang;
															$harga_satuan_asli_barang = $r->harga_satuan;
														}
														//set prev toko untuk iterasi 1
														if($jumlah_barang == 1) {
															$prev_toko = $id_toko;
														}
														//insert data transaksi seller					
														//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
														echo "id transaksi : ".$id_transaksi."<br>";
														echo "id toko : ".$id_toko."<br>";
														$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
														foreach ($id_transaksi_sellers->result() as $id) { 
															$id_transaksi_seller = $id->id;
														}
														//insert data detail transaksi
														$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
														//update jika multiseller
														if($prev_toko != $id_toko) {
															$this->m_transaksi->edit($id_transaksi,'6');
															$multi_seller = 1;
														}
														$prev_toko = $id_toko;
														
														//update stok 
														//update tanggal otomatis dari trigger
														$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
														$record2 = $this->m_transaksi->show_ecommerce();
														foreach ($record2->result() as $z) {
															if($z->id == $id_ecommerce) {
																$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
															}
															else {
																$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
															}
														}
														//alert
														$deskripsi = "Ada Pembelian ".$nama_barang;
														$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
														$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi","kirim");
														//echo "Parameter: id toko :".$id_toko." deskripsi :".$deskripsi." id transaksi seller :".$id_transaksi_seller;
														//telegram
														$this->send_notifikasi_telegram($deskripsi." (".$nama.")");
														//update harga berdasarkan satuan harga tabel barang
														$harga_pembelian_asli_barang = $harga_satuan_asli_barang * $jumlah;
														$total_harga_asli_barang += $harga_pembelian_asli_barang;
												$end_time_blanja = microtime(true);
												$total_time_blanja = $end_time_blanja - $begin_time_blanja;
												echo "Total waktu script blanja: ".$total_time_blanja."<br>";
											}
											/* end here */
											//alert
											$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
											$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_email,$deskripsi_notifikasi);
											if(!$cek_input_alert) {
												echo "Alert gagal diinput<br>";
											}
											else {
												foreach ($cek_input_alert->result() as $c) { 
													$id_alert = $c->id_alert;
												}
												//input notifikasi
												//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
												$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
											}
										}
											
										//total pembayaran
										echo "Jumlah Barang : ".$jumlah_barang."<br>";
										if($jumlah_barang == 0) {
											$total_pembayaran_asli_barang = $total_harga_asli_barang;
										}
										else {
											$total_pembayaran_asli_barang = $total_harga_asli_barang + $biaya_kirim;
										}
										$total_pembayaran = $total_harga + $biaya_kirim;
										//$total_pembayaran_asli_barang = $total_harga_asli_barang + $biaya_kirim;
										//update total harga dan pembayaran
										$this->m_transaksi->update_harga($id_transaksi,$total_harga,$total_pembayaran,$total_harga_asli_barang,$total_pembayaran_asli_barang);
									}
								}
							}
							//tokopedia
							if($ecommerce === "2") {
								echo "wes masukk <br>";
								$get_pesanan_baru = "Pesanan baru dari";
								//cek email pesanan baru
								if(strpos($subject, $get_pesanan_baru) !== false) {
									//buyer
									$get_buyer = "Pesanan baru dari";
									$buyer_leng = strlen($get_buyer);
									$buyer_pos = strpos($subject, $get_buyer)+$buyer_leng;
									$get_field = "tanggal";
									$field_leng = strlen($get_field);
									$field_pos = strpos($subject,$get_field);
									$buyer = html_entity_decode(substr($subject, $buyer_pos, $field_pos-$buyer_pos), ENT_QUOTES);
									
									//get DOM from URL or file
									$html = str_get_html($message);
									//Find all links 
									$string = $html->plaintext;
									//echo $string;
									//rock the star
									//no transaksi
									$no_transaksi = preg_match("/Nomor Pemesanan:(?P<no_transaksi>.*)Tanggal Pemesanan:/", $string, $matches);
									$no_transaksi = trim($matches['no_transaksi']);
									//tanggal pemesanan
									$waktu_pemesanan = preg_match("/Tanggal Pemesanan:(?P<waktu_pemesanan>.*)Total Pesanan:/", $string, $matches);
									$raw_waktu_pemesanan = trim($matches['waktu_pemesanan']);
									//setting bulan
									$mc = array("januari"=>"January","februari"=>"February","februari"=>"February","maret"=>"March","april"=>"April","mei"=>"May","juni"=>"June","juli"=>"July","agustus"=>"August","september"=>"September","oktober"=>"October","november"=>"November","desember"=>"December");
									$new_date = explode(' ',$raw_waktu_pemesanan);
									$new_date[1] = $mc[strtolower($new_date[1])];
									$new_date = implode('-',$new_date);
									$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($new_date));
									//echo "Waktu pemesanan test : ".$matches['waktu_pemesanan']."<br>";
									//pengiriman
									$jasa_pengiriman = preg_match("/Pengiriman via:(?P<jasa_pengiriman>.*)Asuransi:/", $string, $matches);
									$jasa_pengiriman = trim($matches['jasa_pengiriman']);
									//asuransi
									$asuransi = preg_match("/Asuransi:(?P<asuransi>.*)Rincian Pesanan:/", $string, $matches);
									$asuransi = trim($matches['asuransi']);
									//alamat
									$alamat_pengiriman = preg_match("/Tujuan Pengiriman:(?P<alamat_pengiriman>[\s\S]*)Total Harga Produk:/", $string, $matches);
									$alamat_pengiriman = trim($matches['alamat_pengiriman']);
									//telp
									$no_hp = preg_match("/Telp:(?P<no_hp>.*)Total Harga Produk:/", $string, $matches);
									$no_hp = trim($matches['no_hp']);
									//total harga produk
									$total_harga = preg_match("/Total Harga Produk:(?P<total_harga>.*)Ongkos kirim:/", $string, $matches);
									$total_harga = clean_currency(trim($matches['total_harga']));
									//ongkos kirim
									$biaya_kirim = preg_match("/Ongkos kirim:(?P<biaya_kirim>.*)Total Pembayaran:/", $string, $matches);
									$biaya_kirim = clean_currency(trim($matches['biaya_kirim']));
									//total pembayaran
									$total_pembayaran = preg_match("/Total Pembayaran:(?P<total_pembayaran>.*)Mohon konfirmasi/", $string, $matches);
									$total_pembayaran = clean_currency(trim($matches['total_pembayaran']));
									
									//temporary
									$total_harga_asli_barang = 0;
									$total_pembayaran_asli_barang = 0;
									
									//`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,`keterangan`,`status_transaksi`,`checked`,`multi_seller`
									$id_transaksis = $this->m_crontab->new_transaksi($no_transaksi,$no_transaksi,'paid',$waktu_email,$waktu_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','1','0');
									//masih belum fix
									
									if(!$id_transaksis) {
										echo "Transaksi sudah ada<br>";
									}
									else{
										//notifikasi
										echo "Pesananan baru <br>";
										foreach ($id_transaksis->result() as $id) { 
											$id_transaksi = $id->id_transaksi;
										}
										
										//call api transaksi
										//if($id_transaksis) {
										//	
										//}
										
										$jumlah_barang = 0;
										$prev_toko = NULL;
										foreach($html->find('ol li') as $element) {
											//cek multiseller juga
											$jumlah_barang++;
											$string_barang = $element->plaintext;
											echo "String barang : ".$string_barang."<br>";
											//barang
											$barang = preg_match("/[\s\S]+?(?=Jumlah:)/", $string_barang, $matches);
											$barang = trim($matches[0]);
											$id_barang = "";
											//jumlah
											$jumlah = preg_match("/Jumlah:(?P<jumlah>[\s\S]*)Buah/", $string_barang, $matches);
											$jumlah = trim($matches['jumlah']);
											//harga
											$harga = preg_match("/@(?P<harga>.*)\)/", $string_barang, $matches);
											$harga = clean_currency(trim($matches['harga']));
											//keterangan
											$keterangan_barang = preg_match("/Catatan untuk Penjual:(?P<keterangan>.*)/", $string_barang, $matches);
											if($matches['keterangan']) {
												$keterangan_barang = trim($matches['keterangan']);
											}
											else {
												$keterangan_barang = "-";
											}
											
											
											//cek barang dan seller
											//Hotwheels 1 sk_1
											if (($pos = strpos(strtoupper($barang), "SK-")) !== FALSE) { 
												$id_barang = substr($barang, $pos+3); 
												$record = $this->m_barang->show_one($id_barang);
												foreach ($record->result() as $r) { 
													$id_toko = $r->id_toko;
													$nama_toko = $r->nama_toko;
													$nama_barang = $r->nama_barang;
													$harga_satuan_asli_barang = $r->harga_satuan;
												}
												//set prev toko untuk iterasi 1
												if($jumlah_barang == 1) {
													$prev_toko = $id_toko;
												}
												//insert data transaksi seller
												//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
												echo "id transaksi : ".$id_transaksi."<br>";
												echo "id toko : ".$id_toko."<br>";
												$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
												foreach ($id_transaksi_sellers->result() as $id) { 
													$id_transaksi_seller = $id->id;
												}
												//insert data detail transaksi
												$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
												//update jika multiseller
												if($prev_toko != $id_toko) {
													$this->m_transaksi->edit($id_transaksi,'6');
													$multi_seller = 1;
												}
												$prev_toko = $id_toko;
												
												//update stok 
												//update tanggal otomatis dari trigger
												$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
												$record2 = $this->m_transaksi->show_ecommerce();
												foreach ($record2->result() as $z) {
													if($z->id == $id_ecommerce) {
														$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
													}
													else {
														$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
													}
												}
												//alert
												$deskripsi = "Ada Pembelian ".$nama_barang;
												$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
												$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi","kirim");
												//telegram
												$this->send_notifikasi_telegram($deskripsi." (".$nama.")");
												//update harga berdasarkan satuan harga tabel barang
												$harga_pembelian_asli_barang = $harga_satuan_asli_barang * $jumlah;
												$total_harga_asli_barang += $harga_pembelian_asli_barang;
											}
										}
										//alert
										$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
										$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_email,$deskripsi_notifikasi);
										if(!$cek_input_alert) {
											echo "Alert gagal diinput<br>";
										}
										else {
											foreach ($cek_input_alert->result() as $c) { 
												$id_alert = $c->id_alert;
											}
											//input notifikasi
											//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
											$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
										}
										//}
										//else {
										//	echo "id sudah ada";
										//}
										/*
										//get html data
										$dom = new domDocument;
										$internalErrors = libxml_use_internal_errors(true);
										$dom->loadHTML($message); 
										$dom->preserveWhiteSpace = false; 
										$xpath = new DOMXPath($dom);
										
										// A name attribute on a <div>???
										$node = $xpath->query( '//table/tr')->item(1);
										var_dump($node);
										
										//mendapatkan tabel dari id
										$table = $dom->getElementByTagName('table')->item(1);
										//$table = $tables->item(1);
										//mendapatkan id transaksi
										$header = $dom->getElementsByTagName('th');
										$no_transaksi = $header->item(0)->nodeValue;
										//mendapatkan isi tabel
										$rows = $table->getElementsByTagName('tr');
										// loop over the table rows
										foreach ($rows as $row) 
										{ 
											// get each column by tag name
											$cols = $row->getElementsByTagName('td'); 
											// echo the values  
											echo $cols->item(0)->nodeValue.'<br />'; 
											echo $cols->item(1)->nodeValue.'<br />'; 
											echo $cols->item(2)->nodeValue;
										} 
										*/
										/*
										$alamat_pengiriman = "";
										$no_hp = "";
										$no_transaksi = "";
										$barang = "";
										$id_barang = "";
										$jumlah = "";
										$harga = "";
										$biaya_kirim = "";
										$waktu_pemesanan = "";
										*/
									}
									//total pembayaran
									$total_pembayaran_asli_barang = $total_harga_asli_barang + $biaya_kirim;
									//update total harga dan pembayaran
									$this->m_transaksi->update_harga_barang($id_transaksi,$total_harga_asli_barang,$total_pembayaran_asli_barang);
								}
								
								//untuk uang masuk
								//notifikasi
								$uang_masuk = "Transaksi Penjualan dengan";
								//cek email pesanan baru
								if(strpos($subject, $uang_masuk) !== false) {
									//get DOM from URL or file
									$html = str_get_html($message);
									//Find all links 
									$string = $html->plaintext;
									$no_transaksi = preg_match("/Nomor Faktur:(?P<no_transaksi>.*)Jasa Pengiriman:/", $string, $matches);
									$no_trx = trim($matches['no_transaksi']);
									$no_resi = preg_match("/Nomor Resi:(?P<no_resi>.*)Tanggal Selesai Transaksi:/", $string, $matches);
									$no_resi = trim($matches['no_resi']);
									echo "ini yang sudah dibayar";
									echo $no_trx."<br>";
									$record = $this->m_crontab->update_uang_masuk($no_trx,$ecommerce);
									if(!$record) {
										echo "No trx tidak ada di db <br>";
									}
									else {
										echo "No trx ada di db <br>";
										foreach ($record->result() as $r) { 
											$no_trx_updated = trim($r->no_trx);
											$id_transaksi = $r->id_transaksi;
											$tanggal_pemesanan = $r->tanggal_pemesanan;
											if($r->status_transaksi == 4) {
												echo "Transaksi sudah update di db dan alert";
											}
											else {
												echo "Belum update status transaksi<br>";
												$this->m_crontab->update_status_transaksi($no_trx_updated,4);
												//alert
												//cek per transaksi seller barang
												$record_transaksi_seller = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
												foreach ($record_transaksi_seller->result() as $rts) { 
													$id_transaksi_seller = $rts->id;
													$id_toko = $rts->id_toko;
													//cek per toko
													$record_proses = $this->m_transaksi->cari_detail_transaksi_proses($id_transaksi_seller);
													$nama_barang = "";
													if($record_proses) {
														foreach ($record_proses->result() as $rp) { 
															$nama_barang .= $rp->nama_barang." dan ";
															//input resi
															$nomor_resi = $rp->nomor_resi;
															$id_status = $rp->id_status;
															if($nomor_resi === '' && $id_status != 2) {
																//update nomor resi
																echo "nomor resi kosong<br>";
																$this->m_transaksi->update_resi($rp->id_detail_transaksi,$no_resi);
															}
															else {
																echo "nomor resi tidak kosong<br>";
															}
														}
														//alert
														$deskripsi = "Transaksi Penjualan produk ".substr(trim($nama_barang), 0, -5)." telah selesai.";
														$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
														$this->send_to_app($id_toko,'Transaksi Penjualan Telah Selesai',$deskripsi,$id_transaksi_seller,"saldo","kirim");
													}
													
												}
												
												$cek_alert = $this->m_crontab->cek_alert(5,$id_transaksi,$tanggal_pemesanan);
												if(!$cek_alert) {
													echo "Alert tidak ada di db <br>";
													$deskripsi = "Transaksi Penjualan ".$no_trx_updated." telah selesai";
													$cek_input_alert = $this->m_crontab->input_alert(5,$id_transaksi,$tanggal_pemesanan,$deskripsi);
													if(!$cek_input_alert) {
														echo "Alert gagal diinput<br>";
													}
													else {
														foreach ($cek_input_alert->result() as $c) { 
															$id_alert = $c->id_alert;
														}
														//input notifikasi
														//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
														$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
													}
												}
											}
										}
									}
								}
								
							}
							//bukalapak
							if(0) {
								//elseif($ecommerce === "4") 
								if ($ecommerce === "4") 
								{
									echo "wes masukk <br>";
									//cek email pesanan baru
									$get_pesanan_baru = "Pesanan Baru";
									//cek email pesanan baru
									if(strpos($subject, $get_pesanan_baru) !== false) {
										//buyer
										$dom = new domDocument;
										$dom->loadHTML($message); 
										$dom->preserveWhiteSpace = false; 
										$xpath = new DOMXPath($dom);
										//mendapatkan tabel dari id
										$tables = $dom->getElementById('templateList');
										//mendapatkan id transaksi
										$header = $dom->getElementsByTagName('th');
										$no_transaksi = $header->item(0)->nodeValue;
										//mendapatkan isi tabel
										$rows = $tables->getElementsByTagName('tr');
										foreach ($rows as $row) {
											$cols = $row->getElementsByTagName('td');
											print_r($cols);
											if(trim($cols->item(0)->nodeValue) === "Pembeli") {
												//buyer
												$buyer = trim($cols->item(1)->nodeValue);
											}
											elseif(trim($cols->item(0)->nodeValue) === "Jasa Pengiriman") {
												//jasa pengiriman
												$jasa_pengiriman = trim($cols->item(1)->nodeValue);
											}
											elseif ($xpath->evaluate('count(./a)', $cols->item(0)) > 0) {
												//if($cols->getElementsByTagName('a')->item(0)->getAttribute('class') === "product-name-link") {
													//barang bisa banyak
													echo "horeee nemu anchor";
													$barang = trim($cols->item(0)->nodeValue);
													$jumlah = trim($cols->item(1)->nodeValue);
													$harga = trim($cols->item(3)->nodeValue);
												//}
											}
											elseif(trim($cols->item(0)->nodeValue) === "Total Harga Barang") {
												//total harga
												$total_harga = trim($cols->item(1)->nodeValue);
											}
											elseif(trim($cols->item(0)->nodeValue) === "Biaya Kirim") {
												//biaya kirim
												$biaya_kirim = trim($cols->item(1)->nodeValue);
											}
											elseif(trim($cols->item(0)->nodeValue) === "TOTAL PEMBAYARAN") {
												//total pembayaran
												$total_pembayaran = trim($cols->item(1)->nodeValue);
											}
											//alamat_pengiriman
											
										}
										/*
										$alamat_pengiriman = "";
										$no_hp = "";
										$no_transaksi = "";
										$barang = "";
										$id_barang = "";
										$jumlah = "";
										$harga = "";
										$biaya_kirim = "";
										$waktu_pemesanan = "";
										*/
									}
								}
							}
							else {
								echo "loh gak nemu id ecommerce nya <br>";
							}
							
							//shopee
							if($ecommerce === "4") {
								echo "wes masukk <br>";
								$get_pesanan_baru = "Waktunya mengirimkan pesanan";
								//cek email pesanan baru
								if(strpos($subject, $get_pesanan_baru) !== false) {
									//get DOM from URL or file
									$html = str_get_html($message);
									//Find all links 
									$string = $html->plaintext;
									echo $string;
									//rock the star
									if(1)
									{
									//buyer
									$buyer = preg_match("/Pembayaran dari(?P<buyer>.*)untuk pesanan/", $string, $matches);
									$buyer = strip_tags(trim($matches['buyer']));
									//no transaksi
									$no_transaksi = preg_match("/untuk pesanan #(?P<no_transaksi>.*)sudah terkonfirmasi/", $string, $matches);
									$no_transaksi = trim($matches['no_transaksi']);
									//tanggal pemesanan
									//20-09-17 10:25:56 +0700
									echo "String sebelum waktu pemesanan ".$string;
									$waktu_pemesanan = preg_match("/Pemesanan:(?P<waktu_pemesanan_inside>.*)1\./", $string, $matches);
									echo "Cek waktu pemesanan <br>";
									print_r($matches);
									$raw_waktu_pemesanan = trim($matches['waktu_pemesanan_inside']);
									//setting waktu
									//pending
									$new_date = trim(str_replace('+0700', '', $raw_waktu_pemesanan));
									$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($new_date));
									//echo "Waktu pemesanan test : ".$matches['waktu_pemesanan']."<br>";
									//pengiriman
									$jasa_pengiriman = "-";
									//asuransi
									$asuransi = "-";
									//nama penerima
									$nama_penerima = preg_match("/Nama Penerima:(?P<nama_penerima>[\s\S]*)No\. Telepon/", $string, $matches);
									$nama_penerima = trim($matches['nama_penerima']);
									//alamat
									$alamat_pengiriman = preg_match("/Alamat Pengiriman:(?P<alamat_pengiriman>[\s\S]*)SELANJUTNYA/", $string, $matches);
									$alamat_pengiriman = $nama_penerima." ".trim($matches['alamat_pengiriman']);
									//telp
									$no_hp = preg_match("/No. Telepon:(?P<no_hp>.*)Alamat Pengiriman:/", $string, $matches);
									$no_hp = trim($matches['no_hp']);
									//ongkos kirim
									$biaya_kirim = 0;
									//keterangan
									$keterangan = "-";
									
									//temporary
									$total_harga = 0;
									$total_pembayaran = 0;
									$total_harga_asli_barang = 0;
									$total_pembayaran_asli_barang = 0;
									
									//`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,`keterangan`,`status_transaksi`,`checked`,`multi_seller`
									//status transaksi 6 -> perlu check biaya kirim
									$id_transaksis = $this->m_crontab->new_transaksi($no_transaksi,$no_transaksi,'paid',$waktu_email,$waktu_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,'','6','0');
									//masih belum fix
									
										if(!$id_transaksis) {
											echo "Transaksi sudah ada<br>";
										}
										else{
											//notifikasi
											echo "Pesananan baru <br>";
											foreach ($id_transaksis->result() as $id) { 
												$id_transaksi = $id->id_transaksi;
											}
											
											//call api transaksi
											//if($id_transaksis) {
											//	
											//}
											
											$jumlah_barang = 0;
											$prev_toko = NULL;
											
											//total pembayaran
											$string_barang_all = preg_match("/0700(?P<string_barang_all>.*)RINCIAN PENGIRIMAN/", $string, $matches);
											$string_barang_all = trim($matches['string_barang_all']);
											echo "String barang all : ".$string_barang_all."<br>";
											
											$count_barang = null;
											$string_barang_all = preg_replace('/(\\d\\.)/','separator',$string_barang_all, -1,$count_barang);
											echo "String barang all : ".$string_barang_all."<br>";
											$matches = explode('separator', $string_barang_all);
											
											print_r($matches);
											unset($matches[0]);
											foreach ($matches as $string_barang) {
												//cek multiseller juga
												echo "Masuk detail barang <br>";
												$jumlah_barang++;	
												//barang
												$string_barang = trim(str_replace(',', '', $string_barang));
												print_r($string_barang);
												$barang = preg_match("/(?P<barang>.*)Jumlah:/", $string_barang, $match);
												if($match['barang']) {
													$barang = trim($match['barang']);
												}
												else {
													$barang = "-";
												}
												$id_barang = "";
												//jumlah
												$jumlah = preg_match("/Jumlah:(?P<jumlah>.*)Harga:/", $string_barang, $match);
												$jumlah = trim($match['jumlah']);
												//harga
												$harga = preg_match("/Harga:(.*)Rp(?P<harga_inside>.*)/", $string_barang, $match);
												if($match['harga_inside']) {
													$harga = trim($match['harga_inside']);
												}
												else {
													$harga = 0;
												}
												$harga = clean_currency(trim($harga));
												//total harga barang
												$total_harga_barang = $jumlah * $harga;
												$total_harga += $total_harga_barang;
												//keterangan
												$keterangan_barang = "-";
												
												//cek barang dan seller
												//Hotwheels 1 sk_1
												if (($pos = strpos(strtoupper($barang), "SK-")) !== FALSE) { 
													$id_barang = substr($barang, $pos+3); 
													$record = $this->m_barang->show_one($id_barang);
													foreach ($record->result() as $r) { 
														$id_toko = $r->id_toko;
														$nama_toko = $r->nama_toko;
														$nama_barang = $r->nama_barang;
														$harga_satuan_asli_barang = $r->harga_satuan;
													}
													//set prev toko untuk iterasi 1
													if($jumlah_barang == 1) {
														$prev_toko = $id_toko;
													}
													//insert data transaksi seller
													//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
													echo "id transaksi : ".$id_transaksi."<br>";
													echo "id toko : ".$id_toko."<br>";
													$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
													foreach ($id_transaksi_sellers->result() as $id) { 
														$id_transaksi_seller = $id->id;
													}
													//insert data detail transaksi
													$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
													//update jika multiseller
													if($prev_toko != $id_toko) {
														$this->m_transaksi->edit($id_transaksi,'6');
														$multi_seller = 1;
													}
													$prev_toko = $id_toko;
													
													//update stok 
													//update tanggal otomatis dari trigger
													$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
													$record2 = $this->m_transaksi->show_ecommerce();
													foreach ($record2->result() as $z) {
														if($z->id == $id_ecommerce) {
															$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
														}
														else {
															$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
														}
													}
													//alert
													$deskripsi = "Ada Pembelian ".$nama_barang;
													$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
													$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi","kirim");
													//telegram
													$this->send_notifikasi_telegram($deskripsi." (".$nama.")");
													//update harga berdasarkan satuan harga tabel barang
													$harga_pembelian_asli_barang = $harga_satuan_asli_barang * $jumlah;
													$total_harga_asli_barang += $harga_pembelian_asli_barang;
												}
											}
											//alert
											$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
											$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_email,$deskripsi_notifikasi);
											if(!$cek_input_alert) {
												echo "Alert gagal diinput<br>";
											}
											else {
												foreach ($cek_input_alert->result() as $c) { 
													$id_alert = $c->id_alert;
												}
												//input notifikasi
												//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
												$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
											}
										}
											
										//total pembayaran
										$total_pembayaran = $total_harga + $biaya_kirim;
										$total_pembayaran_asli_barang = $total_harga_asli_barang + $biaya_kirim;
										//update total harga dan pembayaran
										$this->m_transaksi->update_harga($id_transaksi,$total_harga,$total_pembayaran,$total_harga_asli_barang,$total_pembayaran_asli_barang);
									}
								}
							}
							
						}
						else {
							echo "badalah g ada nama ecommerce yang cocok <br>";
						}
					}
					echo "Ecommerce : ".$ecommerce."<br>";
					echo "Waktu Email : ".$waktu_email."<br>";
					echo "Buyer : ".$buyer."<br>";
					echo "Alamat Pengiriman : ".$alamat_pengiriman."<br>";
					echo "Jasa Pengiriman : ".$jasa_pengiriman."<br>";
					echo "No HP : ".$no_hp."<br>";
					echo "No Transaksi : ".$no_transaksi."<br>";
					echo "Barang : ".$barang."<br>";
					echo "Kode Barang : ".$id_barang."<br>";
					echo "Jumlah : ".$jumlah."<br>";
					echo "Harga Barang : ".$harga."<br>";
					echo "Keterangan Barang : ".$keterangan_barang."<br>";
					echo "Total Harga Barang : ".$total_harga_asli_barang."<br>";
					echo "Total Harga Barang Ecommerce : ".$total_harga."<br>";
					echo "Biaya Kirim : ".$biaya_kirim."<br>";
					echo "Total Pembayaran : ".$total_pembayaran_asli_barang."<br>";
					echo "Total Pembayaran Ecommerce : ".$total_pembayaran."<br>";
					echo "Waktu Pemesanan : ".$waktu_pemesanan."<br>";
					echo "Asuransi : ".$asuransi."<br>";
					echo "ID Transaksi : ".$id_transaksi."<br>";
					echo "Keterangan : ".$keterangan."<br>";
					
					echo "Message nya adalah : ".$message."<br>";
					
					$output = '';
					$message = '';
					$subject = '';
					if(!$multi_seller) {
						//update biaya kirim
						$this->m_transaksi->update_biaya_kirim_per_seller($id_transaksi_seller,$biaya_kirim);
					}
				}
			}
			//echo $output;
		} 

		//close the connection
		imap_close($inbox);
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'unread email');
		echo "Total waktu script get unread email : ".$total_time."<br>";
    }
	
	//new transaksi bukalapak
	public function get_new_transaksi_bukalapak() {
		//token fpz6OcWDGmI8K5sShkI
		//user id 35758617
		//ambil data transaksi dengan id 2
		//cek status yang paid
		//ambil datanya
		$begin_time = microtime(true);
		$data = $this->get_transaksi_status(2);
		print_r($data);
		if($data['status'] == 'OK') {
			//transaction
			foreach ($data['transactions'] as $value) { 
				//variabel yang diambil
				$buyer = "";
				$alamat_pengiriman = "";
				$no_hp = "";
				$no_transaksi = "";
				$barang = "";
				$id_barang = "";
				$jumlah = "";
				$harga = "";
				$biaya_kirim = "";
				$waktu_pemesanan = "";
				$waktu_email = "";
				$ecommerce = "3";
				$jasa_pengiriman = "";
				$total_harga = "";
				$total_harga_asli_barang = "";
				$total_pembayaran = "";
				$total_pembayaran_asli_barang = "";
				$asuransi = "";
				$id_transaksi = "";
				$keterangan = "";
				$keterangan_barang = "";
				//untuk cek multiseller
				$multi_seller = 0;
				$id_transaksi_seller = "";
				
				//$id_transaksi = trim($value['id']);
				//if($value['state'] == "received") 
				if($value['state'] == "paid") {
					// [id] => 498541682
					$id_trx = trim($value['id']);
					//[transaction_id] => 170495737447 
					$no_transaksi = trim($value['transaction_id']);
					//[amount] => 1000 
					$total_harga = trim($value['amount']);
					//[courier] => JNE REG 
					$jasa_pengiriman = trim($value['courier']);
					//[buyer_notes] => .
					$keterangan = trim($value['buyer_notes']);
					//[shipping_fee] => 9000
					$biaya_kirim = trim($value['shipping_fee']);
					//[insurance_cost] => 0
					$Asuransi = trim($value['insurance_cost']);
					//[total_amount] => 10000
					//$total_pembayaran = trim($value['total_amount']);
					//update terbaru coded amount untuk pembayaran total
					$total_pembayaran = trim($value['coded_amount']);
					//temporary
					$total_harga_asli_barang = 0;
					$total_pembayaran_asli_barang = 0;
					echo "test";
					
					//alamat dan no hp
					/*
					[name] => Reti 
					[phone] => 082216920115 
					[address] => Jl Jendral Gatot Subroto Kav.52 
					[area] => Mampang Prapatan 
					[city] => Jakarta Selatan 
					[province] => DKI Jakarta 
					[post_code] => 12710
					*/
					$no_hp = trim($value['consignee']['phone']);
					$alamat_pengiriman = trim($value['consignee']['name'])." ".trim($value['consignee']['phone'])." ".trim($value['consignee']['address']).", ".trim($value['consignee']['area']).", ".trim($value['consignee']['city']).", ".trim($value['consignee']['province']).", ".trim($value['consignee']['post_code']);
					//[buyer] => Array ( [id] => 14135146 [name]
					$buyer = trim($value['buyer']['name']);
					//[state_changes] => Array (
					$waktu_pemesanan = date('Y-m-d H:i:s', strtotime($value['state_changes']['paid_at']));
					$waktu_email = $waktu_pemesanan;
					
					$id_transaksis = $this->m_crontab->new_transaksi($id_trx,$no_transaksi,'paid',$waktu_pemesanan,$waktu_email,$ecommerce,$jasa_pengiriman,$biaya_kirim,$total_harga,$total_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,$keterangan,'1','0');
					if(!$id_transaksis) {
						echo "Transaksi sudah ada<br>";
					}
					else{
						echo "Pesananan baru <br>";
						foreach ($id_transaksis->result() as $id) { 
							$id_transaksi = $id->id_transaksi;
						}
									
						$jumlah_barang = 0;
						$prev_toko = NULL;
						//[products] => Array 
						foreach($value['products'] as $value3) {
							$jumlah_barang++;
							//[name] => hot wheel
							$barang = trim($value3['name']);
							//[order_quantity] => 1
							$jumlah = trim($value3['order_quantity']);
							//[price] => 500
							$harga = trim($value3['price']);
							$keterangan_barang = "-";
							
							//cek barang dan seller
							//Hotwheels 1 sk_1
							if (($pos = strpos(strtoupper($barang), "SK-")) !== FALSE) { 
								$id_barang = substr($barang, $pos+3); 
								$record = $this->m_barang->show_one($id_barang);
								foreach ($record->result() as $r) { 
									$id_toko = $r->id_toko;
									$nama_toko = $r->nama_toko;
									$nama_barang = $r->nama_barang;
									$harga_satuan_asli_barang = $r->harga_satuan;
								}
								//set prev toko untuk iterasi 1
								if($jumlah_barang == 1) {
									$prev_toko = $id_toko;
								}
								//insert data transaksi seller
								//`id`,`id_transaksi`,`id_toko`,`biaya_kirim_seller`
								$id_transaksi_sellers = $this->m_crontab->new_transaksi_seller($id_transaksi,$id_toko,'');
								foreach ($id_transaksi_sellers->result() as $id) { 
									$id_transaksi_seller = $id->id;
								}
								//insert data detail transaksi
								$id_transaksi_sellers = $this->m_crontab->new_detail_transaksi($id_transaksi_seller,$id_barang,$harga,$jumlah,$keterangan_barang,'');
								//update jika multiseller
								if($prev_toko != $id_toko) {
									$this->m_transaksi->edit($id_transaksi,'6');
									$multi_seller = 1;
								}
								$prev_toko = $id_toko;
								
								//update stok 
								//update tanggal otomatis dari trigger
								$this->m_transaksi->update_stok_minus($jumlah,$id_barang);
								$record2 = $this->m_transaksi->show_ecommerce();
								foreach ($record2->result() as $z) {
									if($z->id == 3) {
										$this->m_transaksi->input_update_stock($id_barang,$z->id,'3','6');
									}
									else {
										$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','6');
									}
								}
								//alert
								//alert
								$deskripsi = "Ada Pembelian ".$nama_barang;
								$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
								$this->send_to_app($id_toko,"Ada Transaksi Baru",$deskripsi,$id_transaksi_seller,"transaksi","kirim");
								//telegram
								$this->send_notifikasi_telegram($deskripsi."(Bukalapak)");
								//update harga berdasarkan satuan harga tabel barang
								$harga_pembelian_asli_barang = $harga_satuan_asli_barang * $jumlah;
								$total_harga_asli_barang += $harga_pembelian_asli_barang;
							}
						}
						//alert
						$deskripsi_notifikasi = "Ada Transaksi Baru ".$no_transaksi;
						$cek_input_alert = $this->m_crontab->input_alert(1,$id_transaksi,$waktu_pemesanan,$deskripsi_notifikasi);
						if(!$cek_input_alert) {
							echo "Alert gagal diinput<br>";
						}
						else {
							foreach ($cek_input_alert->result() as $c) { 
								$id_alert = $c->id_alert;
							}
							//input notifikasi
							//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
							$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
						}
					}
					//total pembayaran
					$total_pembayaran_asli_barang = $total_harga_asli_barang + $biaya_kirim;
					//update total harga dan pembayaran
					$this->m_transaksi->update_harga_barang($id_transaksi,$total_harga_asli_barang,$total_pembayaran_asli_barang);
				}
				echo "Ecommerce : ".$ecommerce."<br>";
				echo "Waktu Email : ".$waktu_email."<br>";
				echo "Buyer : ".$buyer."<br>";
				echo "Alamat Pengiriman : ".$alamat_pengiriman."<br>";
				echo "Jasa Pengiriman : ".$jasa_pengiriman."<br>";
				echo "No HP : ".$no_hp."<br>";
				echo "No Transaksi : ".$no_transaksi."<br>";
				echo "Barang : ".$barang."<br>";
				echo "Kode Barang : ".$id_barang."<br>";
				echo "Jumlah : ".$jumlah."<br>";
				echo "Harga Barang : ".$harga."<br>";
				echo "Keterangan Barang : ".$keterangan_barang."<br>";
				echo "Total Harga Barang : ".$total_harga_asli_barang."<br>";
				echo "Total Harga Barang Ecommerce : ".$total_harga."<br>";
				echo "Biaya Kirim : ".$biaya_kirim."<br>";
				echo "Total Pembayaran : ".$total_pembayaran_asli_barang."<br>";
				echo "Total Pembayaran Ecommerce : ".$total_pembayaran."<br>";
				echo "Waktu Pemesanan : ".$waktu_pemesanan."<br>";
				echo "Asuransi : ".$asuransi."<br>";
				echo "ID Transaksi : ".$id_transaksi."<br>";
				echo "Keterangan : ".$keterangan."<br>";
				
				if(!$multi_seller) {
					//update biaya kirim
					$this->m_transaksi->update_biaya_kirim_per_seller($id_transaksi_seller,$biaya_kirim);
				}
			}
		}
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'new transaksi bukalapak');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	public function get_uang_masuk() {
		//token fpz6OcWDGmI8K5sShkI
		//user id 35758617
		//ambil data transaksi dengan id 2
		//cek status yang paid
		//ambil datanya
		$begin_time = microtime(true);
		$bukalapak = $this->m_transaksi->cari_transaksi_bukalapak();
		foreach ($bukalapak->result() as $bl) { 
			$id_trx = $bl->id_trx;
			$data = $this->get_transaksi_id($id_trx);
			//$data = $this->get_transaksi_status(5);
			//var_dump($data);
			if($data['status'] == 'OK') {
				$id_transaksi = trim($data['transaction']['id']);
				//if($value['state'] == "received") 
				if($data['transaction']['state'] == "remitted") {
					echo "ini yang sudah dibayar";
					$no_trx = $data['transaction']['transaction_id'];
					echo $no_trx."<br>";
					$record = $this->m_crontab->update_uang_masuk($no_trx,3);
					if(!$record) {
						echo "No trx tidak ada di db <br>";
					}
					else {
						echo "No trx ada di db <br>";
						foreach ($record->result() as $r) { 
							$no_trx_updated = trim($r->no_trx);
							$id_transaksi = $r->id_transaksi;
							$tanggal_pemesanan = $r->tanggal_pemesanan;
							if($r->status_transaksi == 4) {
								echo "Transaksi sudah update di db dan alert";
							}
							else {
								$this->m_crontab->update_status_transaksi($no_trx_updated,4);
								//alert
								//cek per transaksi seller barang
								$record_transaksi_seller = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
								foreach ($record_transaksi_seller->result() as $rts) { 
									$id_transaksi_seller = $rts->id;
									$id_toko = $rts->id_toko;
									//cek per toko
									$record_proses = $this->m_transaksi->cari_detail_transaksi_proses($id_transaksi_seller);
									$nama_barang = "";
									if($record_proses) {
										foreach ($record_proses->result() as $rp) { 
											$nama_barang .= $rp->nama_barang." dan ";
											//input resi
											$nomor_resi = $rp->nomor_resi;
											$id_status = $rp->id_status;
											if($nomor_resi === '' && $id_status != 2) {
												//update nomor resi
												echo "nomor resi kosong";
												$this->m_transaksi->update_resi($rp->id_detail_transaksi,$data['transaction']['shipping_code']);
											}
										}
										//alert
										$deskripsi = "Transaksi Penjualan produk ".substr(trim($nama_barang), 0, -5)." telah selesai.";
										$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
										$this->send_to_app($id_toko,'Transaksi Penjualan Telah Selesai',$deskripsi,$id_transaksi_seller,"saldo","kirim");
									}
									
								}
								
								$cek_alert = $this->m_crontab->cek_alert(5,$id_transaksi,$tanggal_pemesanan);
								if(!$cek_alert) {
									echo "Alert tidak ada di db <br>";
									$deskripsi = "Transaksi Penjualan ".$no_trx_updated." telah selesai";
									$cek_input_alert = $this->m_crontab->input_alert(5,$id_transaksi,$tanggal_pemesanan,$deskripsi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										foreach ($cek_input_alert->result() as $c) { 
											$id_alert = $c->id_alert;
										}
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
									}
								}
							}
						}
					}
				}
			}
		}
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'uang masuk bukalapak');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	public function cek_transaksi_bukapalak($id_sistem) {
		//cek status transaksi dengan id
		//cek status yang remitted
		//update proses uang masuk
	}
	
	public function token_api_bukalapak() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/authenticate.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_USERPWD, "sakoofighting2@gmail.com" . ":" . "1tokoonline");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		var_dump(json_decode($server_output, true));
	}
	
	public function get_transaksi_all() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	//mendapatkan data transaksi dengan id
	public function get_transaksi_status($status) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions.json?status=".$status."");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		$return_value = json_decode($server_output, true);
		//print_r(json_decode($server_output, true));
		return $return_value;
	}
	
	public function get_transaksi() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions/575418419.json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	public function get_transaksi_id($id) {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/transactions/".$id.".json");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		$return_value = json_decode($server_output, true);
		return $return_value;
	}
	
	public function get_notifikasi() {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.bukalapak.com/v2/notifications/list.json?[type]=reminder");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "35758617" . ":" . "fpz6OcWDGmI8K5sShkI");

		$server_output = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		//print_r($server_output);
		print_r(json_decode($server_output, true));
	}
	
	public function parser() {
		// new dom object
		$dom = new DOMDocument();

		//load the html
		$html = $dom->loadHTML("contoh/pesan_baru_tokped.html");
		//$html = utf8_encode($html);
		
		print_r($html);
		
		//discard white space 
		$dom->preserveWhiteSpace = false; 

		//the table by its tag name
		$tables = $dom->getElementsByTagName('table'); 

		//get all rows from the table
		$rows = $tables->item(0)->getElementsByTagName('tr'); 

		// loop over the table rows
		foreach ($rows as $row) 
		{ 
		// get each column by tag name
		  $cols = $row->getElementsByTagName('td'); 
		// echo the values  
		  echo $cols->item(0)->nodeValue.'<br />'; 
		  echo $cols->item(1)->nodeValue.'<br />'; 
		  echo $cols->item(2)->nodeValue;
		} 
	}
	
	public function test_api() {
		$begin_time = microtime(true);
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://103.195.31.220:5000/v1/fcm/sendOneNotification");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"userId\":\"10\",\"title\":\"tes\",\"body\":\"Halooo marcho\",\"id\":\"3\",\"type\":\"transaksi\"}");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		echo "<br>Total waktu script: ".$total_time."<br>";
	}
	
	public function send_user_to_helio() {
		$begin_time = microtime(true);
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		
		$record = $this->m_crontab->get_detail_toko();
		foreach ($record->result() as $r) { 
			$id_toko = $r->id;
			$nama = $r->nama;
			$username = $r->username;
			$email = $r->email;
			$no_hp = $r->no_hp;
			if($email == " ") {
				$this->m_crontab->update_state_helio($id_toko);
			}
			else {
				/*
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://amoeba-api.helio.id/insert");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				//chat_id=-264510550&text=".$message."
				//curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"name\":\"".$nama."\",\"username\":\"".$username."\",\"email\":\"".$email."\",\"phonenumber\":\"".$no_hp."\",\"app\":\"sakoo\"}");
				curl_setopt($ch, CURLOPT_POSTFIELDS, "name=".$nama."&username=".$username."&email=".$email."&phonenumber=".$no_hp."&app=sakoo");
				curl_setopt($ch, CURLOPT_POST, 1);

				$headers = array();
				$headers[] = "Apikey: cMwCL9KGQFr1nBIT6GoDFJEen071E3yb";
				$headers[] = "Content-Type: multipart/form-data";
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

				$result = curl_exec($ch);
				//print_r($result);
				$result = json_decode($result, true);
				if($result['message'] == 'Success') {
					echo $nama." ".$email." Berhasil dikirim<br>";
					$this->m_crontab->update_state_helio($id_toko);
				}
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);
				*/
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "http://amoeba-api.helio.id/insert",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 30,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"name\"\r\n\r\n".$nama."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"username\"\r\n\r\n".$username."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n".$email."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"phonenumber\"\r\n\r\n".$no_hp."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"app\"\r\n\r\nsakoo\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
				  CURLOPT_HTTPHEADER => array(
					"apikey: cMwCL9KGQFr1nBIT6GoDFJEen071E3yb",
					"cache-control: no-cache",
					"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
					"postman-token: 78dbcdb9-da02-6013-8a13-8c0c2c9639a5"
				  ),
				));

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
				  echo "cURL Error #:" . $err;
				} else {
				  if($response['message'] == 'Success') {
					echo $nama." ".$email." Berhasil dikirim<br>";
					$this->m_crontab->update_state_helio($id_toko);
				  }  
				  //echo $response;
				}
			}
		}
		
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		echo "<br>Total waktu script: ".$total_time."<br>";
	}
	
	public function send_to_app($id_toko,$title,$deskripsi,$id,$id_tipe_alert,$param_notif) {
		$begin_time = microtime(true);
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$token = "\"userId\":\"".$id_toko."\",\"title\":\"".$title."\",\"body\":\"".trim($deskripsi)."\",\"id\":\"".$id."\",\"type\":\"".$id_tipe_alert."\",\"paramNotif\":\"".$param_notif."\"";
		echo "Token : ".$token."<br>";
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "http://103.195.31.220:5000/v1/fcm/sendOneNotification");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "{".$token."}");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		print_r($result);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
		
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		echo "Total waktu script send to app : ".$total_time."<br>";
	}
	
	//crontab update status dan resi
	public function update_status_transaksi_dan_resi() {
		$begin_time = microtime(true);
		//cari semua transaksi yang masih open
		//cari anakan dan cek status
		//jika statusnya oke langsung ubah status indukan
		//untuk status transaksi
		$proses_not_all = 0;
		$record = $this->m_transaksi->cari_transaksi_status(1);
		//parameter 
		foreach ($record->result() as $r) { 
			$id_transaksi = $r->id_transaksi;
			$tanggal_pemesanan = $r->tanggal_pemesanan;
			$selisih = $r->selisih;
			$record2 = $this->m_transaksi->cari_transaksi_seller($id_transaksi);						
			//if($record2->result() != 0) {
			if($record2) {
				foreach ($record2->result() as $r2) {
					$id_transaksi_seller = $r2->id;
					$id_toko = $r2->id_toko;
					$record3 = $this->m_transaksi->cari_detail_transaksi($id_transaksi_seller);
					foreach ($record3->result() as $r3) {
						$id_status = $r3->id_status;
						$id_detail_transaksi = $r3->id_detail_transaksi;
						$nama_barang = $r3->nama_barang;
						//selain proses perlu konfirmasi
						if($id_status != 1) {
							//1x24 jam ke seller
							if($id_status == 0) {
								if($selisih >= 24) {
									$cek_alert = $this->m_crontab->cek_alert(2,$id_detail_transaksi,$tanggal_pemesanan);
									if(!$cek_alert) {
										echo "Alert tidak ada di db <br>";
										$deskripsi = "Mohon proses atau tolak pemesanan ".$nama_barang." karena lebih dari 24 jam";
										$cek_input_alert = $this->m_crontab->input_alert(2,$id_detail_transaksi,$tanggal_pemesanan,$deskripsi);
										if(!$cek_input_alert) {
											echo "Alert gagal diinput<br>";
										}
										else {
											foreach ($cek_input_alert->result() as $c) { 
												$id_alert = $c->id_alert;
											}
											//input notifikasi
											//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
											//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
											$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
											//alert
											$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
											//$this->send_to_app($id_toko,"Mohon proses/tolak transaksi",$deskripsi,$id_detail_transaksi,"detail_transaksi","kirim");
											$this->send_to_app($id_toko,"Mohon proses/tolak transaksi",$deskripsi,$id_transaksi_seller,"transaksi","kirim");
										}
									}
								}
							}
							$proses_not_all = 1;
						}
					}
				}
				if(!$proses_not_all) {
					$this->m_transaksi->edit($id_transaksi,'2');  
				}
				$proses_not_all = 0;
			}
		}
		
		//untuk input resi
		$resi_not_all = 0;
		$record = $this->m_transaksi->cari_transaksi_status(2);
		//parameter 
		foreach ($record->result() as $r) { 
			$id_transaksi = $r->id_transaksi;
			$tanggal_pemesanan = $r->tanggal_pemesanan;
			$selisih = $r->selisih;
			$record2 = $this->m_transaksi->cari_transaksi_seller($id_transaksi);
			//if($record2->result() != 0) {
			if($record2) {
				foreach ($record2->result() as $r2) {
					$id_transaksi_seller = $r2->id;
					$id_toko = $r2->id_toko;
					$record3 = $this->m_transaksi->cari_detail_transaksi($id_transaksi_seller);
					foreach ($record3->result() as $r3) {
						$nomor_resi = $r3->nomor_resi;
						$id_status = $r3->id_status;
						$id_detail_transaksi = $r3->id_detail_transaksi;
						$nama_barang = $r3->nama_barang;
						$tanggal_action = $r3->tanggal_action;
						$selisih_input = $r3->selisih_input;
						if($id_status == 1) {
							if($nomor_resi == "") {
								//1x24 jam ke seller
								if($selisih_input >= 24) {
									$cek_alert = $this->m_crontab->cek_alert(3,$id_detail_transaksi,$tanggal_action);
									if(!$cek_alert) {
										echo "Alert tidak ada di db <br>";
										$deskripsi = "Mohon input resi pemesanan ".$nama_barang." karena lebih dari 24 jam";
										$cek_input_alert = $this->m_crontab->input_alert(3,$id_detail_transaksi,$tanggal_action,$deskripsi);
										if(!$cek_input_alert) {
											echo "Alert gagal diinput<br>";
										}
										else {
											//foreach ($cek_input_alert->result() as $c) { 
											//	$id_alert = $c->id_alert;
											//}
											//input notifikasi
											//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
											//alert
											$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
											//$this->send_to_app($id_toko,"Mohon Input Resi",$deskripsi,$id_detail_transaksi,"detail_transaksi","kirim");
											$this->send_to_app($id_toko,"Mohon Input Resi",$deskripsi,$id_transaksi_seller,"transaksi","kirim");
										}
									}
								}
								//2x24 jam ke admin
								if($selisih_input >= 48) {
									$cek_alert = $this->m_crontab->cek_alert(6,$id_detail_transaksi,$tanggal_action);
									if(!$cek_alert) {
										echo "Alert tidak ada di db <br>";
										$deskripsi = "Mohon input resi pemesanan ".$nama_barang." karena lebih dari 48 jam";
										$cek_input_alert = $this->m_crontab->input_alert(6,$id_detail_transaksi,$tanggal_action,$deskripsi);
										if(!$cek_input_alert) {
											echo "Alert gagal diinput<br>";
										}
										else {
											foreach ($cek_input_alert->result() as $c) { 
												$id_alert = $c->id_alert;
											}
											//input notifikasi
											//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
											$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
											//alert
											//$this->send_to_app($id_toko,'Mohon Input Resi',$deskripsi,$id_detail_transaksi,6);
										}
									}
								}
								$resi_not_all = 1;
							}
						}
					}
				}
				if(!$resi_not_all) {
					$this->m_transaksi->edit($id_transaksi,'3');
				}
				$resi_not_all = 0;
			}
		}
		
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'update status dan resi');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	//alert untuk barang listing > 5 hari dan transaksi > 5 hari
	public function update_stok() {
		$begin_time = microtime(true);
		//alert
		//cari barang yang sudah listing > 5 hari no need
		//input di db
		//cari barang yang sudah transaksi > 5 hari
		//$record = $this->m_barang->search_barang_tanggal_transaksi(5);
		$dikirim_10 = 0;
		$dikirim_20 = 0;
		$dikirim_30 = 0;
		$judul_10 = ""; $judul_20 = ""; $judul_30 = "";
		$deskripsi_10 = ""; $deskripsi_20 = ""; $deskripsi_30 = "";
		$counting_10 = 0; $counting_20 = 0; $counting_30 = 0;
		$id_toko = 0;
		//per toko
		$toko = $this->m_barang->group_toko();
		foreach ($toko->result() as $tk) {
			$id_toko_param = $tk->id_toko;
			//10 hari
			//$record = $this->m_barang->search_barang_tanggal_transaksi(10);
			$record = $this->m_barang->search_barang_tanggal_transaksi_toko(10,$id_toko_param);
			foreach ($record->result() as $r) {
				$id_barang = $r->id_barang;
				$tanggal = $r->tanggal;
				$nama_barang = $r->nama_barang;
				$id_toko = $r->id_toko;
				$record2 = $this->m_crontab->cek_alert(4,$id_barang,$tanggal);
				if(!$record2) {
					echo "Alert tidak ada di db <br>";
					$deskripsi = $nama_barang." tidak ada transaksi lebih dari 10 hari mohon update stok";
					$cek_input_alert = $this->m_crontab->input_alert(4,$id_barang,$tanggal,$deskripsi);
					if(!$cek_input_alert) {
						echo "Alert gagal diinput<br>";
					}
					else {
						foreach ($cek_input_alert->result() as $c) { 
							$id_alert = $c->id_alert;
						}
						//input notifikasi
						//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
						//$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
						//input update stok
						//$record3 = $this->m_transaksi->show_ecommerce();
						//foreach ($record3->result() as $z) {
						//	$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','1');
						//}		
						//alert
						//pending dulu
						//$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
						//$this->send_to_app($id_toko,'Tidak ada transaksi > 5 Hari',$deskripsi,$id_barang,"barang");
						$dikirim_10 = 1;
						$counting_10++;
					}
				}
				else {
					echo "Alert ada di db 10 hari<br>";
					//cek 20 hari
					$record_6_hari = $this->m_barang->search_barang_tanggal_transaksi_2($id_barang,20);
					if($record_6_hari) {
						echo "Sudah 20 hari <br>";
						$record_6_hari2 = $this->m_crontab->cek_alert(7,$id_barang,$tanggal);
						if(!$record_6_hari2) {
							echo "Alert tidak ada di db 6 hari<br>";
							$deskripsi = $nama_barang." tidak ada transaksi lebih dari 20 hari mohon update stok";
							$cek_input_alert = $this->m_crontab->input_alert(7,$id_barang,$tanggal,$deskripsi);
							if(!$cek_input_alert) {
								echo "Alert gagal diinput<br>";
							}
							else {
								foreach ($cek_input_alert->result() as $c) { 
									$id_alert = $c->id_alert;
								}
								//input notifikasi
								//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
								//$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
								//input update stok
								//$record3 = $this->m_transaksi->show_ecommerce();
								//foreach ($record3->result() as $z) {
								//	$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','1');
								//}		
								//alert
								//pending dulu
								//$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
								//$this->send_to_app($id_toko,'Tidak ada transaksi > 6 Hari',$deskripsi,$id_barang,"barang");
								$dikirim_20 = 1;
								$counting_20++;
							}
						}
						else {
							//cek 30 hari
							echo "Alert ada di db 20 hari<br>";
							$record_7_hari = $this->m_barang->search_barang_tanggal_transaksi_2($id_barang,30);
							if($record_7_hari) {
								$record_7_hari2 = $this->m_crontab->cek_alert(8,$id_barang,$tanggal);
								if(!$record_7_hari2) {
									echo "Alert tidak ada di db 30 hari<br>";
									$deskripsi = $nama_barang." tidak ada transaksi lebih dari 30 hari mohon update stok";
									$cek_input_alert = $this->m_crontab->input_alert(8,$id_barang,$tanggal,$deskripsi);
									if(!$cek_input_alert) {
										echo "Alert gagal diinput<br>";
									}
									else {
										foreach ($cek_input_alert->result() as $c) { 
											$id_alert = $c->id_alert;
										}
										///* disable
										//input notifikasi
										//$this->m_crontab->input_notifikasi($id_alert,'admin','Administrator');
										$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
										//input update stok
										$this->m_transaksi->update_stok_as_is(0,$id_barang);
										//update penyebab unlisting
										$this->m_crontab->update_barang_penyebab_unlisting($id_barang,4);
										$record3 = $this->m_transaksi->show_ecommerce();
										foreach ($record3->result() as $z) {
											$this->m_transaksi->input_update_stock($id_barang,$z->id,'1','1');
										}		
										//*/
										//alert
										//$deskripsi = trim(preg_replace('/\s+/', ' ', $deskripsi));
										//$this->send_to_app($id_toko,'Tidak ada transaksi > 6 Hari',$deskripsi,$id_barang,"barang");
										$dikirim_30 = 1;
										$counting_30++;
									}
								}
							}
						}
					}
					else{
						echo "bla bla<br>";
					}
				}
			}
			//action
			if($dikirim_10) {
				echo "dikirim 10";
				$judul_10 = "Mohon update barang"; 
				$deskripsi_10 = "Ada ".$counting_10." barang yang belum diupdate selama >10 hari";  
				$this->send_to_app($id_toko,$judul_10,$deskripsi_10,"0","barang","sate");
				//telegram
				//$this->send_notifikasi_telegram($deskripsi_10);
			}
			if($dikirim_20) {
				echo "dikirim 20";
				$judul_20 = "Mohon update barang";
				$deskripsi_20 = "Ada ".$counting_20." barang yang belum diupdate selama >20 hari";
				$this->send_to_app($id_toko,$judul_20,$deskripsi_20,"0","barang","sate");
				//telegram
				//$this->send_notifikasi_telegram($deskripsi_20);
			}
			if($dikirim_30) {   
				echo "dikirim 30";
				$judul_30 = "Mohon update barang";
				$deskripsi_30 = "Ada ".$counting_30." barang yang belum diupdate selama >30 hari";
				$this->send_to_app($id_toko,$judul_30,$deskripsi_30,"0","barang","sate");
				//telegram
				//$this->send_notifikasi_telegram($deskripsi_30);
			}
			$dikirim_10 = 0;
			$dikirim_20 = 0;
			$dikirim_30 = 0;
			$counting_10 = 0;
			$counting_20 = 0;
			$counting_30 = 0;
		}
		
		//input di db
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'update stok');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	public function test_notifikasi() {  
		$id_alert = 1;
		$this->send_notifikasi->send_notifikasi_grup($id_alert,1);
	}
	
	//penentuan update detail toko
	public function update_detail_toko() {
		$begin_time = microtime(true);
		//update last update barang
		$this->m_crontab->update_last_update_barang();
		//update jumlah transaksi
		$this->m_crontab->update_jumlah_transaksi();
		//update jumlah listing
		$this->m_crontab->update_jumlah_listing();
		//input di db
		$end_time = microtime(true);
		$total_time = $end_time - $begin_time;
		$today = date("Y-m-d H:i:s");
		$this->m_transaksi->performance_transaksi($today,$total_time,'update detail toko');
		echo "Total waktu script: ".$total_time."<br>";
	}
	
	//penentuan rank seller
	public function rank_seller() {
		//update rank
		$this->m_crontab->update_rank();
	}
	
	//notifikasi upload barang dan pencairan dana
	public function upload_pencairan_notifikasi() {
		//upload barang
		$upload_barang = $this->m_crontab->cek_upload_barang();
		foreach ($upload_barang->result() as $u) { 
			$id = $u->id;
			$nama_barang = $u->nama;
			$deskripsi = "Ada seller upload barang (".$nama_barang.")";
			$this->send_notifikasi_telegram($deskripsi);
			$this->m_crontab->update_upload_barang($id);
		}
		//pencairan dana
		$pencairan_dana = $this->m_crontab->cek_pencairan_dana();
		foreach ($pencairan_dana->result() as $p) { 
			$id_transaksi_seller = $p->id_transaksi_seller;
			$nama_toko = $p->nama;
			$deskripsi = "Ada seller request pencairan dana (".$nama_toko.")";
			$this->send_notifikasi_telegram($deskripsi);
			$this->m_crontab->update_pencairan_dana($id_transaksi_seller);
		}
	}
	
	//send notifikasi telegram
	public function send_notifikasi_telegram($message) {
		// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot479784616:AAFWWgfOwY7Lksc_T2WqVAjcNnSSv-UiUu0/sendMessage");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "chat_id=-264510550&text=".$message."");
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);
	}
	
	//insert to upload barang
	public function insert_into_upload_barang() {  
		$load_id = $this->m_crontab->load_id_upload_barang();
		foreach ($load_id->result() as $l) { 
			$id_barang = $l->id_barang;
			$this->m_crontab->insert_into_upload_barang($id_barang,4,1);
		}
	}
	
	//insert to update stok
	public function insert_into_update_stok() {
		$load_id = $this->m_crontab->load_id_update_stok();
		foreach ($load_id->result() as $l) { 
			$id_barang = $l->id_barang;
			$id_status_penyebab = $l->id_status_penyebab;
			$this->m_crontab->insert_into_update_stok($id_barang,4,3,$id_status_penyebab);
		}
	}
	
}
<?php

class M_pencairan_dana extends CI_Model {	
	//transaksi yang berhasil
	public function show_status_transaksi() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,transaksi_seller.id id_transaksi_seller,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,
			`harga_total`,`harga_pembayaran`,(sum(barang.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_2,(sum(detail_transaksi.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_3 ,`asuransi`,`buyer`,`alamat_pengiriman`,transaksi.`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, 
			count(detail_transaksi.id_detail_transaksi) jumlah, ecommerce.nama nama_ecommerce, pencairan_dana.status status_pencairan_dana, 
			status_pencairan_dana.status nama_status_pencairan_dana,pencairan_dana.jumlah_uang,pencairan_dana.bukti_transfer,pencairan_dana.bank,bank.nama nama_bank,
			pencairan_dana.waktu_request,pencairan_dana.waktu_transfer,toko.nama nama_toko
            FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
            LEFT JOIN transaksi_seller on transaksi_seller.id_transaksi = transaksi.id_transaksi
			LEFT JOIN toko on transaksi_seller.id_toko = toko.id
			LEFT JOIN detail_transaksi on transaksi_seller.id = detail_transaksi.id_transaksi_seller
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
            LEFT JOIN pencairan_dana on pencairan_dana.id_transaksi_seller = transaksi_seller.id
            LEFT JOIN status_pencairan_dana on pencairan_dana.status = status_pencairan_dana.id
            LEFT JOIN bank on pencairan_dana.bank = bank.id
            where transaksi.status_transaksi = 4 AND complete = 1
			GROUP BY transaksi_seller.id";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function show_status_transaksi_berhasil() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,transaksi_seller.id id_transaksi_seller,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,
			`harga_total`,`harga_pembayaran`,(sum(barang.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_2,(sum(detail_transaksi.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_3 ,`asuransi`,`buyer`,`alamat_pengiriman`,transaksi.`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, 
			count(detail_transaksi.id_detail_transaksi) jumlah, ecommerce.nama nama_ecommerce, pencairan_dana.status status_pencairan_dana, 
			status_pencairan_dana.status nama_status_pencairan_dana,pencairan_dana.jumlah_uang,pencairan_dana.bukti_transfer,pencairan_dana.bank,bank.nama nama_bank,
			pencairan_dana.waktu_request,pencairan_dana.waktu_transfer,toko.nama nama_toko
            FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
            LEFT JOIN transaksi_seller on transaksi_seller.id_transaksi = transaksi.id_transaksi
			LEFT JOIN toko on transaksi_seller.id_toko = toko.id
			LEFT JOIN detail_transaksi on transaksi_seller.id = detail_transaksi.id_transaksi_seller
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
            LEFT JOIN pencairan_dana on pencairan_dana.id_transaksi_seller = transaksi_seller.id
            LEFT JOIN status_pencairan_dana on pencairan_dana.status = status_pencairan_dana.id
            LEFT JOIN bank on pencairan_dana.bank = bank.id
            where transaksi.status_transaksi = 4 AND complete = 1 AND pencairan_dana.status = 0
			GROUP BY transaksi_seller.id";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function show_status_transaksi_request() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,transaksi_seller.id id_transaksi_seller,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,
			`harga_total`,`harga_pembayaran`,(sum(barang.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_2,(sum(detail_transaksi.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_3 ,`asuransi`,`buyer`,`alamat_pengiriman`,transaksi.`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, 
			count(detail_transaksi.id_detail_transaksi) jumlah, ecommerce.nama nama_ecommerce, pencairan_dana.status status_pencairan_dana, 
			status_pencairan_dana.status nama_status_pencairan_dana,pencairan_dana.jumlah_uang,pencairan_dana.bukti_transfer,pencairan_dana.bank,bank.nama nama_bank,
			pencairan_dana.waktu_request,pencairan_dana.waktu_transfer,toko.nama nama_toko
            FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
            LEFT JOIN transaksi_seller on transaksi_seller.id_transaksi = transaksi.id_transaksi
			LEFT JOIN toko on transaksi_seller.id_toko = toko.id
			LEFT JOIN detail_transaksi on transaksi_seller.id = detail_transaksi.id_transaksi_seller
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
            LEFT JOIN pencairan_dana on pencairan_dana.id_transaksi_seller = transaksi_seller.id
            LEFT JOIN status_pencairan_dana on pencairan_dana.status = status_pencairan_dana.id
            LEFT JOIN bank on pencairan_dana.bank = bank.id
            where transaksi.status_transaksi = 4 AND complete = 1 AND pencairan_dana.status = 1
			GROUP BY transaksi_seller.id";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function show_status_transaksi_complete() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,transaksi_seller.id id_transaksi_seller,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,
			`harga_total`,`harga_pembayaran`,(sum(barang.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_2,(sum(detail_transaksi.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_3 ,`asuransi`,`buyer`,`alamat_pengiriman`,transaksi.`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, 
			count(detail_transaksi.id_detail_transaksi) jumlah, ecommerce.nama nama_ecommerce, pencairan_dana.status status_pencairan_dana, 
			status_pencairan_dana.status nama_status_pencairan_dana,pencairan_dana.jumlah_uang,pencairan_dana.bukti_transfer,pencairan_dana.bank,bank.nama nama_bank,
			pencairan_dana.waktu_request,pencairan_dana.waktu_transfer,toko.nama nama_toko
            FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
            LEFT JOIN transaksi_seller on transaksi_seller.id_transaksi = transaksi.id_transaksi
			LEFT JOIN toko on transaksi_seller.id_toko = toko.id
			LEFT JOIN detail_transaksi on transaksi_seller.id = detail_transaksi.id_transaksi_seller
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
            LEFT JOIN pencairan_dana on pencairan_dana.id_transaksi_seller = transaksi_seller.id
            LEFT JOIN status_pencairan_dana on pencairan_dana.status = status_pencairan_dana.id
            LEFT JOIN bank on pencairan_dana.bank = bank.id
            where transaksi.status_transaksi = 4 AND complete = 1 AND pencairan_dana.status = 2
			GROUP BY transaksi_seller.id";
		//echo $query;
		return $this->db->query($query);
	}
	
	//satu transaksi`
	public function show_satu_status_transaksi_seller($id_transaksi_seller) {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,transaksi_seller.id id_transaksi_seller,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,
			`harga_total`,`harga_pembayaran`,(sum(barang.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_2,(sum(detail_transaksi.harga_satuan*jumlah_pembelian)+biaya_kirim_seller) harga_pembayaran_3,`asuransi`,`buyer`,`alamat_pengiriman`,transaksi.`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, 
			count(detail_transaksi.id_detail_transaksi) jumlah, ecommerce.nama nama_ecommerce, pencairan_dana.status status_pencairan_dana, 
			status_pencairan_dana.status nama_status_pencairan_dana,pencairan_dana.jumlah_uang,pencairan_dana.bukti_transfer,pencairan_dana.bank,bank.nama nama_bank,
			pencairan_dana.waktu_request,pencairan_dana.waktu_transfer,toko.rekening,toko.bank nama_bank_customer,toko.nama nama_toko,transaksi_seller.id_toko,toko.nama_rekening
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi_seller.id_transaksi = transaksi.id_transaksi
			LEFT JOIN toko on transaksi_seller.id_toko = toko.id
			LEFT JOIN detail_transaksi on transaksi_seller.id = detail_transaksi.id_transaksi_seller
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
            LEFT JOIN pencairan_dana on pencairan_dana.id_transaksi_seller = transaksi_seller.id
            LEFT JOIN status_pencairan_dana on pencairan_dana.status = status_pencairan_dana.id
            LEFT JOIN bank on pencairan_dana.bank = bank.id
            where transaksi_seller.id = '".$id_transaksi_seller."'
			GROUP BY transaksi_seller.id";
		//echo $query;
		return $this->db->query($query);
	}
	//show bank
	public function show_bank() {
		$query = "SELECT id,nama FROM bank";
		//echo $query;
		return $this->db->query($query);
	}
	//show bank
	public function show_bank_detail($id_bank) {
		$query = "SELECT id,nama FROM bank WHERE id = '".$id_bank."'";
		//echo $query;
		return $this->db->query($query);
	}
	//edit data bukti transfer dan bank
	public function edit_data_transfer($id_transaksi_seller,$bukti_transfer,$bukti_transfer_thumbnail,$bank) {
		$query = "UPDATE pencairan_dana SET bukti_transfer_thumbnail = '".$bukti_transfer_thumbnail."',bukti_transfer = '".$bukti_transfer."',bank = '".$bank."',status = '2',waktu_transfer = NOW() WHERE id_transaksi_seller = '".$id_transaksi_seller."'";
		$this->db->query($query);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//show piutang
	public function show_piutang($id_bank) {
		$query = "SELECT (SELECT sum(jumlah_uang) jumlah FROM `pencairan_dana` WHERE bank = '".$id_bank."' AND status = 2 AND piutang = 0) - (SELECT IFNULL(sum(jumlah),0) jumlah FROM saldo WHERE id_bank = '".$id_bank."' AND jenis = '2') as jumlah";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_berhasil() {
		$query = "SELECT count(1) jumlah_berhasil FROM pencairan_dana WHERE status = 0";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_request() {
		$query = "SELECT count(1) jumlah_request FROM pencairan_dana WHERE status = 1";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_complete() {
		$query = "SELECT count(1) jumlah_complete FROM pencairan_dana WHERE status = 2";
		//echo $query;
		return $this->db->query($query);
	}
	
}

?>
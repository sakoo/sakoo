<?php

class M_crontab extends CI_Model {	
	//transaksi yang baru
	public function new_transaksi($id_trx,$no_trx,$status_trx,$tanggal_pemesanan,$tanggal_email,$id_ecommerce,$jasa_pengiriman,$biaya_kirim,$harga_total,$harga_pembayaran,$asuransi,$buyer,$alamat_pengiriman,$no_hp,$keterangan,$status_transaksi,$checked,$complete = 1,$complete_by = 0) {
		//set false debug 
		$orig_db_debug = $this->db->db_debug;
		$this->db->db_debug = FALSE;
		
		$query = "INSERT INTO transaksi(`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,`keterangan`,`status_transaksi`,`checked`,`complete`,`complete_by`) 
		VALUES ('".$id_trx."','".$no_trx."','".$status_trx."','".$tanggal_pemesanan."','".$tanggal_email."','".$id_ecommerce."','".$jasa_pengiriman."','".$biaya_kirim."','".$harga_total."','".$harga_pembayaran."','".$asuransi."',".$this->db->escape($buyer).",".$this->db->escape($alamat_pengiriman).",'".$no_hp."','".$keterangan."','".$status_transaksi."','".$checked."','".$complete."','".$complete_by."')";
		//echo $query;
		$result = $this->db->query($query);
		//set back setting for debug
		$this->db->db_debug = $orig_db_debug;
		
		if($result) {
			$query2 = "SELECT max(id_transaksi) id_transaksi FROM transaksi";
			return $this->db->query($query2);
		}
		else {
			return false;
		}
	}
	//transaksi seller
	public function new_transaksi_seller($id_transaksi,$id_toko,$biaya_kirim_seller) {
		$query = "INSERT IGNORE INTO transaksi_seller(`id_transaksi`,`id_toko`,`biaya_kirim_seller`) 
		VALUES ('".$id_transaksi."','".$id_toko."','".$biaya_kirim_seller."')";
		//echo $query;
		$this->db->query($query);
		$query2 = "SELECT id FROM transaksi_seller WHERE id_transaksi = '".$id_transaksi."' AND id_toko = '".$id_toko."'";
		return $this->db->query($query2);
	}
	//detail transaksi
	public function new_detail_transaksi($id_transaksi_seller,$id_barang,$harga_satuan,$jumlah_pembelian,$keterangan,$nomor_resi) {
		$query = "INSERT INTO detail_transaksi(`id_transaksi_seller`,`id_barang`,`harga_satuan`,`jumlah_pembelian`,`keterangan`,`nomor_resi`) 
		VALUES ('".$id_transaksi_seller."','".$id_barang."','".$harga_satuan."','".$jumlah_pembelian."','".$keterangan."','".$nomor_resi."')";
		//echo $query;
		return $this->db->query($query);
	}
	//uang masuk
	public function update_uang_masuk($no_trx,$id_ecommerce) {
		$query = "SELECT no_trx,status_transaksi,id_transaksi,(case when `tanggal_pemesanan` is NULL or `tanggal_pemesanan` = '' or `tanggal_pemesanan` = '1970-01-01 07:00:00' then `tanggal_email`  else `tanggal_pemesanan` end) tanggal_pemesanan 
		FROM transaksi WHERE no_trx = '".$no_trx."' AND id_ecommerce = '".$id_ecommerce."'";
		$result = $this->db->query($query);
		if ($result->num_rows()) {
			return $result;
		} else {
			return false;
		}
	}
	//update status transaksi
	public function update_status_transaksi($no_trx,$id_status) {
		$query = "UPDATE transaksi SET status_transaksi = '".$id_status."' WHERE no_trx = '".$no_trx."'";
		$this->db->query($query);
	}
	//cek alert sudah ada belum
	public function cek_alert($tipe_alert,$id,$tanggal) {
		//if($tipe_alert == 4) {
			$query = "SELECT id FROM alert WHERE tipe = '".$tipe_alert."' AND id_tipe = '".$id."' AND tanggal = '".$tanggal."'";
			$result = $this->db->query($query);
			if ($result->num_rows()) {
				return $result;
			} else {
				return false;
			}
		//}
	}
	//insert alert
	public function input_alert($tipe_alert,$id,$tanggal,$deskripsi) {
		//if($tipe_alert == 4) {
			//insert alert 
			//set false debug 
			$orig_db_debug = $this->db->db_debug;
			$this->db->db_debug = FALSE;
			$query = "INSERT INTO alert(id_tipe,tipe,deskripsi,tanggal,tanggal_timestamp) VALUES ('".$id."','".$tipe_alert."','".$deskripsi."','".$tanggal."',NOW())";
			$result = $this->db->query($query);
			//set back setting for debug
			$this->db->db_debug = $orig_db_debug;
			//insert notifikasi
			if($result) {
				$query2 = "SELECT max(id) id_alert FROM alert";
				return $this->db->query($query2);
				//$query2 = "INSERT INTO notifikasi(id_alert,user,user_grup) VALUES ('".$id."','".$tipe_alert."','".$deskripsi."','".$tanggal."')";
				//$this->db->query($query2);
			}
			else {
				return false;
			}
		//}
	}
	//input notifikasi
	public function input_notifikasi($id_alert,$user,$user_grup) {
		$query = "INSERT INTO notifikasi(id_alert,user,user_grup) VALUES ('".$id_alert."','".$user."','".$user_grup."')";
		$this->db->query($query);
	}
	
	//update last update barang
	public function update_last_update_barang() {
		$query = "UPDATE toko t1 SET last_update_barang =(select max(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload`  else `tanggal_update` end) FROM barang t2 WHERE t2.id_toko = t1.id)";
		$this->db->query($query);
	}
	
	//update jumlah transaksi
	public function update_jumlah_transaksi() {
		$query = "UPDATE toko t1 SET jumlah_transaksi = (SELECT count(1) FROM transaksi_seller t2,transaksi t3 WHERE t2.id_transaksi = t3.id_transaksi AND t1.id = t2.id_toko AND t3.status_transaksi = 4 AND t3.complete = 1)";
		$this->db->query($query);
	}
	
	//update rank
	public function update_rank() {
		$query = "set @count:=0;";
		$this->db->query($query);
		$query2 = "UPDATE toko SET rank=@count:=@count+1 WHERE last_update_barang IS NOT NULL AND jumlah_transaksi != 0 ORDER BY jumlah_transaksi desc,last_update_barang desc";
		$this->db->query($query2);
	}
	
	//update jumlah listing
	public function update_jumlah_listing() {
		$query = "UPDATE toko t4 SET jumlah_listing = 
					(SELECT count_list FROM
						(SELECT t1.id,count(t2.id) count_list FROM toko t1,
							(SELECT barang.id, id_toko
							FROM barang
							LEFT JOIN upload_barang ON barang.id = upload_barang.id_barang
							WHERE upload_barang.id_status =3 AND barang.stok != 0
							AND barang.id NOT IN(SELECT id_barang FROM log_hapus_barang)
							GROUP BY barang.id) t2
						WHERE t1.id = t2.id_toko
						GROUP BY t1.id) t3 WHERE t4.id = t3.id)";
		$this->db->query($query);
	}
	
	//notif telegram
	//upload barang
	public function cek_upload_barang() {
		$query = "SELECT id,nama FROM barang WHERE status_upload_notif_telegram = '0'";
		return $this->db->query($query);
	}
	public function update_upload_barang($id) {
		$query = "UPDATE barang SET status_upload_notif_telegram = '1' WHERE id = '".$id."'";
		$this->db->query($query);
	}
	
	//pencairan dana
	public function cek_pencairan_dana() {
		$query = "SELECT id_transaksi_seller,toko.nama 
			FROM pencairan_dana LEFT JOIN transaksi_seller ON pencairan_dana.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN toko ON toko.id = transaksi_seller.id_toko
			WHERE pencairan_dana.status = '1' AND status_pencairan_notif_telegram = '0'";
		return $this->db->query($query);
	}
	public function update_pencairan_dana($id_transaksi_seller) {
		$query = "UPDATE pencairan_dana SET status_pencairan_notif_telegram = '1' WHERE id_transaksi_seller = '".$id_transaksi_seller."'";
		$this->db->query($query);
	}
	
	//load id barang
	public function load_id_upload_barang() {
		$query = "SELECT DISTINCT id_barang FROM upload_barang";
		return $this->db->query($query);
	}
	//insert into upload barang
	public function insert_into_upload_barang($id,$id_ecommerce,$id_status) {
		$query = "INSERT IGNORE INTO upload_barang(id_barang,id_ecommerce,id_status) VALUES ('".$id."','".$id_ecommerce."','".$id_status."')";
		//echo $query;
		$this->db->query($query);
	}
	
	//load id barang
	public function load_id_update_stok() {
		$query = "SELECT DISTINCT id_barang,id_status_penyebab FROM update_stok";
		return $this->db->query($query);
	}
	//insert into update stok
	public function insert_into_update_stok($id,$id_ecommerce,$id_status,$id_status_penyebab) {
		$query = "INSERT IGNORE INTO update_stok(id_barang,id_ecommerce,id_status,id_status_penyebab) VALUES ('".$id."','".$id_ecommerce."','".$id_status."','".$id_status_penyebab."')";
		//echo $query;
		$this->db->query($query);
	}
	//update status penyebab unlisting
	public function update_barang_penyebab_unlisting($id_barang,$id_status) {
		$query = "UPDATE barang SET status_penyebab_unlisting ='".$id_status."' WHERE id='".$id_barang."'";
		//echo $query;
		$this->db->query($query);
	}
	//data toko
	public function get_detail_toko() {
		$query = "SELECT id,nama,username,email,no_hp FROM toko WHERE send_helio='0'";
		return $this->db->query($query);
	}
	//update send user to helio
	public function update_state_helio($id_toko) {
		$query = "UPDATE toko SET send_helio ='1' WHERE id='".$id_toko."'";
		//echo $query;
		$this->db->query($query);
	}
	
}
?>
<?php

class M_user extends CI_Model {

	//deklarasi tabel user
    private $table = "user";

	//cek username password
    function cek($username, $password) {
        $this->db->where("u_name", $username);
        $this->db->where("u_paswd", $password);
        return $this->db->get("user");
    }
	//list semua user
    function semua() {
        return $this->db->get("user");
    }
	//cek username
    function cekKode($kode) {
        $this->db->where("u_name", $kode);
        return $this->db->get("user");
    }
    //cek id
    function cekId($kode) {
        $this->db->where("u_id", $kode);
        return $this->db->get("user");
    }
    //cek login
    function getLoginData($usr, $psw) {
        $u = mysql_real_escape_string($usr);
        $p = md5(mysql_real_escape_string($psw));
        $q_cek_login = $this->db->get_where('users', array('username' => $u, 'password' => $p));
        if (count($q_cek_login->result()) > 0) {
            foreach ($q_cek_login->result() as $qck) {
                foreach ($q_cek_login->result() as $qad) {
                    $sess_data['logged_in'] = 'aingLoginWebYeuh';
                    $sess_data['u_id'] = $qad->u_id;
                    $sess_data['u_name'] = $qad->u_name;
                    $sess_data['nama'] = $qad->nama;
                    $sess_data['group'] = $qad->group;
                    $sess_data['rid'] = $qad->rid;
                    $this->session->set_userdata($sess_data);
                }
                redirect('dashboard');
            }
        } else {
            $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
            header('location:' . base_url() . 'login');
        }
    }
    //update user
    function update($id, $info) {
        $this->db->where("u_id", $id);
        $this->db->update("user", $info);
    }
    //insert user
    function simpan($info) {
        $this->db->insert("user", $info);
    }
    //delete user
    function hapus($kode) {
        $this->db->where("u_id", $kode);
        $this->db->delete("user");
    }

}

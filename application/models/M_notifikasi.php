<?php
class M_notifikasi extends CI_Model {
	//get role
	public function get_list_role($id_grup) {
		$query = "SELECT role FROM grup_notifikasi WHERE grup_id = '".$id_grup."'";
		return $this->db->query($query);
	}
	//get user
	public function get_list_user($role) {
		$query = "SELECT u_name FROM user WHERE role = '".$role."'";
		return $this->db->query($query);
	}
	//input notifikasi
	public function input_notifikasi($id_alert,$user,$role) {
		$query = "INSERT INTO notifikasi(id_alert,user,user_grup) VALUES ('".$id_alert."','".$user."','".$role."')";
		$this->db->query($query);
	}
	//get notifikasi
	public function get_notifikasi($user,$user_grup) {
		$query = "SELECT notifikasi.id_alert,alert.deskripsi,notifikasi.baca FROM notifikasi LEFT JOIN alert on notifikasi.id_alert = alert.id 
		WHERE notifikasi.user = '".$user."' AND notifikasi.user_grup = '".$user_grup."' ORDER BY alert.tanggal_timestamp DESC limit 0,10";
		return $this->db->query($query);
	}
	
	//jumlah notifikasi
	public function jumlah_notifikasi($user,$user_grup) {
		$query = "SELECT count(1) jumlah,user,user_grup FROM notifikasi
		WHERE notifikasi.user = '".$user."' AND notifikasi.user_grup = '".$user_grup."' AND klik = 0";
		return $this->db->query($query);
	}
	
	//update jumlah notifikasi
	public function update_notifikasi($user,$user_grup) {
		$query = "UPDATE notifikasi SET klik = 1
		WHERE notifikasi.user = '".$user."' AND notifikasi.user_grup = '".$user_grup."' AND klik = 0";
		return $this->db->query($query);
	}
	
}
?>
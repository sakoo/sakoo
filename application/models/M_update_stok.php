<?php

class M_update_stok extends CI_Model {
	
	//update stok
	//menampilkan semua data barang
	public function show_all() {
		$query = "SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal_update,b.no_hp,merk,bahan,volume
		FROM update_stok c LEFT JOIN barang a ON c.id_barang = a.id
		LEFT JOIN toko b on a.id_toko = b.id WHERE a.id IS NOT NULL AND a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1 group by a.id";
		return $this->db->query($query);
	}
	//menampilkan semua data barang
	public function show_all_update() {
		$query = "SELECT * FROM (SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal_update,b.no_hp,merk,bahan,volume,max(id_status) max_status, min(id_status) min_status
		FROM update_stok c LEFT JOIN barang a ON c.id_barang = a.id
		LEFT JOIN toko b on a.id_toko = b.id WHERE a.id IS NOT NULL AND a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1 group by a.id) X WHERE min_status = 1";
		return $this->db->query($query);
	}
	//menampilkan semua data barang
	public function show_all_checklist() {
		$query = "SELECT * FROM (SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal_update,b.no_hp,merk,bahan,volume,max(id_status) max_status, min(id_status) min_status
		FROM update_stok c LEFT JOIN barang a ON c.id_barang = a.id
		LEFT JOIN toko b on a.id_toko = b.id WHERE a.id IS NOT NULL AND a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1 group by a.id) X WHERE min_status = 2";
		return $this->db->query($query);
	}
	//menampilkan semua data barang
	public function show_all_updated() {
		$query = "SELECT * FROM (SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal_update,b.no_hp,merk,bahan,volume,max(id_status) max_status, min(id_status) min_status
		FROM update_stok c LEFT JOIN barang a ON c.id_barang = a.id
		LEFT JOIN toko b on a.id_toko = b.id WHERE a.id IS NOT NULL AND a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1 group by a.id) X WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		return $this->db->query($query);
	}
	//untuk update status update stok
	public function edit($id_barang,$id_ecommerce,$id_status,$id_status_penyebab) {
		$query = "UPDATE update_stok SET id_status = '".$id_status."' WHERE id_barang = '".$id_barang."' AND id_ecommerce = '".$id_ecommerce."'";
		$this->db->query($query);
		//history
		$query2 = "INSERT INTO update_stok_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$id_status_penyebab."','".$this->session->userdata('u_name')."',NOW())";
		$this->db->query($query2);
	}
	//sinkronisasi stok
	public function sinkronisasi($id_barang,$id_penyebab) {
		if($id_penyebab == 1 || $id_penyebab == 2) {
			$query = "UPDATE barang SET stok = 0,stok_ecommerce = 0 WHERE id = '".$id_barang."'";
		}
		elseif($id_penyebab == 3) {
			$query = "UPDATE barang SET stok_ecommerce = stok WHERE id = '".$id_barang."'";
		}
		$this->db->query($query);
	}
	//detail barang param id_barang,id_ecommerce
	public function show_status_update_stok($id_barang,$id_ecommerce) {
		$query = "SELECT update_stok.id_status, status_update_stok.status status_update_stok,nama,id_status_penyebab,status_penyebab_update_stok.status status_penyebab_update_stok,update_stok.id_ecommerce,url
		FROM update_stok LEFT JOIN status_update_stok ON update_stok.id_status = status_update_stok.id 
		LEFT JOIN ecommerce ON update_stok.id_ecommerce = ecommerce.id
		LEFT JOIN status_penyebab_update_stok on update_stok.id_status_penyebab = status_penyebab_update_stok.id
		LEFT JOIN upload_barang on (upload_barang.id_barang = update_stok.id_barang AND upload_barang.id_ecommerce = update_stok.id_ecommerce)
		WHERE update_stok.`id_barang` = '".$id_barang."' and update_stok.id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_update_stok_all($id_barang) {
		$query = "SELECT update_stok.id_status, status_update_stok.status status_update_stok,nama,id_status_penyebab,status_penyebab_update_stok.status status_penyebab_update_stok,update_stok.id_ecommerce,url
		FROM update_stok LEFT JOIN status_update_stok ON update_stok.id_status = status_update_stok.id 
		LEFT JOIN ecommerce ON update_stok.id_ecommerce = ecommerce.id
		LEFT JOIN status_penyebab_update_stok on update_stok.id_status_penyebab = status_penyebab_update_stok.id
		LEFT JOIN upload_barang on (upload_barang.id_barang = update_stok.id_barang AND upload_barang.id_ecommerce = update_stok.id_ecommerce)
		WHERE update_stok.`id_barang` = '".$id_barang."'";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_update() {
		$query = "SELECT count(1) jumlah_update FROM (SELECT id,id_status,max(id_status) max_status, min(id_status) min_status
		FROM barang
		LEFT JOIN update_stok ON barang.id = update_stok.id_barang
		WHERE barang.status_aktif =1
		GROUP BY barang.id) X WHERE min_status = 1";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_checklist() {
		$query = "SELECT count(1) jumlah_checklist FROM (SELECT id,id_status,max(id_status) max_status, min(id_status) min_status
		FROM barang
		LEFT JOIN update_stok ON barang.id = update_stok.id_barang
		WHERE barang.status_aktif =1
		GROUP BY barang.id) X WHERE min_status = 2";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_updated() {
		$query = "SELECT count(1) jumlah_updated FROM (SELECT id,id_status,max(id_status) max_status, min(id_status) min_status
		FROM barang
		LEFT JOIN update_stok ON barang.id = update_stok.id_barang
		WHERE barang.status_aktif =1
		GROUP BY barang.id) X WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		//echo $query;
		return $this->db->query($query);
	}
	
}

?>
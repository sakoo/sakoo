<?php

class M_transaksi extends CI_Model {	
	//transaksi
	public function show_status_transaksi() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.status_transaksi != 4 AND complete = 1
			GROUP BY transaksi.id_transaksi";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_transaksi_cek_biaya_kirim() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.status_transaksi != 4 AND complete = 1
			AND transaksi.status_transaksi = 6
			GROUP BY transaksi.id_transaksi";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_transaksi_konfirmasi() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.status_transaksi != 4 AND complete = 1
			AND transaksi.status_transaksi = 1
			GROUP BY transaksi.id_transaksi";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_transaksi_input_resi() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.status_transaksi != 4 AND complete = 1
			AND transaksi.status_transaksi = 2
			GROUP BY transaksi.id_transaksi";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_transaksi_dikirim() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.status_transaksi != 4 AND complete = 1
			AND transaksi.status_transaksi = 3
			GROUP BY transaksi.id_transaksi";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_transaksi_ditolak() {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_pembayaran`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.status_transaksi != 4 AND complete = 1
			AND transaksi.status_transaksi = 5
			GROUP BY transaksi.id_transaksi";
		//echo $query;
		return $this->db->query($query);
	}
	//delete
	public function delete_transaksi($id_user) {
		$query = "UPDATE transaksi SET complete = '2' WHERE complete_by = '".$id_user."' AND complete = '0'";
		//echo $query;
		$this->db->query($query);
	}
	//satu transaksi`
	public function show_satu_status_transaksi($id_transaksi) {
		$query = "SELECT transaksi.`id_transaksi`,`id_trx`,`no_trx`,`status_trx`,`tanggal_pemesanan`,`tanggal_email`,`id_ecommerce`,`jasa_pengiriman`,`biaya_kirim`,`harga_total`,`harga_total_barang`,`harga_pembayaran`,`harga_pembayaran_barang`,`asuransi`,`buyer`,`alamat_pengiriman`,`no_hp`,transaksi.`keterangan`,`status_transaksi`,`checked`,status_transaksi.status, sum(detail_transaksi.jumlah_pembelian) jumlah, ecommerce.nama nama_ecommerce,count(DISTINCT(transaksi_seller.id_toko)) multi_seller  
			FROM `transaksi` LEFT JOIN status_transaksi on transaksi.status_transaksi = status_transaksi.id
			LEFT JOIN transaksi_seller on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN ecommerce on transaksi.id_ecommerce = ecommerce.id
			WHERE transaksi.id_transaksi = '".$id_transaksi."'";
		//echo $query;
		return $this->db->query($query);
	}
	//detail transaksi
	public function show_detail_transaksi($id_transaksi) {
		$query = "SELECT `id_barang`,detail_transaksi.`harga_satuan`,`jumlah_pembelian`,`keterangan`,nama,stok_ecommerce,foto,deskripsi,detail_transaksi.id_status status_konfirmasi
			FROM `detail_transaksi` LEFT JOIN barang ON detail_transaksi.id_barang = barang.id
			LEFT JOIN transaksi_seller on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			WHERE `id_transaksi` = '".$id_transaksi."'";
		return $this->db->query($query);
	}
	//show seller
	public function show_seller($id_transaksi) {
		$query = "SELECT id_toko,toko.nama,transaksi_seller.id,transaksi_seller.biaya_kirim_seller FROM `transaksi_seller`
            LEFT JOIN toko on transaksi_seller.id_toko = toko.id
			WHERE `id_transaksi` = '".$id_transaksi."'";
		return $this->db->query($query);
	}
	//detail transaksi per seller
	public function show_detail_transaksi_seler($id_transaksi,$id_toko) {
		$query = "SELECT id_detail_transaksi,`id_barang`,detail_transaksi.`harga_satuan` harga_satuan_ecommerce,`jumlah_pembelian`,`keterangan`,nama,stok_ecommerce,foto,deskripsi,detail_transaksi.id_status status_konfirmasi,barang.harga_satuan,detail_transaksi.nomor_resi FROM transaksi_seller
			LEFT JOIN `detail_transaksi` on transaksi_seller.id = detail_transaksi.id_transaksi_seller
			LEFT JOIN barang ON detail_transaksi.id_barang = barang.id
			WHERE `id_transaksi` = '".$id_transaksi."' AND transaksi_seller.id_toko = '".$id_toko."'";
		return $this->db->query($query);
	}
	//untuk update status transaksi
	public function edit($id_transaksi,$id_status) {
		$query = "UPDATE transaksi SET status_transaksi = '".$id_status."' WHERE id_transaksi = '".$id_transaksi."'";
		$this->db->query($query);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//untuk update status transaksi by no_trx
	public function edit_by_no_trx($no_trx,$id_status) {
		$query = "UPDATE transaksi SET status_transaksi = '".$id_status."' WHERE no_trx = '".$no_trx."'";
		$this->db->query($query);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//penambahan stok
	public function update_stok($update_stok,$id_barang) {
		//update data stok
		$query = "UPDATE barang SET stok = (stok + '".$update_stok."'), stok_ecommerce = (stok_ecommerce + '".$update_stok."') WHERE id = '".$id_barang."'";
		$this->db->query($query);
	}
	//pengurangan stok
	public function update_stok_minus($update_stok,$id_barang) {
		//update data stok
		$query = "UPDATE barang SET stok = (stok - '".$update_stok."'), stok_ecommerce = (stok_ecommerce - '".$update_stok."') WHERE id = '".$id_barang."'";
		$this->db->query($query);
	}
	//update stok
	public function update_stok_as_is($update_stok,$id_barang) {
		//update data stok
		$query = "UPDATE barang SET stok = '".$update_stok."', stok_ecommerce = '".$update_stok."' WHERE id = '".$id_barang."'";
		echo $query;
		$this->db->query($query);
	}
	//edit no resi
	public function edit_no_resi($id_transaksi,$no_resi) {
		$query = "UPDATE transaksi SET no_resi = '".$no_resi."', status_transaksi = '3' WHERE id_transaksi = '".$id_transaksi."'";
		$this->db->query($query);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//edit no resi
	public function edit_no_resi2($id_transaksi,$id_detail_transaksi,$nomor_resi) {
		//detail transaksi
		$query2 = "UPDATE detail_transaksi SET nomor_resi = '".$nomor_resi."' WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
		$this->db->query($query2);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//reset no resi
	public function empty_resi($nomor_resi) {
		//detail transaksi
		$query2 = "UPDATE detail_transaksi SET nomor_resi = '' WHERE nomor_resi = '".$nomor_resi."'";
		$this->db->query($query2);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//checked
	public function checked($id_transaksi) {
		$query = "UPDATE transaksi SET checked = '1' WHERE id_transaksi = '".$id_transaksi."'";
		$this->db->query($query);
		//masih pr
		//$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		//$this->db->query($query2);
	}
	//show url
	public function show_url($id_barang,$id_ecommerce) {
		$query = "SELECT url,toko.nama FROM upload_barang 
			LEFT JOIN barang on upload_barang.id_barang = barang.id
			LEFT JOIN toko on toko.id = barang.id_toko
			WHERE id_barang = '".$id_barang."' AND id_ecommerce = '".$id_ecommerce."'";
		return $this->db->query($query);
	}
	//show barang
	public function show_barang($keyword,$id_ecommerce) {
		$query = "SELECT barang.id,barang.nama,harga_satuan,toko.nama nama_toko, url, stok, toko.id id_toko FROM barang 
			LEFT JOIN toko on toko.id = barang.id_toko
			LEFT JOIN upload_barang on upload_barang.id_barang = barang.id
			WHERE barang.nama LIKE '%".$keyword."%' AND upload_barang.id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	//show url
	public function add_detail_transaksi($id_transaksi,$id_barang,$harga_satuan,$harga_satuan_ecommerce,$jumlah,$keterangan,$id_toko) {
		$harga_total = $harga_satuan*$jumlah;
		$harga_total_ecommerce = $harga_satuan_ecommerce*$jumlah;
		//insert into transaksi_seller
		$query = "INSERT IGNORE INTO transaksi_seller(id_transaksi,id_toko) VALUES('".$id_transaksi."','".$id_toko."')";
		$this->db->query($query);
		$query2 = "INSERT INTO detail_transaksi(`id_transaksi_seller`,`id_barang`,`harga_satuan`,`jumlah_pembelian`,`keterangan`,`id_status`) VALUES((SELECT id FROM transaksi_seller WHERE id_transaksi = '".$id_transaksi."' AND id_toko = '".$id_toko."'),'".$id_barang."','".$harga_satuan_ecommerce."','".$jumlah."','".$keterangan."','1')";
		//echo $query;
		$this->db->query($query2);
		//update data transaksi
		//$query3 = "UPDATE transaksi SET harga_total = harga_total + '".$harga_total."', harga_total_barang = harga_total_barang + '".$harga_total."', harga_pembayaran = harga_pembayaran + '".$harga_total."', harga_pembayaran_barang = harga_pembayaran_barang + '".$harga_total."' WHERE id_transaksi = '".$id_transaksi."'";
		$query3 = "UPDATE transaksi SET harga_total = harga_total + '".$harga_total_ecommerce."',harga_total_barang = harga_total_barang + '".$harga_total."', harga_pembayaran = harga_pembayaran + '".$harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang + '".$harga_total."' WHERE id_transaksi = '".$id_transaksi."'";
		//echo $query3;
		$this->db->query($query3);
		//mengurangi stok
		$query4 = "UPDATE barang SET stok = stok - '".$jumlah."', stok_ecommerce = stok_ecommerce - '".$jumlah."' WHERE id = '".$id_barang."'";
		$this->db->query($query4);
	}
	//show ecommerce
	public function show_ecommerce() {
		$cek_jumlah_ecommerce = "SELECT id,nama FROM ecommerce";
		return $this->db->query($cek_jumlah_ecommerce);
	}
	//update biaya kirim per seller
	public function update_biaya_kirim_per_seller($id_transaksi_seller,$jumlah) {
		$query = "UPDATE transaksi_seller SET biaya_kirim_seller = '".$jumlah."' WHERE id = '".$id_transaksi_seller."'";
		$this->db->query($query);
		//no need trigger update harga pembayaran
	}
	public function update_biaya_kirim_per_seller_transaksi($id_transaksi_seller,$jumlah,$id_transaksi,$biaya_kirim_awal,$id_ecommerce) {
		$query = "UPDATE transaksi_seller SET biaya_kirim_seller = '".$jumlah."' WHERE id = '".$id_transaksi_seller."'";
		echo $query;
		$this->db->query($query);
		$query = "UPDATE transaksi SET harga_pembayaran_barang = (harga_pembayaran_barang - ".$biaya_kirim_awal.") + ".$jumlah." WHERE id_transaksi = '".$id_transaksi."'";
		echo $query;
		$this->db->query($query);
		if($id_ecommerce == 4) {
			$query = "UPDATE transaksi SET biaya_kirim = '".$jumlah."' WHERE id_transaksi = '".$id_transaksi."'";
			//echo $query;
			$this->db->query($query);
			$query = "UPDATE transaksi SET harga_pembayaran = (harga_pembayaran - ".$biaya_kirim_awal.") + ".$jumlah." WHERE id_transaksi = '".$id_transaksi."'";
			//echo $query;
			$this->db->query($query);
		}
	}
	public function update_harga_total_pembelian_ecommerce($id_transaksi,$harga_total_awal,$harga_total_baru) {
		$query = "UPDATE transaksi SET harga_total = ".$harga_total_baru.", harga_pembayaran = (harga_pembayaran - ".$harga_total_awal.") + ".$harga_total_baru." WHERE id_transaksi = '".$id_transaksi."'";
		//echo $query;
		$this->db->query($query);
	}
	
	public function update_biaya_kirim_ecommerce($id_transaksi,$biaya_kirim_awal,$biaya_kirim_baru) {
		$query = "UPDATE transaksi SET biaya_kirim = ".$biaya_kirim_baru.", harga_pembayaran = (harga_pembayaran - ".$biaya_kirim_awal.") + ".$biaya_kirim_baru." WHERE id_transaksi = '".$id_transaksi."'";
		//echo $query;
		$this->db->query($query);
	}
	
	public function update_harga_pembayaran_ecommerce($id_transaksi,$harga_awal,$harga_baru) {
		$query = "UPDATE transaksi SET harga_pembayaran = ".$harga_baru." WHERE id_transaksi = '".$id_transaksi."'";
		//echo $query;
		$this->db->query($query);
	}
	
	//input update_stock
	public function input_update_stock($id_barang,$id_ecommerce,$id_status,$id_status_penyebab) {
		$query = "INSERT INTO update_stok VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$id_status_penyebab."')
		ON DUPLICATE KEY UPDATE id_status = VALUES(id_status), id_status_penyebab = VALUES(id_status_penyebab)";
		//echo $query;
		$this->db->query($query);
		//history
		$query2 = "INSERT INTO update_stok_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$id_status_penyebab."','".$this->session->userdata('u_name')."',NOW())";
		$this->db->query($query2);
	}
	//show barang
	public function show_edit_barang($id_detail_transaksi,$id_ecommerce) {
		$query = "SELECT barang.nama,barang.id,detail_transaksi.id_detail_transaksi,toko.nama nama_toko,barang.harga_satuan,barang.stok_ecommerce,detail_transaksi.jumlah_pembelian,(barang.harga_satuan*jumlah_pembelian) harga_pembelian,upload_barang.url,detail_transaksi.keterangan,detail_transaksi.id_transaksi_seller,detail_transaksi.id_status stat_konfirm,detail_transaksi.harga_satuan harga_satuan_ecommerce,(detail_transaksi.harga_satuan*jumlah_pembelian) harga_pembelian_ecommerce FROM `detail_transaksi`
			LEFT JOIN transaksi_seller on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN toko on toko.id = barang.id_toko
			LEFT JOIN upload_barang on upload_barang.id_barang = detail_transaksi.id_barang
			WHERE `id_detail_transaksi` = '".$id_detail_transaksi."' AND upload_barang.id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	//show barang
	public function show_edit_barang2($id_transaksi,$id_ecommerce) {
		$query = "SELECT barang.nama,barang.id,detail_transaksi.id_detail_transaksi,toko.nama nama_toko,barang.harga_satuan,barang.stok_ecommerce,detail_transaksi.jumlah_pembelian,(barang.harga_satuan*jumlah_pembelian) harga_pembelian,upload_barang.url,detail_transaksi.keterangan, detail_transaksi.nomor_resi,detail_transaksi.harga_satuan harga_satuan_ecommerce,(detail_transaksi.harga_satuan*jumlah_pembelian) harga_pembelian_ecommerce,biaya_kirim_seller,(barang.harga_satuan*jumlah_pembelian+biaya_kirim_seller) harga_total,(detail_transaksi.harga_satuan*jumlah_pembelian+biaya_kirim_seller) harga_total_ecommerce
			FROM `detail_transaksi`
			LEFT JOIN transaksi_seller on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN toko on toko.id = barang.id_toko
			LEFT JOIN upload_barang on upload_barang.id_barang = detail_transaksi.id_barang
			WHERE `id_transaksi` = '".$id_transaksi."' AND upload_barang.id_ecommerce = '".$id_ecommerce."' AND detail_transaksi.id_status != 2";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	public function show_edit_barang3($nomor_resi,$id_ecommerce) {
		$query = "SELECT barang.nama,barang.id,detail_transaksi.id_detail_transaksi,toko.nama nama_toko,barang.harga_satuan,barang.stok_ecommerce,detail_transaksi.jumlah_pembelian,(barang.harga_satuan*jumlah_pembelian) harga_pembelian,upload_barang.url,detail_transaksi.keterangan, detail_transaksi.nomor_resi FROM `detail_transaksi`
			LEFT JOIN transaksi_seller on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN transaksi on transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN toko on toko.id = barang.id_toko
			LEFT JOIN upload_barang on upload_barang.id_barang = detail_transaksi.id_barang
			WHERE nomor_resi = '".$nomor_resi."' and upload_barang.id_ecommerce = '".$id_ecommerce."' AND transaksi.id_ecommerce =  '".$id_ecommerce."' AND detail_transaksi.id_status != 2";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	public function show_edit_barang_no_resi($id_transaksi,$id_ecommerce) {
		$query = "SELECT barang.nama,barang.id,detail_transaksi.id_detail_transaksi,toko.nama nama_toko,barang.harga_satuan,barang.stok_ecommerce,detail_transaksi.jumlah_pembelian,(barang.harga_satuan*jumlah_pembelian) harga_pembelian,upload_barang.url,detail_transaksi.keterangan, detail_transaksi.nomor_resi FROM `detail_transaksi`
			LEFT JOIN transaksi_seller on detail_transaksi.id_transaksi_seller = transaksi_seller.id
			LEFT JOIN barang on detail_transaksi.id_barang = barang.id
			LEFT JOIN toko on toko.id = barang.id_toko
			LEFT JOIN upload_barang on upload_barang.id_barang = detail_transaksi.id_barang
			WHERE `id_transaksi` = '".$id_transaksi."' AND upload_barang.id_ecommerce = '".$id_ecommerce."' AND detail_transaksi.id_status != 2 AND nomor_resi = ''";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	//show resi
	public function show_resi($id_transaksi) {
		$query = "SELECT nomor_resi FROM detail_transaksi WHERE id_transaksi = '".$id_transaksi."' GROUP BY nomor_resi";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	//show resi
	public function show_resi_detail_transaksi($id_transaksi) {
		$query = "SELECT nomor_resi FROM detail_transaksi 
		LEFT JOIN transaksi_seller on transaksi_seller.id = detail_transaksi.id_transaksi_seller
		WHERE id_transaksi = '".$id_transaksi."' AND detail_transaksi.id_status != 2 GROUP BY nomor_resi";
		//echo $query;
		//$query_insert = "INSERT INTO history_query VALUES('".$keyword."')";
		//$this->db->query($query_insert);
		return $this->db->query($query);
	}
	//edit barang
	public function update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm,$var_harga_total_ecommerce,$var_harga_total_ecommerce_awal,$harga_satuan_ecommerce,$harga_satuan_ecommerce_awal,$status_harga,$stat_konfirm_awal) {
		$query = "UPDATE detail_transaksi SET jumlah_pembelian = '".$jumlah."', keterangan = '".$keterangan."', id_status = ".$stat_konfirm.", tanggal_action = NOW(), harga_satuan = '".$harga_satuan_ecommerce."' WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
		echo $query;
		$this->db->query($query);
		echo "Halooo First ...";
		/*
		if($status == "menambah") {
			//update data transaksi
			$query2 = "UPDATE transaksi SET harga_total = harga_total + '".$var_harga_total."', harga_pembayaran = harga_pembayaran + '".$var_harga_total."' WHERE id_transaksi = '".$id_transaksi."'";
			$this->db->query($query2);
		}
		elseif($status == "mengurangi") {
			//update data transaksi
			$query2 = "UPDATE transaksi SET harga_total = harga_total - '".$var_harga_total."', harga_pembayaran = harga_pembayaran - '".$var_harga_total."' WHERE id_transaksi = '".$id_transaksi."'";
			$this->db->query($query2);
		}
		*/
		echo "Stat : ".$stat_konfirm."<br>";
		echo "Stat Awal: ".$stat_konfirm_awal."<br>";
		if(($stat_konfirm_awal == $stat_konfirm) && $stat_konfirm == 2) {
			echo "Sengaja kosong";
		}
		else {
			echo "Halooo ...";
			if($status_harga == "sama") {
				if($status == "menambah") {
					//update data transaksi
					//$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang + '".$var_harga_total."', harga_total = (harga_total - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang + '".$var_harga_total."', harga_pembayaran = (harga_pembayaran - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."' WHERE id_transaksi = '".$id_transaksi."'";
					$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang + '".$var_harga_total."', harga_total = harga_total + '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang + '".$var_harga_total."', harga_pembayaran = harga_pembayaran + '".$var_harga_total_ecommerce."' WHERE id_transaksi = '".$id_transaksi."'";
					echo $query2;
					$this->db->query($query2);
				}
				elseif($status == "mengurangi") {
					//update data transaksi
					$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang - '".$var_harga_total."', harga_total = harga_total - '".$var_harga_total_ecommerce_awal."', harga_pembayaran_barang = harga_pembayaran_barang - '".$var_harga_total."', harga_pembayaran = harga_pembayaran - '".$var_harga_total_ecommerce_awal."' WHERE id_transaksi = '".$id_transaksi."'";
					echo $query2;
					$this->db->query($query2);
				}
			}
			elseif($status_harga == "berbeda") {
				if(($stat_konfirm_awal == 0 || $stat_konfirm_awal == 1) && $stat_konfirm == 2) { 
					if($status == "mengurangi") {
						//update data transaksi
						$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang - '".$var_harga_total."', harga_total = harga_total - '".$var_harga_total_ecommerce_awal."', harga_pembayaran_barang = harga_pembayaran_barang - '".$var_harga_total."', harga_pembayaran = harga_pembayaran - '".$var_harga_total_ecommerce_awal."' WHERE id_transaksi = '".$id_transaksi."'";
						echo $query2;
						$this->db->query($query2);
					}
				}
				elseif($stat_konfirm_awal == 2 && ($stat_konfirm == 0 || $stat_konfirm == 1)) { 
					if($status == "menambah") {
						//update data transaksi
						$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang + '".$var_harga_total."', harga_total = harga_total + '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang + '".$var_harga_total."', harga_pembayaran = harga_pembayaran + '".$var_harga_total_ecommerce."' WHERE id_transaksi = '".$id_transaksi."'";
						echo $query2;
						$this->db->query($query2);
					}
				}
				else {
					if($status == "menambah") {
						//update data transaksi
						//$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang + '".$var_harga_total."', harga_total = (harga_total - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang + '".$var_harga_total."', harga_pembayaran = (harga_pembayaran - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."' WHERE id_transaksi = '".$id_transaksi."'";
						$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang + '".$var_harga_total."', harga_total = (harga_total - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang + '".$var_harga_total."', harga_pembayaran = (harga_pembayaran - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."' WHERE id_transaksi = '".$id_transaksi."'";
						echo $query2;
						$this->db->query($query2);
					}
					elseif($status == "mengurangi") {
						//update data transaksi
						$query2 = "UPDATE transaksi SET harga_total_barang = harga_total_barang - '".$var_harga_total."', harga_total = (harga_total - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang - '".$var_harga_total."', harga_pembayaran = (harga_pembayaran - '".$var_harga_total_ecommerce_awal."') + '".$var_harga_total_ecommerce."' WHERE id_transaksi = '".$id_transaksi."'";
						echo $query2;
						$this->db->query($query2);
					}
				}
			}
			
			//update stok
			$query3 = "UPDATE barang SET stok = '".$updated_stok."', stok_ecommerce = '".$updated_stok."' WHERE id = '".$id_barang."'";
			$this->db->query($query3);
		}
	}
	//simple edit barang
	public function simple_update_detail_transaksi($id_detail_transaksi,$jumlah,$keterangan,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$status,$stat_konfirm,$harga_satuan_ecommerce_awal,$harga_satuan_ecommerce,$stat_konfirm_awal) {
		$query = "UPDATE detail_transaksi SET keterangan = '".$keterangan."', id_status = ".$stat_konfirm.", tanggal_action = NOW(), harga_satuan = '".$harga_satuan_ecommerce."' WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
		echo $query;
		$this->db->query($query);
		if(($stat_konfirm_awal == $stat_konfirm) && $stat_konfirm == 2) {
			echo "Sengaja kosong";
		}
		else {
			$query2 = "UPDATE transaksi SET  harga_total = (harga_total - '".($harga_satuan_ecommerce_awal*$jumlah)."') + '".($harga_satuan_ecommerce*$jumlah)."', harga_pembayaran = (harga_pembayaran - '".($harga_satuan_ecommerce_awal*$jumlah)."') + '".($harga_satuan_ecommerce*$jumlah)."' WHERE id_transaksi = '".$id_transaksi."'";
			echo $query2;
			$this->db->query($query2);
		}
	}
	//delete barang
	public function delete_detail_transaksi($id_detail_transaksi,$id_transaksi,$var_harga_total,$updated_stok,$id_barang,$id_transaksi_seller,$var_harga_total_ecommerce) {
		$query = "DELETE FROM detail_transaksi WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
		//echo $query;
		$this->db->query($query);
		//update data transaksi
		//$query2 = "UPDATE transaksi SET harga_total = harga_total - '".$var_harga_total."', harga_pembayaran = harga_pembayaran - '".$var_harga_total."' WHERE id_transaksi = '".$id_transaksi."'";
		$query2 = "UPDATE transaksi SET harga_total = harga_total - '".$var_harga_total_ecommerce."', harga_total_barang = harga_total_barang - '".$var_harga_total."', harga_pembayaran = harga_pembayaran - '".$var_harga_total_ecommerce."', harga_pembayaran_barang = harga_pembayaran_barang - '".$var_harga_total."' WHERE id_transaksi = '".$id_transaksi."'";
		$this->db->query($query2);
		
		//menambah stok
		$query3 = "UPDATE barang SET stok = '".$updated_stok."', stok_ecommerce = '".$updated_stok."' WHERE id = '".$id_barang."'";
		$this->db->query($query3);
		
		//cek transaksi seller
		//$query4 = "DELETE FROM ";
	}
	//cek transaksi seller
	public function cek_transaksi_seller($id_transaksi_seller) {
		$query = "SELECT count(1) jumlah FROM detail_transaksi WHERE id_transaksi_seller = '".$id_transaksi_seller."'";
		return $this->db->query($query);
	}
	//delete transaksi seller
	public function delete_transaksi_seller($id_transaksi_seller) {
		$query = "DELETE FROM transaksi_seller WHERE id = '".$id_transaksi_seller."'";
		$this->db->query($query);
	}
	//performansi
	public function performance_transaksi($today,$total_time,$keterangan) {
		$query = "INSERT INTO performance_transaksi VALUES('$today','$total_time','$keterangan')";
		$this->db->query($query);
	}
	//cari query untuk pesanan yang aktif dan status anaknya 
	public function cari_transaksi_status($id_status) {
		$query = "SELECT id_transaksi,(case when `tanggal_pemesanan` is NULL or `tanggal_pemesanan` = '' or `tanggal_pemesanan` = '1970-01-01 07:00:00' then `tanggal_email`  else `tanggal_pemesanan` end) tanggal_pemesanan,TIMESTAMPDIFF(HOUR,(case when `tanggal_pemesanan` is NULL or `tanggal_pemesanan` = '' or `tanggal_pemesanan` = '1970-01-01 07:00:00' then `tanggal_email`  else `tanggal_pemesanan` end),NOW()) selisih 
		FROM transaksi WHERE status_transaksi = '".$id_status."' AND complete = 1";
		return $this->db->query($query);
	}
	//cari transaksi seller
	public function cari_transaksi_seller($id_transaksi) {
		$query = "SELECT id,id_toko FROM transaksi_seller WHERE id_transaksi = '".$id_transaksi."'";
		echo $query;
		$return_value = $this->db->query($query);
		$empty_value = 0;
		if($return_value->num_rows() > 0) {
			return $return_value;
		}
		else {
			return $empty_value;
			//return $return_value;
		}
	}
	//cari detail transaksi
	public function cari_detail_transaksi($id_transaksi_seller) {
		$query = "SELECT id_status,nomor_resi,id_detail_transaksi,nama nama_barang,tanggal_action,TIMESTAMPDIFF(HOUR,tanggal_action,NOW()) selisih_input
		FROM detail_transaksi LEFT JOIN barang ON detail_transaksi.id_barang = barang.id WHERE id_transaksi_seller = '".$id_transaksi_seller."'";
		return $this->db->query($query);
	}
	//cari detail transaksi yang di proses
	public function cari_detail_transaksi_proses($id_transaksi_seller) {
		$query = "SELECT id_status,nomor_resi,id_detail_transaksi,nama nama_barang,tanggal_action,TIMESTAMPDIFF(HOUR,tanggal_action,NOW()) selisih_input
		FROM detail_transaksi LEFT JOIN barang ON detail_transaksi.id_barang = barang.id WHERE id_transaksi_seller = '".$id_transaksi_seller."' AND (id_status = 1 OR id_status = 0)";
		$result = $this->db->query($query);
		if ($result->num_rows()) {
			return $result;
		} else {
			return false;
		}
	}
	//cari transaksi bukalapak
	public function cari_transaksi_bukalapak() {
		$query = "SELECT id_trx FROM transaksi WHERE id_ecommerce = 3 AND status_transaksi != 4 AND status_transaksi != 5 AND status_transaksi != 6 AND complete = 1";
		return $this->db->query($query);
	}
	//update resi
	public function update_resi($id_detail_transaksi,$nomor_resi) {
		$query = "UPDATE detail_transaksi SET nomor_resi = '".$nomor_resi."', id_status = '1' WHERE id_detail_transaksi = '".$id_detail_transaksi."'";
		$this->db->query($query);
	}
	//update total harga dan pembayaran
	public function update_harga($id_transaksi,$total_harga,$total_pembayaran,$total_harga_barang,$total_pembayaran_barang) {
		$query = "UPDATE transaksi SET harga_total = '".$total_harga."', harga_pembayaran = '".$total_pembayaran."', harga_total_barang = '".$total_harga_barang."', harga_pembayaran_barang = '".$total_pembayaran_barang."' WHERE id_transaksi = '".$id_transaksi."'";
		echo "Query update harga : ".$query."<br>";
		$this->db->query($query);
	}
	public function update_harga_barang($id_transaksi,$total_harga_barang,$total_pembayaran_barang) {
		$query = "UPDATE transaksi SET harga_total_barang = '".$total_harga_barang."', harga_pembayaran_barang = '".$total_pembayaran_barang."' WHERE id_transaksi = '".$id_transaksi."'";
		//echo $query;
		$this->db->query($query);
	}
	
	public function total_revenue() {
		$query = "SELECT sum(`harga_total`) revenue FROM `transaksi` WHERE status_transaksi != '5' AND complete = 1";
		return $this->db->query($query);
	}
	
	public function target_revenue() {
		$query = "SELECT sum(`harga_total`) revenue,'1000000' target, ((sum(`harga_total`)/(select target))*100) achieve FROM `transaksi` WHERE status_transaksi != '5'";
		return $this->db->query($query);
	}
	
	public function total_sales() {
		$query = "SELECT count(`harga_total`) sales FROM `transaksi` WHERE status_transaksi != '5' AND complete = 1";
		return $this->db->query($query);
	}
	
	public function target_sales() {
		$query = "SELECT count(`harga_total`) sales,'15' target, ((count(`harga_total`)/(select target))*100) achieve FROM `transaksi` WHERE status_transaksi != '5'";
		return $this->db->query($query);
	}
	
	public function latest_transaksi() {
		$query = "SELECT transaksi.id_transaksi no_trx,toko.nama nama_toko,barang.nama,transaksi.status_transaksi,status_transaksi.status FROM `transaksi` 
			LEFT JOIN transaksi_seller ON transaksi.id_transaksi = transaksi_seller.id_transaksi
			LEFT JOIN detail_transaksi ON detail_transaksi.id_transaksi_seller = transaksi_seller.id 
			LEFT JOIN toko ON toko.id = transaksi_seller.id_toko 
			LEFT JOIN barang ON barang.id = detail_transaksi.id_barang 
			LEFT JOIN status_transaksi ON status_transaksi.id = transaksi.status_transaksi 
			WHERE complete = 1
			ORDER BY transaksi.tanggal_pemesanan DESC limit 0,5";
		return $this->db->query($query);
	}
	
	public function rekap_penjualan() {
		$query = "SELECT CONCAT(DAY(tanggal),'-',MONTH(tanggal)) tanggal,harga_total
			FROM tanggal_referensi LEFT JOIN 
			(SELECT DATE(tanggal_pemesanan) tanggal_pemesanan,sum(`harga_total`) harga_total
			FROM transaksi WHERE (tanggal_pemesanan BETWEEN NOW()-interval 1 MONTH AND NOW()+interval 1 DAY) AND complete = 1			
			GROUP BY day(tanggal_pemesanan),month(`tanggal_pemesanan`)
			ORDER BY tanggal_pemesanan) x ON tanggal_referensi.tanggal = x.tanggal_pemesanan
			WHERE (tanggal BETWEEN NOW()-interval 1 MONTH AND NOW()+interval 1 DAY)";
		return $this->db->query($query);
	}
	
	public function rekap_sales() {
		$query = "SELECT CONCAT(DAY(tanggal),'-',MONTH(tanggal)) tanggal,jumlah_pembelian
			FROM tanggal_referensi LEFT JOIN 
			(SELECT DATE( tanggal_pemesanan ) tanggal_pemesanan, COUNT( transaksi.id_transaksi ) jumlah_pembelian
				FROM transaksi
				LEFT JOIN transaksi_seller ON transaksi.id_transaksi = transaksi_seller.id_transaksi
				LEFT JOIN detail_transaksi ON transaksi_seller.id = detail_transaksi.id_transaksi_seller
				WHERE detail_transaksi.id_status !=  '2' AND
				(tanggal_pemesanan BETWEEN NOW()-interval 1 MONTH AND NOW()+interval 1 DAY) AND complete = 1
				GROUP BY DAY( tanggal_pemesanan ) , MONTH(  `tanggal_pemesanan` ) 
				ORDER BY  `tanggal_pemesanan` DESC ) x ON tanggal_referensi.tanggal = x.tanggal_pemesanan 
			WHERE (tanggal BETWEEN NOW()-interval 1 MONTH AND NOW()+interval 1 DAY)";
		return $this->db->query($query);
	}
	
	public function show_jasa_pengiriman() {
		$query = "SELECT id,nama FROM jasa_pengiriman";
		return $this->db->query($query);
	}
	
	public function update_jasa_pengiriman($id_transaksi,$jasa_pengiriman) {
		$query = "UPDATE transaksi SET jasa_pengiriman = '".$jasa_pengiriman."' WHERE id_transaksi='".$id_transaksi."'";
		$this->db->query($query);
	}
	
	public function update_complete_transaksi($id_transaksi,$no_transaksi,$nama_ecommerce,$buyer,$no_hp,$alamat_pengiriman,$tanggal_pemesanan,$jasa_pengiriman,$harga_total,$harga_total_ecommerce,$biaya_kirim,$biaya_kirim_ecommerce,$harga_pembayaran,$harga_pembayaran_ecommerce) {
		$query = "UPDATE transaksi SET id_trx = '".$no_transaksi."',no_trx = '".$no_transaksi."',id_ecommerce = '".$nama_ecommerce."',buyer = '".$buyer."',no_hp = '".$no_hp."',alamat_pengiriman = '".$alamat_pengiriman."',tanggal_pemesanan = '".$tanggal_pemesanan."',tanggal_email = '".$tanggal_pemesanan."',
				jasa_pengiriman = '".$jasa_pengiriman."',harga_total_barang = '".$harga_total."',harga_total = '".$harga_total_ecommerce."',biaya_kirim = '".$biaya_kirim_ecommerce."',harga_pembayaran_barang = '".$harga_pembayaran."',harga_pembayaran = '".$harga_pembayaran_ecommerce."',complete = '1' 
				WHERE id_transaksi = '".$id_transaksi."'";
		$this->db->query($query);
	}
	
	public function total_cek_biaya_kirim() {
		$query = "SELECT count(1) jumlah_cek_biaya_kirim FROM transaksi WHERE status_transaksi = 6 AND complete = 1";
		return $this->db->query($query);
	}
	
	public function total_konfirmasi() {
		$query = "SELECT count(1) jumlah_konfirmasi FROM transaksi WHERE status_transaksi = 1 AND complete = 1";
		return $this->db->query($query);
	}
	
	public function total_input_resi() {
		$query = "SELECT count(1) jumlah_input_resi FROM transaksi WHERE status_transaksi = 2 AND complete = 1";
		return $this->db->query($query);
	}
	
	public function total_input_resi_checklist() {
		$query = "SELECT count(1) jumlah_checklist FROM transaksi WHERE status_transaksi = 2 AND complete = 1 AND checked = 0";
		return $this->db->query($query);
	}
	
	public function total_sedang_dikirim() {  
		$query = "SELECT count(1) jumlah_dikirim FROM transaksi WHERE status_transaksi = 3 AND complete = 1 AND checked = 1";
		return $this->db->query($query);
	}
	
	public function total_sedang_dikirim_checklist() {
		$query = "SELECT count(1) jumlah_dikirim_checklist FROM transaksi WHERE status_transaksi = 3 AND complete = 1 AND checked = 0";
		return $this->db->query($query);
	}
	
	public function total_ditolak() {
		$query = "SELECT count(1) jumlah_ditolak FROM transaksi WHERE status_transaksi = 5 AND complete = 1";
		return $this->db->query($query);
	}
	
}

?>
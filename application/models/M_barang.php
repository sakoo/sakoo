<?php

class M_barang extends CI_Model {
	
	//barang
	//menampilkan semua data barang
	public function show_all() {
		$query = "SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.no_hp,merk,bahan,volume,'all' status_dash
		FROM barang a left join toko b on a.id_toko = b.id WHERE a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1";
		return $this->db->query($query);
	}
	
	public function show_all_listing() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT a.id id_barang_a,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.no_hp,merk,bahan,volume,'listing' status_dash
		FROM barang a left join toko b on a.id_toko = b.id WHERE a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1) X LEFT JOIN upload_barang ON X.id_barang_a = upload_barang.id_barang
		GROUP BY X.id_barang_a) Y WHERE min_status = 1";
		return $this->db->query($query);
	}
	
	public function show_all_checklist() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT a.id id_barang_a,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.no_hp,merk,bahan,volume,'checklist' status_dash
		FROM barang a left join toko b on a.id_toko = b.id WHERE a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1) X LEFT JOIN upload_barang ON X.id_barang_a = upload_barang.id_barang
		GROUP BY X.id_barang_a) Y WHERE min_status = 2";
		return $this->db->query($query);
	}
	
	public function show_all_listed() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT a.id id_barang_a,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.no_hp,merk,bahan,volume,'listed' status_dash
		FROM barang a left join toko b on a.id_toko = b.id WHERE a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1) X LEFT JOIN upload_barang ON X.id_barang_a = upload_barang.id_barang
		GROUP BY X.id_barang_a) Y WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		return $this->db->query($query);
	}
	
	public function show_one($id) {
		$query = "SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.harga_markup,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,a.id_toko,a.berat,harga_markup.lokasi,status_penyebab_unlisting.status status_penyebab,status_penyebab_unlisting.id id_status_penyebab_unlisting,merk,bahan,volume,harga_markup.lokasi
		FROM barang a LEFT JOIN toko b on a.id_toko = b.id LEFT JOIN harga_markup ON harga_markup.id = b.lokasi_markup LEFT JOIN status_penyebab_unlisting ON a.status_penyebab_unlisting = status_penyebab_unlisting.id WHERE a.id = '".$id."'";
        return $this->db->query($query);
	}
	public function show_nama($id) {
		$query = "SELECT id id_barang,nama nama_barang,id_toko
		FROM barang  WHERE id = '".$id."'";
        return $this->db->query($query);
	}
	public function show_foto_tambahan($id) {
		$query = "SELECT foto FROM foto_tambahan WHERE id_barang = '".$id."'";
		return $this->db->query($query);
	}
	public function save($data) {
		$this->db->insert('barang',$data);
	}
	public function edit($id_barang,$id_ecommerce,$id_status) {
		$query = "UPDATE upload_barang SET id_status = '".$id_status."' WHERE id_barang = '".$id_barang."' AND id_ecommerce = '".$id_ecommerce."'";
		$this->db->query($query);
		$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		$this->db->query($query2);
	}
	public function edit_url($id_barang,$id_ecommerce,$id_status,$url) {
		$query = "UPDATE upload_barang SET id_status = '".$id_status."',url = '".$url."' WHERE id_barang = '".$id_barang."' AND id_ecommerce = '".$id_ecommerce."'";
		$this->db->query($query);
		$query2 = "INSERT INTO upload_barang_history VALUES('".$id_barang."','".$id_ecommerce."','".$id_status."','".$this->session->userdata('u_name')."',NOW())";
		$this->db->query($query2);
	}
	//cari nilai terkecil status upload barang
	public function search_status($id_barang,$id_status) {
		$query = "SELECT count(1) jumlah FROM `upload_barang` WHERE `id_barang` = '".$id_barang."' AND id_status = '".$id_status."'";
		return $this->db->query($query);
	}
	//untuk update tanggal listing
	public function edit_listing($id_barang) {
		$query = "UPDATE barang SET tanggal_listing = NOW() WHERE id = '".$id_barang."'";
		//echo $query;
		$this->db->query($query);
	}
	//detail barang param id_barang,id_ecommerce
	public function show_status_upload($id_barang,$id_ecommerce) {
		$query = "SELECT id_status,status,nama,id_ecommerce,url
		FROM upload_barang LEFT JOIN status_upload_barang ON upload_barang.id_status = status_upload_barang.id 
		LEFT JOIN ecommerce ON upload_barang.id_ecommerce = ecommerce.id
		WHERE `id_barang` = '".$id_barang."' and id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		return $this->db->query($query);
	}
	//detail barang param id_barang
	public function show_status_upload_all($id_barang) {
		$query = "SELECT id_status,status,nama,id_ecommerce,url
		FROM upload_barang LEFT JOIN status_upload_barang ON upload_barang.id_status = status_upload_barang.id 
		LEFT JOIN ecommerce ON upload_barang.id_ecommerce = ecommerce.id
		WHERE `id_barang` = '".$id_barang."' ORDER BY upload_barang.id_ecommerce";
		//echo $query;
		return $this->db->query($query);
	}
	//cari barang yang perlu diupdate
	public function search_barang_tanggal_transaksi($thresold) {
		$thresold = $thresold * 24;
		$query = "SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.id id_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal
		FROM barang a left join toko b on a.id_toko = b.id 
		WHERE TIMESTAMPDIFF(HOUR, (case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end), NOW()) > '".$thresold."'";
		//echo $query;
		return $this->db->query($query);
	}
	public function search_barang_tanggal_transaksi_toko($thresold,$id_toko) {  
		$thresold = $thresold * 24;
		$query = "SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.id id_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal
		FROM barang a left join toko b on a.id_toko = b.id 
		WHERE TIMESTAMPDIFF(HOUR, (case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end), NOW()) > '".$thresold."' AND id_toko = '".$id_toko."' AND a.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND a.status_aktif = 1";
		//echo $query;
		return $this->db->query($query);
	}
	
	//cari barang yang perlu diupdate
	public function search_barang_tanggal_transaksi_2($id_barang,$thresold) {
		$thresold = $thresold * 24;
		$query = "SELECT a.id id_barang,a.nama nama_barang,a.stok,a.stok_ecommerce,a.harga_satuan,a.foto,a.deskripsi,a.tanggal_upload,a.tanggal_listing,b.nama nama_toko,b.id id_toko,(case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end) tanggal
		FROM barang a left join toko b on a.id_toko = b.id 
		WHERE TIMESTAMPDIFF(HOUR, (case when `tanggal_update` is NULL or `tanggal_update` = '' then `tanggal_upload` else `tanggal_update` end), NOW()) > '".$thresold."'
		AND a.id = '".$id_barang."'";
		//echo $query;
		$result = $this->db->query($query);
		if ($result->num_rows()) {
			return $result;
		} else {
			return false;
		}
	}
	
	public function total_barang() {
		//$query = "SELECT count(barang.`id`) barang FROM `barang` LEFT JOIN toko ON barang.id_toko = toko.id WHERE status_aktif = 1 AND (tanggal_register between '2018-02-25' AND '2018-03-06')";
		$query = "SELECT count(`id`) barang FROM `barang` WHERE status_aktif = 1";
		return $this->db->query($query);
	}
	
	public function target_barang() {
		$query = "SELECT count(`id`) barang,'100' target, ((count(`id`)/(select target))*100) achieve FROM `barang`";
		return $this->db->query($query);
	}
	
	public function total_seller() {
		//$query = "SELECT count(`id`) toko FROM `toko` WHERE tanggal_register between '2018-02-25' AND '2018-03-06'";
		$query = "SELECT count(`id`) toko FROM `toko`";
		return $this->db->query($query);
	}
	
	public function target_seller() {
		$query = "SELECT count(`id`) toko,'25' target, ((count(`id`)/(select target))*100) achieve FROM `toko`";
		return $this->db->query($query);
	}
	
	public function latest_seller() {
		$query = "SELECT nama,foto,concat(DAY(tanggal_register),' ',MONTHNAME(tanggal_register)) tanggal FROM `toko` WHERE nama != '' ORDER BY tanggal_register DESC limit 0,8";
		return $this->db->query($query);
	}
	
	public function recently_barang() {
		$query = "SELECT nama,foto,harga_satuan harga,deskripsi FROM `barang` WHERE nama != '' ORDER BY tanggal_upload DESC limit 0,4";
		return $this->db->query($query);
	}
	
	public function group_toko() {
		$query = "SELECT id_toko FROM `barang` GROUP BY id_toko";
		return $this->db->query($query);
	}
	
	public function show_penyebab_unlisting() {
		$query = "SELECT id,status FROM status_penyebab_unlisting";
		return $this->db->query($query);
	}

	public function edit_status_penyebab($id_barang,$penyebab) {
		$query = "UPDATE barang SET status_penyebab_unlisting = '".$penyebab."' WHERE id = '".$id_barang."'";
		$this->db->query($query);
	}
	
	public function show_nama_penyebab($id_penyebab) {
		$query = "SELECT status FROM status_penyebab_unlisting WHERE id = '".$id_penyebab."'";
		return $this->db->query($query);  
	}
	
	//Hapus
	public function show_hapus_barang() {
		$query = "SELECT log_hapus_barang.id id_log_hapus_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_hapus_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok
		FROM `log_hapus_barang` LEFT JOIN barang ON log_hapus_barang.id_barang = barang.id
		LEFT JOIN toko ON toko.id = barang.id_toko";
		return $this->db->query($query);
	}
	
	public function show_hapus_barang_delete() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT log_hapus_barang.id id_log_hapus_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_hapus_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,status_hapus_barang id_status
		FROM `log_hapus_barang` LEFT JOIN barang ON log_hapus_barang.id_barang = barang.id
		LEFT JOIN log_ecommerce_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		LEFT JOIN toko ON toko.id = barang.id_toko) X GROUP BY id_log_hapus_barang) Y WHERE min_status = 1";
		return $this->db->query($query);
	}
	
	public function show_hapus_barang_checklist() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT log_hapus_barang.id id_log_hapus_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_hapus_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,status_hapus_barang id_status
		FROM `log_hapus_barang` LEFT JOIN barang ON log_hapus_barang.id_barang = barang.id
		LEFT JOIN log_ecommerce_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		LEFT JOIN toko ON toko.id = barang.id_toko) X GROUP BY id_log_hapus_barang) Y WHERE min_status = 2";
		return $this->db->query($query);
	}
	
	public function show_hapus_barang_deleted() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT log_hapus_barang.id id_log_hapus_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_hapus_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,status_hapus_barang id_status
		FROM `log_hapus_barang` LEFT JOIN barang ON log_hapus_barang.id_barang = barang.id
		LEFT JOIN log_ecommerce_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		LEFT JOIN toko ON toko.id = barang.id_toko) X GROUP BY id_log_hapus_barang) Y WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		return $this->db->query($query);
	}
	
	//detail barang param id_barang,id_ecommerce
	public function show_status_hapus($id_log,$id_ecommerce) {
		$query = "SELECT log_ecommerce_hapus_barang.id_status,log_ecommerce_hapus_barang.id_ecommerce,status_hapus_barang,status_hapus_barang.status,ecommerce.nama,upload_barang.url
		FROM log_ecommerce_hapus_barang 
		LEFT JOIN status_hapus_barang ON log_ecommerce_hapus_barang.status_hapus_barang = status_hapus_barang.id
		LEFT JOIN ecommerce ON ecommerce.id = log_ecommerce_hapus_barang.id_ecommerce
		LEFT JOIN log_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		LEFT JOIN upload_barang ON (upload_barang.id_barang = log_hapus_barang.id_barang AND upload_barang.id_ecommerce = log_ecommerce_hapus_barang.id_ecommerce)
		WHERE `id_log_hapus_barang` = '".$id_log."' and log_ecommerce_hapus_barang.id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		return $this->db->query($query);
	}
	public function show_status_hapus_all($id_log) {
		$query = "SELECT log_hapus_barang.id id_log_hapus_barang,log_ecommerce_hapus_barang.id_status,log_ecommerce_hapus_barang.id_ecommerce,status_hapus_barang,status_hapus_barang.status,ecommerce.nama,upload_barang.url
		FROM log_ecommerce_hapus_barang 
		LEFT JOIN status_hapus_barang ON log_ecommerce_hapus_barang.status_hapus_barang = status_hapus_barang.id
		LEFT JOIN ecommerce ON log_ecommerce_hapus_barang.id_ecommerce = ecommerce.id
		LEFT JOIN log_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		LEFT JOIN upload_barang ON (upload_barang.id_barang = log_hapus_barang.id_barang AND upload_barang.id_ecommerce = log_ecommerce_hapus_barang.id_ecommerce)
		WHERE `id_log_hapus_barang` = '".$id_log."'";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function proses_hapus_barang($id_log_hapus_barang, $id_ecommerce, $status_hapus_barang) {
		$query = "UPDATE log_ecommerce_hapus_barang SET status_hapus_barang = '".$status_hapus_barang."' WHERE id_log_hapus_barang = '".$id_log_hapus_barang."' AND id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		$this->db->query($query);
	}
	
	//Edit
	public function show_edit_barang() {
		$query = "SELECT log_edit_barang.id id_log_edit_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_edit_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,field
		FROM `log_edit_barang` LEFT JOIN barang ON log_edit_barang.id_barang = barang.id
		LEFT JOIN toko ON toko.id = barang.id_toko WHERE barang.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND barang.status_aktif = 1";
		return $this->db->query($query);
	}
	
	public function show_edit_barang_edit() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT log_edit_barang.id id_log_edit_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_edit_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,field,status_edit_barang id_status
		FROM `log_edit_barang` LEFT JOIN barang ON log_edit_barang.id_barang = barang.id
		LEFT JOIN log_ecommerce_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		LEFT JOIN toko ON toko.id = barang.id_toko WHERE barang.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND barang.status_aktif = 1) X GROUP BY id_log_edit_barang) Y WHERE min_status = 1";
		return $this->db->query($query);
	}
	
	public function show_edit_barang_checklist() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT log_edit_barang.id id_log_edit_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_edit_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,field,status_edit_barang id_status
		FROM `log_edit_barang` LEFT JOIN barang ON log_edit_barang.id_barang = barang.id
		LEFT JOIN log_ecommerce_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		LEFT JOIN toko ON toko.id = barang.id_toko WHERE barang.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND barang.status_aktif = 1) X GROUP BY id_log_edit_barang) Y WHERE min_status = 2";
		return $this->db->query($query);
	}
	
	public function show_edit_barang_edited() {
		$query = "SELECT * FROM (SELECT *,max(id_status) max_status, min(id_status) min_status FROM (SELECT log_edit_barang.id id_log_edit_barang,barang.id id_barang,toko.no_hp,barang.foto,barang.nama nama_barang,log_edit_barang.timestamp,barang.deskripsi,toko.nama nama_toko,barang.stok,field,status_edit_barang id_status
		FROM `log_edit_barang` LEFT JOIN barang ON log_edit_barang.id_barang = barang.id
		LEFT JOIN log_ecommerce_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		LEFT JOIN toko ON toko.id = barang.id_toko WHERE barang.id NOT IN(SELECT id_barang FROM log_hapus_barang) AND barang.status_aktif = 1) X GROUP BY id_log_edit_barang) Y WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		return $this->db->query($query);
	}
	
	//detail barang param id_barang,id_ecommerce
	public function show_status_edit($id_log,$id_ecommerce) {
		$query = "SELECT log_ecommerce_edit_barang.id_status,log_ecommerce_edit_barang.id_ecommerce,status_edit_barang,status_edit_barang.status,ecommerce.nama,upload_barang.url,field
		FROM log_ecommerce_edit_barang 
		LEFT JOIN status_edit_barang ON log_ecommerce_edit_barang.status_edit_barang = status_edit_barang.id
		LEFT JOIN ecommerce ON ecommerce.id = log_ecommerce_edit_barang.id_ecommerce
		LEFT JOIN log_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		LEFT JOIN upload_barang ON (upload_barang.id_barang = log_edit_barang.id_barang AND upload_barang.id_ecommerce = log_ecommerce_edit_barang.id_ecommerce)
		WHERE `id_log_edit_barang` = '".$id_log."' and log_ecommerce_edit_barang.id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function show_edit_field($id_log) {
		$query = "SELECT field FROM log_edit_barang WHERE id = '".$id_log."'";
		return $this->db->query($query);
	}
	
	public function show_status_edit_all($id_log) {
		$query = "SELECT log_edit_barang.id id_log_edit_barang,log_ecommerce_edit_barang.id_status,log_ecommerce_edit_barang.id_ecommerce,status_edit_barang,status_edit_barang.status,ecommerce.nama,upload_barang.url
		FROM log_ecommerce_edit_barang 
		LEFT JOIN status_edit_barang ON log_ecommerce_edit_barang.status_edit_barang = status_edit_barang.id
		LEFT JOIN ecommerce ON log_ecommerce_edit_barang.id_ecommerce = ecommerce.id
		LEFT JOIN log_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		LEFT JOIN upload_barang ON (upload_barang.id_barang = log_edit_barang.id_barang AND upload_barang.id_ecommerce = log_ecommerce_edit_barang.id_ecommerce)
		WHERE `id_log_edit_barang` = '".$id_log."'";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function proses_edit_barang($id_log_edit_barang, $id_ecommerce, $status_edit_barang) {
		$query = "UPDATE log_ecommerce_edit_barang SET status_edit_barang = '".$status_edit_barang."' WHERE id_log_edit_barang = '".$id_log_edit_barang."' AND id_ecommerce = '".$id_ecommerce."'";
		//echo $query;
		$this->db->query($query);
	}
	
	public function total_listing() {
		$query = "SELECT count(1) jumlah_listing FROM (SELECT id,id_status,max(id_status) max_status, min(id_status) min_status
		FROM barang
		LEFT JOIN upload_barang ON barang.id = upload_barang.id_barang
		WHERE barang.status_aktif =1
		GROUP BY barang.id) X WHERE min_status = 1";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_checklist() {
		$query = "SELECT count(1) jumlah_checklist FROM (SELECT id,id_status,max(id_status) max_status, min(id_status) min_status
		FROM barang
		LEFT JOIN upload_barang ON barang.id = upload_barang.id_barang
		WHERE barang.status_aktif =1
		GROUP BY barang.id) X WHERE min_status = 2";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_listed() {
		$query = "SELECT count(1) jumlah_listed FROM (SELECT id,id_status,max(id_status) max_status, min(id_status) min_status
		FROM barang
		LEFT JOIN upload_barang ON barang.id = upload_barang.id_barang
		WHERE barang.status_aktif =1
		GROUP BY barang.id) X WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_edit() {
		$query = "SELECT count(1) jumlah_edit FROM (SELECT barang.id,status_edit_barang id_status,max(status_edit_barang) max_status, min(status_edit_barang) min_status
		FROM barang
		LEFT JOIN log_edit_barang ON barang.id = log_edit_barang.id_barang
		LEFT JOIN log_ecommerce_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		WHERE barang.status_aktif =1
		GROUP BY log_edit_barang.id) X WHERE min_status = 1";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_edit_checklist() {
		$query = "SELECT count(1) jumlah_checklist FROM (SELECT barang.id,status_edit_barang id_status,max(status_edit_barang) max_status, min(status_edit_barang) min_status
		FROM barang
		LEFT JOIN log_edit_barang ON barang.id = log_edit_barang.id_barang
		LEFT JOIN log_ecommerce_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		WHERE barang.status_aktif =1
		GROUP BY log_edit_barang.id) X WHERE min_status = 2";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_edited() {
		$query = "SELECT count(1) jumlah_edited FROM (SELECT barang.id,status_edit_barang id_status,max(status_edit_barang) max_status, min(status_edit_barang) min_status
		FROM barang
		LEFT JOIN log_edit_barang ON barang.id = log_edit_barang.id_barang
		LEFT JOIN log_ecommerce_edit_barang ON log_edit_barang.id = log_ecommerce_edit_barang.id_log_edit_barang
		WHERE barang.status_aktif =1
		GROUP BY log_edit_barang.id) X WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_delete() {
		$query = "SELECT count(1) jumlah_delete FROM (SELECT barang.id,status_hapus_barang id_status,max(status_hapus_barang) max_status, min(status_hapus_barang) min_status
		FROM barang
		LEFT JOIN log_hapus_barang ON barang.id = log_hapus_barang.id_barang
		LEFT JOIN log_ecommerce_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		GROUP BY log_hapus_barang.id) X WHERE min_status = 1";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_delete_checklist() {
		$query = "SELECT count(1) jumlah_checklist FROM (SELECT barang.id,status_hapus_barang id_status,max(status_hapus_barang) max_status, min(status_hapus_barang) min_status
		FROM barang
		LEFT JOIN log_hapus_barang ON barang.id = log_hapus_barang.id_barang
		LEFT JOIN log_ecommerce_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		GROUP BY log_hapus_barang.id) X WHERE min_status = 2";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function total_deleted() {
		$query = "SELECT count(1) jumlah_deleted FROM (SELECT barang.id,status_hapus_barang id_status,max(status_hapus_barang) max_status, min(status_hapus_barang) min_status
		FROM barang
		LEFT JOIN log_hapus_barang ON barang.id = log_hapus_barang.id_barang
		LEFT JOIN log_ecommerce_hapus_barang ON log_hapus_barang.id = log_ecommerce_hapus_barang.id_log_hapus_barang
		GROUP BY log_hapus_barang.id) X WHERE min_status != 1 AND min_status != 2 AND max_status = 3";
		//echo $query;
		return $this->db->query($query);
	}
	
}

?>
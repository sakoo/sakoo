<?php

class M_saldo extends CI_Model {
	//show piutang
	public function show_bank($id_bank) {
		$query = "SELECT id,nama,no_rekening,atas_nama FROM `bank` WHERE id = '".$id_bank."'";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function insert_saldo($id_bank,$jumlah,$jenis,$id_ecommerce,$id_transfer) {
		$query = "INSERT INTO saldo(id_bank,jumlah,jenis,id_ecommerce,id_transfer) VALUES ('".$id_bank."','".$jumlah."','".$jenis."','".$id_ecommerce."','".$id_transfer."')";
		//echo $query;
		$this->db->query($query);
	}
	
	public function show_saldo($id_bank) {
		$query = "SELECT sum(jumlah) FROM saldo WHERE id_bank = '".$id_bank."' AND jenis = '2'";
		//echo $query;
		return $this->db->query($query);
	}
	
	public function show_mutasi($id_bank) {
		$query = "SELECT * FROM (SELECT tanggal,transaksi,debet,kredit, @saldo:=@saldo+debet-kredit AS piutang FROM
				((SELECT waktu_transfer tanggal,concat('Transfer ke Seller Transaksi ',transaksi.no_trx,' (',ecommerce.nama,')') transaksi,jumlah_uang debet, '-' kredit FROM `pencairan_dana` 
				LEFT JOIN transaksi_seller ON pencairan_dana.id_transaksi_seller = transaksi_seller.id 
				LEFT JOIN transaksi ON transaksi_seller.id_transaksi = transaksi.id_transaksi 
				LEFT JOIN ecommerce ON transaksi.id_ecommerce = ecommerce.id
				WHERE pencairan_dana.bank = '".$id_bank."' AND pencairan_dana.status = 2 AND pencairan_dana.piutang = 0) 
				UNION
				(SELECT tanggal,CONCAT('Uang masuk dari ',ecommerce.nama,' no transfer ',saldo.id_transfer),'-',jumlah
				FROM saldo LEFT JOIN ecommerce ON saldo.id_ecommerce = ecommerce.id
				WHERE saldo.id_bank = '".$id_bank."' AND saldo.jenis = '2')) x JOIN (SELECT @saldo:=0) y
				ORDER BY tanggal ASC) z 
				UNION select '','','','SALDO AKHIR',@saldo";
		//echo $query;
		return $this->db->query($query);
	}
	
}

?>
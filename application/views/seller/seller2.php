<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Seller
	</h1>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Report</a></li>
		<li class="active">Seller</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-purple"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BRI</span>
				<span class="info-box-number">90<small>%</small></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue"><i class=""></i></span>
			<div class="info-box-content">
				<span class="info-box-text">MANDIRI</span>
				<span class="info-box-number">760</span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-orange"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BNI</span>
				<span class="info-box-number">760</span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BCA</span>
				<span class="info-box-number">760</span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="overflow: auto;">
				<table id="seller_tabel" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>Nama Seller</th>
						<th>Email</th>
						<th>No HP</th>
						<th>Rank</th>
						<th>Jumlah Barang Listing</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Nama Seller</th>
						<th>Email</th>
						<th>No HP</th>
						<th>Rank</th>
						<th>Jumlah Barang Listing</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($record->result() as $r) { ?>
						<tr class="gradeU">
							<td><?php echo "tes"; ?></td>
							<td><?php echo $r->email; ?></td>
							<td><?php echo ""; ?></td>
							<td><?php echo $r->rank; ?></td>
							<td><?php echo $r->jumlah_listing; ?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- /.modal -->
</section><!-- /.content -->
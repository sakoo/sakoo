<!-- Content Header (Page header) -->
<?php 
foreach ($bri->result() as $r) {
	$piutang_bri = (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00";
}
foreach ($mandiri->result() as $r) {
	$piutang_mandiri = (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00";
}
foreach ($bni->result() as $r) {
	$piutang_bni = (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00";
}
foreach ($bca->result() as $r) {
	$piutang_bca = (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00";
}
	
?>
<section class="content-header">
	<h1>
		Saldo Piutang
	</h1>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Report</a></li>
		<li class="active">Saldo</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-purple"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BRI</span>
				<span class="info-box-number"><?php echo $piutang_bri;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue"><i class=""></i></span>
			<div class="info-box-content">
				<span class="info-box-text">MANDIRI</span>
				<span class="info-box-number"><?php echo $piutang_mandiri;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-orange"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BNI</span>
				<span class="info-box-number"><?php echo $piutang_bni;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">BCA</span>
				<span class="info-box-number"><?php echo $piutang_bca;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="overflow: auto;">
				<table id="pencairan_saldo" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>Bank</th>
						<th>Piutang</th>
						<th>Action</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Bank</th>
						<th>Piutang</th>
						<th>Action</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($bri->result() as $r) { ?>
						<tr class="gradeU">
							<td>BRI <form action='saldo/mutasi' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='1' readonly><button type='submit' id='mutasi' name='mutasi' class='btn btn-info'>Mutasi</button></form></td>
							<td style="text_align:right;"><?php echo (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00"; ?></td>
							<?php
								$role = $this->session->userdata('role');
								if($r->jumlah) {
									if($role == 'Administrator') {
										echo "<td><form action='saldo/transfer' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='1' readonly><button type='submit' id='transfer' name='transfer' class='btn btn-warning'>Transfer</button></form></td>";
									}
									else {
										echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
									}
								}
								else {
									echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
								}
							?>
						</tr>
					<?php } ?>
					<?php foreach ($mandiri->result() as $r) { ?>
						<tr class="gradeU">
							<td>MANDIRI <form action='saldo/mutasi' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='2' readonly><button type='submit' id='mutasi' name='mutasi' class='btn btn-info'>Mutasi</button></form></td>
							<td style="text_align:right;"><?php echo (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00"; ?></td>
							<?php
								if($r->jumlah) {
									if($role == 'Administrator') {
										echo "<td><form action='saldo/transfer' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='2' readonly><button type='submit' id='transfer' name='transfer' class='btn btn-warning'>Transfer</button></form></td>";
									}
									else {
										echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
									}
								}
								else {
									echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
								}
							?>
						</tr>
					<?php } ?>
					<?php foreach ($bni->result() as $r) { ?>
						<tr class="gradeU">
							<td>BNI <form action='saldo/mutasi' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='3' readonly><button type='submit' id='mutasi' name='mutasi' class='btn btn-info'>Mutasi</button></form></td>
							<td style="text_align:right;"><?php echo (is_numeric($r->jumlah))?number_format($r->jumlah, 2):"0.00"; ?></td>
							<?php
								if($r->jumlah) {
									if($role == 'Administrator') {
										echo "<td><form action='saldo/transfer' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='3' readonly><button type='submit' id='transfer' name='transfer' class='btn btn-warning'>Transfer</button></form></td>";
									}
									else {
										echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
									}
								}
								else {
									echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
								}
							?>
						</tr>
					<?php } ?>
					<?php foreach ($bca->result() as $r) { ?>
						<tr class="gradeU">
							<td>BCA <form action='saldo/mutasi' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='4' readonly><button type='submit' id='mutasi' name='mutasi' class='btn btn-info'>Mutasi</button></form></td>
							<td style="text_align:right;"><?php echo (is_numeric($r->jumlah))?number_format($r->jumlah, 2):$r->jumlah; ?></td>
							<?php
								if($r->jumlah) {
									if($role == 'Administrator') {
										echo "<td><form action='saldo/transfer' method='POST'><input type='hidden' class='form-control' id='bank' name='bank' value='4' readonly><button type='submit' id='transfer' name='transfer' class='btn btn-warning'>Transfer</button></form></td>";
									}
									else {
										echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
									}
								}
								else {
									echo "<td><button type='submit' name='transfer' class='btn btn-warning' disabled>Transfer</button></td>";
								}
							?>
						</tr>
					<?php } ?>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- /.modal -->
</section><!-- /.content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Transfer</h3>
		</div>
		<?php
		//parameter 
		foreach ($record->result() as $r) { 
		    $id_bank = $r->id;
			$nama_bank = $r->nama;
			$no_rekening = $r->no_rekening;
			$atas_nama = $r->atas_nama;
		}
		foreach ($piutang->result() as $p) { 
		    $piutang = $p->jumlah;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="<?php echo base_url('saldo/proses_completed') ?>" method="POST">
			<?php
			//set peringatan
			if (validation_errors() || $this->session->flashdata('result_login')) {
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Warning!</strong>
					<?php echo validation_errors(); ?>
					<?php echo $this->session->flashdata('result_login'); ?>
				</div>    
			<?php } ?>
			<div class="box-body">
				<div class="form-group">
					<label for="bank" class="col-sm-3 control-label">Bank</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="bank" name="bank" value="<?php echo $nama_bank;?>" readonly>
						<input type="hidden" class="form-control" id="idBank" name="idBank" value="<?php echo $id_bank;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="noRekening" class="col-sm-3 control-label">No Rekening</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="noRekening" name="noRekening" value="<?php echo $no_rekening;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#noRekening">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="atasNama" class="col-sm-3 control-label">Atas Nama</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="atasNama" name="atasNama" value="<?php echo $atas_nama;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="piutang" class="col-sm-3 control-label">Piutang</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="piutang" name="piutang" value="<?php echo $piutang;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#piutang">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="ecommerce" class="col-sm-3 control-label">Ecommerce</label>
					<div class="col-sm-4">
						<select class="form-control" id="ecommerce" name="ecommerce">
							<option>--Pilih Ecommerce--</option>
						<?php
							$eksekusi = $this->m_email->show_all_ecommerce();
							foreach ($eksekusi->result_array() as $data) {
								$id = $data['id'];
								$nama = $data['nama'];
								echo "<option value='".$id."'>".$nama."</option>";
							}
						?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="jumlah" class="col-sm-3 control-label">Jumlah Transfer</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="jumlah" name="jumlah">
					</div>
				</div>
				<div class="form-group">
					<label for="idTransfer" class="col-sm-3 control-label">ID Transfer</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="idTransfer" name="idTransfer">
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			 <div class="box-footer">
				<button type="submit" name="submit" class="btn btn-danger">Transfer Selesai</button>&nbsp &nbsp
				<?php echo anchor('saldo','Kembali',array('class'=>'btn btn-primary'))?>
			</div>
		</form>
		</div>
		<!-- /.box -->
		<!-- loading -->
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
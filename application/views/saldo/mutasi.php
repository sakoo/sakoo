<!-- Content Header (Page header) -->
<section class="content-header">
	<?php foreach ($record->result() as $r) { ?>
			<h1><?php echo "Mutasi BANK ".$r->nama ?></h1>
			<h4><?php echo "Nomor Rekening ".$r->no_rekening ?></h4>
			<h4><?php echo "Atas Nama ".$r->atas_nama ?></h4>
	<?php } ?>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Report</a></li>
		<li class="active">Saldo</li>
	</ol>
</section>
<!-- Main content -->
<div class="row">
	<div class="col-md-12">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="overflow: auto;">
				<table id="mutasi" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>Tanggal</th>
						<th>Transaksi</th>
						<th>Debet</th>
						<th>Kredit</th>
						<th>Piutang</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Tanggal</th>
						<th>Transaksi</th>
						<th>Debet</th>
						<th>Kredit</th>
						<th>Piutang</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($piutang->result() as $p) { ?>
						<tr class="gradeU">
							<td><?php echo $p->tanggal ?></td>
							<td><?php echo $p->transaksi ?></td>
							<td style="text-align:right;"><?php echo (is_numeric($p->debet))?number_format($p->debet, 2):$p->debet; ?></td>
							<td style="text-align:right;"><?php echo (is_numeric($p->kredit))?number_format($p->kredit, 2):$p->kredit; ?></td>
							<td style="text-align:right;"><?php echo (is_numeric($p->piutang))?number_format($p->piutang, 2):$p->piutang; ?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<!-- /.modal -->
</section><!-- /.content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Input Resi</h3>
		</div>
		<?php
		//parameter
		$empty_nomor_resi = 0;
		foreach ($record->result() as $r) { 
			$buyer = trim($r->buyer);
			$nama_ecommerce = $r->nama_ecommerce;
			$id_ecommerce = $r->id_ecommerce;
			$alamat_pengiriman = $r->alamat_pengiriman;
			$alamat_pengiriman = trim(preg_replace('#\h{2,}#m', " ", $alamat_pengiriman));
			$id_transaksi = $r->id_transaksi;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="<?php echo base_url();?>transaksi/proses_input_resi" method="POST">
			<div class="box-body">
				<div class="form-group">
					<label for="buyer" class="col-sm-2 control-label">Buyer</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="buyer" name="buyer" value="<?php echo $buyer;?>" readonly>
						<input type="hidden" class="form-control" id="idTransaksi" name="idTransaksi" value="<?php echo $id_transaksi;?>" readonly>
						<input type="hidden" class="form-control" id="idEcommerce" name="idEcommerce" value="<?php echo $id_ecommerce;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="namaEcommerce" class="col-sm-2 control-label">Toko Online</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaEcommerce" name="namaEcommerce" value="<?php echo $nama_ecommerce;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="alamatPengiriman" class="col-sm-2 control-label">Alamat Pengiriman</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="alamatPengiriman" rows="5" name="alamatPengiriman" readonly><?php echo $alamat_pengiriman;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="noResi" class="col-sm-2 control-label">No Resi</label>
					<div class="col-sm-10">
						<?php echo anchor('#','Add',array('id'=>'modal_add','class'=>'btn btn-success add','data-toggle'=>'modal','data-target'=>'#modal-input','data-transaksi-id'=>''.$id_transaksi.'')); ?>
					</div>
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
					
					
					 <?php foreach ($resi->result() as $r) {
						if($r->nomor_resi != "") { 
							echo "<table class='table'>";
							echo "<thead>";
							echo "<th>Seller</th>";
							echo "<th>Nama</th>";
							echo "<th>Harga Satuan</th>";
							echo "<th>Jumlah</th>";
							echo "<th>Harga Pembelian</th>";
							echo "<th>Url</th>";
							echo "</thead>";
							echo "<tbody>";
							$nomor_resi = $r->nomor_resi;
							echo "<br><label class='col-md-12 col-sm-12 col-xs-12 label label-success' control-label align='left'>Resi</label><label class='col-md-12 col-sm-12 col-xs-12' control-label align='left'>Nomor Resi : ". $nomor_resi."</label>&nbsp&nbsp&nbsp".anchor('#','Edit',array('id'=>'modal_edit','class'=>'btn btn-info edit','data-toggle'=>'modal','data-target'=>'#modal-edit','data-nomor-resi'=>''.$nomor_resi.'','data-ecommerce-id'=>''.$id_ecommerce.'','data-transaksi-id'=>''.$id_transaksi.''))."
							&nbsp&nbsp&nbsp".anchor('#','Delete',array('id'=>'modal_delete','class'=>'btn btn-danger delete','data-toggle'=>'modal','data-target'=>'#modal-delete','data-nomor-resi'=>''.$nomor_resi.'','data-ecommerce-id'=>''.$id_ecommerce.'','data-transaksi-id'=>''.$id_transaksi.''))."</div>";
							//berdasarkan seller param : $id_transaksi,$id_toko
							$eksekusi = $this->m_transaksi->show_edit_barang3($nomor_resi,$id_ecommerce);
							foreach ($eksekusi->result_array() as $data) {
								$nama_toko = $data['nama_toko'];
								$nama = $data['nama'];
								$harga_satuan = $data['harga_satuan'];
								$jumlah_pembelian = $data['jumlah_pembelian'];
								$harga_pembelian = $data['harga_pembelian'];
								$url = $data['url'];
								echo "<tr>";
								echo "<td>".$nama_toko."</td>";
								echo "<td>".$nama."</td>";
								echo "<td>".$harga_satuan."</td>";
								echo "<td>".$jumlah_pembelian."</td>";
								echo "<td>".$harga_pembelian."</td>";
								echo "<td>".$url."</td>";
								echo "</tr>";
							}
							echo "</tbody>";
							echo "</table>";
						}
						else {
							$empty_nomor_resi = 1;
							echo "<table class='table'>";
							echo "<thead>";
							echo "<th>Seller</th>";
							echo "<th>Nama</th>";
							echo "<th>Harga Satuan</th>";
							echo "<th>Jumlah</th>";
							echo "<th>Harga Pembelian</th>";
							echo "<th>Url</th>";
							echo "</thead>";
							echo "<tbody>";
							echo "<br><div class='col-md-12 col-sm-12 col-xs-12 label label-danger' control-label align='left'>Belum Ada Nomor Resi</div>";
							//berdasarkan seller param : $id_transaksi,$id_toko
							$eksekusi = $this->m_transaksi->show_edit_barang_no_resi($id_transaksi,$id_ecommerce);
							foreach ($eksekusi->result_array() as $data) {
								$nama_toko = $data['nama_toko'];
								$nama = $data['nama'];
								$harga_satuan = $data['harga_satuan'];
								$jumlah_pembelian = $data['jumlah_pembelian'];
								$harga_pembelian = $data['harga_pembelian'];
								$url = $data['url'];
								echo "<tr>";
								echo "<td>".$nama_toko."</td>";
								echo "<td>".$nama."</td>";
								echo "<td>".$harga_satuan."</td>";
								echo "<td>".$jumlah_pembelian."</td>";
								echo "<td>".$harga_pembelian."</td>";
								echo "<td>".$url."</td>";
								echo "</tr>";
							}
							echo "</tbody>";
							echo "</table>";
						}
					}
					//print_r($detail);
					?>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
			<?php
			if($empty_nomor_resi) {
				echo "<button type='submit' name='proses' class='btn btn-success disabled'>Ok</button>&nbsp &nbsp";
			}
			else {
				echo "<button type='submit' name='proses' class='btn btn-success'>Ok</button>&nbsp &nbsp";
			}
			?>
			<?php echo anchor('transaksi','Draft',array('class'=>'btn btn-primary'))?>
			</div>
		</form>
		</div>
		<!-- /.box -->
	</div>
	<!-- /.box -->
</div><!-- /.row -->

<div class="modal modal-success fade" id="modal-input">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Input Resi</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding">
				  <table class="table" id="tableList">
					<tr></tr>
					<tr>
					  <td>Nomor</td>
					  <td colspan="6"><input type="text" class="form-control" id="nomor_resi_i" name="nomor_resi_i" required></td>
					</tr>
					<tr>
					  <td colspan="7">List Barang</td>
					</tr>
					<tr>
					  <td>Select</td>
					  <td>Seller</td>
					  <td>Nama</td>
					  <td>Harga Satuan</td>
					  <td>Jumlah</td>
					  <td>Harga Pembelian</td>
					  <td>Url</td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_transaksi_i" name="id_transaksi_i" value="<?php echo $id_transaksi;?>"></input>
				<input type="hidden" class="form-control" id="id_ecommerce_i" name="id_ecommerce_i" value="<?php echo $id_ecommerce;?>"></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal_input">Add</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-info fade" id="modal-edit">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Edit Resi</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding" style="overflow: auto">
				  <table class="table" id="tableListEdit">
					<tr></tr>
					<tr>
					  <td>Nomor</td>
					  <td colspan="6"><input type="text" class="form-control" id="nomor_resi_e" name="nomor_resi_e" required></td>
					</tr>
					<tr>
					  <td colspan="7">List Barang</td>
					</tr>
					<tr>
					  <td>Select</td>
					  <td>Seller</td>
					  <td>Nama</td>
					  <td>Harga Satuan</td>
					  <td>Jumlah</td>
					  <td>Harga Pembelian</td>
					  <td>Url</td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_transaksi_e" name="id_transaksi_e" value="<?php echo $id_transaksi;?>"></input>
				<input type="hidden" class="form-control" id="id_ecommerce_e" name="id_ecommerce_e" value="<?php echo $id_ecommerce;?>"></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal_edit">Edit</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-danger fade" id="modal-delete">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Hapus Resi</h4>
			</div>
			<div class="modal-body"style="overflow: auto">
				<div class="box-body no-padding">
				  Apakah anda yakin menghapus resi ini?
				</div>
				<input type="hidden" class="form-control" id="nomor_resi_d" name="nomor_resi_d" value=""></input>
				<input type="hidden" class="form-control" id="id_ecommerce_d" name="nomor_resi_d" value=""></input>
				<input type="hidden" class="form-control" id="id_transaksi_d" name="nomor_resi_d" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Tidak</button>
			<button type="button" class="btn btn-outline" id="submit_modal_delete">Ya</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

</section><!-- /.content -->

<script type="text/javascript">
$(document).ready(function(){
	
    $(function () {	
        $(".btn.btn-info.edit").on("click", function() {
            var nomor_resi = $(this).data('nomor-resi');
			var id_ecommerce = $(this).data('ecommerce-id');
			var id_transaksi = $(this).data('transaksi-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>/transaksi/modal_edit_resi',
				dataType: "json",
				data:"nomor_resi=" +nomor_resi+"&id_ecommerce="+id_ecommerce+"&id_transaksi="+id_transaksi,
				success:function(data){
					var out = null;
					out = "<tbody><tr><td>Nomor</td><td colspan='6'><input type='text' class='form-control' id='nomor_resi_e' name='nomor_resi_e' value="+nomor_resi+" required></td></tr><tr><td colspan='7'>List Barang</td></tr><tr><td>Select</td><td>Seller</td><td>Nama</td><td>Harga Satuan</td><td>Jumlah</td><td>Harga Pembelian</td><td>Url</td></tr>";
					$.each(data, function (index, object) {
						//dataArray.push([value["nama"].toString(), value["nama_toko"] ]);
						if(object.nomor_resi == 1) {
							out += '<tr><td><input name=id_detail_transaksi id=id_detail_transaksi type=checkbox value='+object.id_detail_transaksi+' checked="checked"></td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td>'+object.harga_satuan+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.harga_pembelian+'</td><td>'+object.url+'</td></tr>';
						}
						else if(object.nomor_resi == 0) {
							out += '<tr><td><input name=id_detail_transaksi id=id_detail_transaksi type=checkbox value='+object.id_detail_transaksi+'></td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td>'+object.harga_satuan+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.harga_pembelian+'</td><td>'+object.url+'</td></tr>';
						}	
					});
					out+= '</tbody>';
					$('#tableListEdit').empty();
					$('#tableListEdit').append($('<table></table>').attr('class', 'table'));
					$('#tableListEdit').append(out);
					
					var out = null;
				}
			});
        });
		$(".btn.btn-danger.delete").on("click", function() {
            var nomor_resi = $(this).data('nomor-resi');
			var id_ecommerce = $(this).data('ecommerce-id');
			var id_transaksi = $(this).data('transaksi-id');
			$(".modal-body #nomor_resi_d").val(nomor_resi);
			$(".modal-body #id_ecommerce_d").val(id_ecommerce);
			$(".modal-body #id_transaksi_d").val(id_transaksi);
        });
		$(".btn.btn-success.add").on("click", function() {
            var id_transaksi = $(this).data('transaksi-id');
			var id_ecommerce = $("#idEcommerce").val();
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>/transaksi/modal_add_resi',
				dataType: "json",
				data:"id_transaksi=" +id_transaksi+"&id_ecommerce="+id_ecommerce,
				success:function(data){
					//console.log(data);
					//var obj = JSON.parse(data);
					var out = null;
					out = "<tbody><tr><td>Nomor</td><td colspan='6'><input type='text' class='form-control' id='nomor_resi_i' name='nomor_resi_i' required></td></tr><tr><td colspan='7'>List Barang</td></tr><tr><td>Select</td><td>Seller</td><td>Nama</td><td>Harga Satuan</td><td>Jumlah</td><td>Harga Pembelian</td><td>Url</td></tr>";
					$.each(data, function (index, object) {
						//dataArray.push([value["nama"].toString(), value["nama_toko"] ]);
						out += '<tr><td><input name=id_detail_transaksi id=id_detail_transaksi type=checkbox value='+object.id_detail_transaksi+'></td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td>'+object.harga_satuan+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.harga_pembelian+'</td><td>'+object.url+'</td></tr>';
					});
					out+= '</tbody>';
					$('#tableList').empty();
					$('#tableList').append($('<table></table>').attr('class', 'table'));
					$('#tableList').append(out);	
					var out = null;
				}
			});
        });
		
		$("#submit_modal_input").click(function() {
			var id_transaksi = $("#id_transaksi_i").val();
			var nomor_resi = $("#nomor_resi_i").val();
			var id_ecommerce = $("#id_ecommerce_i").val();
			var id_detail_transaksis = [];
			$("input[name='id_detail_transaksi']:checked").each(function ()
			{
				id_detail_transaksis.push(parseInt($(this).val()));
			});
			var id_detail_transaksi = JSON.stringify(id_detail_transaksis);
			//var id_detail_transaksi = $("#id_detail_transaksi_i").val();
			//console.log(id_transaksi);
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>/transaksi/modal_input_resi_ok',
				data:"id_transaksi=" +id_transaksi+"&id_detail_transaksi="+id_detail_transaksi+"&nomor_resi="+nomor_resi,
				success: function(message){
					$("#modal-input").modal('hide');
					$(location).attr('href','<?php echo base_url();?>/transaksi/input_resi/'+id_transaksi+'/'+id_ecommerce);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Nomor Resi Tidak Boleh Kosong.");
					}
				}
			});
		});
		
		$("#submit_modal_edit").click(function() {
			var id_transaksi = $("#id_transaksi_e").val();
			var nomor_resi = $("#nomor_resi_e").val();
			var id_ecommerce = $("#id_ecommerce_e").val();
			var id_detail_transaksis = [];
			$("input[name='id_detail_transaksi']:checked").each(function ()
			{
				id_detail_transaksis.push(parseInt($(this).val()));
			});
			var id_detail_transaksi = JSON.stringify(id_detail_transaksis);
			//var id_detail_transaksi = $("#id_detail_transaksi_i").val();
			//console.log(id_transaksi);
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>/transaksi/modal_edit_resi_ok',
				data:"id_transaksi=" +id_transaksi+"&id_detail_transaksi="+id_detail_transaksi+"&nomor_resi="+nomor_resi,
				success: function(message){
					$("#modal-edit").modal('hide');
					$(location).attr('href','<?php echo base_url();?>/transaksi/input_resi/'+id_transaksi+'/'+id_ecommerce);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Nomor Resi Tidak Boleh Kosong.");
					}
				}
			});
		});
		//belum dipake
		$("#submit_modal_delete").click(function() {
			var nomor_resi = $("#nomor_resi_d").val();
			var id_ecommerce = $("#id_ecommerce_d").val();
			var id_transaksi = $("#id_transaksi_d").val();
			//console.log(id_transaksi);
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>/transaksi/modal_delete_resi',
				data:"nomor_resi=" +nomor_resi,
				success: function(message){
					$("#modal-delete").modal('hide');
					$(location).attr('href','<?php echo base_url();?>/transaksi/input_resi/'+id_transaksi+'/'+id_ecommerce);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Error Dalam Proses Menghapus.");
					}
					else {
						alert("Error Dalam Proses Menghapus.");
					}
				}
			});
		});
    });
	
});
</script>
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Konfirmasi</h3>
		</div>
		<?php
		//parameter
		foreach ($record->result() as $r) { 
			$id_transaksi = $r->id_transaksi;
			$id_trx = $r->id_trx;
			$no_trx = $r->no_trx;
			$status_trx = $r->status_trx;
			$tanggal_pemesanan = $r->tanggal_pemesanan;
			$tanggal_email = $r->tanggal_email;
			$id_ecommerce = $r->id_ecommerce;
			$jasa_pengiriman = $r->jasa_pengiriman;
			$biaya_kirim = $r->biaya_kirim;
			$harga_total = $r->harga_total;
			$harga_pembayaran = $r->harga_pembayaran;
			$asuransi = $r->asuransi;
			$buyer = $r->buyer;
			$alamat_pengiriman = $r->alamat_pengiriman;
			$no_hp = $r->no_hp;
			$keterangan = $r->keterangan;
			$status_transaksi = $r->status_transaksi;
			$checked = $r->checked;
			$no_resi = $r->no_resi;
			$status = $r->status; 
			$jumlah = $r->jumlah; 
			$nama_ecommerce = $r->nama_ecommerce;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="update_stok/proses_updating" method="POST">
			<div class="box-body">
				<div class="form-group">
					<label for="nomorTransaksi" class="col-sm-2 control-label">Nomor Transaksi</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nomorTransaksi" name="nomorTransaksi" value="<?php echo $no_trx;?>" readonly>
						<input type="hidden" class="form-control" id="idTransaksi" name="idTransaksi" value="<?php echo $id_transaksi;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="namaEcommerce" class="col-sm-2 control-label">Toko Online</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaEcommerce" name="namaEcommerce" value="<?php echo $nama_ecommerce;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="buyer" class="col-sm-2 control-label">Pembeli</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="buyer" name="buyer" value="<?php echo $buyer;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="noHp" class="col-sm-2 control-label">No HP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="noHp" name="noHp" value="<?php echo $no_hp;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="alamatPengiriman" class="col-sm-2 control-label">Alamat Pengiriman</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="alamatPengiriman" name="alamatPengiriman" value="<?php echo $alamat_pengiriman;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="tanggalPemesanan" class="col-sm-2 control-label">Waktu Pembelian</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="tanggalPemesanan" name="tanggalPemesanan" value="<?php echo $tanggal_pemesanan;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="jasaPengiriman" class="col-sm-2 control-label">Jasa Pengiriman</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="jasaPengiriman" name="jasaPengiriman" value="<?php echo $jasa_pengiriman;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="pembelian" class="col-sm-2 control-label">Pembelian</label>
					
					    <?php foreach ($detail->result() as $d) {
							    $id_barang = $d->id_barang;
								$eksekusi = $this->m_transaksi->show_url($id_barang,$id_ecommerce);
								foreach ($eksekusi->result_array() as $data) {
									$url = $data['url'];
									$nama = $data['nama'];
								}
						        echo "<div class='col-md-12 col-sm-12 col-xs-12'>";
								echo "<div class='col-md-3 col-sm-3 col-xs-12'> <img src=\"".base_url('assets/img/barang/'.$d->foto.'')."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/></div>";
								echo "<div class='col-md-6 col-sm-6 col-xs-12'>";
								echo "<span class='col-md-6 col-sm-6 col-xs-6'>Seller : </span><span col-md-6 col-sm-6 col-xs-6 align='right'>".$nama."</span><br>";
								echo "<span class='col-md-6 col-sm-6 col-xs-6'>Harga Satuan : </span><span col-md-6 col-sm-6 col-xs-6 align='right'>".$d->harga_satuan."</span><br>";
								echo "<span class='col-md-6 col-sm-6 col-xs-6'>Jumlah : </span><span col-md-6 col-sm-6 col-xs-6 align='right' align='right'>".$d->jumlah_pembelian."</span><br>";
								echo "<span class='col-md-6 col-sm-6 col-xs-6'>Harga Pembelian : </span><span col-md-6 col-sm-6 col-xs-6 align='right' align='right'>".$d->harga_satuan*$d->jumlah_pembelian."</span><br>";
								echo "<span class='col-md-6 col-sm-6 col-xs-6'>Keterangan : </span><span col-md-6 col-sm-6 col-xs-6 align='right' align='right'>".$d->keterangan."</span><br>";
								echo "<span class='col-md-6 col-sm-6 col-xs-6'>Url : </span><span col-md-6 col-sm-6 col-xs-6 align='right' align='right'>".$url."</span><br>";
								echo "</div>";
								echo "</div>";
								echo "<div class='col-md-12 col-sm-12 col-xs-12'>&nbsp</div>";
							}
							//print_r($detail);
						?>
				</div>
				<div class="form-group">
					<label for="hargaTotal" class="col-sm-2 control-label">Harga Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaTotal" name="hargaTotal" value="<?php echo $harga_total;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="biayaKirim" class="col-sm-2 control-label">Biaya Pengiriman</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="biayaKirim" name="biayaKirim" value="<?php echo $biaya_kirim;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaPembayaran" class="col-sm-2 control-label">Total Pembayaran</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaPembayaran" name="hargaPembayaran" value="<?php echo $harga_pembayaran;?>" readonly>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			 <div class="box-footer">
				<button type="submit" name="proses" class="btn btn-success">Proses</button>&nbsp &nbsp
				<button type="submit" name="tolak" class="btn btn-danger">Tolak</button>&nbsp &nbsp
				<?php echo anchor('transaksi','Kembali',array('class'=>'btn btn-primary'))?>
			</div>
		</form>
		</div>
		<!-- /.box -->
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
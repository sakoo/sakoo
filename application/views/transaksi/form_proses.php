<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Konfirmasi</h3>
		</div>
		<?php
		//parameter
		$belum_konfirmasi = 0;
		$biaya_kirim_total_barang = 0;
		foreach ($record->result() as $r) { 
			$id_transaksi = $r->id_transaksi;
			$id_trx = $r->id_trx;
			$no_trx = $r->no_trx;
			$status_trx = $r->status_trx;
			$tanggal_pemesanan = $r->tanggal_pemesanan;
			$tanggal_email = $r->tanggal_email;
			$id_ecommerce = $r->id_ecommerce;
			$jasa_pengiriman = $r->jasa_pengiriman;
			$biaya_kirim = $r->biaya_kirim;
			$harga_total = $r->harga_total;
			$harga_total_barang = $r->harga_total_barang;
			$harga_pembayaran = $r->harga_pembayaran;
			$harga_pembayaran_barang = $r->harga_pembayaran_barang;
			$asuransi = $r->asuransi;
			$buyer = $r->buyer;
			$alamat_pengiriman = $r->alamat_pengiriman;
			$alamat_pengiriman = preg_replace('#\h{2,}#m', " ", $alamat_pengiriman);
			$no_hp = $r->no_hp;
			$keterangan = $r->keterangan;
			$status_transaksi = $r->status_transaksi;
			$checked = $r->checked;
			$status = $r->status; 
			$jumlah = $r->jumlah; 
			$nama_ecommerce = $r->nama_ecommerce;
			$multi_seller = $r->multi_seller;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="transaksi/proses_konfirmasi" method="POST">
			<div class="box-body">
				<div class="form-group">
					<label for="nomorTransaksi" class="col-sm-2 control-label">Nomor Transaksi</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nomorTransaksi" name="nomorTransaksi" value="<?php echo $no_trx;?>" readonly>
						<input type="hidden" class="form-control" id="idTransaksi" name="idTransaksi" value="<?php echo $id_transaksi;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#nomorTransaksi">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="namaEcommerce" class="col-sm-2 control-label">Toko Online</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaEcommerce" name="namaEcommerce" value="<?php echo $nama_ecommerce;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="buyer" class="col-sm-2 control-label">Pembeli</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="buyer" name="buyer" value="<?php echo $buyer;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="noHp" class="col-sm-2 control-label">No HP</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="noHp" name="noHp" value="<?php echo $no_hp;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="alamatPengiriman" class="col-sm-2 control-label">Alamat Pengiriman</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="alamatPengiriman" rows="5" name="alamatPengiriman" readonly><?php echo $alamat_pengiriman;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="tanggalPemesanan" class="col-sm-2 control-label">Waktu Pembelian</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="tanggalPemesanan" name="tanggalPemesanan" value="<?php echo $tanggal_pemesanan;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="jasaPengiriman" class="col-sm-2 control-label">Jasa Pengiriman</label>
					<div class="col-sm-10">
					    <?php
							if($id_ecommerce == 4)
							{
								echo "<select class='form-control' id='jasaPengiriman' name='jasaPengiriman'>";
								echo "<option value='0'>--Pilih Jasa Pengiriman--</option>";
								$eksekusi = $this->m_transaksi->show_jasa_pengiriman();
								foreach ($eksekusi->result_array() as $data) {
									$id = $data['id'];
									$nama = $data['nama'];
									if($jasa_pengiriman == $nama) {
										echo "<option value='".$nama."' selected>".$nama."</option>";
									}
									else {
										echo "<option value='".$nama."'>".$nama."</option>";
									}
								}
								echo "</select>";
								echo anchor('#','Save',array('id'=>'button_edit','class'=>'btn btn-info edit2','data-transaksi-id'=>''.$id_transaksi.''));
							}
							else {
								echo "<input type='text' class='form-control' id='tanggalPemesanan' name='tanggalPemesanan' value='".$jasa_pengiriman."' readonly>";
							}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="pembelian" class="col-sm-2 control-label">Pembelian </label>
					<div class="col-sm-10">
						<?php echo anchor('#','Add',array('id'=>'modal_add','class'=>'btn btn-success add','data-toggle'=>'modal','data-target'=>'#modal-input','data-transaksi-id'=>''.$id_transaksi.'')); ?>
					</div>
					
					    <?php foreach ($seller->result() as $s) {
							    $id_toko = $s->id_toko;
								$id_transaksi_seller = $s->id;
								$biaya_kirim_seller = $s->biaya_kirim_seller;
								$biaya_kirim_total_barang += $biaya_kirim_seller;
								echo "<br><div class='col-md-12 col-sm-12 col-xs-12'><span class='col-md-6 col-sm-6 col-xs-6' style='text-align:left'><br><h4><strong>Seller : ". $s->nama."</strong></h4></span></div>";
								//if($multi_seller > 1) {
									echo "<br><label class='col-md-2 col-sm-2 col-xs-2' style='text-align:right'><strong>Biaya Kirim </strong></label><div class='col-md-8 col-sm-6 col-xs-6'><input type='text' class='form-control' id='input-".$id_toko."' name='jasaPengiriman' value='".$biaya_kirim_seller."'>";
									echo anchor('#','Save',array('id'=>'button_update','class'=>'btn btn-success update','data-transaksi_seller-id'=>''.$id_transaksi_seller.'','data-toko-id'=>''.$id_toko.'','data-biaya-kirim-awal'=>''.$biaya_kirim_seller.'','data-ecommerce-id'=>''.$id_ecommerce.''));
									if($id_ecommerce == 4) {
										echo " <span style='color:red'>* Khusus Shopee akan mengupdate total pembayaran di Ecommerce</span>";
									}
									echo "</div>";
								//}
								//else {
								//	echo "<br><label class='col-md-2 col-sm-2 col-xs-2' style='text-align:right'><strong>Biaya Kirim </strong></label><div class='col-md-6 col-sm-6 col-xs-6'><input type='text' class='form-control' id='jasaPengiriman' name='jasaPengiriman' value='".$biaya_kirim."' readonly></div>";
								//}
								//berdasarkan seller param : $id_transaksi,$id_toko
								$eksekusi = $this->m_transaksi->show_detail_transaksi_seler($id_transaksi,$id_toko);
								foreach ($eksekusi->result_array() as $data) {
									$id_barang = $data['id_barang'];
									$id_detail_transaksi = $data['id_detail_transaksi'];
									$status_konfirmasi = $data['status_konfirmasi'];
									$status_konfirmasi_txt = "";
									if ($status_konfirmasi == 0) {
										$status_konfirmasi_txt = "<div class='label label-info'>Belum Konfirmasi</div>";
										$belum_konfirmasi = 1;
									}
									elseif($status_konfirmasi == 1) {
										$status_konfirmasi_txt = "<div class='label label-success'>Proses</div>";
									}
									elseif($status_konfirmasi == 2) {
										$status_konfirmasi_txt = "<div class='label label-danger'>Tolak</div>";
									}
									$eksekusi = $this->m_transaksi->show_url($id_barang,$id_ecommerce);
									foreach ($eksekusi->result_array() as $data2) {
										$url = $data2['url'];
										$nama = $data2['nama'];
									}
									echo "<div class='col-md-12 col-sm-12 col-xs-12'>";
									echo "<div class='col-md-2 col-sm-3 col-xs-12'> <img src=\"".$data['foto']."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/></div>";
									echo "<div class='col-md-10 col-sm-9 col-xs-12'>";
									echo "<div class='grid'><div class='row'><div class='col-md-5 col-sm-5 col-xs-5' style='display:block;'>Nama : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'>".$data['nama']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Harga Satuan : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left' style='background-color: #ff4444'>".$data['harga_satuan']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Harga Satuan Ecommerce: </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'>".$data['harga_satuan_ecommerce']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Jumlah : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'>".$data['jumlah_pembelian']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Harga Pembelian : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left' style='background-color: #ff4444'>".$data['harga_satuan']*$data['jumlah_pembelian']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Harga Pembelian Ecommerce : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'>".$data['harga_satuan_ecommerce']*$data['jumlah_pembelian']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Keterangan : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'>".$data['keterangan']."</div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Url : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'><a href='".$url."' style='color:black;' target='_blank'>".$url."</a></div></div>";
									echo "<div class='row'><div class='col-md-5 col-sm-5 col-xs-5'>Status Konfirmasi : </div><div class='col-md-7 col-sm-7 col-xs-7' align='left'>".$status_konfirmasi_txt."</div></div></div><br>";
									echo "<div class='col-md-3 col-sm-6 col-xs-6'></div>
									
									".anchor('#','Edit',array('id'=>'modal_edit','class'=>'btn btn-info edit','data-toggle'=>'modal','data-target'=>'#modal-edit','data-detail_transaksi-id'=>''.$id_detail_transaksi.'','data-ecommerce-id'=>''.$id_ecommerce.''))."
									
									&nbsp".anchor('#','Delete',array('id'=>'modal_delete','class'=>'btn btn-danger delete','data-toggle'=>'modal','data-target'=>'#modal-delete','data-detail_transaksi-id'=>''.$id_detail_transaksi.'','data-ecommerce-id'=>''.$id_ecommerce.''));
									//if() {
									//}
									echo "</div>";
									echo "</div>";
									echo "<div class='col-md-12 col-sm-12 col-xs-12'>&nbsp</div>";
								}
							}
							//print_r($detail);
						?>
				</div>
				<div class="form-group">
					<label for="hargaTotal" class="col-sm-2 control-label">Total Harga Pembelian</label>
					<div class="col-sm-10" style='background-color: #ff4444'>
						<input type="text" class="form-control" id="hargaTotal" name="hargaTotal" value="<?php echo $harga_total_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaTotal" class="col-sm-2 control-label">Total Harga Pembelian Ecommerce</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaTotal" name="hargaTotal" value="<?php echo $harga_total;?>">
						<?php
							echo anchor('#','Save',array('id'=>'button_update','class'=>'btn btn-success update_pembelian_ecommerce','data-transaksi-id'=>''.$id_transaksi.'','data-harga-total-awal'=>''.$harga_total.''));
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="biayaKirim" class="col-sm-2 control-label">Biaya Pengiriman</label>
					<div class="col-sm-10" style='background-color: #ff4444'>
						<input type="text" class="form-control" id="biayaKirim" name="biayaKirim" value="<?php echo $biaya_kirim_total_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="biayaKirimEcommerce" class="col-sm-2 control-label">Biaya Pengiriman Ecommerce</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="biayaKirimEcommerce" name="biayaKirimEcommerce" value="<?php echo $biaya_kirim;?>">
						<?php
							echo anchor('#','Save',array('id'=>'button_update','class'=>'btn btn-success update_pengiriman_ecommerce','data-transaksi-id'=>''.$id_transaksi.'','data-biaya-kirim-awal'=>''.$biaya_kirim.''));
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaPembayaran" class="col-sm-2 control-label">Total Pembayaran</label>
					<div class="col-sm-10" style='background-color: #ff4444'>
						<input type="text" class="form-control" id="hargaPembayaran" name="hargaPembayaran" value="<?php echo $harga_pembayaran_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaPembayaran" class="col-sm-2 control-label">Total Pembayaran Ecommerce</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaPembayaranEcommerce" name="hargaPembayaranEcommerce" value="<?php echo $harga_pembayaran;?>">
						<?php
							echo anchor('#','Save',array('id'=>'button_update','class'=>'btn btn-success update_total_ecommerce','data-transaksi-id'=>''.$id_transaksi.'','data-harga-pembayaran-awal'=>''.$harga_pembayaran.''));
						?>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			 <div class="box-footer">
				<?php
					if($status_transaksi != 6) {
						if($belum_konfirmasi) {
							echo "<button type='submit' name='proses' class='btn btn-success' disabled>Proses</button>&nbsp &nbsp";
							echo "<button type='submit' name='tolak' class='btn btn-danger' disabled>Tolak</button>&nbsp &nbsp";
						}
						else {
							echo "<button type='submit' name='proses' class='btn btn-success'>Proses</button>&nbsp &nbsp";
							echo "<button type='submit' name='tolak' class='btn btn-danger'>Tolak</button>&nbsp &nbsp";
						}
					}
					else {
						echo "<button type='submit' name='lanjut' class='btn btn-success'>Transaksi Diteruskan</button>&nbsp &nbsp";
					}
				?>
				
				<?php echo anchor('transaksi','Kembali',array('class'=>'btn btn-primary'))?>
			</div>
		</form>
		</div>
		<!-- /.box -->
	</div>
	<!-- /.box -->
</div><!-- /.row -->

<div class="modal modal-success fade" id="modal-input">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Tambah Pembelian Barang</h4>
			</div>
			<div class="modal-body">
				<div class="box-body no-padding">
				  <table class="table">
					<tr>
					  <td>Nama</td>
					  <td><input type="text" class="form-control" id="nama_i" name="nama_i" required></td>
					</tr>
					<tr>
					  <td>Seller</td>
					  <td><div id="seller_i"></div></td>
					</tr>
					<tr>
					  <td>Harga Satuan</td>
					  <td><div id="satuan_i" style="background-color: #ff4444"></div></td>
					</tr>
					<tr>
					  <td>Harga Satuan Ecommerce</td>
					  <td><input type="number" class="form-control" id="satuan_i_ecommerce" name="satuan_i_ecommerce" required></td>
					</tr>
					<tr>
					  <td>Stok</td>
					  <td><div id="stok_i"></div></td>
					</tr>
					<tr>
					  <td>Jumlah</td>
					  <td><input type="number" class="form-control" id="jumlah_i" name="jumlah_i" required></td>
					</tr>
					<tr>
					  <td>Harga Pembelian</td>
					  <td><div id="pembelian_i" style="background-color: #ff4444"></div></td>
					</tr>
					<tr>
					  <td>Harga Pembelian Ecommerce</td>
					  <td><div id="pembelian_i_ecommerce"></div></td>
					</tr>
					<tr>
					  <td>Url</td>
					  <td><div id="url_i"></div></td>
					</tr>
					<tr>
					  <td>Keterangan</td>
					  <td><textarea class="form-control" rows="3" id="ket_i" name="ket_i"></textarea></td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_transaksi_i" name="id_transaksi_i" value="<?php echo $id_transaksi;?>"></input>
				<input type="hidden" class="form-control" id="id_barang_i" name="id_barang_i" value=""></input>
				<input type="hidden" class="form-control" id="id_toko_i" name="id_toko_i" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal_input">Save</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-info fade" id="modal-edit">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Edit Pembelian Barang</h4>
			</div>
			<div class="modal-body">
				<div class="box-body no-padding">
				  <table class="table">
					<tr>
					  <td>Nama</td>
					  <td><div id="nama_e"></div></td>
					</tr>
					<tr>
					  <td>Seller</td>
					  <td><div id="seller_e"></div></td>
					</tr>
					<tr>
					  <td>Harga Satuan</td>
					  <td><div id="satuan_e" style="background-color: #ff4444"></div></td>
					</tr>
					<tr>
					  <td>Harga Satuan Ecommerce</td>
					  <td><input type="number" class="form-control" id="satuan_ecommerce_e" name="satuan_ecommerce_e" required></td>
					</tr>
					<tr>
					  <td>Stok</td>
					  <td><div id="stok_e"></div></td>
					</tr>
					<tr>
					  <td>Jumlah</td>
					  <td><input type="number" class="form-control" id="jumlah_e" name="jumlah_e" required></td>
					</tr>
					<tr>
					  <td>Harga Pembelian</td>
					  <td><div id="pembelian_e" style="background-color: #ff4444"></div></td>
					</tr>
					<tr>
					  <td>Harga Pembelian Ecommerce</td>
					  <td><div id="pembelian_ecommerce_e"></div></td>
					</tr>
					<tr>
					  <td>Url</td>
					  <td><div id="url_e"></div></td>
					</tr>
					<tr>
					  <td>Keterangan</td>
					  <td><textarea class="form-control" rows="3" id="ket_e" name="ket_e"></textarea></td>
					</tr>
					<tr>
					  <td>Status Konfirmasi</td>
					  <td>
							<select id="stat_konfirm_e" class="form-control">
								<option value='0'>Belum Konfirmasi</option>
								<option value='1'>Proses</option>
								<option value='2'>Tolak</option>
							</select>
					  </td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_detail_transaksi_e" name="id_detail_transaksi_e" value=""></input>
				<input type="hidden" class="form-control" id="jumlah_awal_e" name="jumlah_awal_e" value=""></input>
				<input type="hidden" class="form-control" id="id_transaksi_e" name="id_transaksi_e" value="<?php echo $id_transaksi;?>"></input>
				<input type="hidden" class="form-control" id="id_barang_e" name="id_barang_e" value=""></input>
				<input type="hidden" class="form-control" id="stat_konfirm_awal_e" name="id_barang_e" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal_edit">Save</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-danger fade" id="modal-delete">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Hapus Pembelian Barang</h4>
			</div>
			<div class="modal-body">
				<div class="box-body no-padding">
				  Apakah anda yakin menghapus pembelian barang ini?
				</div>
				<input type="hidden" class="form-control" id="id_detail_transaksi_d" name="id_detail_transaksi_d" value=""></input>
				<input type="hidden" class="form-control" id="id_transaksi_d" name="id_transaksi_d" value="<?php echo $id_transaksi;?>"></input>
				<input type="hidden" class="form-control" id="satuan_d" name="satuan_d" value=""></input>
				<input type="hidden" class="form-control" id="satuan_d_ecommerce" name="satuan_d_ecommerce" value=""></input>
				<input type="hidden" class="form-control" id="stok_d" name="stok_d" value=""></input>
				<input type="hidden" class="form-control" id="jumlah_d" name="jumlah_d" value=""></input>
				<input type="hidden" class="form-control" id="id_barang_d" name="id_barang_d" value=""></input>
				<input type="hidden" class="form-control" id="id_transaksi_seller_d" name="id_transaksi_seller_d" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Tidak</button>
			<button type="button" class="btn btn-outline" id="submit_modal_delete">Ya</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

</section><!-- /.content -->

<script type="text/javascript">
$(document).ready(function(){
    $(function () {	
        $(".btn.btn-info.edit").on("click", function() {
            var id_detail_transaksi = $(this).data('detail_transaksi-id');
			var id_ecommerce = $(this).data('ecommerce-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>/transaksi/modal_edit_barang',
				dataType: "json",
				data:"id_detail_transaksi=" +id_detail_transaksi+"&id_ecommerce="+id_ecommerce,
				success:function(data){
					$(".modal-body #nama_e").text(data["nama"]);
					$(".modal-body #seller_e").text(data["nama_toko"]);
					$(".modal-body #satuan_e").text(data["harga_satuan"]);
					$(".modal-body #satuan_ecommerce_e").val(data["harga_satuan_ecommerce"]);
					$(".modal-body #stok_e").text(data["stok_ecommerce"]);
					$(".modal-body #jumlah_e").val(data["jumlah_pembelian"]);
					$(".modal-body #pembelian_e").text(data["harga_pembelian"]);
					$(".modal-body #pembelian_ecommerce_e").text(data["harga_pembelian_ecommerce"]);
					$(".modal-body #url_e").text(data["url"]);
					$(".modal-body #ket_e").val(data["keterangan"]);
					$(".modal-body #id_detail_transaksi_e").val(data["id_detail_transaksi"]);
					$(".modal-body #jumlah_awal_e").val(data["jumlah_pembelian"]);
					$(".modal-body #id_barang_e").val(data["id_barang"]);
					$(".modal-body #stat_konfirm_e").val(data["stat_konfirm"]);
					$(".modal-body #stat_konfirm_awal_e").val(data["stat_konfirm"]);
					
					//untuk max min
					var stok = parseFloat(data["stok_ecommerce"]);
					var juml_pemb = parseFloat(data["jumlah_pembelian"]);
					var max = stok + juml_pemb;
					var min = 0;
					if(stok <= 0) {
						var max = juml_pemb;
					}
					$("#jumlah_e").attr("max", max);
					$("#jumlah_e").attr("min", min);
				}
			});
        });
		$(".btn.btn-danger.delete").on("click", function() {
            var id_detail_transaksi = $(this).data('detail_transaksi-id');
			var id_ecommerce = $(this).data('ecommerce-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url();?>transaksi/modal_edit_barang',
				dataType: "json",
				data:"id_detail_transaksi=" +id_detail_transaksi+"&id_ecommerce="+id_ecommerce,
				success:function(data){
					$(".modal-body #satuan_d").val(data["harga_satuan"]);
					$(".modal-body #satuan_d_ecommerce").val(data["harga_satuan_ecommerce"]);
					$(".modal-body #stok_d").val(data["stok_ecommerce"]);
					$(".modal-body #jumlah_d").val(data["jumlah_pembelian"]);
					$(".modal-body #id_detail_transaksi_d").val(data["id_detail_transaksi"]);
					$(".modal-body #id_barang_d").val(data["id_barang"]);
					$(".modal-body #id_transaksi_seller_d").val(data["id_transaksi_seller"]);
				}
			});
        });
		
		$(".btn.btn-success.update").on("click", function() {
            var id_transaksi_seller = $(this).data('transaksi_seller-id');
			var id_toko = $(this).data('toko-id');
			var biaya_kirim_awal = $(this).data('biaya-kirim-awal');
			var id_input = '#input-'+id_toko;
			var id_input_val = $(id_input).val();
			var id_transaksi = $("#idTransaksi").val();
			var id_ecommerce = $(this).data('ecommerce-id');
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_biaya_kirim_seller',
				data:"id_transaksi_seller=" +id_transaksi_seller+"&jumlah="+id_input_val+"&biaya_kirim_awal="+biaya_kirim_awal+"&id_transaksi="+id_transaksi+"&id_ecommerce="+id_ecommerce,
				success: function(message){
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Jumlah Tidak Boleh Kosong.");
						$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
					}
				}
			});
        });
		
		$(".btn.btn-success.update_pembelian_ecommerce").on("click", function() {
            var id_transaksi = $(this).data('transaksi-id');
			var harga_total_awal = $(this).data('harga-total-awal');
			var harga_total_baru = $("#hargaTotal").val();
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_pembelian_ecommerce',
				data:"id_transaksi=" +id_transaksi+"&harga_total_awal="+harga_total_awal+"&harga_total_baru="+harga_total_baru,
				success: function(message){
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Total Harga Pembelian Ecommerce Tidak Boleh Kosong.");
						$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
					}
				}
			});
        });
		
		$(".btn.btn-success.update_pengiriman_ecommerce").on("click", function() {
            var id_transaksi = $(this).data('transaksi-id');
			var biaya_kirim_awal = $(this).data('biaya-kirim-awal');
			var biaya_kirim_baru = $("#biayaKirimEcommerce").val();
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_biaya_kirim_ecommerce',
				data:"id_transaksi=" +id_transaksi+"&biaya_kirim_awal="+biaya_kirim_awal+"&biaya_kirim_baru="+biaya_kirim_baru,
				success: function(message){
					//alert("Test.");
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Biaya Kirim Ecommerce Tidak Boleh Kosong.");
						$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
					}
				}
			});
        });
		
		$(".btn.btn-success.update_total_ecommerce").on("click", function() {
            var id_transaksi = $(this).data('transaksi-id');
			var harga_awal = $(this).data('harga-pembayaran-awal');
			var harga_baru = $("#hargaPembayaranEcommerce").val();
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_harga_pembayaran_ecommerce',
				data:"id_transaksi=" +id_transaksi+"&harga_awal="+harga_awal+"&harga_baru="+harga_baru,
				success: function(message){
					//alert("Test.");
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Total Pembayaran Ecommerce Tidak Boleh Kosong.");
						$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
					}
				}
			});
        });
		
		$(".btn.btn-info.edit2").on("click", function() {
            var id_transaksi = $(this).data('transaksi-id');
			var jasa_pengiriman = $("#jasaPengiriman").val();
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/edit_jasa_pengiriman',
				data:"id_transaksi=" +id_transaksi+"&jasa_pengiriman="+jasa_pengiriman,
				success: function(message){
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Mohon pilih jasa pengiriman.");
						$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
					}
				}
			});
        });
		
		$("#submit_modal_input").click(function() {
			var id_transaksi = $("#id_transaksi_i").val();
			var id_barang = $("#id_barang_i").val();
			var id_toko = $("#id_toko_i").val();
			var jumlah = parseFloat($("#jumlah_i").val());
			var harga_satuan = parseFloat($("#satuan_i").text());
			var harga_satuan_ecommerce = parseFloat($("#satuan_i_ecommerce").val());
			var keterangan = $("#ket_i").val();
			var stok = $("#stok_i").val();
			//console.log("Sate : " + harga_satuan_ecommerce);
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_add',
				data:"id_transaksi=" +id_transaksi+"&id_barang="+id_barang+"&jumlah="+jumlah+"&satuan="+harga_satuan+"&ket="+keterangan+"&stok="+stok+"&id_toko="+id_toko+"&satuan_ecommerce="+harga_satuan_ecommerce,
				success: function(message){
					$("#modal-input").modal('hide');
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Nama Barang/Jumlah/Harga Satuan Ecommerce Tidak Boleh Kosong.");
					}
					else {
						alert("Jumlah Pembelian Melebihi Stok Yang Tersedia.");
					}
				}
			});
		});
		
		$("#submit_modal_edit").click(function() {
			var id_detail_transaksi = $("#id_detail_transaksi_e").val();
			var jumlah = parseFloat($("#jumlah_e").val());
			var keterangan = $("#ket_e").val();
			var stok = $("#stok_e").text();
			var jumlah_awal = parseFloat($("#jumlah_awal_e").val());
			var id_transaksi = $("#id_transaksi_e").val();
			var id_barang = $("#id_barang_e").val();
			var harga_satuan = parseFloat($("#satuan_e").text());
			var harga_satuan_ecommerce = parseFloat($("#satuan_ecommerce_e").val());
			var harga_satuan_ecommerce_awal = parseFloat($("#satuan_e_ecommerce_awal").val());
			var stat_konfirm = $("#stat_konfirm_e").val();
			var stat_konfirm_awal = $("#stat_konfirm_awal_e").val();
			//console.log(id_transaksi);
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_edit',
				data:"id_detail_transaksi=" +id_detail_transaksi+"&jumlah="+jumlah+"&jumlah_awal="+jumlah_awal+"&ket="+keterangan+"&stok="+stok+"&id_transaksi="+id_transaksi+"&id_barang="+id_barang+"&harga_satuan="+harga_satuan+"&stat_konfirm="+stat_konfirm+"&stat_konfirm_awal="+stat_konfirm_awal+"&harga_satuan_ecommerce="+harga_satuan_ecommerce+"&harga_satuan_ecommerce_awal="+harga_satuan_ecommerce_awal,
				success: function(message){
					$("#modal-edit").modal('hide');
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Jumlah Tidak Boleh Kosong.");
					}
					else {
						alert("Jumlah Pembelian Melebihi Stok Yang Tersedia.");
					}
				}
			});
		});
		
		$("#submit_modal_delete").click(function() {
			var id_detail_transaksi = $("#id_detail_transaksi_d").val();
			var id_transaksi = $("#id_transaksi_d").val();
			var id_barang = $("#id_barang_d").val();
			var id_transaksi_seller = $("#id_transaksi_seller_d").val();
			var harga_satuan = parseFloat($("#satuan_d").val());
			var harga_satuan_ecommerce = parseFloat($("#satuan_d_ecommerce").val());
			var jumlah = parseFloat($("#jumlah_d").val());
			var stok = $("#stok_d").val();
			//console.log(id_transaksi);
			$.ajax({
				type: "POST",
				url: '<?php echo base_url();?>transaksi/modal_input_delete',
				data:"id_detail_transaksi=" +id_detail_transaksi+"&jumlah="+jumlah+"&stok="+stok+"&id_transaksi="+id_transaksi+"&id_barang="+id_barang+"&harga_satuan="+harga_satuan+"&id_transaksi_seller="+id_transaksi_seller+"&harga_satuan_ecommerce="+harga_satuan_ecommerce,
				success: function(message){
					$("#modal-delete").modal('hide');
					$(location).attr('href','<?php echo base_url();?>transaksi/proses_konfirmasi/'+id_transaksi);
				},
				error: function(jqXHR,error,errorThrown){
					if(jqXHR.status&&jqXHR.status==500) {
						alert("Error Dalam Proses Menghapus.");
					}
					else {
						alert("Error Dalam Proses Menghapus.");
					}
				}
			});
		});
		
		$(document).on('focus','#nama_i',function(){
			
			$(this).autocomplete({
				source: function( request, response ) {
					$.ajax({
						url : '<?php echo base_url();?>transaksi/lookup',
						dataType: "json",
						method: 'post',
						data: {
						   name_startsWith: request.term,
						   id_ecommerce: <?php echo $id_ecommerce;?>
						},
						 success: function( data ) {
							 if(data.response =="true"){
								response( $.map( data.message, function(item) {
									return {
											value: item.nama,
											harga: item.harga_satuan,
											nama_toko: item.nama_toko,
											id: item.id,
											url: item.url,
											stok: item.stok,
											id_toko: item.id_toko
									}
								}));
							 }
						}
					});
				},
				autoFocus: true,	      	
				minLength: 1,
				appendTo: "#modal-input",
				select: function( event, ui ) {
					var nama = ui.item.value;
					var harga_satuan = ui.item.harga;
					var nama_toko = ui.item.nama_toko;
					var id = ui.item.id;
					var url = ui.item.url;
					var stok = ui.item.stok;
					var id_toko = ui.item.id_toko;
					var max = stok;
					var min = 0;
					if(stok <= 0) {
						max = 0;
					}
					$("#seller_i").text(nama_toko);
					$("#satuan_i").text(harga_satuan);
					$("#satuan_i_ecommerce").val(harga_satuan);
					$("#url_i").text(url);
					$("#id_barang_i").val(id);
					$("#id_toko_i").val(id_toko);
					$("#stok_i").text(stok);
					$("#jumlah_i").attr("max", max);
					$("#jumlah_i").attr("min", min);
				}		
			});
		});
		$("#jumlah_i").on("change paste keyup", function() {
			var jumlah_total = parseFloat($("#jumlah_i").val())*parseFloat($("#satuan_i").text());
			var jumlah_total_ecommerce = parseFloat($("#jumlah_i").val())*parseFloat($("#satuan_i_ecommerce").val());
			//console.log($("#jumlah_i").val());
			//console.log($("#satuan_i").val());
			$("#pembelian_i").text(jumlah_total); 
			$("#pembelian_i_ecommerce").text(jumlah_total_ecommerce); 
		});
		$("#jumlah_e").on("change paste keyup", function() {
			var jumlah_total = parseFloat($("#jumlah_e").val())*parseFloat($("#satuan_e").text());
			var jumlah_total_ecommerce = parseFloat($("#jumlah_e").val())*parseFloat($("#satuan_ecommerce_e").text());
			//console.log($("#jumlah_i").val());
			//console.log($("#satuan_i").val());
			$("#pembelian_e").text(jumlah_total); 
			$("#pembelian_ecommerce_e").text(jumlah_total_ecommerce); 
		});
		$("#satuan_i_ecommerce").on("change paste keyup", function() {
			var jumlah_total_ecommerce = parseFloat($("#jumlah_i").val())*parseFloat($("#satuan_i_ecommerce").val());
			//console.log($("#jumlah_i").val());
			//console.log($("#satuan_i").val());
			$("#pembelian_i_ecommerce").text(jumlah_total_ecommerce); 
		});
		$("#satuan_ecommerce_e").on("change paste keyup", function() {
			var jumlah_total_ecommerce = parseFloat($("#jumlah_e").val())*parseFloat($("#satuan_ecommerce_e").val());
			//console.log($("#jumlah_i").val());
			//console.log($("#satuan_i").val());
			$("#pembelian_ecommerce_e").text(jumlah_total_ecommerce); 
		});
    });
	
});
</script>
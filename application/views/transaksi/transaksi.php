<!-- Content Header (Page header) -->
<?php

foreach ($total_cek_biaya_kirim->result() as $t1) { 
	$jumlah_cek_biaya_kirim = $t1->jumlah_cek_biaya_kirim;
}
foreach ($total_konfirmasi->result() as $t2) { 
	$jumlah_konfirmasi = $t2->jumlah_konfirmasi;
}
foreach ($total_input_resi->result() as $t3) { 
	$jumlah_input_resi = $t3->jumlah_input_resi;
}
foreach ($total_input_resi_checklist->result() as $t4) { 
	$jumlah_checklist = $t4->jumlah_checklist;
}
foreach ($total_sedang_dikirim->result() as $t5) { 
	$jumlah_dikirim = $t5->jumlah_dikirim;
}
foreach ($total_sedang_dikirim_checklist->result() as $t6) { 
	$jumlah_dikirim_checklist = $t6->jumlah_dikirim_checklist;
}
foreach ($total_ditolak->result() as $t7) { 
	$jumlah_ditolak = $t7->jumlah_ditolak;
}
?>
<section class="content-header">
	<h1>
		Transaksi
	</h1>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Transaksi</a></li>
		<li class="active">Transaksi</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "biaya_kirim") echo "bg-gray";?>" name="div_biaya_kirim" id="div_biaya_kirim" tabindex="0">
			<span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Perlu Cek Biaya Kirim</span>
				<span class="info-box-number"><?php echo $jumlah_cek_biaya_kirim;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "konfirmasi") echo "bg-gray";?>" name="div_konfirmasi" id="div_konfirmasi" tabindex="0">
			<span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Perlu Konfirmasi</span>
				<span class="info-box-number"><?php echo $jumlah_konfirmasi;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "input_resi") echo "bg-gray";?>" name="div_input_resi" id="div_input_resi" tabindex="0">
			<span class="info-box-icon bg-orange"><i class=""></i></span>
			<div class="info-box-content">
				<!--<span class="info-box-text">Input Resi / Checklist</span>-->
				<span class="info-box-text">Input Resi</span>
				<span class="info-box-number"><?php echo $jumlah_input_resi;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "dikirim") echo "bg-gray";?>" name="div_dikirim" id="div_dikirim" tabindex="0">
			<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Dikirim / Checklist</span>  
				<span class="info-box-number"><?php echo $jumlah_dikirim." / ".$jumlah_dikirim_checklist;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "ditolak") echo "bg-gray";?>" name="div_ditolak" id="div_ditolak" tabindex="0">
			<span class="info-box-icon bg-red"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Pesanan Ditolak</span>
				<span class="info-box-number"><?php echo $jumlah_ditolak;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="overflow: auto;">
				<table id="transaksi" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>No Transaksi</th>
						<th>Toko Online</th>
						<th>Jumlah</th>
						<!--<th>Buyer</th>-->
						<th>Alamat Pengiriman</th>
						<!--<th>No HP</th>-->
						<th>Status Transaksi</th>
						<th>Status Resi</th>
						<th>Waktu Pemesanan</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>No Transaksi</th>
						<th>Toko Online</th>
						<th>Jumlah</th>
						<!--<th>Buyer</th>-->
						<th>Alamat Pengiriman</th>
						<!--<th>No HP</th>-->
						<th>Status Transaksi</th>
						<th>Status Resi</th>
						<th>Waktu Pemesanan</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($record->result() as $r) { ?>
						<tr class="gradeU">
							<td>
							<?php 
								echo $r->no_trx; 
								echo " | ".anchor('#','Preview',array('class'=>'btn btn-info btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-info','data-transaksi-id'=>''.$r->id_transaksi.'','data-commerce-id'=>''.$r->id_ecommerce.'','data-buyer-id'=>''.$r->buyer.'','data-alamat-id'=>''.$r->alamat_pengiriman.'','data-nama_ecom-id'=>''.$r->nama_ecommerce.''));
							?>
							</td>
							<td><?php echo $r->nama_ecommerce ?></td>
							<td><?php echo $r->jumlah ?></td>
							<!--<td><?php //echo $r->buyer ?></td>-->
							<td><?php echo $r->alamat_pengiriman ?></td>
							<!--<td><?php //echo $r->no_hp ?></td>-->
							<!-- Status Konfirmasi -->
							<td>
							<?php
								//Administrator, Supervisor, User
								//$this->session->userdata('role');
								$role = $this->session->userdata('role');
								//blanja
								$id_status_transaksi = $r->status_transaksi;
								if($id_status_transaksi == 1) {
									echo "<div class='label label-danger'>".$r->status."</div>";
									if(($role=='Administrator' || $role=='Supervisor' || $role=='User')) {
										echo " | ".anchor('transaksi/proses_konfirmasi/'.$r->id_transaksi.'','Proses',array('class'=>'btn btn-danger btn-sm'));
									}
								}
								elseif($id_status_transaksi == 2) {
									if(($role=='Administrator' || $role=='Supervisor' || $role=='User')) {
										echo "<div class='label label-warning'>Proses | ".$r->status."</div>";
									}
								}
								elseif($id_status_transaksi == 5) {
									echo "<div class='label label-danger'>Tolak</div>";
								}
								elseif($id_status_transaksi == 6) {
									echo "<div class='label label-danger'>".$r->status."</div>";
									if(($role=='Administrator' || $role=='Supervisor' || $role=='User')) {
										echo " | ".anchor('transaksi/proses_konfirmasi/'.$r->id_transaksi.'','Proses',array('class'=>'btn btn-danger btn-sm'));
									}
								}
								else {
									echo "<div class='label label-success'>Proses | ".$r->status."</div>";
								}
								if(($id_status_transaksi == "3" || $id_status_transaksi == "4") && $id_status_transaksi != 5) {
									if(($role=='Administrator' || $role=='Supervisor') && ($r->checked == 0)) {
										echo " | ".anchor('#','Checklist',array('id'=>'modal_checklist','class'=>'btn btn-warning btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-warning','data-transaksi-id'=>''.$r->id_transaksi.'','data-commerce-id'=>''.$r->id_ecommerce.'','data-buyer-id'=>''.$r->buyer.'','data-alamat-id'=>''.$r->alamat_pengiriman.'','data-nama_ecom-id'=>''.$r->nama_ecommerce.''));
									}
									if($id_status_transaksi == "3" && ($role=='Administrator' || $role=='Supervisor') && ($r->checked == 1)) {
										echo " | ".anchor('#','Transaction Complete',array('id'=>'modal_complete','class'=>'btn btn-primary btn-sm budi2','data-toggle'=>'modal','data-target'=>'#modal-complete','data-transaksi-id'=>''.$r->id_transaksi.'','data-commerce-id'=>''.$r->id_ecommerce.'','data-buyer-id'=>''.$r->buyer.'','data-alamat-id'=>''.$r->alamat_pengiriman.'','data-nama_ecom-id'=>''.$r->nama_ecommerce.''));
									}
								}
							?>
							</td>
							<td>
							<?php
								if(($id_status_transaksi == "3" || $id_status_transaksi == "4") && $id_status_transaksi != 5) {
									echo "<div class='label label-success'>Sudah Diinput</div>";  
								}
								elseif(($id_status_transaksi == "1" || $id_status_transaksi == "2" || $id_status_transaksi == "6") && $id_status_transaksi != 5) {
									echo "<div class='label label-warning'>Belum Diinput</div>";
									if(($role=='Administrator' || $role=='Supervisor' || $role=='User')) {
										echo " | ".anchor('transaksi/input_resi/'.$r->id_transaksi.'/'.$r->id_ecommerce.'','Input',array('class'=>'btn btn-warning btn-sm'));
									}
									//echo " | ".anchor('#','Input',array('id'=>'modal_input','class'=>'btn btn-warning btn-sm zafran','data-toggle'=>'modal','data-target'=>'#modal-input','data-transaksi-id'=>''.$r->id_transaksi.''));
								}
								else {
									echo "<div class='label label-danger'>Pesanan Ditolak</div>";
								}
							?>
							</td>
							<td><?php echo $r->tanggal_email ?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
<div class="modal modal-warning fade" id="modal-warning">
<form class="feedback" name="feedback">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Checklist Transaksi</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding">
				 <table class="table" id="tableList">
					<tr>
					  <td colspan="2">Buyer</td>
					  <td colspan="7"><div id="buyer_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Toko Online</td>
					  <td colspan="7"><div id="toko_online_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Alamat Kirim</td>
					  <td colspan="7"><div id="alamat_pengiriman_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Harga Pembelian</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_harga_pembelian_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembelian Ecommerce</td>
					  <td colspan="7"><div id="total_harga_pembelian_ecommerce_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="biaya_pengiriman_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman Ecommerce</td>
					  <td colspan="7"><div id="biaya_pengiriman_ecommerce_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_pembayaran_m"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran Ecommerce</td>
					  <td colspan="7"><div id="total_pembayaran_ecommerce_m"></div></td>
					</tr>
					<tr>
					  <td colspan="9">List Barang</td>
					</tr>
					<tr>
					  <td colspan="9"></td>
					</tr>
					<tr>
					  <td>Seller</td>
					  <td colspan="8"><div id="seller_m"></div></td>
					</tr>
					<tr>
					   <td>Biaya Kirim</td>
					   <td colspan="8"><div id="biaya_kirim_m"></div></td>
					</tr>
					<tr>
					  <td>No Resi</td>
					  <td>Nama</td>
					  <td>Harga Satuan</td>
					  <td>Harga Satuan Ecommerce</td>
					  <td>Jumlah</td>
					  <td>Harga Pembelian</td>
					  <td>Harga Pembelian Ecommerce</td>
					  <td>Url</td>
					  <td>Status Konfirmasi</td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_transaksi_m" name="id_transaksi_m" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal">Checked</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-primary fade" id="modal-complete">
<form class="feedback" name="feedback">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Transaksi Complete</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding">
				 <table class="table" id="tableList3">
					<tr>
					  <td colspan="2">Buyer</td>
					  <td colspan="7"><div id="buyer_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Toko Online</td>
					  <td colspan="7"><div id="toko_online_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Alamat Kirim</td>
					  <td colspan="7"><div id="alamat_pengiriman_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Harga Pembelian</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_harga_pembelian_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembelian Ecommerce</td>
					  <td colspan="7"><div id="total_harga_pembelian_ecommerce_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="biaya_pengiriman_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman Ecommerce</td>
					  <td colspan="7"><div id="biaya_pengiriman_ecommerce_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_pembayaran_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran Ecommerce</td>
					  <td colspan="7"><div id="total_pembayaran_ecommerce_m2"></div></td>
					</tr>
					<tr>
					  <td colspan="9">List Barang</td>
					</tr>
					<tr>
					  <td colspan="9"></td>
					</tr>
					<tr>
					  <td>Seller</td>
					  <td colspan="8"><div id="seller_m2"></div></td>
					</tr>
					<tr>
					   <td>Biaya Kirim</td>
					   <td colspan="8"><div id="biaya_kirim_m2"></div></td>
					</tr>
					<tr>
					  <td>No Resi</td>
					  <td>Nama</td>
					  <td>Harga Satuan</td>
					  <td>Harga Satuan Ecommerce</td>
					  <td>Jumlah</td>
					  <td>Harga Pembelian</td>
					  <td>Harga Pembelian Ecommerce</td>
					  <td>Url</td>
					  <td>Status Konfirmasi</td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_transaksi_m2" name="id_transaksi_m2" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal_completed">Completed</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-info fade" id="modal-info">
<form class="feedback" name="feedback">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Preview Transaksi</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding">
				 <table class="table" id="tableList2">
					<tr>
					  <td colspan="2">Buyer</td>
					  <td colspan="7"><div id="buyer_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Toko Online</td>
					  <td colspan="7"><div id="toko_online_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Alamat Kirim</td>
					  <td colspan="7"><div id="alamat_pengiriman_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Harga Pembelian</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_harga_pembelian_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembelian Ecommerce</td>
					  <td colspan="7"><div id="total_harga_pembelian_ecommerce"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="biaya_pengiriman_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman Ecommerce</td>
					  <td colspan="7"><div id="biaya_pengiriman_ecommerce_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_pembayaran_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran Ecommerce</td>
					  <td colspan="7"><div id="total_pembayaran_ecommerce_p"></div></td>
					</tr>
					<tr>
					  <td colspan="9">List Barang</td>
					</tr>
					<tr>
					  <td colspan="9"></td>
					</tr>
					<tr>
					  <td>Seller</td>
					  <td colspan="8"><div id="seller_p"></div></td>
					</tr>
					<tr>
					   <td>Biaya Kirim</td>
					   <td colspan="8"><div id="biaya_kirim_p"></div></td>
					</tr>
					<tr>
					  <td>No Resi</td>
					  <td>Nama</td>
					  <td>Harga Satuan</td>
					  <td>Harga Satuan Ecommerce</td>
					  <td>Jumlah</td>
					  <td>Harga Pembelian</td>
					  <td>Harga Pembelian Ecommerce</td>
					  <td>Url</td>
					  <td>Status Konfirmasi</td>
					</tr>
				  </table>
				</div>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>

<div class="modal modal-warning fade" id="modal-input">
<form class="feedback" name="feedback">
    <?php
	//set peringatan
	if (validation_errors() || $this->session->flashdata('result_login')) {
		?>
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<strong>Warning!</strong>
			<?php echo validation_errors(); ?>
			<?php echo $this->session->flashdata('result_login'); ?>
		</div>    
	<?php } ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Input Resi</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding">
				  <table class="table">
					<tr>
					  <td>Buyer</td>
					  <td><div id="buyer_i"></div></td>
					</tr>
					<tr>
					  <td>Toko Online</td>
					  <td><div id="toko_online_i"></div></td>
					</tr>
					<tr>
					  <td>Alamat Pengiriman</td>
					  <td><div id="alamat_pengiriman_i"></div></td>
					</tr>
					<tr>
					  <td>No Resi</td>
					  <td><input type="text" class="form-control" id="no_resi_i" name="no_resi_i" value=""></input></td>
					</tr>
					<tr>
						<td>Seller</td>
					</tr>
					<tr>
						<td>Harga Satuan</td>
					</tr>
					<tr>
						<td>Jumlah</td>
					</tr>
					<tr>
						<td>Harga Pembelian</td>
					</tr>
					<tr>
						<td>Keterangan</td>
					</tr>
					<tr>
						<td>Url</td>
					</tr>
					<tr>
					  <td>Barang</td>
					  <td><input type="text" class="form-control" id="no_resi_i" name="no_resi_i" value=""></input></td>
					</tr>
				  </table>
				</div>
				<input type="hidden" class="form-control" id="id_transaksi_i" name="id_transaksi_i" value=""></input>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-outline" id="submit_modal_input">Ok</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>
<!-- /.modal -->
</section><!-- /.content -->
<script type="text/javascript">
$(document).ready(function(){
    $(function () {
        $('#transaksi').on('click','.btn.btn-warning.btn-sm.budi', function() {
            var id_transaksi = $(this).data('transaksi-id');
			var id_ecommerce = $(this).data('commerce-id');
			var buyer = $(this).data('buyer-id');
			var alamat_pengiriman = $(this).data('alamat-id');
			var nama_ecommerce = $(this).data('nama_ecom-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('transaksi/modal2'); ?>',
				dataType: "json",
				data:"id_transaksi="+id_transaksi+"&id_ecommerce="+id_ecommerce,
				success:function(data){
					var out = null;
					var biaya_kirim = data.biaya_kirim;
					//console.log(biaya_kirim);
					var per_seller = data.per_seller;
					out = "<tbody><tr><td colspan='2'>Buyer</td><td colspan='7'><div id='buyer_m'>"+buyer+"</div></td></tr><tr><td colspan='2'>Toko Online</td><td colspan='7'><div id='toko_online_m'>"+nama_ecommerce+"</div></td></tr><tr><td colspan='2'>Alamat Kirim</td><td colspan='7'><div id='alamat_pengiriman_m'>"+alamat_pengiriman+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Harga Pembelian</td><td colspan='7' style='background-color: #ff4444'><div id='total_harga_pembelian_m'>"+data.harga_total_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembelian Ecommerce</td><td colspan='7'><div id='total_harga_pembelian_ecommerce_m'>"+data.harga_total+"</div></td></tr>";
					out += "<tr><td colspan='2'>Biaya Pengiriman</td><td colspan='7' style='background-color: #ff4444'><div id='biaya_pengiriman_m'>"+data.biaya_kirim_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Biaya Pengiriman Ecommerce</td><td colspan='7'><div id='biaya_pengiriman_ecommerce_m'>"+data.biaya_kirim+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembayaran</td><td colspan='7' style='background-color: #ff4444'><div id='total_pembayaran_m'>"+data.harga_pembayaran_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembayaran Ecommerce</td><td colspan='7'><div id='total_pembayaran_ecommerce_m'>"+data.harga_pembayaran+"</div></td></tr>";
					out += "<tr><td colspan='9'>List Barang</td></tr><tr><td colspan='9'></td></tr>";
					$.each(per_seller, function (index, object) {
						//console.log(object);
						/*
						   <tr><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>
						*/
						out += '<tr><td>Seller</td> <td colspan=\'8\'><div id=\'seller_m\'>'+object.nama_toko+'</div></td></tr><tr><td>Biaya Kirim</td><td colspan=\'8\'><div id=\'biaya_kirim_m\'>'+object.biaya_kirim_seller+'</div></td></tr>';
						out += '<tr style=\'background-color: #007E33\'><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>';
						var detail = object.detail;
						$.each(detail, function (index, object2) {
							//console.log(object2);
							out += '<tr style=\'background-color: #00C851\'><td>'+object2.nomor_resi+'</td><td>'+object2.nama_barang+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_satuan+'</td><td>'+object2.harga_satuan_ecommerce+'</td><td>'+object2.jumlah_pembelian+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_pembelian+'</td><td>'+object2.harga_pembelian_ecommerce+'</td><td>'+object2.url+'</td><td>'+object2.status_konfirmasi+'</td></tr>';
						});
						//dataArray.push([value["nama"].toString(), value["nama_toko"] ]);
						//out += '<tr style=\'background-color: #00C851\'><td>'+object.nomor_resi+'</td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td style=\'background-color: #ff4444\'>'+object.harga_satuan+'</td><td>'+object.harga_satuan_ecommerce+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.biaya_kirim_seller+'</td><td style=\'background-color: #ff4444\'>'+object.harga_pembelian+'</td><td>'+object.harga_pembelian_ecommerce+'</td><td style=\'background-color: #ff4444\'>'+object.harga_total+'</td ><td>'+object.harga_total_ecommerce+'</td><td>'+object.url+'</td></tr>';
					});
					out+= '</tbody>';
					$('#tableList').empty();
					$('#tableList').append($('<table></table>').attr('class', 'table'));
					$('#tableList').append(out);
					var out = null;
					$(".modal-body #id_transaksi_m").val(id_transaksi);
				}
			});
        });
		$('#transaksi').on('click','.btn.btn-primary.btn-sm.budi2', function() {
            var id_transaksi = $(this).data('transaksi-id');
			var id_ecommerce = $(this).data('commerce-id');
			var buyer = $(this).data('buyer-id');
			var alamat_pengiriman = $(this).data('alamat-id');
			var nama_ecommerce = $(this).data('nama_ecom-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('transaksi/modal2'); ?>',
				dataType: "json",
				data:"id_transaksi="+id_transaksi+"&id_ecommerce="+id_ecommerce,
				success:function(data){
					var out = null;
					var biaya_kirim = data.biaya_kirim;
					//console.log(biaya_kirim);
					var per_seller = data.per_seller;
					out = "<tbody><tr><td colspan='2'>Buyer</td><td colspan='7'><div id='buyer_m2'>"+buyer+"</div></td></tr><tr><td colspan='2'>Toko Online</td><td colspan='7'><div id='toko_online_m2'>"+nama_ecommerce+"</div></td></tr><tr><td colspan='2'>Alamat Kirim</td><td colspan='7'><div id='alamat_pengiriman_m2'>"+alamat_pengiriman+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Harga Pembelian</td><td colspan='7' style='background-color: #ff4444'><div id='total_harga_pembelian_m2'>"+data.harga_total_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembelian Ecommerce</td><td colspan='7'><div id='total_harga_pembelian_ecommerce_m2'>"+data.harga_total+"</div></td></tr>";
					out += "<tr><td colspan='2'>Biaya Pengiriman</td><td colspan='7' style='background-color: #ff4444'><div id='biaya_pengiriman_m2'>"+data.biaya_kirim_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Biaya Pengiriman Ecommerce</td><td colspan='7'><div id='biaya_pengiriman_ecommerce_m2'>"+data.biaya_kirim+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembayaran</td><td colspan='7' style='background-color: #ff4444'><div id='total_pembayaran_m2'>"+data.harga_pembayaran_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembayaran Ecommerce</td><td colspan='7'><div id='total_pembayaran_ecommerce_m2'>"+data.harga_pembayaran+"</div></td></tr>";
					out += "<tr><td colspan='9'>List Barang</td></tr><tr><td colspan='9'></td></tr>";
					$.each(per_seller, function (index, object) {
						//console.log(object);
						/*
						   <tr><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>
						*/
						out += '<tr><td>Seller</td> <td colspan=\'8\'><div id=\'seller_m2\'>'+object.nama_toko+'</div></td></tr><tr><td>Biaya Kirim</td><td colspan=\'8\'><div id=\'biaya_kirim_m2\'>'+object.biaya_kirim_seller+'</div></td></tr>';
						out += '<tr style=\'background-color: #007E33\'><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>';
						var detail = object.detail;
						$.each(detail, function (index, object2) {
							//console.log(object2);
							out += '<tr style=\'background-color: #00C851\'><td>'+object2.nomor_resi+'</td><td>'+object2.nama_barang+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_satuan+'</td><td>'+object2.harga_satuan_ecommerce+'</td><td>'+object2.jumlah_pembelian+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_pembelian+'</td><td>'+object2.harga_pembelian_ecommerce+'</td><td>'+object2.url+'</td><td>'+object2.status_konfirmasi+'</td></tr>';
						});
						//dataArray.push([value["nama"].toString(), value["nama_toko"] ]);
						//out += '<tr style=\'background-color: #00C851\'><td>'+object.nomor_resi+'</td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td style=\'background-color: #ff4444\'>'+object.harga_satuan+'</td><td>'+object.harga_satuan_ecommerce+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.biaya_kirim_seller+'</td><td style=\'background-color: #ff4444\'>'+object.harga_pembelian+'</td><td>'+object.harga_pembelian_ecommerce+'</td><td style=\'background-color: #ff4444\'>'+object.harga_total+'</td ><td>'+object.harga_total_ecommerce+'</td><td>'+object.url+'</td></tr>';
					});
					out+= '</tbody>';
					$('#tableList3').empty();
					$('#tableList3').append($('<table></table>').attr('class', 'table'));
					$('#tableList3').append(out);
					var out = null;
					$(".modal-body #id_transaksi_m2").val(id_transaksi);
				}
			});
        });
		$('#transaksi').on('click','.btn.btn-info.btn-sm.budi', function() {
            var id_transaksi = $(this).data('transaksi-id');
			var id_ecommerce = $(this).data('commerce-id');
			var buyer = $(this).data('buyer-id');
			var alamat_pengiriman = $(this).data('alamat-id');
			var nama_ecommerce = $(this).data('nama_ecom-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('transaksi/modal2'); ?>',
				dataType: "json",
				data:"id_transaksi="+id_transaksi+"&id_ecommerce="+id_ecommerce,
				success:function(data){
					var out = null;
					var biaya_kirim = data.biaya_kirim;
					//console.log(biaya_kirim);
					var per_seller = data.per_seller;
					out = "<tbody><tr><td colspan='2'>Buyer</td><td colspan='7'><div id='buyer_p'>"+buyer+"</div></td></tr><tr><td colspan='2'>Toko Online</td><td colspan='7'><div id='toko_online_p'>"+nama_ecommerce+"</div></td></tr><tr><td colspan='2'>Alamat Kirim</td><td colspan='7'><div id='alamat_pengiriman_p'>"+alamat_pengiriman+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Harga Pembelian</td><td colspan='7' style='background-color: #ff4444'><div id='total_harga_pembelian_p'>"+data.harga_total_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembelian Ecommerce</td><td colspan='7'><div id='total_harga_pembelian_ecommerce_p'>"+data.harga_total+"</div></td></tr>";
					out += "<tr><td colspan='2'>Biaya Pengiriman</td><td colspan='7' style='background-color: #ff4444'><div id='biaya_pengiriman_p'>"+data.biaya_kirim_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Biaya Pengiriman Ecommerce</td><td colspan='7'><div id='biaya_pengiriman_ecommerce_p'>"+data.biaya_kirim+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembayaran</td><td colspan='7' style='background-color: #ff4444'><div id='total_pembayaran_p'>"+data.harga_pembayaran_barang+"</div></td></tr>";
					out += "<tr><td colspan='2'>Total Pembayaran Ecommerce</td><td colspan='7'><div id='total_pembayaran_ecommerce_p'>"+data.harga_pembayaran+"</div></td></tr>";
					out += "<tr><td colspan='9'>List Barang</td></tr><tr><td colspan='9'></td></tr>";
					$.each(per_seller, function (index, object) {
						//console.log(object);
						/*
						   <tr><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>
						*/
						out += '<tr><td>Seller</td> <td colspan=\'8\'><div id=\'seller_p\'>'+object.nama_toko+'</div></td></tr><tr><td>Biaya Kirim</td><td colspan=\'8\'><div id=\'biaya_kirim_p\'>'+object.biaya_kirim_seller+'</div></td></tr>';
						out += '<tr style=\'background-color: #007E33\'><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>';
						var detail = object.detail;
						$.each(detail, function (index, object2) {
							//console.log(object2);
							out += '<tr style=\'background-color: #00C851\'><td>'+object2.nomor_resi+'</td><td>'+object2.nama_barang+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_satuan+'</td><td>'+object2.harga_satuan_ecommerce+'</td><td>'+object2.jumlah_pembelian+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_pembelian+'</td><td>'+object2.harga_pembelian_ecommerce+'</td><td>'+object2.url+'</td><td>'+object2.status_konfirmasi+'</td></tr>';
						});
						//dataArray.push([value["nama"].toString(), value["nama_toko"] ]);
						//out += '<tr style=\'background-color: #00C851\'><td>'+object.nomor_resi+'</td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td style=\'background-color: #ff4444\'>'+object.harga_satuan+'</td><td>'+object.harga_satuan_ecommerce+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.biaya_kirim_seller+'</td><td style=\'background-color: #ff4444\'>'+object.harga_pembelian+'</td><td>'+object.harga_pembelian_ecommerce+'</td><td style=\'background-color: #ff4444\'>'+object.harga_total+'</td ><td>'+object.harga_total_ecommerce+'</td><td>'+object.url+'</td></tr>';
					});
					out+= '</tbody>';
					$('#tableList2').empty();
					$('#tableList2').append($('<table></table>').attr('class', 'table'));
					$('#tableList2').append(out);
					var out = null;
				}
			});
        });
		//checking
		$(".btn.btn-warning.btn-sm.zafran").on("click", function() {
            var id_transaksi = $(this).data('transaksi-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('transaksi/modal_input'); ?>',
				dataType: "json",
				data:"id_transaksi=" +id_transaksi,
				success:function(data){
					$(".modal-body #seller_i").text(data["nama_barang"]);
					$(".modal-body #buyer_i").text(data["buyer"]);
					$(".modal-body #toko_online_i").text(data["nama_ecommerce"]);
					$(".modal-body #alamat_pengiriman_i").text(data["alamat_pengiriman"]);
					$(".modal-body #id_transaksi_i").val(data["id_transaksi"]);
				}
			});
        });
		
		$("#submit_modal").click(function() {
			var id_transaksi = $("#id_transaksi_m").val();
			var url_link = window.location.href;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('transaksi/modal_checklist'); ?>",
				dataType: "json",
				data:"id_transaksi=" +id_transaksi,
				success: function(message){
					$("#modal-warning").modal('hide');
					//$(location).attr('href','transaksi');
					$(location).attr('href',url_link);
				},
				error: function(){
					alert("Error");
				}
			});
		});
		
		$("#submit_modal_completed").click(function() {
			var id_transaksi = $("#id_transaksi_m2").val();
			var url_link = window.location.href;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('transaksi/modal_completed'); ?>",
				dataType: "json",
				data:"id_transaksi=" +id_transaksi,
				success: function(message){
					$("#modal-warning").modal('hide');
					//$(location).attr('href','transaksi');
					$(location).attr('href',url_link);
				},
				error: function(){
					alert("Error");
				}
			});
		});
		
		$("#submit_modal_input").click(function() {
			var id_transaksi = $("#id_transaksi_i").val();
			var no_resi = $("#no_resi_i").val();
			var url_link = window.location.href;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('transaksi/modal_input_ok'); ?>",
				data:"id_transaksi=" +id_transaksi+"&no_resi="+no_resi,
				success: function(message){
					$("#modal-input").modal('hide');
					//$(location).attr('href','transaksi');url_link
					$(location).attr('href',url_link);
				},
				error: function(){
					alert("No Resi Tidak Boleh Kosong");
				}
			});
		});
		
		$("#div_biaya_kirim").click(function() {
		  window.location = "<?php echo base_url('transaksi/show_cek_biaya_kirim'); ?>"; 
		  return false;
		});
		$("#div_konfirmasi").click(function() {
		  window.location = "<?php echo base_url('transaksi/show_konfirmasi'); ?>";
		  return false;
		});
		$("#div_input_resi").click(function() {
		  window.location = "<?php echo base_url('transaksi/show_input_resi'); ?>";
		  return false;
		});
		$("#div_dikirim").click(function() {
		  window.location = "<?php echo base_url('transaksi/show_dikirim'); ?>"; 
		  return false;
		});
		$("#div_ditolak").click(function() {
		  window.location = "<?php echo base_url('transaksi/show_ditolak'); ?>";
		  return false;
		});
		
    });
	
});
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dashboard
	</h1>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
<!-- Main content -->
<?php

foreach ($total_revenue->result() as $tr) { 
	$revenue = $tr->revenue;
}
foreach ($target_revenue->result() as $tr2) { 
	$target = $tr2->target;
	$achieve = $tr2->achieve;
}
foreach ($total_penjualan->result() as $tp) { 
	$sales = $tp->sales;
}
foreach ($target_penjualan->result() as $tp2) { 
	$target_penjualan = $tp2->target;
	$achieve_penjualan = $tp2->achieve;
}
foreach ($total_barang->result() as $tb) { 
	$barang = $tb->barang;
}
foreach ($target_barang->result() as $tb2) { 
	$target_barang = $tb2->target;
	$achieve_barang = $tb2->achieve;
}
foreach ($total_seller->result() as $ts) { 
	$seller = $ts->toko;
}
foreach ($target_seller->result() as $ts2) { 
	$target_seller = $ts2->target;
	$achieve_seller = $ts2->achieve;
}

$goal_completions = $sales + $barang + $seller;


?>
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Revenue</span>
				<span class="info-box-number"><a href="<?php echo base_url();?>pencairan_dana" style="color:#000000;text-decoration:none;">Rp <?php echo number_format($revenue, 2);?></a></span>
				<!--<span class="info-box-number"><a href="<?php echo base_url();?>pencairan_dana" style="color:#000000;text-decoration:none;">Rp 28,265,400.00</a></span>-->
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i class="fa fa-bar-chart-o"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Penjualan</span>
				<span class="info-box-number"><a href="<?php echo base_url();?>transaksi" style="color:#000000;text-decoration:none;"><?php echo $sales;?></a></span>
				<!--<span class="info-box-number"><a href="<?php echo base_url();?>transaksi" style="color:#000000;text-decoration:none;">161</span>-->
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-green"><i class="fa fa-cubes"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Barang</span>
				<span class="info-box-number"><a href="<?php echo base_url();?>upload_barang" style="color:#000000;text-decoration:none;"><?php echo $barang;?></a></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Seller</span>
				<span class="info-box-number"><a href="<?php echo base_url();?>upload_barang" style="color:#000000;text-decoration:none;"><?php echo $seller;?></a></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Rekap Revenue 1 Bulan Terakhir</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<div class="btn-group">
						<button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						</ul>
					</div>
					<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<p class="text-center">
							<strong>Revenue: 1 Bulan Terakhir 2018</strong>
						</p>
						<div class="chart-responsive">
							<!-- Sales Chart Canvas -->
							<canvas id="salesChartTest" height="180"></canvas>
						</div><!-- /.chart-responsive -->
					</div><!-- /.col -->
					<div class="col-md-6">
						<p class="text-center">
							<strong>Sales: 1 Bulan Terakhir 2018</strong>
						</p>
						<div class="chart-responsive">
							<!-- Sales Chart Canvas -->
							<canvas id="salesChartTest2" height="180"></canvas>
						</div><!-- /.chart-responsive -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- ./box-body -->
			<div class="box-footer">
				<div class="row">
					<div class="col-sm-4 col-xs-6">
						<div class="description-block border-right">
							<span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
							<h5 class="description-header">Rp<?php echo number_format($revenue, 2);?></h5>
							<!--<h5 class="description-header">Rp 28,265,400.00</h5>-->
							<span class="description-text">TOTAL REVENUE</span>
						</div><!-- /.description-block -->
					</div><!-- /.col -->
					<div class="col-sm-4 col-xs-6">
						<div class="description-block border-right">
							<span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
							<h5 class="description-header">Rp<?php echo number_format($revenue, 2);?></h5>
							<!--<h5 class="description-header">Rp 28,265,400.00</h5>-->
							<span class="description-text">TOTAL COST</span>
						</div><!-- /.description-block -->
					</div><!-- /.col -->
					<div class="col-sm-4 col-xs-6">
						<div class="description-block border-right">
							<span class="description-percentage text-yellow"><i class="fa fa-caret-left"></i> 0%</span>
							<h5 class="description-header">Rp0.00</h5>
							<span class="description-text">TOTAL PROFIT</span>
						</div><!-- /.description-block -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.box-footer -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class='row'>
	<div class='col-md-4'>
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Transaksi Terakhir</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>No Transaksi</th>
								<th>Barang</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($latest_transaksi->result() as $lt) { ?>
							<tr>
								<td><a href="#" style="color:#337ab7;"><?php echo $lt->no_trx;?></a></td>
								<td><?php echo $lt->nama;?></td>
								<?php 
								$id_status_transaksi = $lt->status_transaksi;
								if($id_status_transaksi == 1) {
									$label = "label label-danger";
								}
								elseif($id_status_transaksi == 2) {
									$label = "label label-warning";
								}
								elseif(($id_status_transaksi == 5) || ($id_status_transaksi == 6)) {
									$label = "label label-danger";
								}
								else {
									$label = "label label-success";
								}
								?>
								<td><span class="<?php echo $label;?>"><?php echo $lt->status;?></span></td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div><!-- /.table-responsive -->
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua Transaksi</a>
			</div><!-- /.box-footer -->
		</div><!-- /.box -->
	</div><!-- /.col -->
	<div class='col-md-4'>
		<!-- USERS LIST -->
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">Latest Sellers</h3>
				<div class="box-tools pull-right">
					<span class="label label-danger">8 New Sellers</span>
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body no-padding">
				<ul class="users-list clearfix">
				<?php foreach ($latest_seller->result() as $lr) { ?>
					<li>
						<?php 
						if($lr->foto == '' || strpos($lr->foto, 'storage') !== false) {
							$foto = base_url('assets/img/avatar5.png');
						}
						else {
							$foto = $lr->foto;
						}	
						?>
						<img src="<?php echo $foto;?>" width="128" height="128" alt="User Image"/>
						<a class="users-list-name" href="#"><?php echo $lr->nama;?></a>
						<span class="users-list-date"><?php echo $lr->tanggal;?></span>
					</li>
				<?php }?>
				</ul><!-- /.users-list -->
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<a href="javascript::" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua Seller</a>
			</div><!-- /.box-footer -->
		</div><!--/.box -->
	</div><!-- /.col -->
	<div class='col-md-4'>
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Recently Added Products</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div><!-- /.box-header -->
			<div class="box-body">
				<ul class="products-list product-list-in-box">
				<?php foreach ($recently_barang->result() as $rb) { ?>
					<li class="item">
						<div class="product-img">
							<img src="<?php echo $rb->foto;?>" alt="Product Image"/>
						</div>
						<div class="product-info">
							<a href="javascript::;" class="product-title" style="color:#337ab7;"><?php echo $rb->nama;?> <span class="label label-warning pull-right"><?php echo number_format($rb->harga, 2);?></span></a>
							<span class="product-description">
								<?php echo $rb->deskripsi;?>
							</span>
						</div>
					</li><!-- /.item -->
				<?php }?>
				</ul>
			</div><!-- /.box-body -->
			<div class="box-footer clearfix">
				<a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua Barang</a>
			</div><!-- /.box-footer -->
		</div><!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
</section><!-- /.content -->
<?php 
$labels = "";
$data = "";
foreach ($rekap_penjualan->result() as $rp) { 
	$labels .= "\"".$rp->tanggal."\",";
	if($rp->harga_total == "")
		$harga_total = 0;
	else
		$harga_total = $rp->harga_total;
	$data .= $harga_total.",";
}
$labels  = substr($labels, 0, -1);
$data  = substr($data, 0, -1);

$labels2 = "";
$data2 = "";
foreach ($rekap_sales->result() as $rs) { 
	$labels2 .= "\"".$rs->tanggal."\",";
	if($rs->jumlah_pembelian == "")
		$jumlah_pembelian = 0;
	else
		$jumlah_pembelian = $rs->jumlah_pembelian;
	$data2 .= $jumlah_pembelian.",";
}
$labels2  = substr($labels2, 0, -1);
$data2  = substr($data2, 0, -1);

//echo $labels;
//echo $data;
?>
<script type="text/javascript">
$(document).ready(function(){
	/* ChartJS
	* -------
	* Here we will create a few charts using ChartJS
	*/

	//-----------------------
	//- MONTHLY SALES CHART -
	//-----------------------
	
	// Get context with jQuery - using jQuery's .get() method.
	var salesChartCanvas = $("#salesChartTest").get(0).getContext("2d");
	var salesChartCanvas2 = $("#salesChartTest2").get(0).getContext("2d");
	// This will get the first returned node in the jQuery collection.
	var salesChart = new Chart(salesChartCanvas);
	var salesChart2 = new Chart(salesChartCanvas2);

	var salesChartData = {
		labels: [<?php echo $labels; ?>],
		datasets: [
		  {
			label: "Digital Goods",
			fillColor: "rgba(60,141,188,0.9)",
			strokeColor: "rgba(60,141,188,0.8)",
			pointColor: "#3b8bba",
			pointStrokeColor: "rgba(60,141,188,1)",
			pointHighlightFill: "#fff",
			pointHighlightStroke: "rgba(60,141,188,1)",
			data: [<?php echo $data; ?>]
		  }
		]
	};
	var salesChartData2 = {
		labels: [<?php echo $labels2; ?>],
		datasets: [
		  {
			label: "Digital Goods",
			fillColor: "rgba(60,141,188,0.9)",
			strokeColor: "rgba(60,141,188,0.8)",
			pointColor: "#3b8bba",
			pointStrokeColor: "rgba(60,141,188,1)",
			pointHighlightFill: "#fff",
			pointHighlightStroke: "rgba(60,141,188,1)",
			data: [<?php echo $data2; ?>]
		  }
		]
	};

	var salesChartOptions = {
		//Boolean - If we should show the scale at all
		showScale: true,
		//Boolean - Whether grid lines are shown across the chart
		scaleShowGridLines: false,
		//String - Colour of the grid lines
		scaleGridLineColor: "rgba(0,0,0,.05)",
		//Number - Width of the grid lines
		scaleGridLineWidth: 1,
		//Boolean - Whether to show horizontal lines (except X axis)
		scaleShowHorizontalLines: true,
		//Boolean - Whether to show vertical lines (except Y axis)
		scaleShowVerticalLines: true,
		//Boolean - Whether the line is curved between points
		bezierCurve: true,
		//Number - Tension of the bezier curve between points
		bezierCurveTension: 0.3,
		//Boolean - Whether to show a dot for each point
		pointDot: false,
		//Number - Radius of each point dot in pixels
		pointDotRadius: 4,
		//Number - Pixel width of point dot stroke
		pointDotStrokeWidth: 1,
		//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
		pointHitDetectionRadius: 20,
		//Boolean - Whether to show a stroke for datasets
		datasetStroke: true,
		//Number - Pixel width of dataset stroke
		datasetStrokeWidth: 2,
		//Boolean - Whether to fill the dataset with a color
		datasetFill: true,
		//String - A legend template
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
		//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
		maintainAspectRatio: false,
		//Boolean - whether to make the chart responsive to window resizing
		responsive: true
	};

	//Create the line chart
	salesChart.Line(salesChartData, salesChartOptions);
	salesChart2.Line(salesChartData2, salesChartOptions);

	//---------------------------
	//- END MONTHLY SALES CHART -
	//---------------------------
	
});
</script>
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Update Stok</h3>
		</div>
		<?php
		//parameter 
		foreach ($record->result() as $r) { 
			$nama_barang = $r->nama_barang;
			$nama_toko = $r->nama_toko;
			$id_barang = $r->id_barang;
			$deskripsi = $r->deskripsi;
			$foto = $r->foto;
			$waktu_upload = $r->tanggal_upload;
			$stok = $r->stok;
			$stok_ecommerce = $r->stok_ecommerce;
			$harga_satuan = $r->harga_satuan;
		}
		foreach ($status->result() as $s) { 
			$id_status = $s->id_status;
			$status = $s->status_update_stok;
			$id_ecommerce = $s->id_ecommerce;
			$nama_ecommerce = $s->nama;
			$id_status_penyebab = $s->id_status_penyebab;
			$status_penyebab = $s->status_penyebab_update_stok;
			$url = $s->url;
			$id_status_penyebab = $s->id_status_penyebab;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="update_stok/proses_updating" method="POST">
			<div class="box-body">
				<div class="form-group">
					<label for="namaBarang" class="col-sm-2 control-label">Nama Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaBarang" name="namaBarang" value="<?php echo $nama_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="namaToko" class="col-sm-2 control-label">Nama Toko</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaToko" name="namaToko" value="<?php echo $nama_toko;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="kodeBarang" class="col-sm-2 control-label">Kode Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="kodeBarang" name="kodeBarang" value="<?php echo $id_barang;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#kodeBarang">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="stok" class="col-sm-2 control-label">Stok</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="stok" name="stok" value="<?php echo $stok;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#stok">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaSatuan" class="col-sm-2 control-label">Harga Satuan</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaSatuan" name="hargaSatuan" value="<?php echo $harga_satuan;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="deskripsi" class="col-sm-2 control-label">Deskripsi</label>
					<div class="col-sm-10">
						<textarea type="text" class="form-control" id="deskripsi" rows="4" name="deskripsi" readonly><?php echo $deskripsi;?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="gambar" class="col-sm-2 control-label">Gambar</label>
					<div class="col-sm-10">
						<a href="<?php echo htmlspecialchars("".$r->foto."");?>" download="<?php echo htmlspecialchars("".$r->foto."");?>">
							<img src="<?php echo $r->foto; ?>" class="img-thumbnail" width="100" height="100"/>
						</a>
						<?php
							foreach ($foto_tambahan->result() as $f) { 
							echo "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
									<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
								  </a>";
							}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="ecommerce" class="col-sm-2 control-label">Ecommerce</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="ecommerce" name="ecommerce" value="<?php echo $nama_ecommerce;?>" readonly>
						<input type="hidden" class="form-control" id="idEcommerce" name="idEcommerce" value="<?php echo $id_ecommerce;?>" readonly>
						<input type="hidden" class="form-control" id="idStatusPenyebab" name="idStatusPenyebab" value="<?php echo $id_status_penyebab;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-sm-2 control-label">Url</label>
					<div class="col-sm-10">
						<div class="form-control" ><a href="<?php echo $url;?>" target="_blank" style="color:black;"><?php echo $url;?></a></div>
					</div>
				</div>
				<div class="form-group">
					<label for="waktuUpload" class="col-sm-2 control-label">Waktu Upload</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="waktuUpload" name="waktuUpload" value="<?php echo $waktu_upload;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="penyebab" class="col-sm-2 control-label">Penyebab</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="penyebab" name="penyebab" value="<?php echo $status_penyebab;?>" readonly>
					</div>
				</div>
				<?php
				
				/*
				foreach ($status->result() as $s) { 
					$id_status = $s->id_status;
					$status = $s->status_update_stok;
					$id_ecommerce = $s->id_ecommerce;
					$nama_ecommerce = $s->nama;
					$id_status_penyebab = $s->id_status_penyebab;
					$status_penyebab = $s->status_penyebab_update_stok;
					$url = $s->url;
					$id_status_penyebab = $s->id_status_penyebab;
				}
				*/
				
				?>
				<div class="form-group">
					<label for="penyebab" class="col-sm-2 control-label">Blanja</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="penyebab" name="penyebab" value="<?php echo $status_penyebab;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="penyebab" class="col-sm-2 control-label">Tokopedia</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="penyebab" name="penyebab" value="<?php echo $status_penyebab;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="penyebab" class="col-sm-2 control-label">Bukalapak</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="penyebab" name="penyebab" value="<?php echo $status_penyebab;?>" readonly>
					</div>
				</div>
			</div>
			<!-- /.box-body -->

			 <div class="box-footer">
				<button type="submit" name="submit" class="btn btn-danger">Updating</button>&nbsp &nbsp
				<?php echo anchor('update_stok','Kembali',array('class'=>'btn btn-primary'))?>
			</div>
		</form>
		</div>
		<!-- /.box -->
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
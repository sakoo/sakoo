<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sakoo | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link rel="shortcut icon" href="<?php echo base_url('assets/img/web_admin.png'); ?>">
        <!-- Bootstrap 3.3.2 -->  
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <!-- <link href="<?php //echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" /> -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php //echo base_url('assets/css/buttons.dataTables.min.css'); ?>" rel="stylesheet" type="text/css" />
		<!-- Datatables -->
        <link href="<?php echo base_url('assets/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<!-- Jquery UI -->
        <link href="<?php echo base_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo base_url('assets/js/plugins/morris/morris.css'); ?>" rel="stylesheet">
        <!-- jvectormap -->
        <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker-bs3.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/css/skins/_all-skins.min.css'); ?>" rel="stylesheet" type="text/css" />
		<!-- jQuery 3 -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
		<!-- Jquery UI 1.12.1 --> 
		<script src="<?php echo base_url('assets/js/jquery-ui.min.js'); ?>" type="text/javascript"></script>
		<!-- Clipboard --> 
		<script src="<?php echo base_url('assets/js/clipboard.min.js'); ?>" type="text/javascript"></script>
        <!-- Date picker -->
        <link href="<?php echo base_url('assets/css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet" type="text/css" />
	</head>
    <body class="skin-blue">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="dashboard" class="logo"><b>Sakoo</b>Dashboard</a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <?php echo $notifikasi;?>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url('assets/img/avatar5.png'); ?>" class="user-image" alt="User Image"/>
                                    <span class="hidden-xs"><?php echo $this->session->userdata('nama'); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url('assets/img/avatar5.png'); ?>" class="img-circle" alt="User Image" />
                                        <p>
                                            <?php echo $this->session->userdata('nama'); ?> / <?php echo $this->session->userdata('role'); ?>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Followers</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Sales</a>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <a href="#">Friends</a>
                                        </div>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('dashboard/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url('assets/img/avatar5.png'); ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->session->userdata('nama'); ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
						<li>
						  <a href="<?php echo base_url().'dashboard'?>">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						  </a>
						</li>
						<li class="treeview">
						  <a href="#">
							<i class="fa fa-desktop"></i> <span>Barang</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu">
							<li><a href="<?php echo base_url().'upload_barang'?>"><i class="fa fa-circle-o text-red"></i> Proses Upload</a></li>
							<li><a href="<?php echo base_url().'update_stok'?>"><i class="fa fa-circle-o text-red"></i> Proses Update Stok</a></li>
							<li><a href="<?php echo base_url().'edit_barang'?>"><i class="fa fa-circle-o text-red"></i> Proses Edit</a></li>
							<li><a href="<?php echo base_url().'hapus_barang'?>"><i class="fa fa-circle-o text-red"></i> Proses Hapus</a></li>
						  </ul>
						</li>
						<li class="treeview">
						  <a href="#">
							<i class="fa fa-calendar"></i> <span>Transaksi</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu">
							<li><a href="<?php echo base_url().'transaksi'?>"><i class="fa fa-circle-o text-yellow"></i> Proses</a></li>
							<li><a href="<?php echo base_url().'transaksi/tambah'?>"><i class="fa fa-circle-o text-yellow"></i> Tambah</a></li>
						  </ul>
						</li>
						<li>
						  <a href="<?php echo base_url().'pencairan_dana'?>">
							<i class="fa fa-edit"></i> <span>Pencairan Dana</span>
						  </a>
						</li>
						<li class="treeview">
						  <a href="#">
							<i class="fa fa-table"></i> <span>Report</span>
							<span class="pull-right-container">
							  <i class="fa fa-angle-left pull-right"></i>
							</span>
						  </a>
						  <ul class="treeview-menu">
							<!--<li><a href="<?php //echo base_url().'client'?>"><i class="fa fa-circle-o text-red"></i> Client</a></li>-->
							<!--<li><a href="<?php //echo base_url().'produk'?>"><i class="fa fa-circle-o text-yellow"></i> Produk</a></li>-->
							<!--<li><a href="<?php //echo base_url().'user_web'?>"><i class="fa fa-circle-o text-aqua"></i> User Web Admin</a></li>-->
							<li><a href="<?php echo base_url().'saldo'?>"><i class="fa fa-circle-o text-aqua"></i> Saldo</a></li>
							<li><a href="<?php echo base_url().'seller'?>"><i class="fa fa-circle-o text-aqua"></i> Seller</a></li>
						  </ul>
						</li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <div class="content-wrapper">
				<?php echo $contents?>
            </div><!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2018 <a style="color:#337ab7;" href="http://sakoo.id">Sakoo Team</a>.</strong> All rights reserved.
            </footer>

        </div><!-- ./wrapper -->

        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<!-- Datatables -->
        <script src="<?php echo base_url('assets/js/jquery.dataTables.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/dataTables.bootstrap.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/dataTables.buttons.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/button/buttons.flash.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/button/jszip.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/button/pdfmake.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/button/vfs_fonts.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/button/buttons.html5.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/button/buttons.print.min.js'); ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url('assets/js/plugins/fastclick/fastclick.min.js'); ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url('assets/js/AdminLTE/app.min.js'); ?>" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url('assets/js/plugins/sparkline/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker.js'); ?>" type="text/javascript"></script>
        <!-- datepicker -->
        <!-- <script src="<?php echo base_url('assets/js/plugins/datepicker/bootstrap-datepicker.js'); ?>" type="text/javascript"></script> -->
		<script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js'); ?>" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url('assets/js/plugins/iCheck/icheck.min.js'); ?>" type="text/javascript"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo base_url('assets/js/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <!-- ChartJS 1.0.1 -->
        <script src="<?php echo base_url('assets/js/plugins/chartjs/Chart.min.js'); ?>" type="text/javascript"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo base_url('assets/js/AdminLTE/dashboard2.js'); ?>" type="text/javascript"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url('assets/js/AdminLTE/demo.js'); ?>"></script> 
		<!-- page script -->
		<script>
		  $(function () {
			  
			//Date picker
			//$('#datepicker').datepicker({
			//  autoclose: true
			//})
			
			$('.form_datetime').datetimepicker({
				//language:  'fr',
				weekStart: 1,
				todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0,
				showMeridian: 0
			});
			
			$('#upload_barang').DataTable({
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'sPaginationType' : 'full_numbers',
			  'order': [[ 7, "desc" ]],
			  "dom": 'B<"top"flp>rt<"bottom"lpi><"clear">'
			})
			$('#update_stok').DataTable({
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'order': [[ 8, "desc" ]],
			  "dom": 'B<"top"flp>rt<"bottom"lpi><"clear">'
			})
			$('#transaksi').DataTable({
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'order': [[ 6, "desc" ]],
			  "dom": 'B<"top"flp>rt<"bottom"lpi><"clear">'
			})
			$('#pencairan_dana').DataTable({
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'order': [[ 7, "asc" ]],
			  "dom": 'B<"top"flp>rt<"bottom"lpi><"clear">'
			})
			$('#pencairan_saldo').DataTable({
			  'paging'      : false,
			  'lengthChange': false,
			  'searching'   : false,
			  'ordering'    : false,
			  'info'        : false,
			  'autoWidth'   : false
			})
			$('#seller_tabel').DataTable({ 
			   
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'order': [[ 1, "desc" ]], 
			  "dom": 'Bfrtip'
			})
			$('#hapus_barang').DataTable({ 
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'sPaginationType' : 'full_numbers',
			  'order': [[ 7, "desc" ]],
			  "dom": 'B<"top"flp>rt<"bottom"lpi><"clear">'
			})
			$('#edit_barang').DataTable({ 
			  'buttons'     : ['pageLength','copy', 'csv', 'excel', 'pdf', 'print'],
			  'paging'      : true,
			  'lengthChange': false,
			  'searching'   : true,
			  'ordering'    : true,
			  'info'        : true,
			  'autoWidth'   : false,
			  'sPaginationType' : 'full_numbers',
			  'order': [[ 8, "desc" ]],
			  "dom": 'B<"top"flp>rt<"bottom"lpi><"clear">'
			})
		  })
		</script>
		<script>
			var clipboard = new Clipboard('.btn');
		</script>
    </body>
</html>
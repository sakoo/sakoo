<!-- Content Header (Page header) -->
<?php

foreach ($total_berhasil->result() as $t1) { 
	$jumlah_berhasil = $t1->jumlah_berhasil;
}
foreach ($total_request->result() as $t2) { 
	$jumlah_request = $t2->jumlah_request;
}
foreach ($total_complete->result() as $t3) { 
	$jumlah_complete = $t3->jumlah_complete;
}
?>
<section class="content-header">
	<h1>
		Pencairan Dana
	</h1>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Pencairan Dana</a></li>
		<li class="active">Pencairan Dana</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box <?php if($status_dash == "berhasil") echo "bg-gray";?>" name="div_berhasil" id="div_berhasil" tabindex="0">
			<span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Transaksi Berhasil</span>
				<span class="info-box-number"><?php echo $jumlah_berhasil;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box <?php if($status_dash == "request") echo "bg-gray";?>" name="div_request" id="div_request" tabindex="0">
			<span class="info-box-icon bg-orange"><i class=""></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Saldo Perlu Ditransfer</span>
				<span class="info-box-number"><?php echo $jumlah_request;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="info-box <?php if($status_dash == "complete") echo "bg-gray";?>" name="div_complete" id="div_complete" tabindex="0">
			<span class="info-box-icon bg-green"><i class=""></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Completed</span>
				<span class="info-box-number"><?php echo $jumlah_complete;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="overflow: auto;">
				<table id="pencairan_dana" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th>No Transaksi</th>
						<th>Seller</th>
						<th>Toko Online</th>
						<!--<th>Jumlah Jenis Barang</th>-->
						<th>Jumlah Uang Ecommerce</th>
						<th>Jumlah Uang</th>
						<!--<th>Buyer</th>-->
						<th>Alamat Pengiriman</th>
						<th>Status</th>
						<th>Bukti Transfer</th>
						<!--<th>Bank</th>-->
						<th>Waktu Transaksi</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>No Transaksi</th>
						<th>Seller</th>
						<th>Toko Online</th>
						<!--<th>Jumlah Jenis Barang</th>-->
						<th>Jumlah Uang Ecommerce</th>
						<th>Jumlah Uang</th>
						<!--<th>Buyer</th>-->
						<th>Alamat Pengiriman</th>
						<th>Status</th>
						<th>Bukti Transfer</th>
						<!--<th>Bank</th>-->
						<th>Waktu Transaksi</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($record->result() as $r) { ?>
						<tr class="gradeU">
							<td>
							<?php 
								echo $r->no_trx;
								echo " | ".anchor('#','Preview',array('class'=>'btn btn-info btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-info','data-transaksi-id'=>''.$r->id_transaksi.'','data-commerce-id'=>''.$r->id_ecommerce.'','data-buyer-id'=>''.$r->buyer.'','data-alamat-id'=>''.$r->alamat_pengiriman.'','data-nama_ecom-id'=>''.$r->nama_ecommerce.''));
							?>
							</td>
							<td><?php echo $r->nama_toko ?></td>
							<td><?php echo $r->nama_ecommerce ?></td>
							<!--<td><?php //echo $r->jumlah ?></td>-->
							<td><?php echo $r->harga_pembayaran ?></td>
							<td><?php echo $r->harga_pembayaran_2 ?></td>
							<!--<td><?php //echo $r->buyer ?></td>-->
							<td><?php echo $r->alamat_pengiriman ?></td>
							<!-- Status Konfirmasi -->
							<td>
							<?php
								//Administrator, Supervisor, User
								//$this->session->userdata('role');
								$role = $this->session->userdata('role');
								$id_status_transaksi = $r->status_transaksi;
							    if(($r->status_pencairan_dana != 1 && $r->status_pencairan_dana != 2) || empty($r->status_pencairan_dana)) {
									echo "<div class='label label-danger'>Transaksi Berhasil</div>";
								}
								elseif($r->status_pencairan_dana == 1) {
									echo "<div class='label label-warning'>".$r->nama_status_pencairan_dana."</div>";
									if($role == 'Administrator') {
										echo " | ".anchor('pencairan_dana/proses_transfer/'.$r->id_transaksi_seller.'','Proses',array('class'=>'btn btn-warning btn-sm'));
									}
								}
								else {
									echo "<div class='label label-success'>Completed</div>";
								}
							?>
							</td>
							<td>
							<?php
								echo $r->nama_bank."<br>";
								if($r->bukti_transfer != "")
									echo "<img src='".base_url('assets/img/bukti_transfer/'.$r->bukti_transfer.'')."' class='img-thumbnail' width='100' height='100'/>";
							?>
							</td>
							<!--<td><?php //echo $r->nama_bank ?></td>-->
							<td><?php echo $r->tanggal_pemesanan ?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
<div class="modal modal-info fade" id="modal-info">
<form class="feedback" name="feedback">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Preview Transaksi</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
				<div class="box-body no-padding">
				 <table class="table" id="tableList2">
					<tr>
					  <td colspan="2">Buyer</td>
					  <td colspan="7"><div id="buyer_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Toko Online</td>
					  <td colspan="7"><div id="toko_online_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Alamat Kirim</td>
					  <td colspan="7"><div id="alamat_pengiriman_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Harga Pembelian</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_harga_pembelian_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembelian Ecommerce</td>
					  <td colspan="7"><div id="total_harga_pembelian_ecommerce"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="biaya_pengiriman_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Biaya Pengiriman Ecommerce</td>
					  <td colspan="7"><div id="biaya_pengiriman_ecommerce_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran</td>
					  <td colspan="7" style="background-color: #ff4444"><div id="total_pembayaran_p"></div></td>
					</tr>
					<tr>
					  <td colspan="2">Total Pembayaran Ecommerce</td>
					  <td colspan="7"><div id="total_pembayaran_ecommerce_p"></div></td>
					</tr>
					<tr>
					  <td colspan="9">List Barang</td>
					</tr>
					<tr>
					  <td colspan="9"></td>
					</tr>
					<tr>
					  <td>Seller</td>
					  <td colspan="8"><div id="seller_p"></div></td>
					</tr>
					<tr>
					   <td>Biaya Kirim</td>
					   <td colspan="8"><div id="biaya_kirim_p"></div></td>
					</tr>
					<tr>
					  <td>No Resi</td>
					  <td>Nama</td>
					  <td>Harga Satuan</td>
					  <td>Harga Satuan Ecommerce</td>
					  <td>Jumlah</td>
					  <td>Harga Pembelian</td>
					  <td>Harga Pembelian Ecommerce</td>
					  <td>Url</td>
					  <td>Status Konfirmasi</td>
					</tr>
				  </table>
				</div>
			</div>
		<!-- /.box-body -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
		</div>
	<!-- /.box -->
	</div>
</form>	
</div>
</section><!-- /.content -->
<script type="text/javascript">
$(document).ready(function(){
    $('#pencairan_dana').on('click','.btn.btn-info.btn-sm.budi', function() {
		var id_transaksi = $(this).data('transaksi-id');
		var id_ecommerce = $(this).data('commerce-id');
		var buyer = $(this).data('buyer-id');
		var alamat_pengiriman = $(this).data('alamat-id');
		var nama_ecommerce = $(this).data('nama_ecom-id');
		$.ajax({
			type:'POST',
			url:"<?php echo base_url('transaksi/modal2'); ?>",
			dataType: "json",
			data:"id_transaksi="+id_transaksi+"&id_ecommerce="+id_ecommerce,
			success:function(data){
				var out = null;
				var biaya_kirim = data.biaya_kirim;
				//console.log(biaya_kirim);
				var per_seller = data.per_seller;
				out = "<tbody><tr><td colspan='2'>Buyer</td><td colspan='7'><div id='buyer_p'>"+buyer+"</div></td></tr><tr><td colspan='2'>Toko Online</td><td colspan='7'><div id='toko_online_p'>"+nama_ecommerce+"</div></td></tr><tr><td colspan='2'>Alamat Kirim</td><td colspan='7'><div id='alamat_pengiriman_p'>"+alamat_pengiriman+"</div></td></tr>";
				out += "<tr><td colspan='2'>Total Harga Pembelian</td><td colspan='7' style='background-color: #ff4444'><div id='total_harga_pembelian_p'>"+data.harga_total_barang+"</div></td></tr>";
				out += "<tr><td colspan='2'>Total Pembelian Ecommerce</td><td colspan='7'><div id='total_harga_pembelian_ecommerce_p'>"+data.harga_total+"</div></td></tr>";
				out += "<tr><td colspan='2'>Biaya Pengiriman</td><td colspan='7' style='background-color: #ff4444'><div id='biaya_pengiriman_p'>"+data.biaya_kirim_barang+"</div></td></tr>";
				out += "<tr><td colspan='2'>Biaya Pengiriman Ecommerce</td><td colspan='7'><div id='biaya_pengiriman_ecommerce_p'>"+data.biaya_kirim+"</div></td></tr>";
				out += "<tr><td colspan='2'>Total Pembayaran</td><td colspan='7' style='background-color: #ff4444'><div id='total_pembayaran_p'>"+data.harga_pembayaran_barang+"</div></td></tr>";
				out += "<tr><td colspan='2'>Total Pembayaran Ecommerce</td><td colspan='7'><div id='total_pembayaran_ecommerce_p'>"+data.harga_pembayaran+"</div></td></tr>";
				out += "<tr><td colspan='9'>List Barang</td></tr><tr><td colspan='9'></td></tr>";
				$.each(per_seller, function (index, object) {
					//console.log(object);
					/*
					   <tr><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>
					*/
					out += '<tr><td>Seller</td> <td colspan=\'8\'><div id=\'seller_p\'>'+object.nama_toko+'</div></td></tr><tr><td>Biaya Kirim</td><td colspan=\'8\'><div id=\'biaya_kirim_p\'>'+object.biaya_kirim_seller+'</div></td></tr>';
					out += '<tr style=\'background-color: #007E33\'><td>No Resi</td><td>Nama</td><td>Harga Satuan</td><td>Harga Satuan Ecommerce</td><td>Jumlah</td><td>Harga Pembelian</td><td>Harga Pembelian Ecommerce</td><td>Url</td><td>Status Konfirmasi</td></tr>';
					var detail = object.detail;
					$.each(detail, function (index, object2) {
						//console.log(object2);
						out += '<tr style=\'background-color: #00C851\'><td>'+object2.nomor_resi+'</td><td>'+object2.nama_barang+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_satuan+'</td><td>'+object2.harga_satuan_ecommerce+'</td><td>'+object2.jumlah_pembelian+'</td><td style=\'background-color: #ff4444\'>'+object2.harga_pembelian+'</td><td>'+object2.harga_pembelian_ecommerce+'</td><td>'+object2.url+'</td><td>'+object2.status_konfirmasi+'</td></tr>';
					});
					//dataArray.push([value["nama"].toString(), value["nama_toko"] ]);
					//out += '<tr style=\'background-color: #00C851\'><td>'+object.nomor_resi+'</td><td>'+object.nama_toko+'</td><td>'+object.nama+'</td><td style=\'background-color: #ff4444\'>'+object.harga_satuan+'</td><td>'+object.harga_satuan_ecommerce+'</td><td>'+object.jumlah_pembelian+'</td><td>'+object.biaya_kirim_seller+'</td><td style=\'background-color: #ff4444\'>'+object.harga_pembelian+'</td><td>'+object.harga_pembelian_ecommerce+'</td><td style=\'background-color: #ff4444\'>'+object.harga_total+'</td ><td>'+object.harga_total_ecommerce+'</td><td>'+object.url+'</td></tr>';
				});
				out+= '</tbody>';
				$('#tableList2').empty();
				$('#tableList2').append($('<table></table>').attr('class', 'table'));
				$('#tableList2').append(out);
				var out = null;
			}
		});
	});
	
	$("#div_berhasil").click(function() {
	  window.location = "<?php echo base_url('pencairan_dana/show_berhasil'); ?>"; 
	  return false;
	});
	$("#div_request").click(function() {
	  window.location = "<?php echo base_url('pencairan_dana/show_request'); ?>";
	  return false;
	});
	$("#div_complete").click(function() {
	  window.location = "<?php echo base_url('pencairan_dana/show_complete'); ?>";
	  return false;
	});
	
});
</script>
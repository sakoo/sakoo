<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Transfer</h3>
		</div>
		<?php
		//parameter 
		foreach ($record->result() as $r) { 
		    $id_transaksi_seller = $r->id_transaksi_seller;
			$id_transaksi = $r->id_transaksi;
			$no_trx = $r->no_trx;
			$jumlah = $r->jumlah;
			$harga_pembayaran = $r->harga_pembayaran_2;
			$harga_pembayaran_ecommerce = $r->harga_pembayaran_3;
			$buyer = $r->buyer;
			$rekening = $r->rekening;
			$nama_rekening = $r->nama_rekening;
			$nama_bank_customer = $r->nama_bank_customer;
			$eksekusi = $this->m_pencairan_dana->show_bank_detail($nama_bank_customer);
			foreach ($eksekusi->result_array() as $data) {
				$detail_bank = $data['nama'];
			}
			$seller = $r->nama_toko;
			$id_toko = $r->id_toko;
			$alamat_pengiriman = $r->alamat_pengiriman;
			$alamat_pengiriman = preg_replace('#\h{2,}#m', " ", $alamat_pengiriman);
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="<?php echo base_url('pencairan_dana/proses_completed') ?>" method="POST" enctype="multipart/form-data">
			<?php
			//set peringatan
			if (validation_errors() || $this->session->flashdata('result_login')) {
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Warning!</strong>
					<?php echo validation_errors(); ?>
					<?php echo $this->session->flashdata('result_login'); ?>
				</div>    
			<?php } ?>
			<div class="box-body">
				<div class="form-group">
					<label for="noTransaksi" class="col-sm-3 control-label">No Transaksi</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="noTransaksi" name="noTransaksi" value="<?php echo $no_trx;?>" readonly>
						<input type="hidden" class="form-control" id="idTransaksiSeller" name="idTransaksiSeller" value="<?php echo $id_transaksi_seller;?>" readonly>
						<input type="hidden" class="form-control" id="idTransaksiSeller" name="idTransaksi" value="<?php echo $id_transaksi;?>" readonly>
						<input type="hidden" class="form-control" id="noTrx" name="noTrx" value="<?php echo $no_trx;?>" readonly>
						<input type="hidden" class="form-control" id="idToko" name="idToko" value="<?php echo $id_toko;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="noTransaksi" class="col-sm-3 control-label">No Transaksi Seller</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="noTransaksiSeller" name="noTransaksiSeller" value="<?php echo $id_transaksi_seller;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="noTransaksi" class="col-sm-3 control-label">Seller</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="seller" name="seller" value="<?php echo $seller;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="jumlah" class="col-sm-3 control-label">Jumlah Jenis Barang</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="jumlah" name="jumlah" value="<?php echo $jumlah;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="jumlah" class="col-sm-3 control-label">Rekening Seller</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="rekening" name="rekening" value="<?php echo $rekening;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#rekening">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="jumlah" class="col-sm-3 control-label">Nama Rekening Seller</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="namaRekening" name="namaRekening" value="<?php echo $nama_rekening;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="jumlah" class="col-sm-3 control-label">Rekening Bank Seller</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="namaBankCustomer" name="namaBankCustomer" value="<?php echo $detail_bank;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaPembayaran" class="col-sm-3 control-label">Harga Pembayaran Ecommerce</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="hargaPembayaranEcommerce" name="hargaPembayaranEcommerce" value="<?php echo $harga_pembayaran_ecommerce;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaPembayaran" class="col-sm-3 control-label">Harga Pembayaran</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="hargaPembayaran" name="hargaPembayaran" value="<?php echo $harga_pembayaran;?>" readonly>
						<button type="button" class="btn" style="background-color: #ff4444" data-clipboard-action="copy" data-clipboard-target="#hargaPembayaran">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="buyer" class="col-sm-3 control-label">Buyer</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="buyer" name="buyer" value="<?php echo $buyer;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="alamatPengiriman" class="col-sm-3 control-label">Alamat Pengiriman</label>
					<div class="col-sm-9">
						<textarea class="form-control" id="alamatPengiriman" rows="5" name="alamatPengiriman" readonly><?php echo $alamat_pengiriman;?></textarea>
					</div>
				</div>
				<div class="form-group">
				<div id="image-preview-div" style="display: none">
					<label class="col-sm-3 control-label" for="exampleInputFile">Selected image</label>
					<div class="col-sm-9">
						<img id="preview-img" src="noimage">
					</div>
				</div>
				<div class="form-group">
					<label for="buktiTransfer" class="col-sm-3 control-label">Bukti Transfer</label>
					<div class="col-sm-9">
						<input type="file" name="file" id="file" required>
					</div>
				</div>
				<div class="form-group">
					<label for="buyer" class="col-sm-3 control-label">Bank</label>
					<div class="col-sm-4">
						<select class="form-control" id="bank" name="bank">
							<option>--Pilih Bank--</option>
						<?php
							$eksekusi = $this->m_pencairan_dana->show_bank();
							foreach ($eksekusi->result_array() as $data) {
								$id = $data['id'];
								$nama = $data['nama'];
								echo "<option value='".$id."'>".$nama."</option>";
							}
						?>
						</select>
					</div>
                </div>
			</div>
			<!-- /.box-body -->

			 <div class="box-footer">
				<button type="submit" name="submit" class="btn btn-danger">Transfer Selesai</button>&nbsp &nbsp
				<?php echo anchor('pencairan_dana','Kembali',array('class'=>'btn btn-primary'))?>
			</div>
		</form>
		</div>
		<!-- /.box -->
		<!-- loading -->
		<div class="alert alert-info" id="loading" style="display: none;" role="alert">
          Uploading image...
          <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            </div>
          </div>
        </div>
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
<script src="<?php echo base_url('assets/js/upload-image.js'); ?>"></script>
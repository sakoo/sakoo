<!-- Notifications: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
	<a href="#" class="dropdown-toggle" id="klik_notif" data-toggle="dropdown">
		<i class="fa fa-bell-o"></i>
		<?php 
		
			foreach ($jumlah_notifikasi->result() as $j) {
				$jumlah = $j->jumlah;
				echo "<input type='hidden' class='form-control' id='user' name='user' value='".$j->user."'></input>";
				echo "<input type='hidden' class='form-control' id='user_grup' name='user_grup' value='".$j->user_grup."'></input>";
			}
			if($jumlah > 0) {
				echo "<span id='span_label' class='label label-danger'>".$jumlah."</span>";
			}
		
		?>
	</a>
	<ul class="dropdown-menu">
		<li class="header">You have <?php echo $jumlah;?> notifications</li>
		<li>
			<!-- inner menu: contains the actual data -->
			<?php //print_r($record);?>
			<ul class="menu">
				<?php 
					foreach ($record->result() as $r) {
						$jumlah = $r->deskripsi;
						echo "<li class='bg-info'>";
						echo "<a href='#' data-toggle='tooltip' data-placement='auto' title='".$r->deskripsi."'>";
						echo "<i class='fa fa-warning text-yellow'></i>".$r->deskripsi."";
						echo "</a>";
						echo "</li>";
					}
					//echo "";
				
				?>
			</ul>
		</li>
		<li class="footer"><a href="#">View all</a></li>
	</ul>
</li>
<script type="text/javascript">
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
    $(function () {
		$("#klik_notif").click(function() {
			var user = $("#user").val();
			var user_grup = $("#user_grup").val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>notifikasi/klik_notif",
				dataType: "json",
				data:"user=" +user+"&user_grup="+user_grup,
				success: function(data){
					$('#span_label').empty();
					//alert("Sukses");
				}
			});
		});
		
    });
	
});
</script>
<!-- Content Header (Page header) -->
<?php

foreach ($total_edit->result() as $te) { 
	$jumlah_edit = $te->jumlah_edit;
}
foreach ($total_checklist->result() as $tc) { 
	$jumlah_checklist = $tc->jumlah_checklist;
}
foreach ($total_edited->result() as $ted) { 
	$jumlah_edited = $ted->jumlah_edited;
}
?>
<section class="content-header">
	<h1>
		Edit Barang
	</h1>
	<ol class="breadcrumb">
		<li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Edit Barang</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "edit") echo "bg-gray";?>" name="div_edit" id="div_edit" tabindex="0">
			<span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Perlu Diedit</span>
				<span class="info-box-number"><?php echo $jumlah_edit;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "checklist") echo "bg-gray";?>" name="div_checklist" id="div_checklist" tabindex="0">
			<span class="info-box-icon bg-orange"><i class=""></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Perlu Checklist</span>
				<span class="info-box-number"><?php echo $jumlah_checklist;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->

	<!-- fix for small devices only -->
	<div class="clearfix visible-sm-block"></div>

	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box <?php if($status_dash == "edited") echo "bg-gray";?>" name="div_edited" id="div_edited" tabindex="0">
			<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
			<div class="info-box-content">
				<span class="info-box-text">Completed</span>
				<span class="info-box-number"><?php echo $jumlah_edited;?></span>
			</div><!-- /.info-box-content -->
		</div><!-- /.info-box -->
	</div><!-- /.col -->
</div><!-- /.row -->

<div class="row">
	<div class="col-md-12">
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body" style="overflow: auto;">
				<table id="edit_barang" class="table table-bordered table-striped">
                <thead>
					<tr>
						<th rowspan="2">Nama Barang</th>
						<th rowspan="2">Nama Toko</th>
						<th rowspan="2">Stok</th>
						<th rowspan="2">Field yang Diedit</th>
						<!--<th rowspan="2">Gambar</th>-->
						<th colspan="4" class="text-center">Status</th>
						<th rowspan="2">Waktu</th>
					</tr>
					<tr>
						<th>Blanja</th>
						<th>Tokopedia</th>
						<th>Bukalapak</th>
						<th>Shopee</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Nama Barang</th>
						<th>Nama Toko</th>
						<th>Stok</th>
						<th>Field yang Diedit</th>
						<!--<th>Gambar</th>-->
						<th>Blanja</th>
						<th>Tokopedia</th>
						<th>Bukalapak</th>
						<th>Shopee</th>
						<th>Waktu</th>
					</tr>
				</tfoot>
				<tbody>
					<?php foreach ($record->result() as $r) { ?>
						<tr class="gradeU">
						    <?php
								$nomor_hp = preg_replace("[^0]", "62", $r->no_hp);
							?>
							<td>
							<?php 
								echo $r->nama_barang;
								echo " | ".anchor('#','Preview',array('class'=>'btn btn-info btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-info','data-barang-id'=>''.$r->id_barang.''));
							?>
							</td>
							<td><?php echo "<a href='https://api.whatsapp.com/send?phone=".$nomor_hp."' target='_blank' style='color: black;'>".$r->nama_toko."</a>" ?></td>
							<td><?php echo $r->stok ?></td>
							<td>
							<?php 
							/*
								if(strlen($r->deskripsi) > 100) {
									echo substr($r->deskripsi,0,100)." ..."; 
								}
								else
									echo $r->deskripsi;
							*/
								echo substr($r->field, 0, -1);
							?>
							</td>
							<!--<td><img src="<?php //echo $r->foto; ?>" class="img-thumbnail" width="100" height="100"/></td>-->
							<td>
							<?php 
								//Administrator, Supervisor, User
								//$this->session->userdata('role');
								$role = $this->session->userdata('role');
								//blanja
								$id_barang = $r->id_barang;
								$id_log_edit_barang = $r->id_log_edit_barang;
								$eksekusi_1 = $this->m_barang->show_status_edit($id_log_edit_barang,1);
								foreach ($eksekusi_1->result_array() as $data_1) {
									//print_r($data_final_3_datin);
									$id_status = $data_1['id_status'];
									$status_edit_barang = $data_1['status_edit_barang'];
									$status = $data_1['status'];
								}
								if(($role=='Administrator' || $role=='Supervisor' || $role=='User') && $status_edit_barang=='1') {
									echo "<div class='label label-danger'>".$status."</div>";
									echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/1/'.$r->id_log_edit_barang.'','Proses',array('class'=>'btn btn-danger btn-sm'));
								}
								elseif(($role=='Administrator' || $role=='Supervisor') && $status_edit_barang=='2') {
									echo "<div class='label label-warning'>".$status."</div>";
									echo " | ".anchor('#','Proses',array('class'=>'btn btn-warning btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-warning','data-barang-id'=>''.$r->id_barang.'','data-ecommerce-id'=>'1','data-log-edit-barang-id'=>''.$r->id_log_edit_barang.''));
								}
								else {
									echo "<div class='label label-success'>".$status."</div>";
									//if(($role=='Administrator' || $role=='Supervisor')) {
									//	echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/1','Edit',array('class'=>'btn btn-danger btn-sm'));
									//}
								}
							?>
							</td>
							<td>
							<?php
								//tokopedia
								$eksekusi_2 = $this->m_barang->show_status_edit($id_log_edit_barang,2);
								foreach ($eksekusi_2->result_array() as $data_2) {
									//print_r($data_final_3_datin);
									$id_status = $data_2['id_status'];
									$status_edit_barang = $data_2['status_edit_barang'];
									$status = $data_2['status'];
								}
								if(($role=='Administrator' || $role=='Supervisor' || $role=='User') && $status_edit_barang=='1') {
									echo "<div class='label label-danger'>".$status."</div>";
									echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/2/'.$r->id_log_edit_barang.'','Proses',array('class'=>'btn btn-danger btn-sm'));
								}
								elseif(($role=='Administrator' || $role=='Supervisor') && $status_edit_barang=='2') {
									echo "<div class='label label-warning'>".$status."</div>";
									echo " | ".anchor('#','Proses',array('class'=>'btn btn-warning btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-warning','data-barang-id'=>''.$r->id_barang.'','data-ecommerce-id'=>'2','data-log-edit-barang-id'=>''.$r->id_log_edit_barang.''));
								}
								else {
									echo "<div class='label label-success'>".$status."</div>";
									//if(($role=='Administrator' || $role=='Supervisor')) {
									//	echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/1','Edit',array('class'=>'btn btn-danger btn-sm'));
									//}
								}
							?>
							</td>
							<td>
							<?php
								//bukalapak
								$eksekusi_3 = $this->m_barang->show_status_edit($id_log_edit_barang,3);
								foreach ($eksekusi_3->result_array() as $data_3) {
									//print_r($data_final_3_datin);
									$id_status = $data_3['id_status'];
									$status_edit_barang = $data_3['status_edit_barang'];
									$status = $data_3['status'];
								}
								if(($role=='Administrator' || $role=='Supervisor' || $role=='User') && $status_edit_barang=='1') {
									echo "<div class='label label-danger'>".$status."</div>";
									echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/3/'.$r->id_log_edit_barang.'','Proses',array('class'=>'btn btn-danger btn-sm'));
								}
								elseif(($role=='Administrator' || $role=='Supervisor') && $status_edit_barang=='2') {
									echo "<div class='label label-warning'>".$status."</div>";
									echo " | ".anchor('#','Proses',array('class'=>'btn btn-warning btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-warning','data-barang-id'=>''.$r->id_barang.'','data-ecommerce-id'=>'3','data-log-edit-barang-id'=>''.$r->id_log_edit_barang.''));
								}
								else {
									echo "<div class='label label-success'>".$status."</div>";
									//if(($role=='Administrator' || $role=='Supervisor')) {
									//	echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/1','Edit',array('class'=>'btn btn-danger btn-sm'));
									//}
								}
							?>
							</td>
							<td>
							<?php
								//shopee
								$eksekusi_4 = $this->m_barang->show_status_edit($id_log_edit_barang,4); 
								foreach ($eksekusi_4->result_array() as $data_4) {
									//print_r($data_final_3_datin);
									$id_status = $data_4['id_status'];
									$status_edit_barang = $data_4['status_edit_barang'];
									$status = $data_4['status'];
								}
								if(($role=='Administrator' || $role=='Supervisor' || $role=='User') && $status_edit_barang=='1') {
									echo "<div class='label label-danger'>".$status."</div>";
									echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/4/'.$r->id_log_edit_barang.'','Proses',array('class'=>'btn btn-danger btn-sm'));
								}
								elseif(($role=='Administrator' || $role=='Supervisor') && $status_edit_barang=='2') {
									echo "<div class='label label-warning'>".$status."</div>";
									echo " | ".anchor('#','Proses',array('class'=>'btn btn-warning btn-sm budi','data-toggle'=>'modal','data-target'=>'#modal-warning','data-barang-id'=>''.$r->id_barang.'','data-ecommerce-id'=>'4','data-log-edit-barang-id'=>''.$r->id_log_edit_barang.''));
								}
								else {
									echo "<div class='label label-success'>".$status."</div>";
									//if(($role=='Administrator' || $role=='Supervisor')) {
									//	echo " | ".anchor('edit_barang/proses_edit/'.$r->id_barang.'/1','Edit',array('class'=>'btn btn-danger btn-sm'));
									//}
								}
							?>
							</td>
							<td><?php echo $r->timestamp ?></td>
						</tr>
					<?php } ?>
				</tbody>
				</table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
	</div><!-- /.col -->
</div><!-- /.row -->
<div class="modal modal-warning fade" id="modal-warning">
<form class="feedback" name="feedback">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Proses Checklist Edit Barang</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
					<div class="box-body no-padding">
					  <table class="table">
						<tr>
						  <td>Nama Barang</td>
						  <td><div id="nama_barang_m"></div></td>
						</tr>
						<tr>
						  <td>Nama Toko</td>
						  <td><div id="nama_toko_m"></div></td>
						</tr>
						<tr>
						  <td>Kode Barang</td>
						  <td><div id="id_barang_m"></div></td>
						</tr>
						<tr>
						  <td>Stok</td>
						  <td><div id="stok_m"></div></td>
						</tr>
						<tr>
						  <td>Harga Satuan</td>
						  <td><div id="harga_satuan_m"></div></td>
						</tr>
						<tr>
						  <td>Harga Mark Up</td>
						  <td><div id="harga_markup_m"></div></td>
						</tr>
						<tr>
						  <td>Deskripsi</td>
						  <td><div id="deskripsi_m"></div></td>
						</tr>
						<tr>
						  <td>Gambar</td>
						  <td><div id="foto_m"></div></td>
						</tr>
						<tr>
						  <td>Ecommerce</td>
						  <td><div id="nama_ecommerce_m"></div></td>
						</tr>
						<tr>
						  <td>Waktu Upload</td>
						  <td><div id="waktu_upload_m"></div></td>
						</tr>
						<tr>
						  <td>Url</td>
						  <td><a id="url_link" target="_blank" style="text-decoration: none"><div id="url_m"></div></a></td>
						</tr>
					  </table>
					</div>
					<input type="hidden" class="form-control" id="id_barang_m2" name="id_barang_m2" value=""></input>
					<input type="hidden" class="form-control" id="id_ecommerce_m" name="id_ecommerce_m" value=""></input>
					<input type="hidden" class="form-control" id="id_log_edit_barang" name="id_log_edit_barang" value=""></input>
					<!-- /.box-body -->
				  </div>
				  <!-- /.box -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-outline" id="submit_modal">Checked</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</form>	
</div>
<!-- /.modal -->
<div class="modal modal-info fade" id="modal-info">
<form class="feedback" name="feedback">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Preview</h4>
			</div>
			<div class="modal-body" style="overflow: auto">
					<div class="box-body no-padding">
					  <table class="table">
						<tr>
						  <td>Nama Barang</td>
						  <td><div id="nama_barang_p"></div></td>
						</tr>
						<tr>
						  <td>Nama Toko</td>
						  <td><div id="nama_toko_p"></div></td>
						</tr>
						<tr>
						  <td>Kode Barang</td>
						  <td><div id="id_barang_p"></div></td>
						</tr>
						<tr>
						  <td>Stok</td>
						  <td><div id="stok_p"></div></td>
						</tr>
						<tr>
						  <td>Harga Satuan</td>
						  <td><div id="harga_satuan_p"></div></td>
						</tr>
						<tr>
						  <td>Harga Mark Up</td>
						  <td><div id="harga_markup_p"></div></td>
						</tr>
						<tr>
						  <td>Deskripsi</td>
						  <td><div id="deskripsi_p"></div></td>
						</tr>
						<tr>
						  <td>Gambar</td>
						  <td><div id="foto_p"></div></td>
						</tr>
						<tr>
						  <td>Waktu Upload</td>
						  <td><div id="waktu_upload_p"></div></td>
						</tr>
						<tr>
						  <td colspan="2" style="text-align:center;">Url</td>
						</tr>
						<tr>
						  <td>Blanja</td>
						  <td><a id="url_link1" target="_blank" style="text-decoration: none"><div id="url_p1"></div></a></td>
						</tr>
						<tr>
						  <td>Tokopedia</td>
						  <td><a id="url_link2" target="_blank" style="text-decoration: none"><div id="url_p2"></div></a></td>
						</tr>
						<tr>
						  <td>Bukalapak</td>
						  <td><a id="url_link3" target="_blank" style="text-decoration: none"><div id="url_p3"></div></a></td>
						</tr>
						<tr>
						  <td>Shopee</td>
						  <td><a id="url_link4" target="_blank" style="text-decoration: none"><div id="url_p4"></div></a></td>
						</tr>
					  </table>
					</div>
					<!-- /.box-body -->
				  </div>
				  <!-- /.box -->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-outline pull-right" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</form>	
</div>
<!-- /.modal -->
</section><!-- /.content -->
<script type="text/javascript">
$(document).ready(function(){
    $(function () {
        $('#edit_barang').on('click','.btn.btn-warning.btn-sm.budi', function() {
            var id_barang = $(this).data('barang-id');
			var id_ecommerce = $(this).data('ecommerce-id');
			var id_log_edit_barang = $(this).data('log-edit-barang-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('edit_barang/modal'); ?>',
				dataType: "json",
				data:"id_barang=" +id_barang+"&id_ecommerce="+id_ecommerce+"&id_log_edit_barang="+id_log_edit_barang,
				success:function(data){
					$(".modal-body #nama_barang_m").text(data["nama_barang"]);
					$(".modal-body #nama_toko_m").text(data["nama_toko"]+" ("+data["lokasi"] +")");
					$(".modal-body #id_barang_m").text(data["id_barang"]);
					$(".modal-body #stok_m").text(data["stok"]);
					$(".modal-body #deskripsi_m").text(data["deskripsi"]);
					//JSON.parse(this_has_quotes);
					foto = data["foto"];
					foto2 = foto.replace(/\"/g, "");
					$("#foto_m").contents().remove();
					$(".modal-body #foto_m").append(foto);
					$(".modal-body #nama_ecommerce_m").text(data["nama_ecommerce"]);
					$(".modal-body #waktu_upload_m").text(data["waktu_upload"]);
					$(".modal-body #url_link").attr("href",data["url"]);
					$(".modal-body #url_m").text(data["url"]);
					$(".modal-body #id_barang_m2").val(data["id_barang"]);
					$(".modal-body #id_ecommerce_m").val(data["id_ecommerce"]);
					$(".modal-body #id_log_edit_barang").val(data["id_log_edit_barang"]);
					var foto_tambahan = data.foto_tambahan;
					for (var i = 0, len = foto_tambahan.length; i < len; i++) 
					{
						$(".modal-body #foto_m").append(foto_tambahan[i]);
					}
					$(".modal-body #harga_satuan_m").text(data["harga_satuan"]);
					$(".modal-body #harga_markup_m").text(data["harga_markup"]);
				}
			});
        });
		
		$('#edit_barang').on('click','.btn.btn-info.btn-sm.budi', function() {
            var id_barang = $(this).data('barang-id');
			$.ajax({
				type:'POST',
				url:'<?php echo base_url('edit_barang/preview'); ?>',
				dataType: "json",
				data:"id_barang=" +id_barang,
				success:function(data){
					$(".modal-body #nama_barang_p").text(data["nama_barang"]);
					$(".modal-body #nama_toko_p").text(data["nama_toko"]+" ("+data["lokasi"] +")");
					$(".modal-body #id_barang_p").text(data["id_barang"]);
					$(".modal-body #stok_p").text(data["stok"]);
					$(".modal-body #deskripsi_p").text(data["deskripsi"]);
					//JSON.parse(this_has_quotes);
					foto = data["foto"];
					foto2 = foto.replace(/\"/g, "");
					$("#foto_p").contents().remove();
					$(".modal-body #foto_p").append(foto);
					$(".modal-body #waktu_upload_p").text(data["waktu_upload"]);
					var foto_tambahan = data.foto_tambahan;
					for (var i = 0, len = foto_tambahan.length; i < len; i++) 
					{
						$(".modal-body #foto_p").append(foto_tambahan[i]);
					}
					var url_list = data.url_list;
					console.log(url_list);
					for (var i = 0, len = url_list.length; i < len; i++) 
					{
						var i_plus = i+1;
						var url_param = "#url_p"+i_plus;
						var url_link_param = "#url_link"+i_plus;
						$(".modal-body "+url_param).text(url_list[i]);
						$(".modal-body "+url_link_param).attr("href",url_list[i]);
					}
					$(".modal-body #harga_satuan_p").text(data["harga_satuan"]);
					$(".modal-body #harga_markup_p").text(data["harga_markup"]);
				}
			});
        });
		
		$("#submit_modal").click(function() {
			var id_barang = $("#id_barang_m2").val();
			var id_ecommerce = $("#id_ecommerce_m").val();
			var id_log_edit_barang = $("#id_log_edit_barang").val();
			var url_link = window.location.href;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('edit_barang/modal_checklist'); ?>",
				dataType: "json",
				data:"id_barang=" +id_barang+"&id_ecommerce="+id_ecommerce+"&id_log_edit_barang="+id_log_edit_barang,
				success: function(message){
					$("#modal-warning").modal('hide');
					$(location).attr('href',url_link);
				},
				error: function(){
					alert("Error");
				}
			});
		});
		
		$("#div_edit").click(function() {
		  window.location = "<?php echo base_url('edit_barang/show_edit'); ?>"; 
		  return false;
		});
		$("#div_checklist").click(function() {
		  window.location = "<?php echo base_url('edit_barang/show_checklist'); ?>";
		  return false;
		});
		$("#div_edited").click(function() {
		  window.location = "<?php echo base_url('edit_barang/show_edited'); ?>";
		  return false;
		});
		
    });
	
});
</script>
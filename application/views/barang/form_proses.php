<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Barang</h3>
		</div>
		<?php
		//parameter 
		$role = $this->session->userdata('role');
		//print_r($record->result());
		foreach ($record->result() as $r) { 
			$nama_barang = $r->nama_barang;
			$nama_toko = $r->nama_toko;
			$id_barang = $r->id_barang;
			$deskripsi = "Penjual : ".$r->nama_toko."\r\n";
			$deskripsi .= "Dikirim dari : ".$r->lokasi."\r\n";
			if($r->merk != "")
				$deskripsi .= "Merk : ".$r->merk."\r\n";
			if($r->bahan != "")
				$deskripsi .= "Bahan : ".$r->bahan."\r\n";
			if($r->volume != "0x0x0")
				$deskripsi .= "Volume : ".$r->volume."\r\n";
			$deskripsi .= $r->deskripsi;
			//$deskripsi = $r->deskripsi;
			$foto = $r->foto;
			$waktu_upload = $r->tanggal_upload;
			$stok = $r->stok;
			$harga_satuan = $r->harga_satuan;
			$harga_markup = $r->harga_markup;
			$berat = $r->berat;
			$lokasi = $r->lokasi;
			$id_status_penyebab_unlisting = $r->id_status_penyebab_unlisting;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action= "<?php echo base_url(); ?>upload_barang/proses_listing" method="POST">
			<?php
			//set peringatan
			if (validation_errors() || $this->session->flashdata('result_login')) {
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Warning!</strong>
					<?php echo validation_errors(); ?>
					<?php echo $this->session->flashdata('result_login'); ?>
				</div>    
			<?php } ?>
			<div class="box-body">
				<div class="form-group">
					<label for="namaBarang" class="col-sm-2 control-label">Nama Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaBarang" name="namaBarang" value="<?php echo $nama_barang;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#namaBarang">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="namaToko" class="col-sm-2 control-label">Nama Toko</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaToko" name="namaToko" value="<?php echo $nama_toko." (".$lokasi.")";?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#namaToko">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="kodeBarang" class="col-sm-2 control-label">Kode Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="kodeBarang" name="kodeBarang" value="<?php echo $id_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="stok" class="col-sm-2 control-label">Stok</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="stok" name="stok" value="<?php echo $stok;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#stok">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="berat" class="col-sm-2 control-label">Berat (gram)</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="berat" name="berat" value="<?php echo $berat;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#berat">Copy</button>
					</div>
				</div>
				<!--<div class="form-group">
					<label for="hargaSatuan" class="col-sm-2 control-label">Harga Satuan</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaSatuan" name="hargaSatuan" value="<?php //echo $harga_satuan;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#hargaSatuan">Copy</button>
					</div>
				</div>-->
				<div class="form-group">
					<label for="hargaSatuan" class="col-sm-2 control-label">Harga Mark Up</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaMarkUp" name="hargaMarkUp" value="<?php echo $harga_markup;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#hargaMarkUp">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="deskripsi" class="col-sm-2 control-label">Deskripsi</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="deskripsi" name="deskripsi" rows="4" readonly><?php echo $deskripsi;?></textarea>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#deskripsi">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="gambar" class="col-sm-2 control-label">Gambar</label>
					<div class="col-sm-10">
						<a href="<?php echo htmlspecialchars("".$r->foto."");?>" download="<?php echo htmlspecialchars("".$r->foto."");?>">
							<img src="<?php echo $r->foto; ?>" class="img-thumbnail" width="100" height="100"/>
						</a>
						<?php
							foreach ($foto_tambahan->result() as $f) { 
							echo "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
									<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
								  </a>";
							}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="waktuUpload" class="col-sm-2 control-label">Waktu Upload</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="waktuUpload" name="waktuUpload" value="<?php echo $waktu_upload;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="waktuUpload" class="col-sm-2 control-label">Penyebab Unlisting</label>
					<div class="col-sm-5">
						<select class="form-control" id="penyebab" name="penyebab">
							<option value='0'>--Pilih Penyebab--</option>
						<?php
							$eksekusi = $this->m_barang->show_penyebab_unlisting();
							foreach ($eksekusi->result_array() as $data) {
								$id = $data['id'];
								$status_penyebab = $data['status'];
								if($id_status_penyebab_unlisting == $id) {
									echo "<option value='".$id."' selected>".$status_penyebab."</option>";
									echo "Jancuk";
								}
								else {
									echo "<option value='".$id."'>".$status_penyebab."</option>";
								}
							}
						?>
						</select>
							<button type='submit' id='proses_penyebab' name='proses_penyebab' class='btn btn-danger'>Save</button>
					</div>
				</div>
				<?php
				foreach ($status->result() as $s) { 
					$id_status = $s->id_status;
					$status = $s->status;
					$id_ecommerce = $s->id_ecommerce;
					$nama_ecommerce = $s->nama;
					$url = $s->url;
					echo "<div class='form-group'>";
						echo "<label for='url' class='col-sm-2 control-label'>Url ".$nama_ecommerce."</label>";
						echo "<div class='col-sm-10'>";
							echo "<input type='text' class='form-control' id='url_".$id_ecommerce."' name='url_".$id_ecommerce."' value='".$url."'>";
								if(($role=='Administrator' || $role=='Supervisor' || $role=='User') && $id_status=='1') {
									echo "<div class='label label-danger'>".$status."</div>";
									echo " |  <button type='submit' id='proses_url_".$id_ecommerce."' name='proses_url_".$id_ecommerce."' class='btn btn-danger'>Proses</button>"; 
								}
								elseif(($role=='Administrator' || $role=='Supervisor') && $id_status=='2') {
									echo "<div class='label label-warning'>".$status."</div>";
									echo " |  <button type='submit' id='proses_url_".$id_ecommerce."' name='proses_url_".$id_ecommerce."' class='btn btn-danger'>Edit</button>"; 
									echo " <button type='submit' id='checklist_url_".$id_ecommerce."' name='checklist_url_".$id_ecommerce."' class='btn btn-warning'>Checklist</button>"; 
								}
								else {
									echo "<div class='label label-success'>".$status."</div>";
									if(($role=='Administrator' || $role=='Supervisor')) {
										echo " |  <button type='submit' id='proses_url_".$id_ecommerce."' name='proses_url_".$id_ecommerce."' class='btn btn-danger'>Edit</button>"; 
									}
								}
						echo "</div>";
					echo "</div>";
				}?>
				<div class="box-footer">
					<?php echo anchor('upload_barang','Kembali',array('class'=>'btn btn-primary'))?>
				</div>
			</div>
		</form>
		<!-- /.box -->
		</div>
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
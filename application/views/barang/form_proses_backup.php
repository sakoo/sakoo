<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Barang</h3>
		</div>
		<?php
		//parameter 
		foreach ($record->result() as $r) { 
			$nama_barang = $r->nama_barang;
			$nama_toko = $r->nama_toko;
			$id_barang = $r->id_barang;
			$deskripsi = $r->deskripsi;
			$foto = $r->foto;
			$waktu_upload = $r->tanggal_upload;
			$stok = $r->stok;
			$harga_satuan = $r->harga_satuan;
			$berat = $r->berat;
		}
		foreach ($status->result() as $s) { 
			$id_status = $s->id_status;
			$status_barang = $s->status;
			$id_ecommerce = $s->id_ecommerce;
			$nama_ecommerce = $s->nama;
		}
		?>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action="upload_barang/proses_listing" method="POST">
			<?php
			//set peringatan
			if (validation_errors() || $this->session->flashdata('result_login')) {
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Warning!</strong>
					<?php echo validation_errors(); ?>
					<?php echo $this->session->flashdata('result_login'); ?>
				</div>    
			<?php } ?>
			<div class="box-body">
				<div class="form-group">
					<label for="namaBarang" class="col-sm-2 control-label">Nama Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaBarang" name="namaBarang" value="<?php echo $nama_barang;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#namaBarang">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="namaToko" class="col-sm-2 control-label">Nama Toko</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaToko" name="namaToko" value="<?php echo $nama_toko;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#namaToko">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="kodeBarang" class="col-sm-2 control-label">Kode Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="kodeBarang" name="kodeBarang" value="<?php echo $id_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="stok" class="col-sm-2 control-label">Stok</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="stok" name="stok" value="<?php echo $stok;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#stok">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="berat" class="col-sm-2 control-label">Berat (gram)</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="berat" name="berat" value="<?php echo $berat;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#berat">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="hargaSatuan" class="col-sm-2 control-label">Harga Satuan</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaSatuan" name="hargaSatuan" value="<?php echo $harga_satuan;?>" readonly>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#hargaSatuan">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="deskripsi" class="col-sm-2 control-label">Deskripsi</label>
					<div class="col-sm-10">
						<textarea class="form-control" id="deskripsi" name="deskripsi" rows="4" readonly><?php echo $deskripsi;?></textarea>
						<button type="button" class="btn" data-clipboard-action="copy" data-clipboard-target="#deskripsi">Copy</button>
					</div>
				</div>
				<div class="form-group">
					<label for="gambar" class="col-sm-2 control-label">Gambar</label>
					<div class="col-sm-10">
						<a href="<?php echo htmlspecialchars("".$r->foto."");?>" download="<?php echo htmlspecialchars("".$r->foto."");?>">
							<img src="<?php echo $r->foto; ?>" class="img-thumbnail" width="100" height="100"/>
						</a>
						<?php
							foreach ($foto_tambahan->result() as $f) { 
							echo "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
									<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
								  </a>";
							}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="ecommerce" class="col-sm-2 control-label">Ecommerce</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="ecommerce" name="ecommerce" value="<?php echo $nama_ecommerce;?>" readonly>
						<input type="hidden" class="form-control" id="idEcommerce" name="idEcommerce" value="<?php echo $id_ecommerce;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="waktuUpload" class="col-sm-2 control-label">Waktu Upload</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="waktuUpload" name="waktuUpload" value="<?php echo $waktu_upload;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="url" class="col-sm-2 control-label">Url</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="url" name="url">
					</div>
				</div>
			<!-- /.box-body -->

				<div class="box-footer">
					<button type="submit" name="submit" class="btn btn-danger">Listing</button>&nbsp &nbsp
					<?php echo anchor('upload_barang','Kembali',array('class'=>'btn btn-primary'))?>
				</div>
			</div>
		</form>
		<!-- /.box -->
		</div>
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
<section class="content">
<!-- Info boxes -->
<div class="row">
	<div class="col-md-9">
		<!-- general form elements -->
		<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Proses Edit Barang</h3>
			<font color="red"><br><b>field yang diedit *</b></font>
		</div>
		<?php
		//parameter 
		$role = $this->session->userdata('role');
		//print_r($record->result());
		foreach ($record->result() as $r) { 
			$nama_barang = $r->nama_barang;
			$nama_toko = $r->nama_toko;
			$id_barang = $r->id_barang;
			$deskripsi = "Penjual : ".$r->nama_toko."\r\n";
			$deskripsi .= "Dikirim dari : ".$r->lokasi."\r\n";
			if($r->merk != "")
				$deskripsi .= "Merk : ".$r->merk."\r\n";
			if($r->bahan != "")
				$deskripsi .= "Bahan : ".$r->bahan."\r\n";
			if($r->volume != "0x0x0")
				$deskripsi .= "Volume : ".$r->volume."\r\n";
			$deskripsi .= $r->deskripsi;
			$foto = $r->foto;
			$waktu_upload = $r->tanggal_upload;
			$stok = $r->stok;
			$harga_satuan = $r->harga_satuan;
			$harga_markup = $r->harga_markup;
			$berat = $r->berat;
			$lokasi = $r->lokasi;		
			$id_status_penyebab_unlisting = $r->id_status_penyebab_unlisting;
		}
		foreach ($field->result() as $f) { 
			$field = substr($f->field, 0, -1);
		}
		//proses nama field
		$field_array = explode(",", $field);
		$field_array=array_map('trim',$field_array);
		//print_r($field_array);
		$nama_field = "Nama Barang";
		$stok_field = "Stok";
		$stok_ecommerce_field = "Stok Ecommerce";
		$harga_field = "Harga";
		$harga_markup_field = "Harga MarkUp";
		$foto_field = "Foto";
		$deskripsi_field = "Deskripsi";
		$berat_field = "Berat";
		$volume_field = "Volume";
		$bahan_field = "Bahan";
		$merk_field = "Merk";
		$gambar_field = "Gambar";
		?>
		<!--nama,stok,stok ecommerce,harga,harga satuan,harga markup,foto,deskripsi(berat,volume,bahan,merk) Gambar-->
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal" action= "<?php echo base_url(); ?>edit_barang/proses_edit" method="POST">
			<?php
			//set peringatan
			if (validation_errors() || $this->session->flashdata('result_login')) {
				?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>Warning!</strong>
					<?php echo validation_errors(); ?>
					<?php echo $this->session->flashdata('result_login'); ?>
				</div>    
			<?php } ?>
			<div class="box-body">
				<div class="form-group">
				<?php 
					if (in_array($nama_field, $field_array)) {
						echo "<label for='namaBarang' class='col-sm-2 control-label'><font color='red'>Nama Barang *</font></label>";
					}
					else {
						echo "<label for='namaBarang' class='col-sm-2 control-label'>Nama Barang</label>";
					}
				?>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaBarang" name="namaBarang" value="<?php echo $nama_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="namaToko" class="col-sm-2 control-label">Nama Toko</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namaToko" name="namaToko" value="<?php echo $nama_toko." (".$lokasi.")";?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="kodeBarang" class="col-sm-2 control-label">Kode Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="kodeBarang" name="kodeBarang" value="<?php echo $id_barang;?>" readonly>
					</div>
				</div>
				<div class="form-group">
				<?php 
					if (in_array($stok_field, $field_array)) {
						echo "<label for='stok' class='col-sm-2 control-label'><font color='red'>Stok *</font></label>";
					}
					else {
						echo "<label for='stok' class='col-sm-2 control-label'>Stok</label>";
					}
				?>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="stok" name="stok" value="<?php echo $stok;?>" readonly>
					</div>
				</div>
				<div class="form-group">
				<?php 
					if (in_array($berat_field, $field_array)) {
						echo "<label for='berat' class='col-sm-2 control-label'><font color='red'>Berat (gram)*</font></label>";
					}
					else {
						echo "<label for='berat' class='col-sm-2 control-label'>Berat (gram)</label>";
					}
				?>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="berat" name="berat" value="<?php echo $berat;?>" readonly>
					</div>
				</div>
				<div class="form-group">
				<?php 
					if (in_array($harga_field, $field_array)) {
						echo "<label for='hargaSatuan' class='col-sm-2 control-label'><font color='red'>Harga Satuan *</font></label>";
					}
					else {
						echo "<label for='hargaSatuan' class='col-sm-2 control-label'>Harga Satuan</label>";
					}
				?>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaSatuan" name="hargaSatuan" value="<?php echo $harga_satuan;?>" readonly>
					</div>
				</div>
				<div class="form-group">
				<?php 
					if (in_array($harga_markup_field, $field_array)) {
						echo "<label for='hargaMarkUp' class='col-sm-2 control-label'><font color='red'>Harga Mark Up *</font></label>";
					}
					else {
						echo "<label for='hargaMarkUp' class='col-sm-2 control-label'>Harga Mark Up</label>";
					}
				?>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hargaMarkUp" name="hargaMarkUp" value="<?php echo $harga_markup;?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="field" class="col-sm-2 control-label">Field yang Diedit</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="field" name="field" value="<?php echo $field;?>" readonly>
					</div>
				</div>
				<div class="form-group">
				<?php 
					if (in_array($deskripsi_field, $field_array) || in_array($bahan_field, $field_array) || in_array($merk_field, $field_array) || in_array($volume_field, $field_array)) {
						echo "<label for='deskripsi' class='col-sm-2 control-label'><font color='red'>Deskripsi *</font></label>";
					}
					else {
						echo "<label for='deskripsi' class='col-sm-2 control-label'>Deskripsi</label>";
					}
				?>
					<div class="col-sm-10">
						<textarea class="form-control" id="deskripsi" name="deskripsi" rows="4" readonly><?php echo $deskripsi;?></textarea>
					</div>
				</div>
				<div class="form-group">
				<?php 
					if (in_array($foto_field, $field_array) || in_array($gambar_field, $field_array)) {
						echo "<label for='gambar' class='col-sm-2 control-label'><font color='red'>Gambar *</font></label>";
					}
					else {
						echo "<label for='gambar' class='col-sm-2 control-label'>Gambar</label>";
					}
				?>
					<div class="col-sm-10">
						<a href="<?php echo htmlspecialchars("".$r->foto."");?>" download="<?php echo htmlspecialchars("".$r->foto."");?>">
							<img src="<?php echo $r->foto; ?>" class="img-thumbnail" width="100" height="100"/>
						</a>
						<?php
							foreach ($foto_tambahan->result() as $f) { 
							echo "<a href=\"".$f->foto."\" download=\"".$f->foto."\">
									<img src=\"".$f->foto."\" class=\"img-thumbnail\" width=\"100\" height=\"100\"/>
								  </a>";
							}
						?>
					</div>
				</div>
				<div class="form-group">
					<label for="waktuUpload" class="col-sm-2 control-label">Waktu Upload</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="waktuUpload" name="waktuUpload" value="<?php echo $waktu_upload;?>" readonly>
					</div>
				</div>
				<?php
				foreach ($status->result() as $s) { 
					$id_status = $s->id_status;
					$status = $s->status;
					$id_ecommerce = $s->id_ecommerce;
					$nama_ecommerce = $s->nama;
					$status_edit_barang = $s->status_edit_barang;
					$id_log_edit_barang = $s->id_log_edit_barang;
					$url = $s->url;
					echo "<input type='hidden' class='form-control' id='idLogEditBarang' name='idLogEditBarang' value='".$id_log_edit_barang."'>";
					echo "<div class='form-group'>";
						echo "<label for='url' class='col-sm-2 control-label'>".$nama_ecommerce."</label>";
						echo "<div class='col-sm-10'>";
							echo "<a href='".$url."' target='_blank' style='color:black;'>".$url."</a>";
						echo "</div>";
					echo "</div>";
					
					echo "<div class='form-group'>";
						echo "<label for='ecommerce' class='col-sm-2 control-label'>Action</label>";
						echo "<div class='col-sm-10'>";
								if(($role=='Administrator' || $role=='Supervisor' || $role=='User') && $status_edit_barang=='1') {
									echo "<div class='label label-danger'>".$status."</div>";
									echo " |  <button type='submit' id='proses_url_".$id_ecommerce."' name='proses_url_".$id_ecommerce."' class='btn btn-danger'>Proses</button>"; 
								}
								elseif(($role=='Administrator' || $role=='Supervisor') && $status_edit_barang=='2') {
									echo "<div class='label label-warning'>".$status."</div>";
									//echo " |  <button type='submit' id='proses_url_".$id_ecommerce."' name='proses_url_".$id_ecommerce."' class='btn btn-danger'>Edit</button>"; 
									echo " <button type='submit' id='checklist_url_".$id_ecommerce."' name='checklist_url_".$id_ecommerce."' class='btn btn-warning'>Checklist</button>"; 
								}
								else {
									echo "<div class='label label-success'>".$status."</div>";
									//if(($role=='Administrator' || $role=='Supervisor')) {
									//	echo " |  <button type='submit' id='proses_url_".$id_ecommerce."' name='proses_url_".$id_ecommerce."' class='btn btn-danger'>Edit</button>"; 
									//}
								}
						echo "</div>";
					echo "</div>";
				}?>
				<div class="box-footer">
					<?php echo anchor('edit_barang','Kembali',array('class'=>'btn btn-primary'))?>
				</div>
			</div>
		</form>
		<!-- /.box -->
		</div>
	</div>
	<!-- /.box -->
</div><!-- /.row -->
</section><!-- /.content -->
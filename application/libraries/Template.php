<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template {
    
                
		var $template_data = array();
		
		function set($name, $value) {
			$this->template_data[$name] = $value;
		}
	
		function load($template = '', $view = '' , $view_data = array(), $return = FALSE) {               
			$this->CI =& get_instance();
			$this->CI->load->model('m_notifikasi');
			$user = $this->CI->session->userdata('u_name');
			$user_grup = $this->CI->session->userdata('role');
			$data['record'] = $this->CI->m_notifikasi->get_notifikasi($user,$user_grup);
			$data['jumlah_notifikasi'] = $this->CI->m_notifikasi->jumlah_notifikasi($user,$user_grup);
			$this->set('contents', $this->CI->load->view($view, $view_data, TRUE));		
			$this->set('notifikasi', $this->CI->load->view('notifikasi',$data, TRUE));
			return $this->CI->load->view($template, $this->template_data, $return);
		}
		
		function get_notifikasi() {
			//$this
		}
}

?>
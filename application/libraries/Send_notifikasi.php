<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send_notifikasi {
		
		function send_notifikasi_grup($id_alert,$id_grup)
		{               
			$this->CI =& get_instance();
			$this->CI->load->model('m_notifikasi'); 
			$record = $this->CI->m_notifikasi->get_list_role($id_grup);
			foreach ($record->result() as $r) { 
				$role = $r->role;
				$record2 = $this->CI->m_notifikasi->get_list_user($role);
				foreach ($record2->result() as $r2) {
					$user = $r2->u_name;
					//send notifikasi
					$this->CI->m_notifikasi->input_notifikasi($id_alert,$user,$role);
				}
			}
		}
}

?>